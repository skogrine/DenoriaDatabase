package fr.denoria.denocore.listeners;

import fr.denoria.denocore.DenoCore;
import fr.denoria.denocore.denoriaplayer.DenoriaPlayer;
import fr.denoria.denocore.support.packets.ChannelHandler;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;

public class DenoriaPlayerLeftListener implements Listener {

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onPlayerQuit(PlayerQuitEvent event) {
        Player player = event.getPlayer();
        DenoCore.iPlayerMap.remove(player.getUniqueId());
        ChannelHandler.removeChannel(player);

        event.setQuitMessage(null);

        DenoriaPlayer dp = new DenoriaPlayer(player);

        if (dp.isNick()) {
            dp.setNick(false);
        }


    }
}
