package fr.denoria.denocore.listeners;

import java.util.*;
import java.util.stream.Collectors;

import fr.denoria.denocore.commands.mods.SlowChatCommand;
import fr.denoria.denocore.commands.nick.NickManager;
import fr.denoria.denocore.denoriaplayer.DenoriaPlayer;
import fr.denoria.denocore.denoriaplayer.currency.Currency;
import fr.denoria.denocore.denoriaplayer.rank.Rank;
import fr.denoria.denocore.support.builder.JsonMessageBuilder;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import net.md_5.bungee.api.ChatColor;

public class DenoriaPlayerChatListener implements Listener {

    private final Map<String, String> spamLast = new HashMap<>();
    private final Map<String, Long> lastSpeak = new HashMap<>();

    @EventHandler(priority = EventPriority.LOWEST)
    public void onPlayerChat(AsyncPlayerChatEvent event) {
        Player player = event.getPlayer();
        DenoriaPlayer denoriaPlayer = DenoriaPlayer.getDenoriaPlayer(player);
        event.setCancelled(true);

        // Mute
        if (denoriaPlayer.isMute()) {
            player.sendMessage("§f[§6Mute§f] §cVous êtes mute de ce serveur, merci de patienter.");
            return;
        }

        // Slow Message
        if (SlowChatCommand.slow > 0) {
            if (denoriaPlayer.getRankPower() < Rank.COEUR.getPower()) {
                Long lastSpeakStamp = lastSpeak.get(player.getName());
                if (lastSpeakStamp == null)
                    lastSpeakStamp = 0L;
                long result = System.currentTimeMillis() - lastSpeakStamp;
                if (result < (SlowChatCommand.slow * 1000L)) {
                    event.setCancelled(true);
                    player.sendMessage(SlowChatCommand.SLOW_TAG + "" + ChatColor.GRAY + " Un message toutes les "
                            + ChatColor.GOLD + SlowChatCommand.slow + " seconde(s)" + ChatColor.GRAY + ".");
                    return;
                }
                lastSpeak.put(player.getName(), System.currentTimeMillis());
            }
        }
        // Flood
        UUID pu = player.getUniqueId();

        if (NickManager.isNick(pu)) {
            sendMessageNicked(player, event);
        } else {
            sendMessageNormal(player, event);
        }
    }


    public void sendMessageNormal(Player player, AsyncPlayerChatEvent event){
        DenoriaPlayer denoriaPlayer = DenoriaPlayer.getDenoriaPlayer(player);


        JsonMessageBuilder infoPlayer = new JsonMessageBuilder();
        infoPlayer.newJComp(
                denoriaPlayer.getRank().getChatColor() + denoriaPlayer.getRank().getPrefix()
                        + " "
                        + denoriaPlayer.getHeadName()
                        + " §8» §f"
                        + event.getMessage())
                .addHoverText("§7Information de "
                                + denoriaPlayer.getHeadName(), "§8❘ §7Grade: "
                                + denoriaPlayer.getRank().getChatColor()
                                + denoriaPlayer.getRank().getPrefix(), "§8❘ §7DenoCoins: "
                                + denoriaPlayer.getCoins(Currency.DENOCOINS).toString(), "§8❘ §7DenoGemmes: "
                                + denoriaPlayer.getCoins(Currency.DENOGEMMES).toString(),
                        "§8❘ §7Guilde: Aucune", "",
                        "§8» §eClique ici pour ouvrir son profil")
                .addCommandExecutor("/stats " + denoriaPlayer.getHeadName()).build(infoPlayer);
        infoPlayer.send();
    }

    public void sendMessageNicked(Player player, AsyncPlayerChatEvent event){
        DenoriaPlayer denoriaPlayer = DenoriaPlayer.getDenoriaPlayer(player);

        Bukkit.broadcastMessage(NickManager.getRankName(denoriaPlayer.getUUID()).getChatColor()
                + NickManager.getRankName(denoriaPlayer.getUUID()).getPrefix() + " "
                + NickManager.getNickName(denoriaPlayer.getUUID())
                + " §8» §f"
                + event.getMessage());
    }
}
