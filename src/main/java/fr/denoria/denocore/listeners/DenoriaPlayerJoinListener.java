package fr.denoria.denocore.listeners;


import com.comphenix.protocol.wrappers.WrappedGameProfile;
import fr.denoria.denocore.DenoCore;
import fr.denoria.denocore.commands.nick.NickManager;
import fr.denoria.denocore.denoriaplayer.DenoriaPlayer;
import fr.denoria.denocore.denoriaplayer.infos.IData;
import fr.denoria.denocore.network.infos.NetworkAPI;
import fr.denoria.denocore.support.MessagesUtils;
import fr.denoria.denocore.support.packets.ChannelHandler;
import fr.denoria.denocore.support.skin.manager.SkinManager;
import fr.denoria.denocore.support.skin.manager.SkinModel;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.json.JSONException;


public class DenoriaPlayerJoinListener implements Listener {



    @EventHandler(priority = EventPriority.LOWEST)
    public void onPlayerJoin(PlayerJoinEvent event) throws JSONException {
        Player player = event.getPlayer();
        event.setJoinMessage(null);

        if (player.getUniqueId().toString().equalsIgnoreCase("e6ecb214-7daa-4d58-b1e2-8d26ba48ad1d")
                || player.getUniqueId().toString().equalsIgnoreCase("8f220840-8bf1-4677-9806-65fcfda8285b")
                || player.getUniqueId().toString().equalsIgnoreCase("ef1a1fed-fdb2-4cc0-85f1-36f4cd6a8455")) {
            player.kickPlayer(ChatColor.RED + "Votre compte a été bloqué de l'infrastructure de Denoria (Allez sur ts.denoria.fr).");
            return;
        }


        SkinManager sm = new SkinManager(DenoCore.getInstance(), player, null);
        if(NickManager.isNick(player.getUniqueId())){
            sm.setProfile(player, NickManager.getNickName(player.getUniqueId()));
            SkinModel sM;
            sM = sm.setSkin(NickManager.getSkinName(player.getUniqueId()));
            sm.applyProperties(WrappedGameProfile.fromPlayer(player), sM);
        } else {
            sm.setProfile(player, player.getName());
            SkinModel sM;
            sM = sm.setSkin(player.getName());
            sm.applyProperties(WrappedGameProfile.fromPlayer(player), sM);
        }


        Bukkit.getScheduler().runTaskAsynchronously(DenoCore.getInstance(), ()-> {
            // Création du iPlayer.
            DenoriaPlayer iPlayer = DenoriaPlayer.getDenoriaPlayer(player);
            IData.setLastLogin(iPlayer.getUUID());
            ChannelHandler.addChannel(player);

        });
        if (NetworkAPI.getBoosterNetwork() > 0) {
            player.sendMessage(MessagesUtils.getDenoriaPrefix() + "§7Le serveur a actuellement un booster de " + NetworkAPI.getBoosterNetwork() + "%");
        }
    }
}
