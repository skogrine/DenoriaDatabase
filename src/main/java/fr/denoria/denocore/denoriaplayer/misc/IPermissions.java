package fr.denoria.denocore.denoriaplayer.misc;

import fr.denoria.denocore.mysql.SQLManager;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class IPermissions {

    protected static String table = "players_permissions";

    /**
     * Ajouter une permission à un Joueur.
     *
     * @param uuid
     * @param permission
     */
    public static void addPermission(UUID uuid, String permission) {
        try {
            final Connection connection = SQLManager.DENORIA_SERVER.getDatabaseAccess().getRessource();
            PreparedStatement preparedStatement = connection
                    .prepareStatement("INSERT INTO " + table + " (player_uuid, permission) VALUES (?, ?)");
            preparedStatement.setString(1, uuid.toString());
            preparedStatement.setString(2, permission);
            preparedStatement.executeUpdate();
            preparedStatement.close();
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Ajouter une permission avec durée à un Joueur.
     *
     * @param uuid
     * @param permission
     * @param time_in_minute
     */
    public static void addPermissionWithTimer(UUID uuid, String permission, Integer time_in_minute) {
        try {
            final Connection connection = SQLManager.DENORIA_SERVER.getDatabaseAccess().getRessource();
            PreparedStatement preparedStatement = connection.prepareStatement("INSERT INTO " + table
                    + " (player_uuid, permission, permission_duration) VALUES (?, ?, DATE_ADD(NOW(), INTERVAL "
                    + time_in_minute + " MINUTE))");
            preparedStatement.setString(1, uuid.toString());
            preparedStatement.setString(2, permission);
            preparedStatement.executeUpdate();
            preparedStatement.close();
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Vérifier si un Joueur possède une permission.
     *
     * @param uuid
     * @param permission
     * @return
     */
    public static boolean hasPermission(UUID uuid, String permission) {
        suppPermissionsTime();
        try {
            final Connection connection = SQLManager.DENORIA_SERVER.getDatabaseAccess().getRessource();
            PreparedStatement preparedStatement = connection
                    .prepareStatement("SELECT permission FROM " + table + " WHERE player_uuid = ?");
            preparedStatement.setString(1, uuid.toString());
            ResultSet rs = preparedStatement.executeQuery();
            while (rs.next()) {
                if (permission.equalsIgnoreCase(rs.getString("permission"))) {
                    connection.close();
                    return true;
                }
            }
            preparedStatement.close();
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    /**
     * Récupérer toutes les permissions du Joueur.
     *
     * @param uuid
     * @return
     */
    public static List<String> getPermissions(UUID uuid) {
        suppPermissionsTime();
        List<String> permissionList = new ArrayList<String>();
        try {
            final Connection connection = SQLManager.DENORIA_SERVER.getDatabaseAccess().getRessource();
            PreparedStatement preparedStatement = connection
                    .prepareStatement("SELECT permission FROM " + table + " WHERE player_uuid = ?");
            preparedStatement.setString(1, uuid.toString());
            ResultSet rs = preparedStatement.executeQuery();
            while (rs.next()) {
                permissionList.add((rs.getString("permission")));
            }
            preparedStatement.close();
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return permissionList;
    }

    /**
     * Supprimer une permission à un Joueur.
     *
     * @param uuid
     * @param permission
     */
    public static void suppPermission(UUID uuid, String permission) {
        try {
            final Connection connection = SQLManager.DENORIA_SERVER.getDatabaseAccess().getRessource();
            PreparedStatement preparedStatement = connection
                    .prepareStatement("DELETE FROM " + table + " WHERE player_uuid = ? and permission = ?");
            preparedStatement.setString(1, uuid.toString());
            preparedStatement.setString(2, permission);
            preparedStatement.executeUpdate();
            preparedStatement.close();
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Suppression des permissions qui expires.
     */
    private static void suppPermissionsTime() {
        try {
            final Connection connection = SQLManager.DENORIA_SERVER.getDatabaseAccess().getRessource();
            PreparedStatement preparedStatement = connection
                    .prepareStatement("DELETE FROM " + table + " WHERE permission_duration <= NOW()");
            preparedStatement.executeUpdate();
            preparedStatement.close();
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
