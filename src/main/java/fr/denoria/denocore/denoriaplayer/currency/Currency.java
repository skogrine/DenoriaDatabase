package fr.denoria.denocore.denoriaplayer.currency;

public enum Currency {

    DENOGEMMES("DenoGemmes", "money_maincredits", 0),
    DENOCOINS("DenoCoins", "money_coins", 1500);

    private String name;
    private String columName;
    private Integer defaultMoney;

    Currency(String name, String columName, Integer defaultMoney) {
        this.name = name;
        this.columName = columName;
        this.defaultMoney = defaultMoney;
    }

    public String getName() {
        return this.name;
    }

    public String getColumName() {
        return columName;
    }

    public Integer getDefaultMoney() {
        return this.defaultMoney;
    }

    /**
     * Récupérer la money par son nom.
     *
     * @param money
     * @return
     */
    public static Currency getMoneyByName(String money) {
        for (Currency currency : values()) {
            if (currency.name.contains(money)) {
                return currency;
            }
        }
        return null;
    }
}
