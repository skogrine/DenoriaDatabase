package fr.denoria.denocore.denoriaplayer.currency;

import fr.denoria.denocore.mysql.SQLManager;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.UUID;


public class ICurrency {

    protected static String table = "players_currency";

    /**
     * Créer un compte dans la base de données au Joueur.
     *
     * @param uuid
     */
    public static void createAccount(UUID uuid) {
        try {
            final Connection connection = SQLManager.DENORIA_SERVER.getDatabaseAccess().getRessource();
            PreparedStatement preparedStatement = connection
                    .prepareStatement("SELECT money_maincredits FROM " + table + " WHERE player_uuid = ?");
            preparedStatement.setString(1, uuid.toString());
            ResultSet rs = preparedStatement.executeQuery();
            if (!rs.next()) {
                preparedStatement.close();
                preparedStatement = connection.prepareStatement("INSERT INTO " + table
                        + " (player_uuid, money_maincredits, money_coins) VALUES (?, ?, ?)");
                preparedStatement.setString(1, uuid.toString());
                preparedStatement.setInt(2, 0);
                preparedStatement.setInt(3, 0);
                preparedStatement.executeUpdate();
                preparedStatement.close();
            }
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Ajouter des Coins dans la base de données.
     *
     * @param uuid
     * @param currency
     * @param money
     */
    public static void addCoins(UUID uuid, Currency currency, Integer money) {
        try {
            final Connection connection = SQLManager.DENORIA_SERVER.getDatabaseAccess().getRessource();
            PreparedStatement preparedStatement = connection.prepareStatement("UPDATE " + table + " SET "
                    + currency.getColumName() + " = " + currency.getColumName() + " + ? WHERE player_uuid = ?");
            preparedStatement.setInt(1, money);
            preparedStatement.setString(2, uuid.toString());
            preparedStatement.executeUpdate();
            preparedStatement.close();
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Définir des Coins dans la base de données.
     *
     * @param uuid
     * @param currency
     * @param money
     */
    public static void setCoins(UUID uuid, Currency currency, Integer money) {
        try {
            final Connection connection = SQLManager.DENORIA_SERVER.getDatabaseAccess().getRessource();
            PreparedStatement preparedStatement = connection.prepareStatement(
                    "UPDATE " + table + " SET " + currency.getColumName() + " = ? WHERE player_uuid = ?");
            preparedStatement.setInt(1, money);
            preparedStatement.setString(2, uuid.toString());
            preparedStatement.executeUpdate();
            preparedStatement.close();
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Supprimer des Coins dans la base de données.
     *
     * @param uuid
     * @param currency
     * @param money
     */
    public static void removeCoins(UUID uuid, Currency currency, Integer money) {
        try {
            final Connection connection = SQLManager.DENORIA_SERVER.getDatabaseAccess().getRessource();
            PreparedStatement preparedStatement = connection.prepareStatement("UPDATE " + table + " SET "
                    + currency.getColumName() + " = " + currency.getColumName() + " - ? WHERE player_uuid = ?");
            preparedStatement.setInt(1, money);
            preparedStatement.setString(2, uuid.toString());
            preparedStatement.executeUpdate();
            preparedStatement.close();
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Récupérer des Coins dans la base de données.
     *
     * @param uuid
     * @param currency
     * @return
     */
    public static Integer getCoins(UUID uuid, Currency currency) {
        Integer coins = 0;
        try {
            final Connection connection = SQLManager.DENORIA_SERVER.getDatabaseAccess().getRessource();
            PreparedStatement preparedStatement = connection.prepareStatement(
                    "SELECT " + currency.getColumName() + " FROM " + table + " WHERE player_uuid = ?");
            preparedStatement.setString(1, uuid.toString());
            ResultSet rs = preparedStatement.executeQuery();
            if (!rs.next()) {
                connection.close();
                return Integer.valueOf(coins);
            }
            coins = rs.getInt(currency.getColumName());
            preparedStatement.close();
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return Integer.valueOf(coins);
    }
}
