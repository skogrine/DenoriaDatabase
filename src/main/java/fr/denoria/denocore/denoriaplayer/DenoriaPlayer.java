package fr.denoria.denocore.denoriaplayer;

import fr.denoria.denocore.DenoCore;
import fr.denoria.denocore.commands.nick.NickManager;
import fr.denoria.denocore.denoriaplayer.currency.Currency;
import fr.denoria.denocore.denoriaplayer.currency.ICurrency;
import fr.denoria.denocore.denoriaplayer.discord.ILink;
import fr.denoria.denocore.denoriaplayer.game.bedwars.BedWarsPlayer;
import fr.denoria.denocore.denoriaplayer.game.skywars.SkyWarsPlayer;
import fr.denoria.denocore.denoriaplayer.infos.DataType;
import fr.denoria.denocore.denoriaplayer.infos.IData;
import fr.denoria.denocore.denoriaplayer.level.ILevelExperience;
import fr.denoria.denocore.denoriaplayer.level.LevelType;
import fr.denoria.denocore.denoriaplayer.misc.IPermissions;
import fr.denoria.denocore.denoriaplayer.rank.IRank;
import fr.denoria.denocore.denoriaplayer.rank.RANKSPECIAL;
import fr.denoria.denocore.denoriaplayer.rank.Rank;
import fr.denoria.denocore.network.IMute;
import fr.denoria.denocore.redis.RedisManager;
import fr.denoria.denocore.support.builder.JsonMessageBuilder;
import fr.denoria.denocore.support.utils.DefaultFontInfo;
import net.md_5.bungee.api.ChatColor;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import redis.clients.jedis.Jedis;
import us.myles.ViaVersion.api.Via;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.util.*;

public class DenoriaPlayer {

    private UUID uuid;
    private String playerRealName, headName;
    private Player player;
    private final int CENTER_PX = 154;

    private Integer rankPower;
    private Rank rank;
    private Rank subRank;
    private RANKSPECIAL rankspecial;
    private boolean nick, mute;
    private boolean fly;

    private String lastConnexion = null;
    private String firstConnexion = null;

    private Map<Currency, Integer> mapCurrency = new HashMap<>();
    private Map<LevelType, Integer> mapLevel = new HashMap<>();
    private Map<DataType, Object> mapData = new HashMap<>();

    private List<String> permissionsList;
    //private List<LuckBox> luckBoxList = new ArrayList<LuckBox>();

    private SkyWarsPlayer skyWarsPlayer;
    private BedWarsPlayer bedWarsPlayer;

    public Map<DataType, Object> getMapData() {
        return mapData;
    }

    /**
     * Constructeur du IPlayer.
     *
     * @param player
     */
    public DenoriaPlayer(Player player) {

        // Données de profile du Joueur.
        this.uuid = player.getUniqueId();
        this.playerRealName = player.getName();
        this.headName = player.getName();
        this.player = player;

        byte[] array = new byte[10]; // length is bounded by 7
        new Random().nextBytes(array);
        String generatedString = new String(array, StandardCharsets.UTF_8);


        // Données du compte du Joueur.
        IRank.createAccount(uuid);
        this.rankPower = Objects.requireNonNull(IRank.getPlayerRank(uuid)).getPower();
        this.subRank = IRank.getPlayerSubRank(uuid);
        this.rank = IRank.getPlayerRank(uuid);
        this.rankspecial = IRank.getPlayerSpecialRank(uuid);

        // Utilitaire du compte du Joueur.
        this.loadAccountData();
        this.loadCoins();
        this.loadExperienceLevel();
        this.loadPlayerData();
        this.permissionsList = IPermissions.getPermissions(uuid);
        this.nick = NickManager.isNick(uuid);
        this.fly = false;
        if (nick) this.rank = NickManager.getRankName(uuid);
        if (nick) this.headName = NickManager.getNickName(uuid);
        this.mute = IMute.isMute(playerRealName);

        //this.loadNickPlayer();
        //this.denoBoxList = IDenoBox.getPlayerDenoBoxList(uuid);

        // Chargement des données liés aux jeux.
        this.skyWarsPlayer = new SkyWarsPlayer(this);
        this.bedWarsPlayer = new BedWarsPlayer(this);
    }


    /**
     * Charger les données du Joueur.
     */
    public void loadAccountData() {
        ICurrency.createAccount(uuid);
        ILevelExperience.createAccount(uuid);
        IData.createAccount(headName, uuid);
    }

    /**
     * Envoyer le message du Joueur qui n'a pas la permission.
     */
    public void sendMessageNoPermission(String perm) {
        player.sendMessage("§dDenoria §8❘ §cErreur: Vous n'avez pas la permission ("+ perm +" < CMD)");
    }



    /**
     * Vous devez utiliser la version 1.9 message.
     */
    public void sendMessageNeedV1_9() {
        player.sendMessage("");
        sendCenteredMessage("§4■ §cProblème de connexion §4■");
        player.sendMessage("");
        sendCenteredMessage("§7➠ §fCe jeu n'est disponible qu'en version §b1.9+§f.");
        sendCenteredMessage("§f§l[§2§l?§f§l] §aChangez de version pour pouvoir y jouer.");
        player.sendMessage("");
    }

    public UUID getUUID() {
        return uuid;
    }

    public Boolean getFly() {
        return this.fly;
    }

    public String getPlayerRealName() {
        return playerRealName;
    }

    public String getHeadName() {
        return headName;
    }

    public Player getPlayer() {
        return player;
    }

    public Integer getRankPower() {
        return rankPower;
    }

    public Rank getRank() {
        return rank;
    }

    public void setRank(Rank rank) {
        this.rank = rank;
    }

    public boolean isNick() {
        return nick;
    }

    public void setNick(boolean nick) {
        this.nick = nick;
    }

    public boolean isMute() {
        return mute;
    }

    public void setMute(boolean mute) {
        this.mute = mute;
    }

    public RANKSPECIAL getRankspecial() {
        return rankspecial;
    }

    /**
     * Commande d'aide.
     */
    public void sendPlayerHelp() {
        player.sendMessage("");
        player.sendMessage(" §c§lDenoria §7(Aide)");
        player.sendMessage("");
        player.sendMessage(" §8() §c/lobby §8» §7Retourner au lobby.");
        player.sendMessage(" §8() §c/report (pseudo) §8» §7Signaler un joueur.");
        player.sendMessage(" §8() §c/friend §8» §7Gérer ses amis.");
        player.sendMessage(" §8() §c/groupe §8» §7Gestion de groupes.");
        player.sendMessage(" §8() §c/msg (pseudo) (message) §8» §7Envoyer un message privé.");

        if(NickManager.isNick(player.getUniqueId())) {
            JsonMessageBuilder rCommand = new JsonMessageBuilder();
            rCommand.newJComp(" §8() §c/r (message) §8» §7Répondre à un message privé.").addCommandSuggestion("/r (message)").addHoverText("§7Cliquez pour executer la command /r").build(rCommand);
            rCommand.send(Bukkit.getPlayer(NickManager.getNickName(player.getUniqueId())));
            JsonMessageBuilder lagCommand = new JsonMessageBuilder();
            lagCommand.newJComp(" §8() §c/lag §8» §7Afficher les informations du serveur.").addCommandExecutor("/lag").addHoverText("§7Cliquez pour executer la command /lag").build(lagCommand);
            lagCommand.send(Bukkit.getPlayer(NickManager.getNickName(player.getUniqueId())));


            Bukkit.getPlayer(NickManager.getNickName(player.getUniqueId())).sendMessage("");
            JsonMessageBuilder jmShop = new JsonMessageBuilder();
            jmShop.newJComp(" §8■ §7Boutique : §estore.denoria.fr")
                    .addHoverText("§7Visitez notre boutique").addURL("https://store.denoria.fr").build(jmShop);
            jmShop.send(Bukkit.getPlayer(NickManager.getNickName(player.getUniqueId())));
            JsonMessageBuilder jmTwitter = new JsonMessageBuilder();
            jmTwitter.newJComp(" §8■ §7Twitter : §b@NetworkDenoria")
                    .addHoverText("§7Suivez-nous sur Twitter.").addURL("https://www.twitter.com/networkDenoria").build(jmTwitter);
            jmTwitter.send(Bukkit.getPlayer(NickManager.getNickName(player.getUniqueId())));
            JsonMessageBuilder jmBug = new JsonMessageBuilder();
            jmBug.newJComp(" §8■ §7Discord : §ddiscord.denoria.fr")
                    .addHoverText("§7Rejoingnez notre discord.").addURL("https://discord.gg/hXfQ8hpjvG").build(jmBug);
            jmBug.send(Bukkit.getPlayer(NickManager.getNickName(player.getUniqueId())));
            Bukkit.getPlayer(NickManager.getNickName(player.getUniqueId())).sendMessage("");
        } else {
            JsonMessageBuilder rCommand = new JsonMessageBuilder();
            rCommand.newJComp(" §8() §c/r (message) §8» §7Répondre à un message privé.").addCommandSuggestion("/r (message)").addHoverText("§7Cliquez pour executer la command /r").build(rCommand);
            rCommand.send(player);
            JsonMessageBuilder lagCommand = new JsonMessageBuilder();
            lagCommand.newJComp(" §8() §c/lag §8» §7Afficher les informations du serveur.").addCommandExecutor("/lag").addHoverText("§7Cliquez pour executer la command /lag").build(lagCommand);
            lagCommand.send(player);


            player.sendMessage("");
            JsonMessageBuilder jmShop = new JsonMessageBuilder();
            jmShop.newJComp(" §8■ §7Boutique : §estore.denoria.fr")
                    .addHoverText("§7Visitez notre boutique").addURL("https://store.denoria.fr").build(jmShop);
            jmShop.send(player);
            JsonMessageBuilder jmTwitter = new JsonMessageBuilder();
            jmTwitter.newJComp(" §8■ §7Twitter : §b@NetworkDenoria")
                    .addHoverText("§7Suivez-nous sur Twitter.").addURL("https://www.twitter.com/networkDenoria").build(jmTwitter);
            jmTwitter.send(player);
            JsonMessageBuilder jmBug = new JsonMessageBuilder();
            jmBug.newJComp(" §8■ §7Discord : §ddiscord.denoria.fr")
                    .addHoverText("§7Rejoingnez notre discord.").addURL("https://discord.gg/hXfQ8hpjvG").build(jmBug);
            jmBug.send(player);
            player.sendMessage("");
        }
    }



    @SuppressWarnings("unchecked")
    /**
     * Récupérer la version du Joueur.
     *
     * @return playerVersion
     */
    public int getMinecraftVersion() {
        return Via.getAPI().getPlayerVersion(player);
    }

    /**
     * Ajouter une permission au Joueur.
     *
     * @param permission
     */
    public void addPermission(String permission) {
        IPermissions.addPermission(uuid, permission);
        permissionsList.add(permission);
    }

    /**
     * Ajouter une permission avec un temps à un Joueur.
     *
     * @param permission
     * @param delayMinutes
     */
    public void addPermissionTime(String permission, int delayMinutes) {
        IPermissions.addPermissionWithTimer(uuid, permission, delayMinutes);
        permissionsList.add(permission);
    }

    /**
     * Supprimer une permission à un Joueur.
     *
     * @param permission
     */
    public void removePermission(String permission) {
        IPermissions.suppPermission(uuid, permission);
        permissionsList.remove(permission);
    }

    /**
     * Vérifier la permission d'un Joueur.
     *
     * @param permission
     * @return
     */
    public boolean hasPermission(String permission) {
        return permissionsList.contains(permission);
    }

    /**
     * Définir une Information d'un Joueur.
     *
     * @param dataType
     * @param values
     */
    public void setPlayerData(DataType dataType, boolean values) {
        try (Jedis jedis = RedisManager.getJedisPool().getResource()) {
            jedis.set("dataplayerinfos:" + uuid.toString() + "_" + dataType.getName(), values + "");
            this.mapData.put(dataType, values);
        } catch (Exception e) {
            System.out.println("[IPlayer] Impossible de changer les datas au Joueur.");
        }
    }

    /**
     * Récupérer une Information d'un Joueur.
     *
     * @param dataType
     * @return
     */
    public boolean getPlayerData(DataType dataType) {
        return (boolean) this.mapData.get(dataType);
    }

    /**
     * Générer le cache des informations du Joueur.
     */
    public void loadPlayerData() {
        for (DataType dataType : DataType.values()) {
            Object object = "";
            try (Jedis jedis = RedisManager.getJedisPool().getResource()) {
                if (!(jedis.exists("dataplayerinfos:" + uuid.toString() + "_" + dataType.getName()))) {
                    jedis.set("dataplayerinfos:" + uuid.toString() + "_" + dataType.getName(),
                            IData.getPlayerData(uuid, dataType) + "");
                }
                object = Boolean
                        .parseBoolean(jedis.get("dataplayerinfos:" + uuid.toString() + "_" + dataType.getName()));
            } catch (Exception e) {
                System.out.println("[IPlayer] Impossible de récupérer les informations du Joueur.");
            }
            this.mapData.put(dataType, object);
        }
    }

    /**
     * Ajouter des Coins à un Joueur.
     *
     * @param currency
     * @param coins
     */
    public void addCoins(Currency currency, Integer coins) {
        try (Jedis jedis = RedisManager.getJedisPool().getResource()) {
            jedis.set("currency:" + uuid.toString() + "_" + currency.getName(), (getCoins(currency) + coins) + "");
            this.mapCurrency.put(currency, getCoins(currency) + coins);
        } catch (Exception e) {
            System.out.println("[IPlayer] Impossible d'ajouter des coins au Joueur.");
        }
    }

    /**
     * Ajouter des coins à un Joueur dans une partie.
     *
     * @param currency
     * @param coinsReward
     * @param reason
     */
    public void addCoinsGame(Currency currency, int coinsReward, String reason) {
        player.sendMessage("§f[§c§l!§f] §fGain de DenoCoins: §e+" + coinsReward + " ⛃ §6(" + reason + ")");
        addCoins(currency, coinsReward);
    }

    /**
     * Ajouter des crédits à un Joueur.
     *
     * @param coinsReward
     * @param reason
     */
    public void addCreditsGame(int coinsReward, String reason) {
        player.sendMessage("§f[§c§l!§f] Gain de DenoGemmes: §b+" + coinsReward + " ⛂ §6(" + reason + ")");
        addCoins(Currency.DENOGEMMES, coinsReward);
    }

    /**
     * Ajouter de l'expérience à un Joueur.
     *
     * @param levelType
     * @param luckBoxType
     * @param experienceReward
     */
    /*public void addExperience(LevelType levelType, LuckBoxType luckBoxType, int experienceReward) {
        player.sendMessage("§f[§c§l!§f] Gain d'expérience: §d+" + experienceReward + " EXP §6(Victoire)");
        int levelLast = LevelMechanic.getPlayerLevel(getExperienceLevel(levelType));
        addExperienceLevel(levelType, experienceReward);
        int levelNow = LevelMechanic.getPlayerLevel(getExperienceLevel(levelType));
        if(levelLast < levelNow) {
            player.sendMessage("§b§lExpérience§f│ §fVous avez gagné un niveau !");
            player.sendMessage(" §f■ §7Vous avez maintenant §e" + levelNow + "✯ §7.");
            ILuckBox.addPlayerLuckBox(uuid, luckBoxType);
        }
    }*/

    /**
     * Supprimer des coins à un Joueur.
     *
     * @param currency
     * @param coins
     */
    public void removeCoins(Currency currency, Integer coins) {
        try (Jedis jedis = RedisManager.getJedisPool().getResource()) {
            jedis.set("currency:" + uuid.toString() + "_" + currency.getName(), (getCoins(currency) - coins) + "");
            this.mapCurrency.put(currency, getCoins(currency) - coins);
        } catch (Exception e) {
            System.out.println("[IPlayer] Impossible de supprimer des coins au Joueur.");
        }
    }

    /**
     * Récupérer les Coins d'un Joueur.
     *
     * @param currency
     * @return
     */
    public Integer getCoins(Currency currency) {
        return this.mapCurrency.get(currency);
    }

    /**
     * Générer le cache interne pour les Coins.
     */
    public void loadCoins() {
        for (Currency currency : Currency.values()) {
            int iCoins = 0;
            try (Jedis jedis = RedisManager.getJedisPool().getResource()) {
                if (!(jedis.exists("currency:" + uuid.toString() + "_" + currency.getName()))) {
                    jedis.set("currency:" + uuid.toString() + "_" + currency.getName(),
                            ICurrency.getCoins(uuid, currency) + "");
                }
                iCoins = Integer.parseInt(jedis.get("currency:" + uuid.toString() + "_" + currency.getName()));
            } catch (Exception e) {
                System.out.println("[IPlayer] Impossible de récupérer les Coins du Joueur.");
            }
            this.mapCurrency.put(currency, iCoins);
        }
    }

    /**
     * Ajouter de l'experience à un Joueur.
     *
     * @param levelType
     * @param experience
     */
    public void addExperienceLevel(LevelType levelType, Integer experience) {
        try (Jedis jedis = RedisManager.getJedisPool().getResource()) {
            jedis.set("levelexperience:" + uuid.toString() + "_" + levelType.getName(),
                    (getExperienceLevel(levelType) + experience) + "");
            this.mapLevel.put(levelType, getExperienceLevel(levelType) + experience);
        } catch (Exception e) {
            System.out.println("[IPlayer] Impossible d'ajouter de l'experience au Joueur.");
        }
    }

    /**
     * Récupérer l'experience d'un Joueur.
     *
     * @param levelType
     * @return
     */
    public Integer getExperienceLevel(LevelType levelType) {
        return this.mapLevel.get(levelType);
    }

    /**
     * Générer le cache interne pour le Système de Level.
     */
    public void loadExperienceLevel() {
        for (LevelType levelType : LevelType.values()) {
            int iLevel = 0;
            try (Jedis jedis = RedisManager.getJedisPool().getResource()) {
                if (!(jedis.exists("levelexperience:" + uuid.toString() + "_" + levelType.getName()))) {
                    jedis.set("levelexperience:" + uuid.toString() + "_" + levelType.getName(),
                            ILevelExperience.getExperienceLevel(uuid, levelType) + "");
                }
                iLevel = Integer.parseInt(jedis.get("levelexperience:" + uuid.toString() + "_" + levelType.getName()));
            } catch (Exception e) {
                System.out.println("[IPlayer] Impossible de récupérer les Level Experience du Joueur.");
            }
            this.mapLevel.put(levelType, iLevel);
        }
    }

    /**
     * Récupérer les informations du Joueur en mode SkyWars.
     *
     * @return SkyWarsPlayer
     */
    public SkyWarsPlayer getSkyWarsPlayer() {
        return skyWarsPlayer;
    }


    /**
     * Récupérer les informations du Joueurs en mode BedWars.
     *
     * @return
     */
    public BedWarsPlayer getBedWarsPlayer() {
        return bedWarsPlayer;
    }


    /**
     * Récupérer la dernière connexion du Joueur.
     *
     * @return
     */
    public String getLastConnexion() {
        if (lastConnexion == null)
            lastConnexion = (String) IData.getPlayerDataWithString(uuid, "last_login");
        return lastConnexion;
    }

    /**
     * Récupérer la première connexion du Joueur.
     *
     * @return
     */
    public String getFirstConnexion() {
        if (firstConnexion == null)
            firstConnexion = (String) IData.getPlayerDataWithString(uuid, "first_login");
        return firstConnexion;
    }

    public Rank getSubRank() {
        return subRank;
    }

    /**
     * Envoyer un message centré à un Joueur.
     *
     * @param message
     */
    public void sendCenteredMessage(String message) {
        if (message == null || message.equals(""))
            player.sendMessage("");
        assert message != null;
        message = ChatColor.translateAlternateColorCodes('&', message);

        int messagePxSize = 0;
        boolean previousCode = false;
        boolean isBold = false;

        for (char c : message.toCharArray()) {
            if (c == '§') {
                previousCode = true;
            } else if (previousCode) {
                previousCode = false;
                isBold = c == 'l' || c == 'L';
            } else {
                DefaultFontInfo dFI = DefaultFontInfo.getDefaultFontInfo(c);
                messagePxSize += isBold ? dFI.getBoldLength() : dFI.getLength();
                messagePxSize++;
            }
        }

        int halvedMessageSize = messagePxSize / 2;
        int toCompensate = CENTER_PX - halvedMessageSize;
        int spaceLength = DefaultFontInfo.SPACE.getLength() + 1;
        int compensated = 0;
        StringBuilder sb = new StringBuilder();
        while (compensated < toCompensate) {
            sb.append(" ");
            compensated += spaceLength;
        }
        player.sendMessage(sb.toString() + message);
    }

    /**
     * Récupérer un IPlayer.
     *
     * @param player
     * @return
     */
    public static DenoriaPlayer getDenoriaPlayer(Player player) {
        if (DenoCore.iPlayerMap.get(player.getUniqueId()) == null) {
            DenoCore.iPlayerMap.put(player.getUniqueId(), new DenoriaPlayer(player));
        }
        return DenoCore.iPlayerMap.get(player.getUniqueId());
    }
}
