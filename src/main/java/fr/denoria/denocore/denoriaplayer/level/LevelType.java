package fr.denoria.denocore.denoriaplayer.level;

public enum LevelType {

    DENORIA_LEVEL("DenoriaLevel", "stars_denorianetwork"),
    ARCADE_LEVEL("ArcadeLevel", "stars_arcadepoints"),
    SKYWARS_LEVEL("SkyWarsLevel", "stars_skywarspoints"),
    SKYRUSH_LEVEL("SkyRushLevel", "stars_skyrushpoints");

    public String name;
    public String folderName;

    /**
     * Constructeur du LevelType.
     *
     * @param name
     * @param folderName
     */
    LevelType(String name, String folderName) {
        this.name = name;
        this.folderName = folderName;
    }

    public String getName() {
        return name;
    }

    public String getFolderName() {
        return folderName;
    }
}
