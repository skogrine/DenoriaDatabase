package fr.denoria.denocore.denoriaplayer.level;

import fr.denoria.denocore.mysql.SQLManager;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.UUID;

public class ILevelExperience {

    protected static String table = "players_points_stars";

    /**
     * Créer un compte dans la base de données au Joueur.
     *
     * @param uuid
     */
    public static void createAccount(UUID uuid) {
        try {
            final Connection connection = SQLManager.DENORIA_SERVER.getDatabaseAccess().getRessource();
            PreparedStatement preparedStatement = connection
                    .prepareStatement("SELECT stars_denorianetwork " + " FROM " + table + " WHERE player_uuid = ?");
            preparedStatement.setString(1, uuid.toString());
            ResultSet rs = preparedStatement.executeQuery();
            if (!rs.next()) {
                preparedStatement.close();
                preparedStatement = connection.prepareStatement(
                        "INSERT INTO " + table + " (player_uuid, stars_denorianetwork, stars_arcadepoints, stars_skywarspoints, stars_skyrushpoints) VALUES(?, ?, ?, ?, ?)");
                preparedStatement.setString(1, uuid.toString());
                preparedStatement.setInt(2, 0);
                preparedStatement.setInt(3, 0);
                preparedStatement.setInt(4, 0);
                preparedStatement.setInt(5, 0);
                preparedStatement.executeUpdate();
                preparedStatement.close();
            }
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Définir une nouvelle experience à un Joueur.
     *
     * @param uuid
     * @param levelType
     * @param values
     */
    public static void setExperienceLevel(UUID uuid, LevelType levelType, Integer values) {
        try {
            final Connection connection = SQLManager.DENORIA_SERVER.getDatabaseAccess().getRessource();
            PreparedStatement preparedStatement = connection.prepareStatement(
                    "UPDATE " + table + " SET " + levelType.getFolderName() + " = ? WHERE player_uuid = ?");
            preparedStatement.setInt(1, values);
            preparedStatement.setString(2, uuid.toString());
            preparedStatement.executeUpdate();
            preparedStatement.close();
            connection.close();
        } catch (SQLException e) {
            return;
        }
    }

    /**
     * Récupérer l'experience d'un Joueur.
     *
     * @param uuid uuid du joueur
     * @param levelType Type du level souhaité
     * @return
     */
    public static Integer getExperienceLevel(UUID uuid, LevelType levelType) {
        int levelExperience = 0;
        try {
            final Connection connection = SQLManager.DENORIA_SERVER.getDatabaseAccess().getRessource();
            PreparedStatement preparedStatement = connection.prepareStatement(
                    "SELECT " + levelType.getFolderName() + " FROM " + table + " WHERE player_uuid = ?");
            preparedStatement.setString(1, uuid.toString());
            ResultSet rs = preparedStatement.executeQuery();
            if (!rs.next()) {
                connection.close();
                return levelExperience;
            }
            levelExperience = rs.getInt(levelType.getFolderName());
            preparedStatement.close();
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return levelExperience;
    }
}
