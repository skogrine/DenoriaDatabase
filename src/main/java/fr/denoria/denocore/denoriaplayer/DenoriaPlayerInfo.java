package fr.denoria.denocore.denoriaplayer;

import fr.denoria.denocore.denoriaplayer.infos.DataType;
import fr.denoria.denocore.redis.RedisManager;
import redis.clients.jedis.Jedis;

import java.util.UUID;

public class DenoriaPlayerInfo {

    /**
     * Récupérer le nom du Serveur.
     *
     * @param uuid uuid du joueur
     * @return serverName
     */
    public static String getServerName(UUID uuid) {
        String serverName = "DenoServer";
        try (Jedis jedis = RedisManager.getRedisDatabase().getJedisPool().getResource()) {
            serverName = jedis.get("denoriaplayerinfos:" + uuid.toString() + "_" + "serverName");
        } catch (Exception e) {
            System.out.println("[DenoriaPlayerInfo] Impossible de récupérer le nom du serveur.");
        }
        return serverName;
    }

    public static void setServerName(UUID uuid, String serverName) {
        try (Jedis jedis = RedisManager.getRedisDatabase().getJedisPool().getResource()) {
            jedis.set("denoriaplayerinfos:" + uuid.toString() + "_" + "serverName", serverName);
        } catch (Exception e) {
            System.out.println("[DenoriaPlayerInfo] Impossible de définir le nom du serveur.");
        }
    }

    public static void setPlayerLoginSecurity(UUID uuid) {
        try (Jedis jedis = RedisManager.getRedisDatabase().getJedisPool().getResource()) {
            jedis.set("securityBungeeLogin:" + uuid.toString() + "_" + "useBungee", "true");
        } catch (Exception e) {
            System.out.println("[DenoriaPlayerInfo] Impossible de définir le nom du serveur.");
        }
    }

    public static String getPlayerLoginSecurity(UUID uuid) {
        String booleanName = "false";
        try (Jedis jedis = RedisManager.getRedisDatabase().getJedisPool().getResource()) {
            booleanName = jedis.get("securityBungeeLogin:" + uuid.toString() + "_" + "useBungee");
        } catch (Exception e) {
            System.out.println("[DenoriaPlayerInfo] Impossible de récupérer le nom du serveur.");
        }
        return booleanName;
    }

    public static boolean getPlayerData(UUID uuid, DataType dataType) {
        try (Jedis jedis = RedisManager.getRedisDatabase().getJedisPool().getResource()) {
            String booleanName = jedis.get("dataplayerinfos:" + uuid.toString() + "_" + dataType.getName());
            if((booleanName != null) && (booleanName.equalsIgnoreCase("true"))) { return true; }
        } catch (Exception e) {
            System.out.println("[DenoriaPlayerInfo] Impossible de récupérer le data du joueur.");
        }
        return false;
    }

    /**
     * Supprimer toutes les clés du Joueur.
     *
     * @param uuid uuid du joueur
     */
    public static void removeKey(UUID uuid) {
        try (Jedis jedis = RedisManager.getRedisDatabase().getJedisPool().getResource()) {
            jedis.del("denoriaplayerinfos:" + uuid.toString() + "_" + "serverName");
            jedis.del("securityBungeeLogin:" + uuid.toString() + "_" + "useBungee");
        } catch (Exception e) {
            System.out.println("[DenoriaPlayerInfo] Impossible de supprimer les clés des Joueurs.");
        }
    }

}
