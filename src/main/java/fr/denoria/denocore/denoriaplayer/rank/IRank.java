package fr.denoria.denocore.denoriaplayer.rank;

import fr.denoria.denocore.mysql.SQLManager;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class IRank {

    protected static String table = "players_rank";

    /**
     * Créer un compte dans la base de données au Joueur.
     *
     * @param uuid
     */
    public static void createAccount(UUID uuid) {
        try {
            final Connection connection = SQLManager.DENORIA_SERVER.getDatabaseAccess().getRessource();
            PreparedStatement preparedStatement = connection
                    .prepareStatement("SELECT rank_identificatorname FROM " + table + " WHERE player_uuid = ?");
            preparedStatement.setString(1, uuid.toString());
            ResultSet rs = preparedStatement.executeQuery();
            if (!rs.next()) {
                preparedStatement.close();
                preparedStatement = connection.prepareStatement(
                        "INSERT INTO " + table + " (player_uuid, " +
                                "rank_identificatorname, rank_power, " +
                                "subrank_power, special_rank) VALUES(?, ?, ?, ?, ?)");
                preparedStatement.setString(1, uuid.toString());
                preparedStatement.setString(2, "PLAYER");
                preparedStatement.setInt(3, 0);
                preparedStatement.setInt(4, 0);
                preparedStatement.setString(5, "NONE");
                preparedStatement.executeUpdate();
                preparedStatement.close();
            }
            connection.close();
        } catch (SQLException e) { e.printStackTrace(); }
    }



    /**
     * Récupérer le Grade d'un Joueur dans la base de données.
     *
     * @param uuid l'uuid du joueur
     * @return le grade du joueur
     */
    public static Rank getPlayerRank(UUID uuid) {
        Rank rank = null;
        try {
            final Connection connection = SQLManager.DENORIA_SERVER.getDatabaseAccess().getRessource();
            PreparedStatement preparedStatement = connection
                    .prepareStatement("SELECT rank_identificatorname FROM " + table + " WHERE player_uuid = ?");
            preparedStatement.setString(1, uuid.toString());
            ResultSet rs = preparedStatement.executeQuery();
            if (!rs.next()) {
                preparedStatement.close();
                connection.close();
                return null;
            }
            rank = Rank.getRankWithIdentificatorName(rs.getString("rank_identificatorname"));
            preparedStatement.close();
            connection.close();
        } catch (SQLException e) { e.printStackTrace(); }
        return rank;
    }

    public static Rank getPlayerSubRank(UUID uuid) {
        Rank rank = null;
        try {
            final Connection connection = SQLManager.DENORIA_SERVER.getDatabaseAccess().getRessource();
            PreparedStatement preparedStatement = connection
                    .prepareStatement("SELECT subrank_power FROM " + table + " WHERE player_uuid = ?");
            preparedStatement.setString(1, uuid.toString());
            ResultSet rs = preparedStatement.executeQuery();
            if (!rs.next()) {
                preparedStatement.close();
                connection.close();
                return null;
            }
            rank = Rank.getRankWithPower(rs.getInt("subrank_power"));
            preparedStatement.close();
            connection.close();
        } catch (SQLException e) { e.printStackTrace(); }
        return rank;
    }

    public static RANKSPECIAL getPlayerSpecialRank(UUID uuid) {
        RANKSPECIAL rank = null;
        try {
            final Connection connection = SQLManager.DENORIA_SERVER.getDatabaseAccess().getRessource();
            PreparedStatement preparedStatement = connection
                    .prepareStatement("SELECT special_rank FROM " + table + " WHERE player_uuid = ?");
            preparedStatement.setString(1, uuid.toString());
            ResultSet rs = preparedStatement.executeQuery();
            if (!rs.next()) {
                preparedStatement.close();
                connection.close();
                return null;
            }
            rank = RANKSPECIAL.getRankWithIdentificatorName(rs.getString("special_rank"));
            preparedStatement.close();
            connection.close();
        } catch (SQLException e) { e.printStackTrace(); }
        return rank;
    }

    /**
     * Définir le Grade d'un Joueur dans la base de données.
     *
     * @param uuid
     * @param rank
     */
    public static void setPlayerRank(UUID uuid, Rank rank) {
        try {
            final Connection connection = SQLManager.DENORIA_SERVER.getDatabaseAccess().getRessource();
            PreparedStatement preparedStatement = connection.prepareStatement(
                    "UPDATE " + table + " SET rank_identificatorname = ?, rank_power = ? WHERE player_uuid = ?");
            preparedStatement.setString(1, rank.getIdentificatorName());
            preparedStatement.setInt(2, rank.getPower());
            preparedStatement.setString(3, uuid.toString());
            preparedStatement.executeUpdate();
            preparedStatement.close();
            connection.close();
        } catch (SQLException ignored) {}
    }

    public static void setPlayerRank(UUID uuid, RANKSPECIAL rank) {
        try {
            final Connection connection = SQLManager.DENORIA_SERVER.getDatabaseAccess().getRessource();
            PreparedStatement preparedStatement = connection.prepareStatement(
                    "UPDATE " + table + " SET special_rank = ? WHERE player_uuid = ?");
            preparedStatement.setString(1, rank.getIdentificatorName());
            preparedStatement.setString(2, uuid.toString());
            preparedStatement.executeUpdate();
            preparedStatement.close();
            connection.close();
        } catch (SQLException ignored) {}
    }

    /**
     * Récupérer la liste des comptes.
     *
     * @return
     */
    public static List<String> getPlayerAccount() {
        List<String> accountList = new ArrayList<>();
        try {
            final Connection connection = SQLManager.DENORIA_SERVER.getDatabaseAccess().getRessource();
            PreparedStatement preparedStatement = connection.prepareStatement("SELECT player_uuid FROM " + table);
            ResultSet rs = preparedStatement.executeQuery();
            while (rs.next()) {
                accountList.add(rs.getString("player_uuid"));
            }
            preparedStatement.close();
            connection.close();
        } catch (SQLException ignored) {}
        return accountList;
    }


}
