package fr.denoria.denocore.denoriaplayer.rank;

import net.md_5.bungee.api.ChatColor;
import org.bukkit.craftbukkit.libs.jline.internal.Nullable;

public enum Rank {

    // Grade du haut-staff.
    ADMINISTRATOR(100, "§a", "ADMINISTRATOR", "Admin", "Admin", null, null, ChatColor.RED, 3.0, 45, 12),

    // Grade Responsables.
    RESPONSABLE(95, "§b", "RESPONSABLE", "Responsable", "Responsable", null, null, ChatColor.RED, 3.0, 45, 12),
    DEVELOPER(90, "§c", "DEVELOPER", "Développeur", "Développeur", null, null, ChatColor.RED, 3.0, 45, 12),

    // Grade Skatoux
    DESIGNER(92, "§d", "DESIGNER", "Designer", "Designer", null, "GameDesigner", ChatColor.DARK_AQUA, 3.0, 45, 12),

    // Grade de Modérations.
    MODERATOR(90, "§e", "MODERATOR", "Modérateur", "Modérateur", null, null, ChatColor.BLUE, 3.0, 45, 12),
    GUIDE(80, "§f", "ASSISTANT", "Assistant", "Assistant", null, null, ChatColor.AQUA, 3.0, 45, 12),
    BUILDER(70, "§g", "BUILDER", "Builder", "Builder", null, null, ChatColor.GREEN, 3.0, 45, 12),

    // Grade de Staff.
    STAFF(60, "§h", "STAFF", "Staff", "Staff", null, null, ChatColor.DARK_GREEN, 3.0, 45, 12),

    // Grade des Amis/YouTuber.
    COEUR(50, "§i", "COEUR", "❤", "Friend", null, null, ChatColor.LIGHT_PURPLE, 3.0, 45, 12),
    FRIEND(40, "§j", "FRIEND", "Friend", "Friend", null, null, ChatColor.LIGHT_PURPLE, 3.0, 45, 12),
    YOUTUBER(40, "§k", "YOUTUBER", "YouTuber", "YouTuber", null, null, ChatColor.GOLD, 3.0, 45, 12),

    // Grades des Joueurs.
    LEGEND(30, "§l", "LEGEND", "Legend", "Légende", null, null, ChatColor.LIGHT_PURPLE, 3.0, 45, 12),
    HYPE(20, "§m", "HYPE", "HYPE", "Hype", null, null, ChatColor.AQUA, 2.5, 28, 10),
    VIPP(10, "§n", "VIPPLUS", "VIP+", "VIP+", null, null, ChatColor.YELLOW, 2.0, 16, 8),
    VIP(5, "§o", "VIP", "VIP", "VIP", null, null, ChatColor.GREEN, 1.5, 12, 6),
    PLAYER(0, "§p", "PLAYER", "Joueur", "Joueur", null, null, ChatColor.GRAY, 1.0, 6, 4);



    private Integer power;

    private String orderRank;
    private String identificatorName;
    private String name;
    private String prefix;
    private String suffix;
    private String ChatPrefix;

    private ChatColor chatColor;

    private Double coinsMultiplicator;

    private Integer friendLimit;
    private Integer partyLimit;

    /**
     * Constructeur de l'ENUM des RANK.
     *
     * @param power
     * @param identificatorName
     * @param name
     * @param prefix
     * @param chatColor
     * @param coinsMultiplicator
     * @param friendLimit
     * @param partyLimit
     */
    Rank(Integer power, String orderRank, String identificatorName, String name, String prefix, @Nullable String suffix,
         @Nullable String ChatPrefix, ChatColor chatColor, Double coinsMultiplicator, Integer friendLimit, Integer partyLimit) {
        this.power = power;
        this.orderRank = orderRank;
        this.identificatorName = identificatorName;
        this.name = name;
        this.prefix = prefix;
        this.suffix = suffix;
        this.ChatPrefix = ChatPrefix;
        this.chatColor = chatColor;
        this.coinsMultiplicator = coinsMultiplicator;
        this.friendLimit = friendLimit;
        this.partyLimit = partyLimit;
    }

    public Integer getPower() {
        return power;
    }

    public String getOrderRank() {
        return orderRank;
    }

    public String getIdentificatorName() {
        return identificatorName;
    }

    public String getName() {
        return name;
    }

    public String getPrefix() {
        return prefix;
    }

    public String getSuffix() {
        return suffix;
    }

    public ChatColor getChatColor() {
        return chatColor;
    }

    public Double getCoinsMultiplicator() {
        return coinsMultiplicator;
    }

    public Integer getFriendLimit() {
        return friendLimit;
    }

    public Integer getPartyLimit() {
        return partyLimit;
    }


    public String getChatPrefix() {
        return ChatPrefix;
    }

    /**
     * Récupérer un Rank à partir de l'indentificatorName.
     *
     * @param idName
     * @return
     */
    public static Rank getRankWithIdentificatorName(String idName) {
        for(Rank rank : Rank.values()) {
            if(rank.getIdentificatorName().equalsIgnoreCase(idName)) {
                return rank;
            }
        }
        return null;
    }

    public static Rank getRankWithPower(int power) {
        for(Rank rank : Rank.values()) {
            if(rank.getPower() == power) {
                return rank;
            }
        }
        return null;
    }
}
