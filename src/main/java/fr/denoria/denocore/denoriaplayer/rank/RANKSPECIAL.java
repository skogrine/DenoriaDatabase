/*
 * Copyright (c) 2019 - 2021 Denoria Network
 *
 *  ______   _______  _        _______  _______ _________ _______             _______  _        _______  _______  _______ _________ _        _______
 * (  __  \ (  ____ \( (    /|(  ___  )(  ____ )\__   __/(  ___  )       /\  (  ____ \| \    /\(  ___  )(  ____ \(  ____ )\__   __/( (    /|(  ____ \
 * | (  \  )| (    \/|  \  ( || (   ) || (    )|   ) (   | (   ) |      / /  | (    \/|  \  / /| (   ) || (    \/| (    )|   ) (   |  \  ( || (    \/
 * | |   ) || (__    |   \ | || |   | || (____)|   | |   | (___) |     / /   | (_____ |  (_/ / | |   | || |      | (____)|   | |   |   \ | || (__
 * | |   | ||  __)   | (\ \) || |   | ||     __)   | |   |  ___  |    / /    (_____  )|   _ (  | |   | || | ____ |     __)   | |   | (\ \) ||  __)
 * | |   ) || (      | | \   || |   | || (\ (      | |   | (   ) |   / /           ) ||  ( \ \ | |   | || | \_  )| (\ (      | |   | | \   || (
 * | (__/  )| (____/\| )  \  || (___) || ) \ \_____) (___| )   ( |  / /      /\____) ||  /  \ \| (___) || (___) || ) \ \_____) (___| )  \  || (____/\
 * (______/ (_______/|/    )_)(_______)|/   \__/\_______/|/     \|  \/       \_______)|_/    \/(_______)(_______)|/   \__/\_______/|/    )_)(_______/
 *
 */

package fr.denoria.denocore.denoriaplayer.rank;

public enum RANKSPECIAL {

    BETATESTER(1, "BETATESTER", "§7Bêta Testeur", " §f✦"),
    NONE(0, "NONE", "§cEnlevez les SpécialRanks", "");

    private String identificatorName, name, suffix;
    private int id;

    RANKSPECIAL(int id, String identificatorName, String name, String suffix) {
        this.identificatorName = identificatorName;
        this.name = name;
        this.suffix = suffix;
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setSuffix(String suffix) {
        this.suffix = suffix;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getIdentificatorName() {
        return identificatorName;
    }

    public String getName() {
        return name;
    }

    public String getSuffix() {
        return suffix;
    }

    public int getId() {
        return id;
    }

    public static RANKSPECIAL getRankWithIdentificatorName(String idName) {
        for(RANKSPECIAL rank : RANKSPECIAL.values()) {
            if(rank.getIdentificatorName().equalsIgnoreCase(idName)) {
                return rank;
            }
        }
        return null;
    }

    public static RANKSPECIAL getRankWithId(int id) {
        for(RANKSPECIAL rank : RANKSPECIAL.values()) {
            if(rank.getId() == id) {
                return rank;
            }
        }
        return null;
    }
}
