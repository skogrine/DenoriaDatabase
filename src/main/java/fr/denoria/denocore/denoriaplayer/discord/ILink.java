/*
 * Copyright (c) 2019 - 2021 Denoria Network
 *
 *  ______   _______  _        _______  _______ _________ _______             _______  _        _______  _______  _______ _________ _        _______
 * (  __  \ (  ____ \( (    /|(  ___  )(  ____ )\__   __/(  ___  )       /\  (  ____ \| \    /\(  ___  )(  ____ \(  ____ )\__   __/( (    /|(  ____ \
 * | (  \  )| (    \/|  \  ( || (   ) || (    )|   ) (   | (   ) |      / /  | (    \/|  \  / /| (   ) || (    \/| (    )|   ) (   |  \  ( || (    \/
 * | |   ) || (__    |   \ | || |   | || (____)|   | |   | (___) |     / /   | (_____ |  (_/ / | |   | || |      | (____)|   | |   |   \ | || (__
 * | |   | ||  __)   | (\ \) || |   | ||     __)   | |   |  ___  |    / /    (_____  )|   _ (  | |   | || | ____ |     __)   | |   | (\ \) ||  __)
 * | |   ) || (      | | \   || |   | || (\ (      | |   | (   ) |   / /           ) ||  ( \ \ | |   | || | \_  )| (\ (      | |   | | \   || (
 * | (__/  )| (____/\| )  \  || (___) || ) \ \_____) (___| )   ( |  / /      /\____) ||  /  \ \| (___) || (___) || ) \ \_____) (___| )  \  || (____/\
 * (______/ (_______/|/    )_)(_______)|/   \__/\_______/|/     \|  \/       \_______)|_/    \/(_______)(_______)|/   \__/\_______/|/    )_)(_______/
 *
 */

package fr.denoria.denocore.denoriaplayer.discord;

import com.google.gson.Gson;
import fr.denoria.denocore.bungeecord.DenoriaBungeeDatabase;
import fr.denoria.denocore.bungeecord.IBungeePlayer;
import fr.denoria.denocore.denoriaplayer.DenoriaPlayer;
import fr.denoria.denocore.mysql.SQLManager;
import fr.denoria.denocore.support.utils.CheckForExistingLinkRequest;
import fr.denoria.denocore.support.utils.HttpClient;
import net.md_5.bungee.api.plugin.Plugin;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class ILink {

    protected static String table = "link_infos";

    /**
     * Créer la base des données
     *
     * @param dp DenoriaPlayer
     */
    public static void createAccount(IBungeePlayer dp) {

        try {
            final Connection connection = SQLManager.DENORIA_SERVER.getDatabaseAccess().getRessource();
            PreparedStatement ps = connection.prepareStatement("INSERT INTO " + table + " (uuid, profile, email, password) VALUES (?, ?, ?, ?)");
            ps.setString(1, dp.getUUID().toString());
            ps.setString(2, dp.getProxiedPlayer().getDisplayName());
            ps.setString(3, "undefined");
            ps.setString(4, "undefined");
            ps.execute();
            ps.close();
            addNewKey(dp);
        } catch (SQLException e){
            e.printStackTrace();
        }
    }

    private static String addNewKey(IBungeePlayer p) {
        boolean doesKeyExist = isLink(p);
        if (doesKeyExist) return null;
        return _addNewKey(p);
    }



    private static String _addNewKey(final IBungeePlayer p) {
        String token = UUID.randomUUID().toString();
        String group = p.getRank().getPrefix();
        List<NameValuePair> args = new ArrayList<>();
        args.add(new BasicNameValuePair("api_key", "iVtnYnsOruFC-CJS9XRbfoGwOtv4tsiV"));
        args.add(new BasicNameValuePair("token", token));
        args.add(new BasicNameValuePair("uuid", p.getUUID().toString()));
        args.add(new BasicNameValuePair("mc_username", p.getProxiedPlayer().getDisplayName()));
        args.add(new BasicNameValuePair("valid", "1"));
        args.add(new BasicNameValuePair("key_type", "2"));
        List<NameValuePair> args2 = new ArrayList<>();
        args2.add(new BasicNameValuePair("api_key", "iVtnYnsOruFC-CJS9XRbfoGwOtv4tsiV"));
        args2.add(new BasicNameValuePair("uuid", p.getUUID().toString()));
        args2.add(new BasicNameValuePair("username", p.getProxiedPlayer().getDisplayName()));
        args2.add(new BasicNameValuePair("groups", group));
        try {
            HttpClient.post("https://forum.denoria.fr/createLinkKey", args);
            System.out.println(args);
            System.out.println(args2);
            HttpClient.post("https://forum.denoria.fr/updatePlayerCache", args2);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return token;
    }

    public static boolean isLink(IBungeePlayer player) {
        try {
            final Connection connection = SQLManager.DENORIA_SERVER.getDatabaseAccess().getRessource();
            PreparedStatement statement = connection.prepareStatement("SELECT uuid FROM " + table + " WHERE uuid = ?");
            statement.setString(1, player.getUUID().toString());
            ResultSet set = statement.executeQuery();
            if(set.next()) {
                return true;
            }
            connection.close();
            return false;
        } catch (SQLException e){
            e.printStackTrace();
            return false;
        }
    }

}
