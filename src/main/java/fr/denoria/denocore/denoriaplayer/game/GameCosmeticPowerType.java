package fr.denoria.denocore.denoriaplayer.game;

import org.bukkit.ChatColor;

public enum GameCosmeticPowerType {

    NONE("Aucune", ChatColor.RED),
    STANDARD("Standard", ChatColor.GREEN),
    RARE("Rare", ChatColor.GOLD),
    LEGENDAIRE("Legendaire", ChatColor.GOLD);

    private String name;
    private ChatColor chatColor;

    GameCosmeticPowerType(String name, ChatColor chatColor) {
        this.name = name;
        this.chatColor = chatColor;
    }

    public String getName() {
        return name;
    }

    public ChatColor getChatColor() {
        return chatColor;
    }
}
