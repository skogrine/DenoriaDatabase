package fr.denoria.denocore.denoriaplayer.game.skywars.stats;

import fr.denoria.denocore.mysql.SQLManager;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class ISkyWarsStats {

    /**
     * Créer un compte de Statistiques au Joueur.
     *
     * @param uuid
     * @param skyWarsStatsMode
     */
    public static void createAccount(UUID uuid, SkyWarsStatsMode skyWarsStatsMode) {
        try {
            final Connection connection = SQLManager.DENORIA_SERVER.getDatabaseAccess().getRessource();
            PreparedStatement preparedStatement = connection.prepareStatement(
                    "SELECT stats_wins " + " FROM " + skyWarsStatsMode.getName() + " WHERE player_uuid = ?");
            preparedStatement.setString(1, uuid.toString());
            ResultSet rs = preparedStatement.executeQuery();
            if (!rs.next()) {
                preparedStatement.close();
                preparedStatement = connection.prepareStatement("INSERT INTO " + skyWarsStatsMode.getName()
                        + " (player_uuid, stats_wins, stats_loses, "
                        + "stats_kills, stats_deaths, stats_timeplayed, stats_gameplayed, stats_blocksplaces, stats_blocksbreaks) VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?)");
                preparedStatement.setString(1, uuid.toString());
                preparedStatement.setInt(2, 0);
                preparedStatement.setInt(3, 0);
                preparedStatement.setInt(4, 0);
                preparedStatement.setInt(5, 0);
                preparedStatement.setInt(6, 0);
                preparedStatement.setInt(7, 0);
                preparedStatement.setInt(8, 0);
                preparedStatement.setInt(9, 0);
                preparedStatement.executeUpdate();
                preparedStatement.close();
            }
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Définir de nouvelle Statistiques au Joueur.
     *
     * @param uuid
     * @param skyWarsStatsMode
     * @param skyWarsStatsType
     * @param value
     */
    public static void setStats(UUID uuid, SkyWarsStatsMode skyWarsStatsMode, SkyWarsStatsType skyWarsStatsType,
                                Object value) {
        try {
            final Connection connection = SQLManager.DENORIA_SERVER.getDatabaseAccess().getRessource();
            PreparedStatement preparedStatement = connection.prepareStatement("UPDATE " + skyWarsStatsMode.getName()
                    + " SET " + skyWarsStatsType.getName() + " = ? WHERE player_uuid = ?");
            preparedStatement.setObject(1, value);
            preparedStatement.setString(2, uuid.toString());
            preparedStatement.executeUpdate();
            preparedStatement.close();
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Récupérer les Statistiques d'un Joueur.
     *
     * @param uuid
     * @param skyWarsStatsMode
     * @param skyWarsStatsType
     * @return
     */
    public static Object getStats(UUID uuid, SkyWarsStatsMode skyWarsStatsMode, SkyWarsStatsType skyWarsStatsType) {
        Object value = null;
        try {
            final Connection connection = SQLManager.DENORIA_SERVER.getDatabaseAccess().getRessource();
            PreparedStatement preparedStatement = connection.prepareStatement("SELECT " + skyWarsStatsType.getName()
                    + " FROM " + skyWarsStatsMode.getName() + " WHERE player_uuid = ?");
            preparedStatement.setString(1, uuid.toString());
            ResultSet rs = preparedStatement.executeQuery();
            if (!rs.next()) {
                connection.close();
                return value;
            }
            value = rs.getObject(skyWarsStatsType.getName());
            preparedStatement.close();
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return value;
    }

    /**
     * Récupérer une liste de TOP.
     *
     * @param skyWarsStatsMode
     * @param skyWarsStatsType
     * @return
     */
    public static List<String> getTopStats(SkyWarsStatsMode skyWarsStatsMode, SkyWarsStatsType skyWarsStatsType,
                                           int topNumber) {
        List<String> playerNameTopList = new ArrayList<String>();
        try {
            final Connection connection = SQLManager.DENORIA_SERVER.getDatabaseAccess().getRessource();
            PreparedStatement preparedStatement = connection
                    .prepareStatement("SELECT player_uuid FROM " + skyWarsStatsMode.getName() + " ORDER BY "
                            + skyWarsStatsType.getName() + " DESC LIMIT 10");
            ResultSet rs = preparedStatement.executeQuery();
            while (rs.next()) {
                playerNameTopList.add(rs.getString("player_uuid"));
            }
            preparedStatement.close();
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return playerNameTopList;
    }
}
