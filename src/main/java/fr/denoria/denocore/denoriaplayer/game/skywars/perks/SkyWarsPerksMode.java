package fr.denoria.denocore.denoriaplayer.game.skywars.perks;

public enum SkyWarsPerksMode {

    SKYWARS_SOLO("skywars_players_perks_solo"),
    SKYWARS_TEAM("skywars_players_perks_team");

    private String name;

    private SkyWarsPerksMode(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
