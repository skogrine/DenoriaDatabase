package fr.denoria.denocore.denoriaplayer.game.bedwars.stats;

import fr.denoria.denocore.mysql.SQLManager;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class iBedWarsStats {

    /**
     * Créer un compte de Statistiques au Joueur.
     *
     * @param uuid
     * @param bedWarsStatsMode
     */
    public static void createAccount(UUID uuid, BedWarsStatsMode bedWarsStatsMode) {
        try {
            final Connection connection = SQLManager.DENORIA_SERVER.getDatabaseAccess().getRessource();
            PreparedStatement preparedStatement = connection.prepareStatement(
                    "SELECT stats_wins " + " FROM " + bedWarsStatsMode.getName() + " WHERE player_uuid = ?");
            preparedStatement.setString(1, uuid.toString());
            ResultSet rs = preparedStatement.executeQuery();
            if (!rs.next()) {
                preparedStatement.close();
                preparedStatement = connection.prepareStatement("INSERT INTO " + bedWarsStatsMode.getName()
                        + " (player_uuid, stats_wins, stats_loses, "
                        + "stats_kills, stats_deaths, stats_timeplayed, stats_gameplayed, stats_blocksplaces, stats_blocksbreaks, stats_bedbreaks) VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
                preparedStatement.setString(1, uuid.toString());
                preparedStatement.setInt(2, 0);
                preparedStatement.setInt(3, 0);
                preparedStatement.setInt(4, 0);
                preparedStatement.setInt(5, 0);
                preparedStatement.setInt(6, 0);
                preparedStatement.setInt(7, 0);
                preparedStatement.setInt(8, 0);
                preparedStatement.setInt(9, 0);
                preparedStatement.setInt(10, 0);
                preparedStatement.executeUpdate();
                preparedStatement.close();
            }
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Définir de nouvelle Statistiques au Joueur.
     *
     * @param uuid
     * @param bedWarsStatsMode
     * @param bedWarsStatsType
     * @param value
     */
    public static void setStats(UUID uuid, BedWarsStatsMode bedWarsStatsMode, BedWarsStatsType bedWarsStatsType,
                                Object value) {
        try {
            final Connection connection = SQLManager.DENORIA_SERVER.getDatabaseAccess().getRessource();
            PreparedStatement preparedStatement = connection.prepareStatement("UPDATE " + bedWarsStatsMode.getName()
                    + " SET " + bedWarsStatsType.getName() + " = ? WHERE player_uuid = ?");
            preparedStatement.setObject(1, value);
            preparedStatement.setString(2, uuid.toString());
            preparedStatement.executeUpdate();
            preparedStatement.close();
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Récupérer les Statistiques d'un Joueur.
     *
     * @param uuid
     * @param bedWarsStatsMode
     * @param bedWarsStatsType
     * @return
     */
    public static Object getStats(UUID uuid, BedWarsStatsMode bedWarsStatsMode, BedWarsStatsType bedWarsStatsType) {
        Object value = null;
        try {
            final Connection connection = SQLManager.DENORIA_SERVER.getDatabaseAccess().getRessource();
            PreparedStatement preparedStatement = connection.prepareStatement("SELECT " + bedWarsStatsType.getName()
                    + " FROM " + bedWarsStatsMode.getName() + " WHERE player_uuid = ?");
            preparedStatement.setString(1, uuid.toString());
            ResultSet rs = preparedStatement.executeQuery();
            if (!rs.next()) {
                connection.close();
                return value;
            }
            value = rs.getObject(bedWarsStatsType.getName());
            preparedStatement.close();
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return value;
    }

    /**
     * Récupérer une liste de TOP.
     *
     * @param bedWarsStatsMode
     * @param bedWarsStatsType
     * @return
     */
    public static List<String> getTopStats(BedWarsStatsMode bedWarsStatsMode, BedWarsStatsType bedWarsStatsType,
                                           int topNumber) {
        List<String> playerNameTopList = new ArrayList<String>();
        try {
            final Connection connection = SQLManager.DENORIA_SERVER.getDatabaseAccess().getRessource();
            PreparedStatement preparedStatement = connection.prepareStatement("SELECT player_uuid FROM "
                    + bedWarsStatsMode.getName() + " ORDER BY " + bedWarsStatsType.getName() + " DESC LIMIT 10");
            ResultSet rs = preparedStatement.executeQuery();
            while (rs.next()) {
                playerNameTopList.add(rs.getString("player_uuid"));
            }
            preparedStatement.close();
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return playerNameTopList;
    }

}
