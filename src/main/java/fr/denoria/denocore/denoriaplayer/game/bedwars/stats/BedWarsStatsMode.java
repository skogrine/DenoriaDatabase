package fr.denoria.denocore.denoriaplayer.game.bedwars.stats;

public enum BedWarsStatsMode {

    BEDWARS_DUEL("bedwars_players_stats_duel");

    private String name;

    private BedWarsStatsMode(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
