package fr.denoria.denocore.denoriaplayer.game.skywars.kits;

public enum SkyWarsKitsMode {

    SKYWARS_SOLO("skywars_players_kits_solo"),
    SKYWARS_TEAM("skywars_players_kits_team");

    private String name;

    private SkyWarsKitsMode(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
