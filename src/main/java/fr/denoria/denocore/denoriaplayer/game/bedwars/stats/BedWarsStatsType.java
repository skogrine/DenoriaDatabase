package fr.denoria.denocore.denoriaplayer.game.bedwars.stats;

public enum BedWarsStatsType {

    WINS("stats_wins"),
    LOSES("stats_loses"),
    KILLS("stats_kills"),
    DEATHS("stats_deaths"),
    TIMEPLAYED("stats_timeplayed"),
    GAMEPLAYED("stats_gameplayed"),
    BLOCKSPLACES("stats_blocksplaces"),
    BLOCKSBREAKS("stats_blocksbreaks"),
    BEDBREAKS("stats_bedbreaks");

    private String name;

    private BedWarsStatsType(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}

