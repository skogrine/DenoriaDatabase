package fr.denoria.denocore.denoriaplayer.game.skywars.kits;

import fr.denoria.denocore.denoriaplayer.game.GameCosmeticPowerType;

public enum SkyWarsKitsType {

    ARCHER("kit_archer", "Archer", 3, GameCosmeticPowerType.RARE),
    ASSASSIN("kit_assassin", "Assassin", 3, GameCosmeticPowerType.RARE),
    ARMURIER("kit_armurier", "Armurier", 3, GameCosmeticPowerType.RARE),
    FORGERON("kit_forgeron", "Forgeron", 3, GameCosmeticPowerType.STANDARD),
    BASEBALLEUR("kit_baseballeur", "Baseballeur", 3, GameCosmeticPowerType.RARE),
    TERRORISTE("kit_terroriste", "Terroriste", 3, GameCosmeticPowerType.LEGENDAIRE),
    ENCHANTEUR("kit_enchanteur", "Enchanteur", 3, GameCosmeticPowerType.STANDARD),
    BUCHERON("kit_bucheron", "Bucheron", 3, GameCosmeticPowerType.STANDARD),
    GOLDENAPPLE("kit_golden", "Golden", 3, GameCosmeticPowerType.RARE),
    PYROMAN("kit_pyroman", "Pyroman", 3, GameCosmeticPowerType.LEGENDAIRE),
    SNOWMAN("kit_snowman", "Bonhomme de neige", 3, GameCosmeticPowerType.STANDARD),
    MINEUR("kit_mineur", "Mineur", 3, GameCosmeticPowerType.RARE),
    TROLLEUR("kit_trolleur", "Trolleur", 3, GameCosmeticPowerType.LEGENDAIRE),
    SCOUT("kit_scout", "Scout", 3, GameCosmeticPowerType.STANDARD),
    FERMIER("kit_fermier", "Fermier", 3, GameCosmeticPowerType.STANDARD),
    GOLEM("kit_golem", "Golem", 3, GameCosmeticPowerType.LEGENDAIRE),
    PECHEUR("kit_pecheur", "Pécheur", 3, GameCosmeticPowerType.STANDARD),
    MUSCLE("kit_muscle", "Musclé", 3, GameCosmeticPowerType.STANDARD),
    GRENADE("kit_grenade", "Grenade", 3, GameCosmeticPowerType.STANDARD);

    private String name;
    private String kitName;
    private int maxLevel;
    private GameCosmeticPowerType gameCosmeticPowerType;

    private SkyWarsKitsType(String name, String kitName, int maxLevel, GameCosmeticPowerType gameCosmeticPowerType) {
        this.name = name;
        this.kitName = kitName;
        this.maxLevel = maxLevel;
        this.gameCosmeticPowerType = gameCosmeticPowerType;
    }

    public String getName() {
        return name;
    }

    public String getKitName() {
        return kitName;
    }

    public int getMaxLevel() {
        return maxLevel;
    }

    public GameCosmeticPowerType getGameCosmeticPowerType() {
        return gameCosmeticPowerType;
    }

    /**
     * Récupérer un Kit pour son nom.
     *
     * @param kitName
     * @return
     */
    public SkyWarsKitsType getSkyWarsKitsTypeByName(String kitName) {
        for (SkyWarsKitsType skyWarsKitsType : SkyWarsKitsType.values()) {
            if (skyWarsKitsType.getName().equalsIgnoreCase(kitName)) {
                return skyWarsKitsType;
            }
        }
        return null;
    }
}
