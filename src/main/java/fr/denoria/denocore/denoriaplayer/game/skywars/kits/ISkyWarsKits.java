package fr.denoria.denocore.denoriaplayer.game.skywars.kits;

import fr.denoria.denocore.mysql.SQLManager;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.UUID;

public class ISkyWarsKits {

    /**
     * Créer un compte de Kits au Joueurs.
     *
     * @param uuid
     * @param skyWarsKitsMode
     */
    public static void createAccount(UUID uuid, SkyWarsKitsMode skyWarsKitsMode) {
        try {
            final Connection connection = SQLManager.DENORIA_SERVER.getDatabaseAccess().getRessource();
            PreparedStatement preparedStatement = connection.prepareStatement(
                    "SELECT kit_archer " + " FROM " + skyWarsKitsMode.getName() + " WHERE player_uuid = ?");
            preparedStatement.setString(1, uuid.toString());
            ResultSet rs = preparedStatement.executeQuery();
            if (!rs.next()) {
                preparedStatement.close();
                preparedStatement = connection.prepareStatement("INSERT INTO " + skyWarsKitsMode.getName()
                        + " (player_uuid, kit_archer, kit_assassin, "
                        + "kit_armurier, kit_forgeron, kit_baseballeur, kit_terroriste, kit_enchanteur, kit_bucheron, kit_golden, kit_pyroman, kit_snowman, kit_mineur, kit_trolleur, kit_scout, "
                        + "kit_fermier, kit_golem, kit_pecheur, kit_muscle, kit_grenade) VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
                preparedStatement.setString(1, uuid.toString());
                preparedStatement.setInt(2, 0);
                preparedStatement.setInt(3, 0);
                preparedStatement.setInt(4, 0);
                preparedStatement.setInt(5, 0);
                preparedStatement.setInt(6, 0);
                preparedStatement.setInt(7, 0);
                preparedStatement.setInt(8, 0);
                preparedStatement.setInt(9, 0);
                preparedStatement.setInt(10, 0);
                preparedStatement.setInt(11, 0);
                preparedStatement.setInt(12, 0);
                preparedStatement.setInt(13, 0);
                preparedStatement.setInt(14, 0);
                preparedStatement.setInt(15, 0);
                preparedStatement.setInt(16, 0);
                preparedStatement.setInt(17, 0);
                preparedStatement.setInt(18, 0);
                preparedStatement.setInt(19, 0);
                preparedStatement.setInt(20, 0);
                preparedStatement.executeUpdate();
                preparedStatement.close();
            }
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Définir le niveau du Kit d'un Joueur.
     *
     * @param uuid
     * @param skyWarsKitsMode
     * @param skyWarsKitsType
     * @param value
     */
    public static void setKit(UUID uuid, SkyWarsKitsMode skyWarsKitsMode, SkyWarsKitsType skyWarsKitsType,
                              Object value) {
        try {
            final Connection connection = SQLManager.DENORIA_SERVER.getDatabaseAccess().getRessource();
            PreparedStatement preparedStatement = connection.prepareStatement("UPDATE " + skyWarsKitsMode.getName()
                    + " SET " + skyWarsKitsType.getName() + " = ? WHERE player_uuid = ?");
            preparedStatement.setObject(1, value);
            preparedStatement.setString(2, uuid.toString());
            preparedStatement.executeUpdate();
            preparedStatement.close();
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Récupérer le niveau du Kit d'un Joueur.
     *
     * @param uuid uuid du joueur
     * @param skyWarsKitsMode swkitmode
     * @param skyWarsKitsType swkittype
     * @return object
     */
    public static Object getPlayerKit(UUID uuid, SkyWarsKitsMode skyWarsKitsMode, SkyWarsKitsType skyWarsKitsType) {
        Object value = null;
        try {
            final Connection connection = SQLManager.DENORIA_SERVER.getDatabaseAccess().getRessource();
            PreparedStatement preparedStatement = connection.prepareStatement("SELECT " + skyWarsKitsType.getName()
                    + " FROM " + skyWarsKitsMode.getName() + " WHERE player_uuid = ?");
            preparedStatement.setString(1, uuid.toString());
            ResultSet rs = preparedStatement.executeQuery();
            if (!rs.next()) {
                connection.close();
                return value;
            }
            value = rs.getObject(skyWarsKitsType.getName());
            preparedStatement.close();
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return value;
    }
}
