package fr.denoria.denocore.denoriaplayer.game.skywars.stats;

public enum SkyWarsStatsMode {

    SOLONORMAL("skywars_players_stats_solo_normal"),
    SOLOCHEAT("skywars_players_stats_solo_cheat"),
    TEAMNORMAL("skywars_players_stats_team_normal"),
    TEAMCHEAT("skywars_players_stats_team_cheat");

    private String name;

    private SkyWarsStatsMode(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
