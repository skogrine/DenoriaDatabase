package fr.denoria.denocore.denoriaplayer.game.skywars.stats;

public enum SkyWarsStatsType {

    WINS("stats_wins"),
    LOSES("stats_loses"),
    KILLS("stats_kills"),
    DEATHS("stats_deaths"),
    TIMEPLAYED("stats_timeplayed"),
    GAMEPLAYED("stats_gameplayed"),
    BLOCKSPLACES("stats_blocksplaces"),
    BLOCKSBREAKS("stats_blocksbreaks");

    private String name;

    private SkyWarsStatsType(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
