package fr.denoria.denocore.denoriaplayer.game.skywars.perks;

import fr.denoria.denocore.denoriaplayer.game.GameCosmeticPowerType;

public enum SkyWarsPerksType {

    ARROW_KEEP("perk_keeparrow", "Récupération de flêche", 4, GameCosmeticPowerType.RARE),
    ARROW_FIRE("perk_firearrow", "Flèche de feu", 4, GameCosmeticPowerType.RARE),
    ENDERPEARL_LIMIT_DAMAGE("perk_ownerenderpearl", "Maître de l'enderpearl", 4, GameCosmeticPowerType.STANDARD),
    REGENERATION("perk_regen", "Regeneration", 4, GameCosmeticPowerType.RARE),
    SPEED("perk_speed", "Vitesse", 4, GameCosmeticPowerType.RARE),
    EXPERIENCE("perk_experience", "Expérience", 4, GameCosmeticPowerType.STANDARD),
    GOLDEN_APPLE("perk_goldenapple", "Pomme dorée", 10, GameCosmeticPowerType.STANDARD),
    BULLDOZE("perk_bulldozer", "Bulldozer", 4, GameCosmeticPowerType.LEGENDAIRE);

    private String name;
    private String perkName;
    private int maxLevel;
    private GameCosmeticPowerType gameCosmeticPowerType;

    private SkyWarsPerksType(String name, String perkName, int maxLevel, GameCosmeticPowerType gameCosmeticPowerType) {
        this.name = name;
        this.perkName = perkName;
        this.maxLevel = maxLevel;
        this.gameCosmeticPowerType = gameCosmeticPowerType;
    }

    public String getName() {
        return name;
    }

    public String getPerkName() {
        return perkName;
    }

    public int getMaxLevel() {
        return maxLevel;
    }

    public GameCosmeticPowerType getGameCosmeticPowerType() {
        return gameCosmeticPowerType;
    }

    /**
     * Récupérer une Perk par son nom.
     *
     * @param perksName
     * @return
     */
    public SkyWarsPerksType getSkyWarsPerksTypeByName(String perksName) {
        for (SkyWarsPerksType skyWarsPerksType : SkyWarsPerksType.values()) {
            if (skyWarsPerksType.getName().equalsIgnoreCase(perksName)) {
                return skyWarsPerksType;
            }
        }
        return null;
    }
}
