package fr.denoria.denocore.denoriaplayer.game.skywars.perks;

import fr.denoria.denocore.mysql.SQLManager;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.UUID;

public class ISkyWarsPerks {

    /**
     * Créer un compte pour les Perks.
     *
     * @param uuid
     * @param skyWarsPerksMode
     */
    public static void createAccount(UUID uuid, SkyWarsPerksMode skyWarsPerksMode) {
        try {
            final Connection connection = SQLManager.DENORIA_SERVER.getDatabaseAccess().getRessource();
            PreparedStatement preparedStatement = connection.prepareStatement(
                    "SELECT perk_keeparrow " + " FROM " + skyWarsPerksMode.getName() + " WHERE player_uuid = ?");
            preparedStatement.setString(1, uuid.toString());
            ResultSet rs = preparedStatement.executeQuery();
            if (!rs.next()) {
                preparedStatement.close();
                preparedStatement = connection.prepareStatement("INSERT INTO " + skyWarsPerksMode.getName()
                        + " (player_uuid, perk_keeparrow, perk_firearrow, perk_ownerenderpearl, perk_regen, perk_speed, perk_experience, perk_goldenapple, perk_bulldozer) VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?)");
                preparedStatement.setString(1, uuid.toString());
                preparedStatement.setInt(2, 0);
                preparedStatement.setInt(3, 0);
                preparedStatement.setInt(4, 0);
                preparedStatement.setInt(5, 0);
                preparedStatement.setInt(6, 0);
                preparedStatement.setInt(7, 0);
                preparedStatement.setInt(8, 0);
                preparedStatement.setInt(9, 0);
                preparedStatement.executeUpdate();
                preparedStatement.close();
            }
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Définir un Perk à un Joueur.
     *
     * @param uuid
     * @param skyWarsPerksMode
     * @param skyWarsPerksType
     * @param value
     */
    public static void setPerk(UUID uuid, SkyWarsPerksMode skyWarsPerksMode, SkyWarsPerksType skyWarsPerksType,
                               Object value) {
        try {
            final Connection connection = SQLManager.DENORIA_SERVER.getDatabaseAccess().getRessource();
            PreparedStatement preparedStatement = connection.prepareStatement("UPDATE " + skyWarsPerksMode.getName()
                    + " SET " + skyWarsPerksType.getName() + " = ? WHERE player_uuid = ?");
            preparedStatement.setObject(1, value);
            preparedStatement.setString(2, uuid.toString());
            preparedStatement.executeUpdate();
            preparedStatement.close();
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Récupérer le niveau d'un Perk d'un Joueur.
     *
     * @param uuid
     * @param skyWarsPerksMode perk
     * @param skyWarsPerksType perks
     * @return
     */
    public static Object getPerk(UUID uuid, SkyWarsPerksMode skyWarsPerksMode, SkyWarsPerksType skyWarsPerksType) {
        Object value = null;
        try {
            final Connection connection = SQLManager.DENORIA_SERVER.getDatabaseAccess().getRessource();
            PreparedStatement preparedStatement = connection.prepareStatement(
                    "SELECT " + skyWarsPerksType.getName() + " FROM " + skyWarsPerksMode.getName() + " WHERE player_uuid = ?");
            preparedStatement.setString(1, uuid.toString());
            ResultSet rs = preparedStatement.executeQuery();
            if (!rs.next()) {
                connection.close();
                return value;
            }
            value = rs.getObject(skyWarsPerksType.getName());
            preparedStatement.close();
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return value;
    }
}
