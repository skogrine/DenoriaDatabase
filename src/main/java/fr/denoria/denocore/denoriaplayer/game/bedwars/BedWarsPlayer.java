package fr.denoria.denocore.denoriaplayer.game.bedwars;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import fr.denoria.denocore.DenoCore;
import fr.denoria.denocore.denoriaplayer.DenoriaPlayer;
import fr.denoria.denocore.denoriaplayer.game.bedwars.stats.BedWarsStatsMode;
import fr.denoria.denocore.denoriaplayer.game.bedwars.stats.BedWarsStatsType;
import fr.denoria.denocore.denoriaplayer.game.bedwars.stats.iBedWarsStats;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

public class BedWarsPlayer {

    public UUID uuid;
    public String playerRealName;
    public Player player;

    private final Map<BedWarsStatsMode, Map<BedWarsStatsType, Integer>> bedWarsPlayerStatsMap = new HashMap<>();

    /**
     * Gestion du Joueur en BedWars.
     *
     * @param denoriaPlayer Instance du joueur
     */
    public BedWarsPlayer(DenoriaPlayer denoriaPlayer) {
        this.uuid = denoriaPlayer.getUUID();
        this.playerRealName = denoriaPlayer.getPlayerRealName();
        this.player = denoriaPlayer.getPlayer();
        this.loadAccountData();

        // Charger les statistiques avec un décallage.
        Bukkit.getScheduler().runTaskLater(DenoCore.getInstance(), this::loadPlayerStats, 20L);
    }

    /**
     * Générer les comptes liés au BedWars.
     */
    public void loadAccountData() {
        for(BedWarsStatsMode bedWarsStatsMode : BedWarsStatsMode.values()) {
            iBedWarsStats.createAccount(uuid, bedWarsStatsMode); }
    }

    /**
     * Définir les Statistiques d'un Joueur.
     *
     * @param bedWarsStatsMode
     * @param bedWarsStatsType
     * @param value
     */
    public void setPlayerStats(BedWarsStatsMode bedWarsStatsMode, BedWarsStatsType bedWarsStatsType, Integer value) {
        this.bedWarsPlayerStatsMap.get(bedWarsStatsMode).put(bedWarsStatsType, value);
        iBedWarsStats.setStats(uuid, bedWarsStatsMode, bedWarsStatsType,
                getPlayerStats(bedWarsStatsMode, bedWarsStatsType));
    }

    /**
     * Ajouter une Statistiques à un Joueur.
     *
     * @param bedWarsStatsMode
     * @param bedWarsStatsType
     * @param value
     */
    public void addPlayerStats(BedWarsStatsMode bedWarsStatsMode, BedWarsStatsType bedWarsStatsType, Integer value) {
        this.bedWarsPlayerStatsMap.get(bedWarsStatsMode).put(bedWarsStatsType,
                (getPlayerStats(bedWarsStatsMode, bedWarsStatsType) + value));
        iBedWarsStats.setStats(uuid, bedWarsStatsMode, bedWarsStatsType,
                getPlayerStats(bedWarsStatsMode, bedWarsStatsType));
    }

    /**
     * Récupérer les Statistiques d'un Joueur.
     *
     * @param bedWarsStatsMode
     * @param bedWarsStatsType
     * @return
     */
    public Integer getPlayerStats(BedWarsStatsMode bedWarsStatsMode, BedWarsStatsType bedWarsStatsType) {
        return this.bedWarsPlayerStatsMap.get(bedWarsStatsMode).get(bedWarsStatsType);
    }

    /**
     * Charger les Statistiques du Joueur.
     */
    public void loadPlayerStats() {
        for (BedWarsStatsMode bedWarsStatsMode : BedWarsStatsMode.values()) {
            this.bedWarsPlayerStatsMap.put(bedWarsStatsMode, new HashMap<>());
            for (BedWarsStatsType bedWarsStatsType : BedWarsStatsType.values()) {
                this.bedWarsPlayerStatsMap.get(bedWarsStatsMode).put(bedWarsStatsType,
                        (int) iBedWarsStats.getStats(uuid, bedWarsStatsMode, bedWarsStatsType));
            }
        }
    }
}
