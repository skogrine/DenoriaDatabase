package fr.denoria.denocore.denoriaplayer.game.skywars;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import fr.denoria.denocore.DenoCore;
import fr.denoria.denocore.denoriaplayer.DenoriaPlayer;
import fr.denoria.denocore.denoriaplayer.game.skywars.kits.ISkyWarsKits;
import fr.denoria.denocore.denoriaplayer.game.skywars.kits.SkyWarsKitsMode;
import fr.denoria.denocore.denoriaplayer.game.skywars.kits.SkyWarsKitsType;
import fr.denoria.denocore.denoriaplayer.game.skywars.perks.ISkyWarsPerks;
import fr.denoria.denocore.denoriaplayer.game.skywars.perks.SkyWarsPerksMode;
import fr.denoria.denocore.denoriaplayer.game.skywars.perks.SkyWarsPerksType;
import fr.denoria.denocore.denoriaplayer.game.skywars.stats.ISkyWarsStats;
import fr.denoria.denocore.denoriaplayer.game.skywars.stats.SkyWarsStatsMode;
import fr.denoria.denocore.denoriaplayer.game.skywars.stats.SkyWarsStatsType;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

public class SkyWarsPlayer {

    public UUID uuid;
    public String playerRealName;
    public Player player;

    private Map<SkyWarsStatsMode, Map<SkyWarsStatsType, Integer>> skyWarsPlayerStatsMap = new HashMap<>();
    private Map<SkyWarsPerksMode, Map<SkyWarsPerksType, Integer>> skyWarsPlayerPerksMap = new HashMap<>();
    private Map<SkyWarsKitsMode, Map<SkyWarsKitsType, Integer>> skyWarsPlayerKitsMap = new HashMap<>();

    /**
     * Gestion du Joueur en SkyWars.
     *
     * @param denoriaPlayer
     */
    public SkyWarsPlayer(DenoriaPlayer denoriaPlayer) {
        this.uuid = denoriaPlayer.getUUID();
        this.playerRealName = denoriaPlayer.getPlayerRealName();
        this.player = denoriaPlayer.getPlayer();
        this.loadAccountData();
        this.loadPlayerPerks();
        this.loadPlayerKits();

        // Charger les statistiques avec un décallage.
        Bukkit.getScheduler().runTaskLater(DenoCore.getInstance(), this::loadPlayerStats, 20L);
    }

    /**
     * Générer les comptes liés au SkyWars.
     */
    public void loadAccountData() {
        for (SkyWarsStatsMode skyWarsStatsMode : SkyWarsStatsMode.values()) {
            ISkyWarsStats.createAccount(uuid, skyWarsStatsMode); }
        for (SkyWarsPerksMode skyWarsPerksMode : SkyWarsPerksMode.values()) {
            ISkyWarsPerks.createAccount(uuid, skyWarsPerksMode); }
        for (SkyWarsKitsMode skyWarsKitsMode : SkyWarsKitsMode.values()) {
            ISkyWarsKits.createAccount(uuid, skyWarsKitsMode); }
    }

    /**
     * Définir les Statistiques d'un Joueur.
     *
     * @param skyWarsStatsMode
     * @param skyWarsStatsType
     * @param value
     */
    public void setPlayerStats(SkyWarsStatsMode skyWarsStatsMode, SkyWarsStatsType skyWarsStatsType, Integer value) {
        this.skyWarsPlayerStatsMap.get(skyWarsStatsMode).put(skyWarsStatsType, value);
        ISkyWarsStats.setStats(uuid, skyWarsStatsMode, skyWarsStatsType,
                getPlayerStats(skyWarsStatsMode, skyWarsStatsType));
    }

    /**
     * Ajouter une Statistiques à un Joueur.
     *
     * @param skyWarsStatsMode
     * @param skyWarsStatsType
     * @param value
     */
    public void addPlayerStats(SkyWarsStatsMode skyWarsStatsMode, SkyWarsStatsType skyWarsStatsType, Integer value) {
        this.skyWarsPlayerStatsMap.get(skyWarsStatsMode).put(skyWarsStatsType,
                (getPlayerStats(skyWarsStatsMode, skyWarsStatsType) + value));
        ISkyWarsStats.setStats(uuid, skyWarsStatsMode, skyWarsStatsType,
                getPlayerStats(skyWarsStatsMode, skyWarsStatsType));
    }

    /**
     * Récupérer les Statistiques d'un Joueur.
     *
     * @param skyWarsStatsMode
     * @param skyWarsStatsType
     * @return
     */
    public Integer getPlayerStats(SkyWarsStatsMode skyWarsStatsMode, SkyWarsStatsType skyWarsStatsType) {
        return this.skyWarsPlayerStatsMap.get(skyWarsStatsMode).get(skyWarsStatsType);
    }

    /**
     * Charger les Statistiques du Joueur.
     */
    public void loadPlayerStats() {
        for (SkyWarsStatsMode skyWarsStatsMode : SkyWarsStatsMode.values()) {
            this.skyWarsPlayerStatsMap.put(skyWarsStatsMode, new HashMap<>());
            for (SkyWarsStatsType skyWarsStatsType : SkyWarsStatsType.values()) {
                this.skyWarsPlayerStatsMap.get(skyWarsStatsMode).put(skyWarsStatsType,
                        (int) ISkyWarsStats.getStats(uuid, skyWarsStatsMode, skyWarsStatsType));
            }
        }
    }

    /**
     * Définir les Perks d'un Joueur.
     *
     * @param skyWarsPerksMode
     * @param skyWarsPerksType
     * @param value
     */
    public void setPlayerPerks(SkyWarsPerksMode skyWarsPerksMode, SkyWarsPerksType skyWarsPerksType, Integer value) {
        this.skyWarsPlayerPerksMap.get(skyWarsPerksMode).put(skyWarsPerksType, value);
        ISkyWarsPerks.setPerk(uuid, skyWarsPerksMode, skyWarsPerksType,
                getPlayerPerks(skyWarsPerksMode, skyWarsPerksType));
    }

    /**
     * Augmenter le niveau d'un Perk d'un Joueur.
     *
     * @param skyWarsPerksMode
     * @param skyWarsPerksType
     */
    public void upLevelPlayerPerks(SkyWarsPerksMode skyWarsPerksMode, SkyWarsPerksType skyWarsPerksType) {
        this.skyWarsPlayerPerksMap.get(skyWarsPerksMode).put(skyWarsPerksType,
                (getPlayerPerks(skyWarsPerksMode, skyWarsPerksType) + 1));
        ISkyWarsPerks.setPerk(uuid, skyWarsPerksMode, skyWarsPerksType,
                getPlayerPerks(skyWarsPerksMode, skyWarsPerksType));
    }

    /**
     * Récupérer le niveau d'un Perk d'un Joueur.
     *
     * @param skyWarsPerksMode
     * @param skyWarsPerksType
     * @return
     */
    public Integer getPlayerPerks(SkyWarsPerksMode skyWarsPerksMode, SkyWarsPerksType skyWarsPerksType) {
        return (Integer) this.skyWarsPlayerPerksMap.get(skyWarsPerksMode).get(skyWarsPerksType);
    }

    /**
     * Charger les Perks d'un Joueur.
     *
     */
    public void loadPlayerPerks() {
        for (SkyWarsPerksMode skyWarsPerksMode : SkyWarsPerksMode.values()) {
            this.skyWarsPlayerPerksMap.put(skyWarsPerksMode, new HashMap<SkyWarsPerksType, Integer>());
            for (SkyWarsPerksType skyWarsPerksType : SkyWarsPerksType.values()) {
                this.skyWarsPlayerPerksMap.get(skyWarsPerksMode).put(skyWarsPerksType,
                        (int) ISkyWarsPerks.getPerk(uuid, skyWarsPerksMode, skyWarsPerksType));
            }
        }
    }

    /**
     * Définir un niveau de Kit à un Joueur.
     *
     * @param skyWarsKitsMode
     * @param skyWarsKitsType
     * @param value
     */
    public void setPlayerKits(SkyWarsKitsMode skyWarsKitsMode, SkyWarsKitsType skyWarsKitsType, Integer value) {
        this.skyWarsPlayerKitsMap.get(skyWarsKitsMode).put(skyWarsKitsType, value);
        ISkyWarsKits.setKit(uuid, skyWarsKitsMode, skyWarsKitsType, getPlayerKits(skyWarsKitsMode, skyWarsKitsType));
    }

    /**
     * Augmenter le niveau de Kit d'un Joueur.
     *
     * @param skyWarsKitsMode
     * @param skyWarsKitsType
     */
    public void upLevelPlayerKits(SkyWarsKitsMode skyWarsKitsMode, SkyWarsKitsType skyWarsKitsType) {
        this.skyWarsPlayerKitsMap.get(skyWarsKitsMode).put(skyWarsKitsType,
                (getPlayerKits(skyWarsKitsMode, skyWarsKitsType) + 1));
        ISkyWarsKits.setKit(uuid, skyWarsKitsMode, skyWarsKitsType, getPlayerKits(skyWarsKitsMode, skyWarsKitsType));
    }

    /**
     * Récupérer le Kit d'un Joueur.
     *
     * @param skyWarsKitsMode
     * @param skyWarsKitsType
     * @return
     */
    public Integer getPlayerKits(SkyWarsKitsMode skyWarsKitsMode, SkyWarsKitsType skyWarsKitsType) {
        return (Integer) this.skyWarsPlayerKitsMap.get(skyWarsKitsMode).get(skyWarsKitsType);
    }

    /**
     * Charger les Kits d'un Joueur.
     */
    public void loadPlayerKits() {
        for (SkyWarsKitsMode skyWarsKitsMode : SkyWarsKitsMode.values()) {
            this.skyWarsPlayerKitsMap.put(skyWarsKitsMode, new HashMap<SkyWarsKitsType, Integer>());
            for (SkyWarsKitsType skyWarsKitsType : SkyWarsKitsType.values()) {
                this.skyWarsPlayerKitsMap.get(skyWarsKitsMode).put(skyWarsKitsType,
                        (int) ISkyWarsKits.getPlayerKit(uuid, skyWarsKitsMode, skyWarsKitsType));
            }
        }
    }
}
