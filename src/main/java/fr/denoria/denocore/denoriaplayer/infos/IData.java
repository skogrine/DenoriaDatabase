package fr.denoria.denocore.denoriaplayer.infos;

import fr.denoria.denocore.denoriaplayer.DenoriaPlayer;
import fr.denoria.denocore.mysql.SQLManager;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;

public class IData {
    protected static String table = "players_infos";

    /**
     * Créer un compte IData dans la base de données.
     *
     * @param uuid
     */
    public static void createAccount(String name, UUID uuid) {
        try {
            final Connection connection = SQLManager.DENORIA_SERVER.getDatabaseAccess().getRessource();
            PreparedStatement preparedStatement = connection
                    .prepareStatement("SELECT first_login " + " FROM " + table + " WHERE player_uuid = ?");
            preparedStatement.setString(1, uuid.toString());
            ResultSet rs = preparedStatement.executeQuery();
            if (!rs.next()) {
                preparedStatement.close();
                preparedStatement = connection.prepareStatement("INSERT INTO " + table
                        + " (player_uuid, first_login, last_login, language,"
                        + " afk_status, lagless_mode, allow_messages, allow_party, allow_party_follow, allow_friend, allow_guilds, allow_chat, allow_pvp_particle,"
                        + " allow_pvp_particle_death, allow_pvp_blood, allow_show_players, allow_lobby_command_protection, allow_chat_mention, name) VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
                preparedStatement.setString(1, uuid.toString());
                preparedStatement.setString(2, new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").format(new Date()));
                preparedStatement.setString(3, new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").format(new Date()));
                preparedStatement.setString(4, Langues.FRENCH.getIdString());
                preparedStatement.setBoolean(5, false);
                preparedStatement.setBoolean(6, false);
                preparedStatement.setBoolean(7, true);
                preparedStatement.setBoolean(8, true);
                preparedStatement.setBoolean(9, true);
                preparedStatement.setBoolean(10, true);
                preparedStatement.setBoolean(11, true);
                preparedStatement.setBoolean(12, true);
                preparedStatement.setBoolean(13, true);
                preparedStatement.setBoolean(14, true);
                preparedStatement.setBoolean(15, true);
                preparedStatement.setBoolean(16, true);
                preparedStatement.setBoolean(17, true);
                preparedStatement.setBoolean(18, true);
                preparedStatement.setString(19, name);
                preparedStatement.executeUpdate();
                preparedStatement.close();
            }
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    /**
     * Met à jour la dernère connection du joueur
     *
     * @param uuid uuid du joueur
     */
    public static void setLastLogin(UUID uuid) {
        try {
            final Connection connection = SQLManager.DENORIA_SERVER.getDatabaseAccess().getRessource();
            PreparedStatement preparedStatement = connection.prepareStatement(
                    "UPDATE " + table + " SET last_login = ? WHERE player_uuid = ?");
            preparedStatement.setObject(1, new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").format(new Date()));
            preparedStatement.setString(2, uuid.toString());
            preparedStatement.executeUpdate();
            preparedStatement.close();
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Définir un IData dans la base de donnée.
     *
     * @param uuid uuid of joueur
     * @param dataType Type of data
     * @param value value of data
     */
    public static void setPlayerData(UUID uuid, DataType dataType, Object value) {
        try {
            final Connection connection = SQLManager.DENORIA_SERVER.getDatabaseAccess().getRessource();
            PreparedStatement preparedStatement = connection.prepareStatement(
                    "UPDATE " + table + " SET " + dataType.getColumName() + " = ? WHERE player_uuid = ?");
            preparedStatement.setObject(1, value);
            preparedStatement.setString(2, uuid.toString());
            preparedStatement.executeUpdate();
            preparedStatement.close();
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Définir un IData dans la base de donnée.
     *
     * @param uuid
     * @param dataType
     * @param value
     */
    public static void setPlayerDataWithString(UUID uuid, String dataType, Object value) {
        try {
            final Connection connection = SQLManager.DENORIA_SERVER.getDatabaseAccess().getRessource();
            PreparedStatement preparedStatement = connection.prepareStatement(
                    "UPDATE " + table + " SET " + dataType+ " = ? WHERE player_uuid = ?");
            preparedStatement.setObject(1, value);
            preparedStatement.setString(2, uuid.toString());
            preparedStatement.executeUpdate();
            preparedStatement.close();
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Récupérer un IData dans la base de données.
     *
     * @param uuid
     * @param dataType
     * @return
     */
    public static Object getPlayerData(UUID uuid, DataType dataType) {
        Object value = null;
        try {
            final Connection connection = SQLManager.DENORIA_SERVER.getDatabaseAccess().getRessource();
            PreparedStatement preparedStatement = connection.prepareStatement(
                    "SELECT " + dataType.getColumName() + " FROM " + table + " WHERE player_uuid = ?");
            preparedStatement.setString(1, uuid.toString());
            ResultSet rs = preparedStatement.executeQuery();
            if (!rs.next()) {
                connection.close();
                return value;
            }
            value = rs.getObject(dataType.getColumName());
            preparedStatement.close();
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return value;
    }

    /**
     * Récupérer un IData dans la base de données.
     *
     * @param uuid
     * @param dataType
     * @return
     */
    public static Object getPlayerDataWithString(UUID uuid, String dataType) {
        Object value = null;
        try {
            final Connection connection = SQLManager.DENORIA_SERVER.getDatabaseAccess().getRessource();
            PreparedStatement preparedStatement = connection.prepareStatement(
                    "SELECT " + dataType + " FROM " + table + " WHERE player_uuid = ?");
            preparedStatement.setString(1, uuid.toString());
            ResultSet rs = preparedStatement.executeQuery();
            if (!rs.next()) {
                connection.close();
                return value;
            }
            value = rs.getObject(dataType);
            preparedStatement.close();
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return value;
    }
}
