package fr.denoria.denocore.denoriaplayer.infos;

public enum Langues {

    FRENCH("fr_FR", "French", "Français"),
    ENGLISH("en", "English", "Anglais"),
    SPANISH("es", "Spanish", "Espagnol"),
    GERMANY("de", "German", "Allemand");

    private String idString;
    private String name;
    private String nameFrench;

    Langues(String idString, String name, String nameFrench) {
        this.idString = idString;
        this.name = name;
        this.nameFrench = nameFrench;
    }

    public String getIdString() {
        return idString;
    }

    public String getName() {
        return name;
    }

    public String getNameFrench() {
        return nameFrench;
    }

    /**
     * Récupérer la Langue par l'IdString.
     *
     * @param id if of language (ex: "fr_FR")
     * @return language
     */
    public static Langues getLangueByIdString(String id) {
        for (Langues lang : Langues.values()) {
            if (lang.idString.equalsIgnoreCase(id)) {
                return lang;
            }
        }
        return Langues.ENGLISH;
    }
}
