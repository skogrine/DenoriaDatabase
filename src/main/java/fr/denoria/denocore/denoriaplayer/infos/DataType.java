package fr.denoria.denocore.denoriaplayer.infos;

public enum DataType {

    AFK(1, "AFK", "afk_status"),
    LAGLESS_MODE(2, "LAGLESS_MODE", "lagless_mode"),
    ALLOW_MESSAGES_PRIVATES(3, "ALLOW_MESSAGES_PRIVATES", "allow_messages"),
    ALLOW_PARTY(4, "ALLOW_PARTY", "allow_party"),
    ALLOW_PARTY_FOLLOW(5, "ALLOW_PARTY_FOLLOW", "allow_party_follow"),
    ALLOW_FRIEND(6, "ALLOW_FRIEND", "allow_friend"),
    ALLOW_GUILDS(7, "ALLOW_GUILDS", "allow_guilds"),
    ALLOW_CHAT(8, "ALLOW_CHAT", "allow_chat"),
    ALLOW_PVP_PARTICLE(9, "ALLOW_PVP_PARTICLE", "allow_pvp_particle"),
    ALLOW_PVP_PARTICLE_DEATH(10, "ALLOW_PVP_PARTICLE_DEATH", "allow_pvp_particle_death"),
    ALLOW_PVP_BLOOD(11, "ALLOW_PVP_BLOOD", "allow_pvp_blood"),
    ALLOW_SHOW_PLAYERS(12, "ALLOW_SHOW_PLAYERS", "allow_show_players"),
    ALLOW_LOBBY_COMMAND_PROTECTION(13, "ALLOW_LOBBY_COMMAND_PROTECTION", "allow_lobby_command_protection"),
    ALLOW_CHAT_MENTION(14, "ALLOW_MENTION_CHAT", "allow_chat_mention");

    private Integer id;
    private String name;
    private String columName;

    DataType(Integer id, String name, String columName) {
        this.id = id;
        this.name = name;
        this.columName = columName;
    }

    public Integer getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getColumName() {
        return columName;
    }

}
