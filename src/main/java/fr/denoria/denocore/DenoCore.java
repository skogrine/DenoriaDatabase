package fr.denoria.denocore;

import fr.denoria.denocore.commands.AbstractCommand;
import fr.denoria.denocore.commands.PlayerProcessCommandListener;
import fr.denoria.denocore.commands.admin.*;
import fr.denoria.denocore.commands.friends.FriendsCommand;
import fr.denoria.denocore.commands.misc.LagCommand;
import fr.denoria.denocore.commands.misc.LobbyCommand;
import fr.denoria.denocore.commands.misc.ReportCommand;
import fr.denoria.denocore.commands.misc.SpawnCommand;
import fr.denoria.denocore.commands.mods.*;
import fr.denoria.denocore.commands.mods.mute.MuteCommand;
import fr.denoria.denocore.commands.mods.mute.UnmuteCommand;
import fr.denoria.denocore.commands.msg.MessagePrivateCommand;
import fr.denoria.denocore.commands.nick.NickCommand;
import fr.denoria.denocore.commands.nick.NickListCommand;
import fr.denoria.denocore.commands.nick.nickadmCommand;
import fr.denoria.denocore.commands.staff.FlyCommand;
import fr.denoria.denocore.commands.temp.ListNPC;
import fr.denoria.denocore.commands.temp.ListWorlds;
import fr.denoria.denocore.denoriaplayer.DenoriaPlayer;
import fr.denoria.denocore.listeners.DenoriaPlayerChatListener;
import fr.denoria.denocore.listeners.DenoriaPlayerJoinListener;
import fr.denoria.denocore.listeners.DenoriaPlayerLeftListener;
import fr.denoria.denocore.mysql.SQLManager;
import fr.denoria.denocore.network.infos.NetworkAPI;
import fr.denoria.denocore.redis.RedisManager;
import fr.denoria.denocore.redis.spigot.RedisPubSubSpigot;
import fr.denoria.denocore.support.npc.NPCManager;
import fr.denoria.denocore.support.packets.DenoriaPackets;
import fr.denoria.denocore.support.pvp.KnockBack;
import fr.denoria.denocore.support.task.GDelayLauncher;
import fr.denoria.denocore.support.task.GTimerLauncher;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;
import org.slf4j.Logger;

import java.util.*;

public class DenoCore extends JavaPlugin {

    // Utilisation des Maps.
    public static Map<UUID, DenoriaPlayer> iPlayerMap = new HashMap<>();
    // Instance du plugin Bukkit.
    private static DenoCore instance;
    // Utilisation des Lists.
    private final List<AbstractCommand> abstractCommands = new ArrayList<>();
    // Base de données (Redis).
    public RedisManager redisManager;
    public RedisPubSubSpigot redisPubSubSpigot;
    public Logger logger;
    // Utilisation des Runnables.
    private GTimerLauncher gTimerLauncher;
    private GDelayLauncher gDelayLauncher;

    private NPCManager npcManager;

    public static DenoCore getInstance() {
        return instance;
    }

    @Override
    public void onEnable() {

        // Gestion de l'instance du plugin.
        instance = this;
        saveDefaultConfig();


        // Utilisation de MySQL.
        SQLManager.initAllDatabaseConnection();
        redisManager = new RedisManager(true);
        redisManager.connect();


        // Gestion des packets.
        this.getServer().getMessenger().registerOutgoingPluginChannel(this, "BungeeCord");
        new DenoriaPackets().AddChanel();

        // Gestion des Listeners.
        this.npcManager = new NPCManager();
        PluginManager pluginManager = Bukkit.getPluginManager();
        pluginManager.registerEvents(new DenoriaPlayerJoinListener(), this);
        pluginManager.registerEvents(new DenoriaPlayerLeftListener(), this);
        pluginManager.registerEvents(new DenoriaPlayerChatListener(), this);
        pluginManager.registerEvents(new PlayerProcessCommandListener(), this);
        pluginManager.registerEvents(new KnockBack(), this);
        //pluginManager.registerEvents(new DenoriaPlayerTabComplete(), this);

        // Enregistrement des Commandes.
        abstractCommands.add(new RankCommand());
        abstractCommands.add(new NickCommand());
        abstractCommands.add(new KickCommand());
        abstractCommands.add(new SlowChatCommand());
        abstractCommands.add(new NickListCommand());
        abstractCommands.add(new LobbyCommand());
        abstractCommands.add(new HelpChatCommand());
        abstractCommands.add(new MessagePrivateCommand());
        abstractCommands.add(new AFKCommand());
        abstractCommands.add(new DenoriaTeleportCommand());
        abstractCommands.add(new TeleportCommand());
        abstractCommands.add(new MuteCommand());
        abstractCommands.add(new UnmuteCommand());
        abstractCommands.add(new LocalBroadcastCommand());
        abstractCommands.add(new LagCommand());
        abstractCommands.add(new ReportCommand());
        abstractCommands.add(new PermissionCommand());
        abstractCommands.add(new FriendsCommand());
        abstractCommands.add(new SpecialRankCommand());
        abstractCommands.add(new CurrencyCommand());

        // Commande tempo
        abstractCommands.add(new nickadmCommand());

        abstractCommands.add(new ListWorlds());
        abstractCommands.add(new ListNPC());

        // Mes Commandes
        abstractCommands.add(new FlyCommand());
        abstractCommands.add(new SpawnCommand());
        abstractCommands.add(new ModBroadcastCommand());

        // Initialisation de Redis.
        this.redisPubSubSpigot = new RedisPubSubSpigot();


        // Gestion du Network et ses Informations.
        NetworkAPI.loadNetworkInfo();
        Bukkit.getScheduler().scheduleSyncRepeatingTask(this, NetworkAPI::loadNetworkInfo, 20 * 60L, 20 * 60L);


        super.onEnable();


    }

    @Override
    public void onDisable() {
        // Utilisation de MySQL.
        SQLManager.closeAllDatabaseConnection();
        // Utilisation de Redis.
        redisManager.disconnect();


        super.onDisable();
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        for (AbstractCommand ac : abstractCommands) {
            if (ac.cmd.contains(label)) {
                if (args.length == 0) {
                    ac.handle(sender, Arrays.asList(args));
                } else {
                    List<String> params = Arrays.asList(args);
                    ac.handle(sender, params);
                }
            }
        }
        return false;
    }

    public RedisPubSubSpigot getRedisPubSubSpigot() {
        return redisPubSubSpigot;
    }

    public GTimerLauncher getGTimerLauncher() {
        return gTimerLauncher;
    }

    public GDelayLauncher getGDelayLauncher() {
        return gDelayLauncher;
    }

    public Logger getLog() {
        return logger;
    }

    public NPCManager getNpcManager() {
        return npcManager;
    }

}
