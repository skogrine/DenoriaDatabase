/*
 * Copyright (c) 2019 - 2021 Denoria Network
 *
 *  ______   _______  _        _______  _______ _________ _______             _______  _        _______  _______  _______ _________ _        _______
 * (  __  \ (  ____ \( (    /|(  ___  )(  ____ )\__   __/(  ___  )       /\  (  ____ \| \    /\(  ___  )(  ____ \(  ____ )\__   __/( (    /|(  ____ \
 * | (  \  )| (    \/|  \  ( || (   ) || (    )|   ) (   | (   ) |      / /  | (    \/|  \  / /| (   ) || (    \/| (    )|   ) (   |  \  ( || (    \/
 * | |   ) || (__    |   \ | || |   | || (____)|   | |   | (___) |     / /   | (_____ |  (_/ / | |   | || |      | (____)|   | |   |   \ | || (__
 * | |   | ||  __)   | (\ \) || |   | ||     __)   | |   |  ___  |    / /    (_____  )|   _ (  | |   | || | ____ |     __)   | |   | (\ \) ||  __)
 * | |   ) || (      | | \   || |   | || (\ (      | |   | (   ) |   / /           ) ||  ( \ \ | |   | || | \_  )| (\ (      | |   | | \   || (
 * | (__/  )| (____/\| )  \  || (___) || ) \ \_____) (___| )   ( |  / /      /\____) ||  /  \ \| (___) || (___) || ) \ \_____) (___| )  \  || (____/\
 * (______/ (_______/|/    )_)(_______)|/   \__/\_______/|/     \|  \/       \_______)|_/    \/(_______)(_______)|/   \__/\_______/|/    )_)(_______/
 *
 */

package fr.denoria.denocore.support.gameprofile;

import com.google.gson.*;
import com.mojang.authlib.GameProfile;
import com.mojang.authlib.properties.Property;
import fr.denoria.denocore.redis.RedisManager;
import fr.denoria.denocore.support.npc.SkinEnum;
import fr.denoria.denocore.support.skin.Base64Utils;
import net.minecraft.server.v1_8_R3.MinecraftServer;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import redis.clients.jedis.Jedis;

import java.net.URI;
import java.util.Collection;
import java.util.UUID;

public class ProfileLoader {

    private final String uuid;
    private final String name;
    private final String skinOwner;
    private final SkinEnum uri;

    public ProfileLoader(String uuid, String name) {
        this(uuid, name, name);
    }

    public ProfileLoader(String uuid, String name, String skinOwner) {
        this.uuid = uuid == null ? null : uuid.replaceAll("-", ""); //We add these later
        String displayName = ChatColor.translateAlternateColorCodes('&', name);
        this.name = ChatColor.stripColor(displayName);
        this.skinOwner = getUUID(skinOwner);
        this.uri = null;
    }

    public ProfileLoader(String uuid, String name, SkinEnum urlSkin) {
        this.uuid = uuid == null ? null : uuid.replaceAll("-", "");
        String displayName = ChatColor.translateAlternateColorCodes('&', name);
        this.name = ChatColor.stripColor(displayName);
        this.uri = urlSkin;
        this.skinOwner = null;
    }

    public ProfileLoader(String uuid, String name, UUID skinOwner) {
        this.uuid = uuid == null ? null : uuid.replaceAll("-", ""); //We add these later
        String displayName = ChatColor.translateAlternateColorCodes('&', name);
        this.name = ChatColor.stripColor(displayName);
        this.skinOwner = skinOwner.toString().replaceAll("-", "");
        this.uri = null;
    }

    /**
     Need to be called async
     */
    public GameProfile loadProfile() {
        UUID id = uuid == null ? parseUUID(getUUID(name)) : parseUUID(uuid);
        GameProfile skinProfile = new GameProfile(UUID.randomUUID(), name);

        try(Jedis jedis = RedisManager.getJedisPool().getResource()) {
            String json = jedis == null ? null : jedis.get("cacheSkin:" + uuid);
            GameProfile profile;

            if (json == null)
            {
                //Requete
                profile = MinecraftServer.getServer().aD().fillProfileProperties(new GameProfile(id, null), true);



                if (jedis != null && profile.getName() != null)//Don't save if didn't got data from mojang
                {
                    JsonArray jsonArray = new JsonArray();
                    for (Property property : profile.getProperties().values())
                    {
                        jsonArray.add(new Gson().toJsonTree(property));
                    }
                    jedis.set("cacheSkin:" + uuid, jsonArray.toString());
                    jedis.expire("cacheSkin:" + uuid, 172800);//2 jours
                }

                if (uri != null) {
                    skinProfile.getProperties().put("textures", new Property(
                            "textures",
                            uri.getData(),
                            uri.getSignature()
                    ));
                    Collection<Property> prop = skinProfile.getProperties().get("textures");
                    skinProfile.getProperties().putAll("textures", prop);
                } else {
                    skinProfile.getProperties().putAll(profile.getProperties());
                }
            } else {
                JsonArray parse = new JsonParser().parse(json).getAsJsonArray();
                for (JsonElement object : parse)
                {
                    Property property = new Gson().fromJson(object.toString(), Property.class);
                    skinProfile.getProperties().put(property.getName(), property);

                }
            }
        }catch (Exception e)
        {
            e.printStackTrace();
        }

        return skinProfile;
    }

    @SuppressWarnings("deprecation")
    private String getUUID(String name) {
        return Bukkit.getOfflinePlayer(name).getUniqueId().toString().replaceAll("-", "");
    }

    private UUID parseUUID(String uuidStr) {
        // Split uuid in to 5 components
        String[] uuidComponents = new String[] { uuidStr.substring(0, 8),
                uuidStr.substring(8, 12), uuidStr.substring(12, 16),
                uuidStr.substring(16, 20),
                uuidStr.substring(20)
        };

        // Combine components with a dash
        StringBuilder builder = new StringBuilder();
        for (String component : uuidComponents) {
            builder.append(component).append('-');
        }

        // Correct uuid length, remove last dash
        builder.setLength(builder.length() - 1);
        return UUID.fromString(builder.toString());
    }

}
