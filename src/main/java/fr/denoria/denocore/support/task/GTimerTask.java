package fr.denoria.denocore.support.task;

public abstract class GTimerTask extends GTask {

    int delayLeft = 0;
    String name;
    boolean cancelled;


    public GTimerTask(int delay, String name) {
        this.delay = delay;
        this.delayLeft = delay;
        this.name = name;
    }

    public int getDelay() {
        return delay;
    }

    public String getName() {
        return name;
    }

    public int getDelayLeft() {
        return delayLeft;
    }

    public void removeDelay() {
        delayLeft --;
    }

    public void resetDelayLeft() {
        delayLeft = delay;
    }

    public boolean isCancelled() {
        return cancelled;
    }

}

