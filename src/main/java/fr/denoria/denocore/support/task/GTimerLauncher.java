package fr.denoria.denocore.support.task;

import java.util.List;

import fr.denoria.denocore.DenoCore;
import org.bukkit.scheduler.BukkitRunnable;

import com.google.common.collect.Lists;

public class GTimerLauncher extends BukkitRunnable {

    private List<GTimerTask> tasks = Lists.newArrayList();

    public GTimerLauncher() {
        runTaskTimer(DenoCore.getInstance(), 0, 1);
    }

    @Override
    public void run() {
        List<GTimerTask> timers = Lists.newArrayList();
        for(GTimerTask t : tasks) {
            if(t.getDelayLeft() == 0) {
                t.run();
                if(t.isCancelled()) {
                    timers.add(t);
                }
                t.resetDelayLeft();
            } else {
                t.removeDelay();
            }
        }
        for(GTimerTask t : timers) {
            tasks.remove(t);
        }
    }

    public GTimerTask registerNewTimer(GTimerTask t) {
        tasks.add(t);
        return t;
    }

}
