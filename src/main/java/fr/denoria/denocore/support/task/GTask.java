package fr.denoria.denocore.support.task;

public abstract class GTask {

    int delay;
    String name;

    public abstract void run();

    /**
     !! Schema d'utilisation !!

     public GTimerLauncher gTimerLauncher;
     public GDelayLauncher gDelayLauncher;

     public void onEnable() {

     gTimerLauncher = new GTimerLauncher();
     gDelayLauncher = new GDelayLauncher();

     gTimerLauncher.registerNewTimer(new StartGameTask());

     gTimerLauncher.registerNewTimer(new GTimerTask(1, "Démarrage de la partie") {
    @Override
    public void run() {
    if(Bukkit.getOnlinePlayers().size() >= 8) {
    !! Début de la partie !!
    }
    }
    });

     gDelayLauncher.registerNewDelay(new GDelayTask(20, "Message aléatoire") {
    @Override
    public void run() {
    // TODO Auto-generated method stub
    }
    });

     }

     !! EXEMPLE AVEC UNE CLASS DISTANTE

     public class StartGameTask extends GTimerTask {

     public StartGameTask() {
     super(1, "Démarrage de la partie");
     }

     @Override
     public void run() {
     if(Bukkit.getOnlinePlayers().size() >= 8) {
     //!! Début de la partie !!
     }
     }

     } **/

}
