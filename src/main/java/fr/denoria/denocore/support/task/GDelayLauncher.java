package fr.denoria.denocore.support.task;

import java.util.List;

import fr.denoria.denocore.DenoCore;
import org.bukkit.scheduler.BukkitRunnable;

import com.google.common.collect.Lists;

public class GDelayLauncher extends BukkitRunnable {

    private List<GDelayTask> tasks = Lists.newArrayList();

    public GDelayLauncher() {
        runTaskTimer(DenoCore.getInstance(), 0, 1);
    }

    @Override
    public void run() {
        List<GDelayTask> delays = Lists.newArrayList();
        for (GDelayTask t : tasks) {
            if (t.getDelay() == 0) {
                t.run();
                delays.add(t);
            } else {
                t.removeDelay();
            }
        }
        for (GDelayTask d : delays) {
            tasks.remove(d);
        }
    }

    public GDelayTask registerNewDelay(GDelayTask t) {
        tasks.add(t);
        return t;
    }

}
