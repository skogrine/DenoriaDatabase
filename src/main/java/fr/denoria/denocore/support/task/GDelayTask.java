package fr.denoria.denocore.support.task;

public abstract class GDelayTask extends GTask {

    public GDelayTask(int delay, String name) {
        this.delay = delay;
        this.name = name;
    }

    public int getDelay() {
        return delay;
    }

    public String getName() {
        return name;
    }

    public void removeDelay() {
        delay--;
    }
}
