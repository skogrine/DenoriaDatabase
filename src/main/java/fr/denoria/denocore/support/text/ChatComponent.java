package fr.denoria.denocore.support.text;

import org.bukkit.ChatColor;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

/**
 * Created by Stornitz
 * On 03/01/2015
 * At 11:50
 */
public class ChatComponent {

    private JSONObject chatObject;

    public ChatComponent(ChatComponentExtra extraObject) {
        chatObject = extraObject.toJSON();
    }

    public ChatComponent(String text, ChatColor... colors) {
        ChatComponentExtra extraObject = new ChatComponentExtra(text, colors);
        chatObject = extraObject.toJSON();
    }

    public ChatComponent add(String text, ChatColor... colors) {
        ChatComponentExtra extraObject = new ChatComponentExtra(text, colors);
        add(extraObject);
        return this;
    }

    @SuppressWarnings("unchecked")
    public ChatComponent add(ChatComponentExtra extraObject) {
        if (!chatObject.containsKey("extra")) {
            // noinspection unchecked
            chatObject.put("extra", new JSONArray());
        }
        JSONArray extra = (JSONArray) chatObject.get("extra");
        // noinspection unchecked
        extra.add(extraObject.toJSON());
        // noinspection unchecked
        chatObject.put("extra", extra);

        return this;
    }

    public String toString() {
        return chatObject.toJSONString();
    }
}
