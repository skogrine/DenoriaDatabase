package fr.denoria.denocore.support.text;

import org.bukkit.ChatColor;
import org.json.simple.JSONObject;

/**
 * Composant pouvant être afficher dans le chat
 * Created by Stornitz
 * On 03/01/2015
 * At 12:03
 */
public class ChatComponentExtra {

    private final JSONObject chatExtra;

    /**
     * Constructeur d'un élément de chat
     *
     * @param text   Texte de l'élément
     * @param colors Element de coloration/modification
     */
    @SuppressWarnings("unchecked")
    public ChatComponentExtra(String text, ChatColor... colors) {
        chatExtra = new JSONObject();
        // noinspection unchecked
        chatExtra.put("text", text);

        if (colors != null) {
            for (ChatColor color : colors) {
                String value = color.name().toLowerCase();

                if (!value.equals("reset")) {
                    if (color.isColor()) {
                        // noinspection unchecked
                        chatExtra.put("color", value);
                    } else if (color.isFormat()) {
                        if (value.equals("magic")) {
                            value = "obfuscated";
                        } else if (value.equals("underline")) {
                            value = "underlined";
                        }

                        // noinspection unchecked
                        chatExtra.put(value, true);
                    }
                }
            }
        }
    }

    /**
     * Définie un évent au clique sur le compsant
     *
     * @param action Action
     * @param value  Commande/commande suggeré/URL
     */
    @SuppressWarnings("unchecked")
    public void setClickEvent(ClickEventType action, String value) {
        JSONObject clickEvent = new JSONObject();
        // noinspection unchecked
        clickEvent.put("action", action.name().toLowerCase());
        // noinspection unchecked
        clickEvent.put("value", value);
        // noinspection unchecked
        chatExtra.put("clickEvent", clickEvent);
    }

    /**
     * Définie un évent au passage de la souris
     *
     * @param action Action
     * @param value  Text/item/achievment
     */
    @SuppressWarnings("unchecked")
    public void setHoverEvent(HoverEventType action, String value) {
        JSONObject hoverEvent = new JSONObject();
        // noinspection unchecked
        hoverEvent.put("action", action.name().toLowerCase());
        // noinspection unchecked
        hoverEvent.put("value", value);
        // noinspection unchecked
        chatExtra.put("hoverEvent", hoverEvent);
    }

    public JSONObject toJSON() {
        return chatExtra;
    }

    public enum ClickEventType {
        RUN_COMMAND,
        SUGGEST_COMMAND,
        OPEN_URL
    }


    public enum HoverEventType {
        SHOW_TEXT,
        SHOW_ITEM,
        SHOW_ACHIEVEMENT
    }
}
