/*
 * Copyright (c) 2019 - 2021 Denoria Network
 *
 *  ______   _______  _        _______  _______ _________ _______             _______  _        _______  _______  _______ _________ _        _______
 * (  __  \ (  ____ \( (    /|(  ___  )(  ____ )\__   __/(  ___  )       /\  (  ____ \| \    /\(  ___  )(  ____ \(  ____ )\__   __/( (    /|(  ____ \
 * | (  \  )| (    \/|  \  ( || (   ) || (    )|   ) (   | (   ) |      / /  | (    \/|  \  / /| (   ) || (    \/| (    )|   ) (   |  \  ( || (    \/
 * | |   ) || (__    |   \ | || |   | || (____)|   | |   | (___) |     / /   | (_____ |  (_/ / | |   | || |      | (____)|   | |   |   \ | || (__
 * | |   | ||  __)   | (\ \) || |   | ||     __)   | |   |  ___  |    / /    (_____  )|   _ (  | |   | || | ____ |     __)   | |   | (\ \) ||  __)
 * | |   ) || (      | | \   || |   | || (\ (      | |   | (   ) |   / /           ) ||  ( \ \ | |   | || | \_  )| (\ (      | |   | | \   || (
 * | (__/  )| (____/\| )  \  || (___) || ) \ \_____) (___| )   ( |  / /      /\____) ||  /  \ \| (___) || (___) || ) \ \_____) (___| )  \  || (____/\
 * (______/ (_______/|/    )_)(_______)|/   \__/\_______/|/     \|  \/       \_______)|_/    \/(_______)(_______)|/   \__/\_______/|/    )_)(_______/
 *
 */

package fr.denoria.denocore.support.npc;

public enum SkinEnum {

    HOST_DENO("ewogICJ0aW1lc3RhbXAiIDogMTYxOTA0MTc4NDM0MCwKICAicHJvZmlsZUlkIiA6ICIyNzc1MmQ2ZTUyYmM0MzVjYmNhOWQ5NzY1MjQ2YWNhNSIsCiAgInByb2ZpbGVOYW1lIiA6ICJkZW1pbWVkIiwKICAic2lnbmF0dXJlUmVxdWlyZWQiIDogdHJ1ZSwKICAidGV4dHVyZXMiIDogewogICAgIlNLSU4iIDogewogICAgICAidXJsIiA6ICJodHRwOi8vdGV4dHVyZXMubWluZWNyYWZ0Lm5ldC90ZXh0dXJlLzlhZGNjZGI2NTgwOTBkOTFiNzljZTkwNGY5NjViNjU0ZWJkYjE5NDA5Y2FjNWVkYmM0MjE0ZjcwYTgzN2JjMjYiCiAgICB9CiAgfQp9",
            "JdhPZj9qdwDcgE5o0vcR2B55ePrbMwa/pWwlsOqB5DoBTDguq25O87iPVn2anl4GB84bFxdtJO1FSdhNBaH9eL1ly362oTiclM+wJbcqDhVurlMWqea1zuwzw+/B+6rLUnpqpOTSsRtkEYhpGUtdPZxJX9tvuRLdSc6cTb+knk5SIp8U5Gc8whBjXRw4I3FZFkD6GfPpTRs6zQKRCv+NDUNJHz8vGVoiunUE2lyxhdQRGloDuvyF9DpTqL5qgBgmsQ//SgW4eN6F70yBZBUofCDSZOX+BE9rGi2JWCfCbun40xBjrZ3YDugNXm8oI4dj5eSgvcH4987+wbaMM4YCJ8WVn46hy+XhPLgTiJQvd1URIngVo/Vdp3JHjz2kUCzzNmDV6q5BDXLgCyavk+1gH8z1ijivxXILxxrvjqUaRrPDjEaVrqKiFTGtY155U4kXiROSNE4CHQ8oXNYXQWUjMoxYdHZeNr7LFRZiJzxt1cPxwHHTkp98M1RxrYYevXUenjLspRLCo+BL9YgeC5IvJtCUnzSfLzfxLl0V38yzg5p6LTN1f5s8GTQvkYG22K95TSGqLAFPaMCiH+8j9/LU9l1xlG60Aa5GFd94/hcTZifAjpEsdUN5d1jDLi1Of8lbEpq5rW6l/yRQFzQE1am+o0ed+OC+aivca/zWjfKDhuY="),

    UHC_DENO("",
            ""),

    BEDWARS_DENO("",
            ""),

    HUNTER_DENO("",
            ""),

    FB_DENO("",
            ""),

    COM_DENO("",
            "");

    private String data, signature;

    SkinEnum(String data, String signature) {
        this.data = data;
        this.signature = signature;
    }

    public String getData() {
        return data;
    }

    public String getSignature() {
        return signature;
    }
}
