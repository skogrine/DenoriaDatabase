package fr.denoria.denocore.support.npc;

import com.mojang.authlib.GameProfile;
import fr.denoria.denocore.DenoCore;
import fr.denoria.denocore.support.CallBack;
import fr.denoria.denocore.support.Reflection;
import fr.denoria.denocore.support.gameprofile.ProfileLoader;
import net.minecraft.server.v1_8_R3.PacketPlayOutPlayerInfo;
import net.minecraft.server.v1_8_R3.World;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.craftbukkit.v1_8_R3.CraftWorld;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.CreatureSpawnEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.player.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.*;

public class NPCManager implements Listener {

    private Map<CustomNPC, Hologram> entities = new HashMap<>();

    private CallBack<CustomNPC> scoreBoardRegister;

    public NPCManager()
    {

        Bukkit.getPluginManager().registerEvents(this, DenoCore.getInstance());
    }

    private void updateForAllNPC(CustomNPC npc)
    {
        List<Player> players = new ArrayList<>(Bukkit.getOnlinePlayers());
        /*
        for(Player p : players)
        {
            sendNPC(p, npc);
        }
        */
    }

    public void sendNPC(Player p, CustomNPC npc)
    {
        Reflection.sendPacket(p, new PacketPlayOutPlayerInfo(PacketPlayOutPlayerInfo.EnumPlayerInfoAction.ADD_PLAYER, npc));

        p.hidePlayer(npc.getBukkitEntity());
        p.showPlayer(npc.getBukkitEntity());

        DenoCore.getInstance().getServer().getScheduler().runTaskLater(DenoCore.getInstance(), () ->
        {
            Reflection.sendPacket(p, new PacketPlayOutPlayerInfo(PacketPlayOutPlayerInfo.EnumPlayerInfoAction.REMOVE_PLAYER, npc));
        }, 60L);
    }

    public void removeNPC(Player p, CustomNPC npc)
    {
        p.hidePlayer(npc.getBukkitEntity());
    }

    /**
     * Need to be called async
     * @param location location of the npc
     * @param skinUUID uuid of npc skin
     * @return CustomNPC
     */
    public CustomNPC createNPC(Location location, UUID skinUUID)
    {
        return createNPC(location, skinUUID, new String[] { "[NPC]"});
    }

    public CustomNPC createNPC(Location location, UUID skinUUID, String[] hologramLines)
    {
        return createNPC(location, skinUUID, hologramLines, true);
    }

    public CustomNPC createNPC(Location location, SkinEnum skinURL, String[] hologramLines, boolean showByDefault) {
        Location loc = new Location(DenoCore.getInstance().getServer().getWorld("world"), location.getX(), location.getY(), location.getZ(), location.getYaw(), location.getPitch());
        World world = ((CraftWorld) loc.getWorld()).getHandle();
        GameProfile gameProfile = new ProfileLoader(UUID.randomUUID().toString(), " ", skinURL).loadProfile();

        CustomNPC npc = new CustomNPC(world, gameProfile);
        npc.setLocation(loc);
        Hologram hologram = null;
        if (hologramLines != null)
        {
            hologram = new Hologram(hologramLines);
            hologram.generateLines(loc.clone().add(0.0D, 2.2D, 0.0D));
        }

        world.addEntity(npc, CreatureSpawnEvent.SpawnReason.CUSTOM);

        npc.setHologram(hologram);
        entities.put(npc, hologram);

        if (scoreBoardRegister != null)
            scoreBoardRegister.done(npc, null);

        if (showByDefault)
        {
            for(Player player : Bukkit.getOnlinePlayers())
            {
                sendNPC(player, npc);

                if (hologram != null)
                    hologram.addReceiver(player);
            }
        }

        //Bukkit.getScheduler().runTaskLater(api.getPlugin(), () -> updateForAllNPC(npc), 2L);
        return npc;

    }

    public CustomNPC createNPC(Location location, UUID skinUUID, String[] hologramLines, boolean showByDefault)
    {
        Location loc = new Location(DenoCore.getInstance().getServer().getWorld("world"), location.getX(), location.getY(), location.getZ(), location.getYaw(), location.getPitch());
        World world = ((CraftWorld) loc.getWorld()).getHandle();
        GameProfile gameProfile = new ProfileLoader(skinUUID.toString(), " ", skinUUID).loadProfile();


        CustomNPC npc = new CustomNPC(world, gameProfile);

        npc.setLocation(loc);

        Hologram hologram = null;

        if (hologramLines != null)
        {
            hologram = new Hologram(hologramLines);
            hologram.generateLines(loc.clone().add(0.0D, 2.2D, 0.0D));
        }

        world.addEntity(npc, CreatureSpawnEvent.SpawnReason.CUSTOM);

        npc.setHologram(hologram);
        entities.put(npc, hologram);

        if (scoreBoardRegister != null)
            scoreBoardRegister.done(npc, null);

        if (showByDefault)
        {
            for(Player player : Bukkit.getOnlinePlayers())
            {
                sendNPC(player, npc);

                if (hologram != null)
                    hologram.addReceiver(player);
            }
        }

        //Bukkit.getScheduler().runTaskLater(api.getPlugin(), () -> updateForAllNPC(npc), 2L);
        return npc;
    }

    public void removeAllNPC() {
        for(CustomNPC customNPC : entities.keySet()) {
            removeNPC(customNPC);
        }
    }

    public void removeNPC(String name)
    {
        removeNPC(getNPCEntity(name));
    }

    public void removeNPC(CustomNPC npc)
    {
        if (npc != null)
        {
            if (npc.getHologram() != null)
                npc.getHologram().destroy();

            for (Player p : Bukkit.getOnlinePlayers())
                removeNPC(p, npc);

            npc.getWorld().removeEntity(npc);
        }
    }

    public CustomNPC getNPCEntity(String name)
    {
        for (CustomNPC entity : entities.keySet())
            if (entity.getName().equals(name))
                return entity;

        return null;
    }

    public void setScoreBoardRegister(CallBack<CustomNPC> scoreBoardRegister) {
        this.scoreBoardRegister = scoreBoardRegister;
    }

    @EventHandler
    public void onPlayerHitNPC(EntityDamageByEntityEvent event)
    {
        if (Reflection.getHandle(event.getEntity()) instanceof CustomNPC && event.getDamager() instanceof Player)
        {
            CustomNPC npc = (CustomNPC) Reflection.getHandle(event.getEntity());
            npc.onInteract(false, (Player) event.getDamager());
        }
    }

    @EventHandler
    public void onPlayerInteractNPC(PlayerInteractEntityEvent event)
    {
        if (Reflection.getHandle(event.getRightClicked()) instanceof CustomNPC)
        {
            CustomNPC npc = (CustomNPC) Reflection.getHandle(event.getRightClicked());
            npc.onInteract(true, event.getPlayer());
        }
    }

    @EventHandler
    public void onPlayerMove(PlayerMoveEvent event)
    {
        this.entities.keySet().forEach(customNPC ->
        {
            if (event.getFrom().distanceSquared(customNPC.getBukkitEntity().getLocation()) > 2500
                    && event.getTo().distanceSquared(customNPC.getBukkitEntity().getLocation()) < 2500)
                sendNPC(event.getPlayer(), customNPC);
        });
    }

    @EventHandler
    public void onPlayerTeleport(PlayerTeleportEvent event)
    {
        this.entities.keySet().forEach(customNPC ->
        {
            if (event.getFrom().distanceSquared(customNPC.getBukkitEntity().getLocation()) > 2500
                    && event.getTo().distanceSquared(customNPC.getBukkitEntity().getLocation()) < 2500)
                sendNPC(event.getPlayer(), customNPC);
        });
    }

    @EventHandler
    public void onPlayerJoin(PlayerJoinEvent event)
    {
        Bukkit.getScheduler().runTaskLater(DenoCore.getInstance(), () -> entities.keySet().forEach(customNPC ->
        {
            sendNPC(event.getPlayer(), customNPC);
            entities.get(customNPC).addReceiver(event.getPlayer());
        }), 2L);
    }

    @EventHandler
    public void onPlayerLeave(PlayerKickEvent event)
    {
        this.entities.entrySet().forEach(customNPC ->
                customNPC.getValue().removeReceiver(event.getPlayer()));
    }

    @EventHandler
    public void onPlayerLeave(PlayerQuitEvent event)
    {
        this.entities.entrySet().forEach(customNPC -> customNPC.getValue().removeReceiver(event.getPlayer()));
    }


    public Map<CustomNPC, Hologram> getEntities() {
        return entities;
    }
}
