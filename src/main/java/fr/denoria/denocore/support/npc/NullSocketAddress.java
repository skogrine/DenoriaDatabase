package fr.denoria.denocore.support.npc;

import java.net.SocketAddress;

public class NullSocketAddress extends SocketAddress {

    private static final long serialVersionUID = 1L;
}
