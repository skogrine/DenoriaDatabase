package fr.denoria.denocore.support.npc;

import org.bukkit.entity.Player;

public interface NPCInteractCallback
{
    /**
     * Function called after action
     *
     * @param rightClick true if right clicked or false left clicked
     * @param player player who performed the action
     */
    void done(boolean rightClick, Player player);
}
