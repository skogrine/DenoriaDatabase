package fr.denoria.denocore.support.date;

import java.text.SimpleDateFormat;

public class DateFormatType {

    public static final SimpleDateFormat NORMAL = new SimpleDateFormat("dd-MM-yyyy hh:mm");
    public static final SimpleDateFormat SHORT = new SimpleDateFormat("dd-MM");

}
