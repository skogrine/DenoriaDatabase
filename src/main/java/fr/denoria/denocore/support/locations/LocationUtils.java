package fr.denoria.denocore.support.locations;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.util.Vector;

public class LocationUtils {

    /**
     * Vérifier si il est entre deux location. V1
     *
     * @param from
     * @param to
     * @param where
     * @return
     */
    public static boolean isBetween(Location from, Location to, Location where) {
        boolean x1 = from.getX() < where.getX();
        boolean y1 = from.getY() < where.getY();
        boolean z1 = from.getZ() < where.getZ();

        boolean x2 = to.getX() > where.getX();
        boolean y2 = to.getY() > where.getY();
        boolean z2 = to.getZ() > where.getZ();

        return x1 && y1 && z1 && x2 && y2 && z2;
    }

    public static Location str2loc(String loc)
    {
        if (loc == null)
            return null;

        String[] location = loc.split(", ");

        if(location.length == 6)
            return new Location(Bukkit.getServer().getWorld(location[0]), Double.parseDouble(location[1]), Double.parseDouble(location[2]), Double.parseDouble(location[3]), Float.parseFloat(location[4]), Float.parseFloat(location[5]));
        else
            return new Location(Bukkit.getServer().getWorld(location[0]), Double.parseDouble(location[1]), Double.parseDouble(location[2]), Double.parseDouble(location[3]));
    }

    public static String loc2str(Location loc)
    {
        return loc.getWorld().getName() + ", " + loc.getX() + ", " + loc.getY() + ", " + loc.getZ() + ", " + loc.getYaw() + ", " + loc.getPitch();
    }

    /**
     * Vérifier si il est entre deux location. V2
     *
     * @param from
     * @param to
     * @param where
     * @return
     */
    public static boolean isBetweenTwoLocations(Location from, Location to, Location where) {
        boolean x1 = from.getX() < where.getX();
        boolean y1 = from.getY() < where.getY();
        boolean z1 = from.getZ() < where.getZ();

        boolean x2 = to.getX() > where.getX();
        boolean y2 = to.getY() > where.getY();
        boolean z2 = to.getZ() > where.getZ();

        return x1 && y1 && z1 && x2 && y2 && z2;
    }

    /**
     * Créer une Location du Joueur côté Gauche.
     *
     * @param player
     * @return
     */
    public static Location getLeftLocation(Player player) {
        Location loc = player.getEyeLocation().subtract(0.0D, 0.0D, 0.0D);
        loc.setPitch(0.0f);
        loc.setYaw(player.getEyeLocation().getYaw() - 120);
        Vector vector = loc.getDirection().normalize().multiply(-1D);
        vector.setY(0);
        loc.add(vector);
        Location location = new Location(loc.getWorld(), loc.getX(), loc.getY(), loc.getZ());
        location.setYaw(player.getLocation().getYaw());
        return location;
    }

    /**
     * Créer une Location du Joueur côté Droit.
     *
     * @param player
     * @return
     */
    public static Location getRightLocation(Player player) {
        Location loc = player.getEyeLocation().subtract(0.0D, 0.0D, 0.0D);
        loc.setPitch(0.0f);
        loc.setYaw(player.getEyeLocation().getYaw() - 60);
        Vector vector = loc.getDirection().normalize().multiply(-1D);
        vector.setY(0);
        loc.add(vector);
        Location location = new Location(loc.getWorld(), loc.getX(), loc.getY(), loc.getZ());
        location.setYaw(player.getLocation().getYaw());
        return location;
    }

    /**
     * Créer une Location.
     *
     * @param world
     * @param x
     * @param y
     * @param z
     * @param yaw
     * @param pitch
     * @return
     */
    public static Location createLocation(World world, double x, double y, double z, float yaw, float pitch) {
        Location location = new Location(world, x, y, z);
        location.setYaw(yaw);
        location.setPitch(pitch);
        return location;
    }

    /**
     * Converti une location en String
     *
     * @param location
     * @return un String representant la locatio,
     */
    public static String locationAsString(Location location) {
        return location.getWorld().getName() + ";" + location.getX() + ";" + location.getY() + ";" + location.getZ()
                + ";" + location.getPitch() + ";" + location.getYaw();
    }

    /**
     * Converti un String en Location
     *
     * @param string
     * @return la Location
     */
    public static Location stringAsLocation(String string) {
        String[] parts = string.split(";");

        if (parts.length >= 4) { // minimum world, x, y, z
            World world = Bukkit.getWorld(parts[0]);

            if (world != null) {
                double x = Double.parseDouble(parts[1]);
                double y = Double.parseDouble(parts[2]);
                double z = Double.parseDouble(parts[3]);

                Location location = new Location(world, x, y, z);
                if (parts.length == 6) {
                    float pitch = Float.parseFloat(parts[4]);
                    float yaw = Float.parseFloat(parts[5]);
                    location.setYaw(yaw);
                    location.setPitch(pitch);
                }

                return location;
            }
        }

        return null;
    }

    /**
     * Téléporation plus optimisé.
     *
     * @param x
     * @param z
     * @param p
     * @param world
     */
    public static void teleportTo(int x, int z, Player p, String world) {
        int y = 250;
        Location baseT = new Location(Bukkit.getWorld(world), x, y, z);
        Block b = baseT.getBlock();

        do {
            y--;
            baseT.setY(y);
            b = baseT.getBlock();
            if (!(b.getType().equals(Material.AIR) || b.getType().equals(Material.LOG)
                    || b.getType().equals(Material.LEAVES) || b.getType().equals(Material.LOG_2)
                    || b.getType().equals(Material.LEAVES_2))) {
                Location base = new Location(Bukkit.getWorld(world), x, y + 3, z);

                p.teleport(base);

            }
        } while (b.getType().equals(Material.AIR) || b.getType().equals(Material.LOG)
                || b.getType().equals(Material.LEAVES) || b.getType().equals(Material.LOG_2)
                || b.getType().equals(Material.LEAVES_2));
    }

    /**
     * Récupérer la position du Spawn !
     * @return spawn Location
     */
    public static Location getSpawnLocation() {
        return new Location(Bukkit.getWorld("world"), 0.439, 88, 0.629, -0.6F, 5.1F);
    }
}
