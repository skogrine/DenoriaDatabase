package fr.denoria.denocore.support;

public enum GameStatus {

    LOBBY(true), GAME(false), FINISH(false);

    private final boolean canJoin;
    private static GameStatus currentStatus;

    GameStatus(boolean canJoin) {
        this.canJoin = canJoin;
    }

    public boolean canJoin() {
        return this.canJoin;
    }

    public static void setStatus(GameStatus status) {
        currentStatus = status;
    }

    public static boolean isStatus(GameStatus status) {
        return currentStatus == status;
    }

    public static GameStatus getStatus() {
        return currentStatus;
    }

}
