package fr.denoria.denocore.support;

import fr.denoria.denocore.denoriaplayer.DenoriaPlayer;
import fr.denoria.denocore.denoriaplayer.rank.Rank;

public class FunctionRaccourcie {

    public static boolean isStaff(DenoriaPlayer player) {
        return player.getRank().getPower() >= Rank.STAFF.getPower();
    }

    public static boolean isDirection(DenoriaPlayer player) {
        return player.getRank().getPower() >= Rank.RESPONSABLE.getPower();
    }

    public static boolean isAdmin(DenoriaPlayer player) {
        return player.getRank().getPower().equals(Rank.ADMINISTRATOR.getPower());
    }

    public static boolean isVIP(DenoriaPlayer player) {
        return player.getRankPower() >= Rank.VIP.getPower() && player.getRankPower() < Rank.STAFF.getPower();
    }



}
