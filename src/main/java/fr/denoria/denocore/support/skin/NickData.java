package fr.denoria.denocore.support.skin;

import org.bukkit.entity.Player;

import java.util.UUID;

public class NickData {

    private UUID uuid;
    private Player player;
    private SkinData skinData;
    private boolean hidden;

    public NickData(UUID uuid, SkinData skinData, boolean hidden) {
        this.uuid = uuid;
        this.hidden = hidden;
        this.skinData = skinData;
    }

}
