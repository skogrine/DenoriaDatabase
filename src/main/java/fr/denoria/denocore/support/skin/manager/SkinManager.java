package fr.denoria.denocore.support.skin.manager;

import com.comphenix.protocol.events.PacketContainer;
import com.comphenix.protocol.reflect.FieldAccessException;
import com.comphenix.protocol.wrappers.*;
import com.comphenix.protocol.wrappers.EnumWrappers.Difficulty;
import com.comphenix.protocol.wrappers.EnumWrappers.NativeGameMode;
import com.comphenix.protocol.wrappers.EnumWrappers.PlayerInfoAction;
import com.mojang.authlib.GameProfile;
import com.mojang.authlib.properties.Property;
import fr.denoria.denocore.DenoCore;
import fr.denoria.denocore.listeners.PlayerChangeSkinEvent;
import net.minecraft.server.v1_8_R3.PacketPlayOutPlayerInfo;
import org.apache.commons.io.IOUtils;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;
import org.bukkit.entity.Player;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.simple.JSONArray;
import org.json.simple.JSONValue;
import org.json.simple.parser.ParseException;

import javax.net.ssl.HttpsURLConnection;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.lang.reflect.Field;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.Collections;

import static com.comphenix.protocol.PacketType.Play.Server.*;


public class SkinManager extends SharedApplier {

    protected final DenoCore plugin;
    private Player player;
    private final SkinModel targetSkin;

    public SkinManager(DenoCore plugin, Player player, SkinModel targetSkin) {
        super(plugin, targetSkin, true);
        this.plugin = plugin;
        this.targetSkin = targetSkin;
        this.player = player;
    }

    private void applySkin(Player player, SkinModel targetSkin){
        Bukkit.getPluginManager().callEvent(new PlayerChangeSkinEvent(player, targetSkin));
        WrappedGameProfile gameProfile = WrappedGameProfile.fromPlayer(player);
        applyProperties(gameProfile, targetSkin);
    }

    public void applyProperties(WrappedGameProfile profile, SkinModel targetSkin) {
        //remove existing skins
        profile.getProperties().clear();
        if (targetSkin != null) {
            profile.getProperties().put(SkinProperty.SKIN_KEY, convertToProperty(targetSkin));
        }
    }

    private WrappedSignedProperty convertToProperty(SkinModel skinData) {
        String encodedValue = skinData.getEncodedValue();
        String signature = skinData.getSignature();
        return WrappedSignedProperty.fromValues(SkinProperty.SKIN_KEY, encodedValue, signature);
    }



    public String getUUIDFromName(String name) {
        String url = "https://api.mojang.com/users/profiles/minecraft/" + name;
        try {
            String UUIDJson = IOUtils.toString(new URL(url));
            if(UUIDJson.isEmpty()) return null;
            JSONObject UUIDObject = (JSONObject) JSONValue.parseWithException(UUIDJson);
            return UUIDObject.get("id").toString();
        } catch (IOException | ParseException | JSONException e){
            e.printStackTrace();
        }

        return null;
    }

    private static String readAll(Reader rd) throws IOException {
        StringBuilder sb = new StringBuilder();
        int cp;
        while ((cp = rd.read()) != -1) {
            sb.append((char) cp);
        }
        return sb.toString();
    }

    public SkinModel setSkin(String name) throws JSONException {

        JSONObject jsonObject = getJsonResponse(String.format("https://sessionserver.mojang.com/session/minecraft/profile/%s?unsigned=false", Bukkit.getOfflinePlayer(name).getUniqueId().toString().replace("-", "")));
        assert jsonObject != null;

        return SkinModel.createSkinFromEncoded(jsonObject.getJSONArray("properties").getJSONObject(0).get("value").toString(),
                jsonObject.getJSONArray("properties").getJSONObject(0).get("signature").toString());
    }

    private JSONObject getJsonResponse(String url) {
        try {
            HttpsURLConnection connection = (HttpsURLConnection) new URL(url).openConnection();
            BufferedReader rd = new BufferedReader(new InputStreamReader(connection.getInputStream(), StandardCharsets.UTF_8));
            String jsonText = readAll(rd);
            JSONObject jsonObject = new JSONObject(jsonText);
            return jsonObject;
        } catch (IOException | JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    public void setSkinToPlayer(GameProfile profile, String uuid) {
        try {
            HttpsURLConnection connection = (HttpsURLConnection) new URL(String.format("https://sessionserver.mojang.com/session/minecraft/profile/%s?unsigned=false", uuid)).openConnection();
            if (connection.getResponseCode() == HttpsURLConnection.HTTP_OK) {
                BufferedReader rd = new BufferedReader(new InputStreamReader(connection.getInputStream(), StandardCharsets.UTF_8));
                String jsonText = readAll(rd);
                /* System.out.println(jsonText);
                 *  System.out.println("-------------------------------------------------------");
                 */
                org.json.JSONObject jsonObject = new org.json.JSONObject(jsonText);
                /*  System.out.println("VALUE: " + jsonObject.getJSONArray("properties").getJSONObject(0).get("value").toString());
                *   System.out.println("-------------------------------------------------------");
                *   System.out.println("SIGN: " + jsonObject.getJSONArray("properties").getJSONObject(0).get("signature").toString());
                */

                profile.getProperties().put("textures", new Property("textures", jsonObject.getJSONArray("properties").getJSONObject(0).get("value").toString(),
                        jsonObject.getJSONArray("properties").getJSONObject(0).get("signature").toString()));

                updatePacketSelf(WrappedGameProfile.fromPlayer(player));
                sendUpdateOthers();
            } else {
                System.out.println("Connection could not be opened (Response code " + connection.getResponseCode() + ", " + connection.getResponseMessage() + ")");
            }
        } catch (IOException | JSONException e) {
            e.printStackTrace();
        }
    }

    private void sendUpdateOthers() throws FieldAccessException {
        //triggers an update for others player to see the new skin
        Bukkit.getOnlinePlayers().stream()
                .filter(onlinePlayer -> !onlinePlayer.equals(player))
                .filter(onlinePlayer -> onlinePlayer.canSee(player))
                .forEach(this::hideAndShow);

    }

    private void hideAndShow(Player other) {
        other.hidePlayer(player);
        other.showPlayer(player);
    }


    public String getNameByUUID(String playerUUID) throws JSONException {
        String url = "https://api.mojang.com/user/profiles/"+playerUUID.replace("-", "")+"/names";
        try {
            String nameJson = IOUtils.toString(new URL(url));
            JSONArray nameValue = (JSONArray) JSONValue.parseWithException(nameJson);
            String playerSlot = nameValue.get(nameValue.size()-1).toString();
            JSONObject nameObject = (JSONObject) JSONValue.parseWithException(playerSlot);
            return nameObject.get("name").toString();
        } catch (IOException | ParseException e) {
            e.printStackTrace();
        }
        return "error";

    }

    public void setProfile(Player p, String Name) {
        PacketPlayOutPlayerInfo player = new PacketPlayOutPlayerInfo(PacketPlayOutPlayerInfo.EnumPlayerInfoAction.UPDATE_DISPLAY_NAME, ((CraftPlayer) p).getHandle());
        for (Player all : Bukkit.getOnlinePlayers()) {
            ((CraftPlayer) all).getHandle().playerConnection.sendPacket(player);
        }

        try {
            //Getting game profile
            GameProfile profile = ((CraftPlayer) p).getProfile();

            //Getting name field
            Field nameField = profile.getClass().getDeclaredField("name");
            nameField.setAccessible(true);

            //Modifing class
            Field modifierField = nameField.getClass().getDeclaredField("modifiers");
            int modifiers = nameField.getModifiers() & 0x10;
            modifierField.setAccessible(true);
            modifierField.setInt(nameField, modifiers);
            nameField.set(profile, Name);

        } catch (NoSuchFieldException | SecurityException | IllegalArgumentException | IllegalAccessException ex) {
            ex.printStackTrace();
        }

        //p.setDisplayName(Name);
    }


    private void updatePacketSelf(WrappedGameProfile gameProfile){
        PacketContainer removeInfo;
        PacketContainer addInfo;
        PacketContainer respawn;
        PacketContainer teleport;

        try {
            NativeGameMode gamemode = EnumWrappers.NativeGameMode.fromBukkit(player.getGameMode());
            WrappedChatComponent displayName = WrappedChatComponent.fromText(player.getPlayerListName());
            PlayerInfoData playerInfoData = new PlayerInfoData(gameProfile, 0, gamemode, displayName);

            removeInfo = new PacketContainer(PLAYER_INFO);
            removeInfo.getPlayerInfoAction().write(0, PlayerInfoAction.REMOVE_PLAYER);
            removeInfo.getPlayerInfoDataLists().write(0, Collections.singletonList(playerInfoData));

            addInfo = removeInfo.deepClone();
            addInfo.getPlayerInfoAction().write(0, PlayerInfoAction.ADD_PLAYER);

            createRespawnPacket(gamemode);
            createTeleportPacket(player.getLocation().clone());
        } catch (ReflectiveOperationException e){
            plugin.getLog().error("Error occured preparing packets. Cancelling self update", e);
        }
    }


    private PacketContainer createTeleportPacket(Location location) {
        PacketContainer teleport = new PacketContainer(POSITION);
        teleport.getModifier().writeDefaults();

        teleport.getDoubles().write(0, location.getX())
                .write(1, location.getY())
                .write(2, location.getZ());

        teleport.getFloat().write(0, location.getYaw())
                .write(1, location.getPitch());

        //send an invalid teleport id in order to let Bukkit ignore the incoming confirm packet
        teleport.getIntegers().writeSafely(0, -1337);
        return teleport;
    }

    private PacketContainer createRespawnPacket(NativeGameMode gamemode) throws ReflectiveOperationException {
        PacketContainer respawn = new PacketContainer(RESPAWN);

        World world = player.getWorld();
        Difficulty difficulty = EnumWrappers.getDifficultyConverter().getSpecific(world.getDifficulty());

        //<= 1.13.1
        int dimensionId = world.getEnvironment().getId();
        respawn.getIntegers().writeSafely(0, dimensionId);

        respawn.getWorldTypeModifier().write(0, world.getWorldType());
        respawn.getGameModes().write(0, gamemode);

        return respawn;
    }


    @Override
    public void run() {
        applySkin();
    }

    @Override
    protected void runAsync(Runnable runnable) {
        Bukkit.getScheduler().runTaskAsynchronously(plugin, runnable);
    }

    @Override
    protected boolean isConnected() {
        return player != null && player.isOnline();
    }

    @Override
    protected void sendMessage(String key) {
        player.sendMessage(key);
    }

    @Override
    protected void applyInstantUpdate() {
        applySkin(player, targetSkin);
        updatePacketSelf(WrappedGameProfile.fromPlayer(player));
        sendUpdateOthers();
    }
}
