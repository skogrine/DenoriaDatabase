package fr.denoria.denocore.support.skin.manager;

public enum TextureType {

    SKIN,

    CAPE,

    ELYTRA
}
