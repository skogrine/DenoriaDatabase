package fr.denoria.denocore.support.skin.manager;

import fr.denoria.denocore.DenoCore;

public abstract class SharedApplier implements Runnable{


    protected final DenoCore core;
    protected final SkinModel targetSkin;
    protected final boolean keepSkin;

    public SharedApplier(DenoCore core, SkinModel targetSkin, boolean keepSkin) {
        this.core = core;
        this.targetSkin = targetSkin;
        this.keepSkin = keepSkin;
    }

    protected abstract void runAsync(Runnable runnable);
    protected abstract boolean isConnected();

    protected abstract void applyInstantUpdate();
    protected abstract void sendMessage(String key);

    protected void applySkin() {
        applyInstantUpdate();
    }



    protected void save(UserPreference preference) {
        runAsync(() -> {
            //Save the target uuid from the requesting player source
            preference.setTargetSkin(targetSkin);
            preference.setKeepSkin(keepSkin);
        });
    }
}
