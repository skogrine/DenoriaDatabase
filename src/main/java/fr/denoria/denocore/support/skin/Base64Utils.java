package fr.denoria.denocore.support.skin;

import org.bukkit.util.io.BukkitObjectInputStream;
import org.bukkit.util.io.BukkitObjectOutputStream;
import org.yaml.snakeyaml.external.biz.base64Coder.Base64Coder;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

public class Base64Utils {

    public static <T> String toBase64(T value) throws IllegalStateException {
        try {
            ByteArrayOutputStream output = new ByteArrayOutputStream();
            BukkitObjectOutputStream dataOutput = new BukkitObjectOutputStream(output);

            dataOutput.writeObject(value);

            dataOutput.close();
            return Base64Coder.encodeLines(output.toByteArray());

        } catch (Exception e){
            throw new IllegalStateException("Impossible de transformer la Value en base64", e);
        }
    }

    public static Object fromBase64(String data) throws IOException {
        try {
            ByteArrayInputStream inputStream = new ByteArrayInputStream(Base64Coder.decodeLines(data));
            BukkitObjectInputStream dataInput = new BukkitObjectInputStream(inputStream);

            Object obj = dataInput.readObject();

            dataInput.close();
            return obj;
        } catch (ClassNotFoundException e) {
            throw new IOException("Unable to decode class type.", e);
        }
    }
}
