package fr.denoria.denocore.support.skin;

import java.io.Serializable;

public class SkinData implements Serializable {

    private String name, value, signature;

    public SkinData(String name, String value, String signature) {
        this.name = name;
        this.value = value;
        this.signature = signature;
    }

    @Override
    public String toString() {
        return "SkinData{name=" + name + ",value=" + value + ",signature=" + signature + "}";
    }

    public String getName() {
        return name;
    }

    public String getValue() {
        return value;
    }

    public String getSignature() {
        return signature;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public void setSignature(String signature) {
        this.signature = signature;
    }
}
