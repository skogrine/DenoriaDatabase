package fr.denoria.denocore.support.packets;

import java.lang.reflect.Field;

import fr.denoria.denocore.support.packets.utils.NMS;
import fr.denoria.denocore.support.packets.utils.ReflectionUtil;
import org.bukkit.entity.Player;

import io.netty.channel.Channel;
import io.netty.channel.ChannelDuplexHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelPromise;

public class ChannelHandler extends ChannelDuplexHandler {

    private Player player;

    public ChannelHandler(Player player) {
        this.player = player;

    }

    private static Field getChannelField() {
        Field channelField = null;
        try {
            channelField = ReflectionUtil.getField(NMS.getNMSClass("NetworkManager"), "channel");
        } catch (Exception e) {
            System.out.print("Channel class not found");
        }
        if (channelField != null) {
            channelField.setAccessible(true);
        }
        return channelField;
    }

    public static void addChannel(final Player player) {
        try {
            Object handle = NMS.getNMSPlayer(player);
            Object connection = ReflectionUtil.getFieldObject(handle.getClass(), "playerConnection", handle);
            Field network = ReflectionUtil.getField(NMS.getNMSClass("PlayerConnection"), "networkManager");
            final Channel channel = (Channel) getChannelField().get(network.get(connection));
            new Thread(() -> {
                try {
                    channel.pipeline().addBefore("packet_handler", "packet_listener_player",
                            new ChannelHandler(player));
                } catch (Exception localException) {
                    localException.printStackTrace();
                }
            }, " channel adder (" + player.getName() + ")").start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void removeChannel(final Player player) {
        try {
            final Object handle = NMS.getNMSPlayer(player);
            Object connection = ReflectionUtil.getFieldObject(handle.getClass(), "playerConnection", handle);
            Field network = ReflectionUtil.getField(NMS.getNMSClass("PlayerConnection"), "networkManager");
            final Channel channel = (Channel) getChannelField().get(network.get(connection));
            new Thread(new Runnable() {

                @Override
                public void run() {
                    try {
                        channel.pipeline().remove("packet_listener_player");
                    } catch (Exception e) {
                    }
                }
            }, " channel remover (" + player.getName() + ")").start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void write(ChannelHandlerContext ctx, Object msg, ChannelPromise promise) throws Exception {
        // if(packet.isAssignableFrom(msg.getClass())){

        super.write(ctx, msg, promise);
    }

    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
        // Cancellable cancellable = new Cancellable();

        msg = DenoriaPackets.getInstance().onReceive(new Packet(msg, player));

        // if (cancellable.isCancelled()) return;
        super.channelRead(ctx, msg);
    }

}
