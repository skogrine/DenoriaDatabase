package fr.denoria.denocore.support.packets;

import java.lang.reflect.Field;
import java.util.ArrayList;

import fr.denoria.denocore.support.packets.utils.NMS;
import fr.denoria.denocore.support.packets.utils.ReflectionUtil;
import io.netty.channel.Channel;


@SuppressWarnings("serial")
class ListenerList<E> extends ArrayList<E> {

    private static Field channelField = getChannelField();

    private static Field getChannelField() {
        Field channelField = null;
        try {
            channelField = ReflectionUtil.getField(NMS.getNMSClass("NetworkManager"), "channel");
        } catch (Exception e) {
            System.out.print("Channel class not found");
        }
        if (channelField != null) {
            channelField.setAccessible(true);
        }
        return channelField;
    }

    @Override
    public boolean add(E paramE) {
        try {
            final E a = paramE;
            new Thread(new Runnable() {

                @Override
                public void run() {
                    try {
                        Channel channel = null;
                        while (channel == null) {
                            channel = (Channel) channelField.get(a);
                        }
                        if (channel.pipeline().get("packet_listener_server") == null) {
                            channel.pipeline().addBefore("packet_handler", "packet_listener_server",
                                    new ChannelHandler(null));
                        }
                    } catch (Exception e) {
                    }
                }
            }, "channel adder").start();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return super.add(paramE);
    }

    @Override
    public boolean remove(Object arg0) {
        try {
            final Object a = arg0;
            new Thread(new Runnable() {

                @Override
                public void run() {
                    try {
                        Channel channel = null;
                        while (channel == null) {
                            channel = (Channel) channelField.get(a);
                        }
                        channel.pipeline().remove("packet_listener_server");
                    } catch (Exception e) {
                    }
                }
            }, " channel remover").start();
        } catch (Exception e) {
        }
        return super.remove(arg0);
    }

}
