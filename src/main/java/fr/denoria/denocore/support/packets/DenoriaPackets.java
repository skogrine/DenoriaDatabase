package fr.denoria.denocore.support.packets;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import fr.denoria.denocore.support.packets.packetlist.PacketType;
import fr.denoria.denocore.support.packets.utils.NMS;
import fr.denoria.denocore.support.packets.utils.ReflectionUtil;
import org.bukkit.Bukkit;
import org.bukkit.Server;

public class DenoriaPackets {

    public static List<DenoriaPacketHandler> handles = new ArrayList<>();

    private static DenoriaPackets instance = new DenoriaPackets();

    public DenoriaPackets() {
    }

    public static DenoriaPackets getInstance() {
        return instance;
    }

    @SuppressWarnings("unchecked")
    public void AddChanel() {
        try {
            Server server = Bukkit.getServer();
            Object dedidicatedserver = ReflectionUtil.getMethod(server.getClass(), "getServer").invoke(server);
            Object serverconnection = ReflectionUtil
                    .getMethod(NMS.getNMSClass("MinecraftServer"), "getServerConnection").invoke(dedidicatedserver);
            List<?> currentlist = (List<?>) ReflectionUtil.getFieldObject(serverconnection.getClass(), "h",
                    serverconnection);
            Object list = ReflectionUtil.getFieldObject(currentlist.getClass().getSuperclass(), "list", currentlist);
            if (list.getClass().equals(ListenerList.class))
                return;
            @SuppressWarnings("rawtypes")
            List newlist = Collections.synchronizedList(new ListenerList());
            for (Object o : currentlist) {
                newlist.add(o);
            }
            ReflectionUtil.setValue(serverconnection, "h", newlist);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void addPacketListener(DenoriaPacketHandler handler) {
        handles.add(handler);
    }

    public Object onReceive(Packet packet) {
        for (DenoriaPacketHandler h : handles) {
            if (h.getPacketType().equals(PacketType.ALL)) {
                h.Receive(packet);
            } else if (h.getPacketType().getName().equals(packet.getPacketName())) {
                h.Receive(packet);
            }
        }
        return packet.getPacket();
    }

    public Object onSend(Packet packet) {
        for (DenoriaPacketHandler h : handles) {
            if (h.getPacketType().equals(PacketType.ALL)) {
                h.Send(packet);
            } else if (h.getPacketType().getName().equals(packet.getPacketName())) {
                h.Send(packet);
            }
        }
        return packet.getPacket();
    }

}


