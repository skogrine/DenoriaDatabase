package fr.denoria.denocore.support.packets;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.List;

import org.bukkit.entity.Player;

public class Packet {

    private Object packet;
    private Player player;

    public Packet(Object packet, Player player) {
        this.packet = packet;
        this.player = player;
    }

    public String getPacketName() {
        return packet.getClass().getSimpleName();
    }

    public Object getPacket() {
        return packet;
    }

    public Player getPlayer() {
        return player;
    }

    public List<String> getFields() {
        List<String> list = new ArrayList<>();
        for (Field f : isSuper() ? packet.getClass().getSuperclass().getDeclaredFields()
                : packet.getClass().getDeclaredFields()) {
            list.add(f.getType().getSimpleName() + " " + f.getName());
        }
        return list;
    }

    public Object getPacketValue(String name) {
        Object value = null;
        try {
            Field f = isSuper() ? packet.getClass().getSuperclass().getDeclaredField(name)
                    : packet.getClass().getDeclaredField(name);
            f.setAccessible(true);
            value = f.get(packet);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return value;
    }

    public Object invoke(String methodname, Object[] args, Class<?>... parameterTypes) {
        Object value = null;
        try {
            Method m = isSuper() ? packet.getClass().getSuperclass().getDeclaredMethod(methodname, parameterTypes)
                    : packet.getClass().getDeclaredMethod(methodname, parameterTypes);
            m.setAccessible(true);
            value = m.invoke(packet, args);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return value;
    }

    private boolean isSuper() {
        return Modifier.isStatic(packet.getClass().getModifiers());
    }
}
