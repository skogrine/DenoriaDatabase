package fr.denoria.denocore.support.packets.packetlist;

public class PacketBuild {

    public int id;
    public String name;

    public PacketBuild(int id, String name) {
        this.id = id;
        this.name = "PacketPlayIn" + name;
        ;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
