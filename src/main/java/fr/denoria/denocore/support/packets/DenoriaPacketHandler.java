package fr.denoria.denocore.support.packets;

import fr.denoria.denocore.support.packets.packetlist.PacketBuild;

public abstract class DenoriaPacketHandler {

    public PacketBuild packet;

    public void GolemaPacketHandler(PacketBuild packet) {
        this.packet = packet;
    }

    public PacketBuild getPacketType() {
        return packet;
    }

    public abstract Response Send(Packet event);

    public abstract void Receive(Packet event);

}
