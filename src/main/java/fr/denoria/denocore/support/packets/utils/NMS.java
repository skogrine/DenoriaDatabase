package fr.denoria.denocore.support.packets.utils;

import java.lang.reflect.Method;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

public abstract class NMS {

    /** Le protocol de version Minecraft */
    public static String nms_protocol;

    /**
     * Retourne un player NMS
     *
     * @param player
     * @return
     */
    public static Object getNMSPlayer(Player player) {
        try {
            Method getHandle = player.getClass().getMethod("getHandle");
            Object nms_entity = getHandle.invoke(player);
            return nms_entity;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Retour une classe NMS
     *
     * @param name
     * @return
     */
    public static Class<?> getNMSClass(String name) {
        if (nms_protocol == null)
            nms_protocol = getVersion();
        String nmsClass = "net.minecraft.server." + nms_protocol + "." + name;
        Class<?> clazz = null;
        try {
            clazz = Class.forName(nmsClass);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return clazz;
    }

    /**
     * Retourne une classe CB
     *
     * @param name
     * @return
     */
    public static Class<?> getCBClass(String name) {
        if (nms_protocol == null)
            nms_protocol = getVersion();
        String cbClass = "org.bukkit.craftbukkit." + nms_protocol + "." + name;
        Class<?> clazz = null;
        try {
            clazz = Class.forName(cbClass);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return clazz;
    }

    /**
     * Retourne le protocol du serveur
     *
     * @return
     */
    public static String getVersion() {
        return Bukkit.getServer().getClass().getPackage().getName().replace(".", ",").split(",")[3];
    }

}
