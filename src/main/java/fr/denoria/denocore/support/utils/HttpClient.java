/*
 * Copyright (c) 2021.
 */
package fr.denoria.denocore.support.utils;

import java.io.IOException;
import java.net.URI;
import java.util.List;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;

public class HttpClient {
    private final String USER_AGENT = "MineSync_Client/" +
            getClass().getPackage().getImplementationVersion();

    public static String get(String url) throws Exception {
        String responseBody;
        CloseableHttpClient httpclient = HttpClients.createDefault();
        try {
            HttpGet httpget = new HttpGet(url);
            ResponseHandler<String> responseHandler = new ResponseHandler<String>() {
                public String handleResponse(HttpResponse response) throws ClientProtocolException, IOException {
                    int status = response.getStatusLine().getStatusCode();
                    if (status >= 200 && status < 300) {
                        HttpEntity entity = response.getEntity();
                        return (entity != null) ? EntityUtils.toString(entity) : null;
                    }
                    throw new ClientProtocolException("Unexpected response status: " + status);
                }
            };
            responseBody = (String)httpclient.execute((HttpUriRequest)httpget, responseHandler);
        } finally {
            httpclient.close();
        }
        return responseBody;
    }

    public static String post(String url, List<NameValuePair> args) throws Exception {
        String responseBody;
        CloseableHttpClient httpclient = HttpClients.createDefault();
        try {
            HttpPost httppost = new HttpPost(url);
            URI uri = (new URIBuilder(httppost.getURI())).addParameters(args).build();
            httppost.setURI(uri);
            ResponseHandler<String> responseHandler = new ResponseHandler<String>() {
                public String handleResponse(HttpResponse response) throws ClientProtocolException, IOException {
                    int status = response.getStatusLine().getStatusCode();
                    if (status >= 200 && status < 300) {
                        HttpEntity entity = response.getEntity();
                        return (entity != null) ? EntityUtils.toString(entity) : null;
                    }
                    System.out.println("Unexpected response status: " + status);
                    throw new ClientProtocolException("Unexpected response status: " + status);
                }
            };
            responseBody = (String)httpclient.execute((HttpUriRequest)httppost, responseHandler);
        } finally {
            httpclient.close();
        }
        return responseBody;
    }
}
