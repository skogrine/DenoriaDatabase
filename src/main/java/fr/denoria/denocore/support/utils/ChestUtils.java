package fr.denoria.denocore.support.utils;


import java.util.Map;
import java.util.Map.Entry;
import java.util.Random;

import org.apache.commons.lang.Validate;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.Chest;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

/**
 * @author Lucidiax
 *
 * What is that ?
 *  - Add some goodies to manipulate chests
 **/
public class ChestUtils {

    private static Random random = new Random();

    /**
     * Check if block is a Chest
     *
     * @param block Block
     * @return Boolean
     */
    public static boolean isChest(Block block){
        Validate.notNull(block, "Block cannot be null");
        return block.getState() instanceof Chest;
    }

    /**
     * Get chest by Block
     *
     * @param block Block
     * @return Chest
     */
    public static Chest getChest(Block block){
        return isChest(block) ? (Chest) block.getState() : null;
    }

    /**
     * Fill a chest with an ItemStack
     *
     * @param chest Chest
     * @param stack ItemStack
     * @return Chest
     */
    public static Chest fillChest(Chest chest, ItemStack stack){
        Validate.notNull(chest, "Chest cannot be null");
        Validate.notNull(stack, "ItemStack cannot be null");
        Inventory inv = chest.getInventory();
        for(int i = 0; i < inv.getSize(); i++)
            if(inv.getItem(i) == null)
                inv.setItem(i, stack);
        return chest;
    }

    /**
     * Place a chest at a Location
     *
     * @param loc Location
     * @return Chest
     */
    public static Chest placeChest(Location loc){
        Validate.notNull(loc, "Location cannot be null");
        loc.getBlock().setType(Material.CHEST);
        return getChest(loc.getBlock());
    }

    /**
     * Fill a chest with a map of item and percent chance
     *
     * @param chest Chest
     * @return items Map with ItemStack and Double
     */
    public static Chest fillChestWithRandomAndPercent(Chest chest, Map<ItemStack, Double> items){
        Validate.notNull(chest, "Chest cannot be null");
        Validate.notNull(items, "Map cannot be null");
        for(Entry<ItemStack, Double> entry : items.entrySet())
            if(entry.getKey() != null && entry.getValue() > 0)
                if(random.nextInt(101) <= entry.getValue())
                    chest.getInventory().addItem(entry.getKey());
        return chest;
    }

}
