package fr.denoria.denocore.support.utils;

public interface Callback<T> {

    void done(T value);

}
