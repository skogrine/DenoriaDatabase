package fr.denoria.denocore.support.utils;

import net.md_5.bungee.api.ChatColor;

public class DenoriaLogger {

    public static void log(String logger){
        System.out.println(ChatColor.GREEN + "[API_LOG]" + logger);
    }

    public static void logWarning(String logger){
        System.out.println(ChatColor.YELLOW + "[API_WARN]" + logger);
    }

    public static void logError(String logger){
        System.out.println(ChatColor.DARK_RED + "[API_ERROR]" + logger);
    }

    public static void logDebug(String logger){
        System.out.println(ChatColor.BLUE + "[API_DEBUG]" + logger);
    }

    public static void log(ChatColor color, String logger){
        System.out.println(color + logger);
    }
}
