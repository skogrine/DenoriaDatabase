package fr.denoria.denocore.support.utils;

public enum PermissionEnum {

    NICK("denoria.nick"),
    STAFFCHAT("denoria.staffchat"),
    ;

    private String name;

    private PermissionEnum(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
