package fr.denoria.denocore.support.utils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class MysteryBoxUtils {

    public MysteryBoxUtils(int rows, int numberOfItems) {

        if (rows * 9 > 54) {

            rows = 6;
        }
        if (numberOfItems > 54) {

            numberOfItems = 54;
        }
        this.boxes = rows * 9;
        this.numberOfItems = numberOfItems;

    }

    public List<ItemStack> items = new ArrayList<ItemStack>();

    private int boxes;
    private int numberOfItems;

    private Inventory getMysteryInventory() {

        Inventory inv = Bukkit.createInventory(null, boxes, "§6Mystery Box");
        for (int i = 1; i < numberOfItems; i++) {

            inv.addItem(genRandomItem());
        }

        return inv;
    }

    public void addItem(ItemStack item) {

        items.add(item);

    }

    public ItemStack genRandomItem() {

        int num = (int) Math.floor(items.size() * Math.random());

        return items.get(num);

    }

    public void openMysteryBox(Player player) {
        ItemStack box = player.getItemInHand();
        player.getInventory().remove(box);
        player.openInventory(getMysteryInventory());
    }

    public static ItemStack getMyteryBoxItem() {
        ItemStack mb = new ItemStack(Material.ENDER_CHEST);
        ItemMeta meta = mb.getItemMeta();
        meta.setDisplayName("§6Mystery Box");
        List<String> lore = Arrays.asList("", "§7Clique pour ouvrir !");
        meta.setLore(lore);
        mb.setItemMeta(meta);
        return new ItemStack(mb);
    }

}
