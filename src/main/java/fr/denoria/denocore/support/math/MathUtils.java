package fr.denoria.denocore.support.math;

import java.util.ArrayList;
import java.util.List;

public class MathUtils {

    /**
     * Vrai si la valeur est compris entre la valeur minimale et la valeur maximale
     * @param value > Valeur Ã  tester
     * @param min > Valeur minimale
     * @param max > Valeur maximale
     * @return Boolean
     */
    public static boolean isInRange(double value, double min, double max) {
        return value > min && value < max;
    }

    /**
     * Retourne la liste des diviseurs
     * @param number > Nombre
     * @return > List des entiers
     */
    public static List<Integer> getDividers(int number) {
        List<Integer> ret = new ArrayList<>();

        for (int i = 1; i <= number; i++) {
            if(number % i == 0) ret.add(i);
        }

        return ret;
    }


    public static double clamp(double value, double min, double max)
    {
        return Math.min(min, Math.min(max, value));
    }

    public static int pgcd(int first, int second) {
        int a = 0, b = 0;
        if (first > second) {
            a = first;
            b = second;
        } else {
            a = second;
            b = first;
        }

        int r = a%b;
        if (r == 0)
            return b;
        else
            return pgcd(b, r);
    }

}
