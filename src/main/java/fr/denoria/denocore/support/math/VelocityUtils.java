package fr.denoria.denocore.support.math;

import org.bukkit.Location;
import org.bukkit.entity.Entity;
import org.bukkit.util.Vector;

public class VelocityUtils {

    public static Vector getProjectionFromAToB(Location from, Location to) {
        return to.toVector().subtract(from.toVector());
    }

    public static Vector getBumpVector(Entity ent, Location from, double power) {
        Vector bump = ent.getLocation().toVector().subtract(from.toVector()).normalize();
        bump.multiply(power);
        return bump;
    }

    public static Vector getPullVector(Location from, Location to, double power) {
        Vector pull = to.toVector().subtract(from.toVector()).normalize();
        pull.multiply(power);
        return pull;
    }

    public static Vector getPullVector(Entity entity, Location to, double power) {
        return getPullVector(entity.getLocation(), to, power);
    }

    public static void bumpEntity(Entity ent, Location from, double power) {
        ent.setVelocity(getBumpVector(ent, from, power));
    }

    public static void bumpEntity(Entity entity, Location from, double power, double fixedY) {
        Vector vector = getBumpVector(entity, from, power);
        vector.setY(fixedY);
        entity.setVelocity(vector);
    }

    public static void pullEntity(Entity ent, Location from, double power, double fixedY) {
        Vector vector = getPullVector(ent, from, power);
        vector.setY(fixedY);
        ent.setVelocity(vector);
    }

    public static double realPitch(double pitch) {
        return ((pitch + 90) * Math.PI) / 180;
    }

    public static double realYaw(double yaw) {
        return ((yaw + 90) * Math.PI) / 180;
    }
}
