package fr.denoria.denocore.support.math;

public class Chrono {
    private long start;
    private long end;

    public void end() {
        end = System.currentTimeMillis();
    }

    public void start() {
        start = System.currentTimeMillis();
    }

    public long getTime() {
        return (end < start) ? 0 : end - start;
    }
}
