package fr.denoria.denocore.support.world;

import java.util.List;
import java.util.Random;

import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.generator.BlockPopulator;
import org.bukkit.generator.ChunkGenerator;
import org.bukkit.plugin.Plugin;

@SuppressWarnings("deprecation")
public class ChunkLoader extends ChunkGenerator {

    public Plugin plugin;

    public ChunkLoader(Plugin plugin) {
        this.plugin = plugin;
    }

    @Override
    public byte[] generate(World world, Random random, int x, int z) {
        return super.generate(world, random, x, z);
    }

    @Override
    public List<BlockPopulator> getDefaultPopulators(World world) {
        return super.getDefaultPopulators(world);
    }

    @Override
    public Location getFixedSpawnLocation(World world, Random random) {
        return super.getFixedSpawnLocation(world, random);
    }

    @Override
    public byte[][] generateBlockSections(World world, Random random, int x, int z, BiomeGrid biomes) {
        return super.generateBlockSections(world, random, x, z, biomes);
    }

    @Override
    public ChunkData generateChunkData(World world, Random random, int x, int z, BiomeGrid biome) {
        return super.generateChunkData(world, random, x, z, biome);
    }

    @Override
    public short[][] generateExtBlockSections(World world, Random random, int x, int z, BiomeGrid biomes) {
        return super.generateExtBlockSections(world, random, x, z, biomes);
    }
}
