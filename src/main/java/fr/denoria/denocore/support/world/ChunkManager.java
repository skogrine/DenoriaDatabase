package fr.denoria.denocore.support.world;

import fr.denoria.denocore.DenoCore;
import org.bukkit.Bukkit;
import org.bukkit.Chunk;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.generator.ChunkGenerator;



public class ChunkManager {

    /**
     * Générer un chunk par un chunk
     *
     * @param world
     * @param chunk
     */
    public static void generateChunkbyChunk(World world, Chunk chunk) {
        int x = chunk.getX()-Bukkit.getViewDistance();
        int toX = x + (Bukkit.getViewDistance() * 2);
        int toZ = x + (Bukkit.getViewDistance() * 2);

        while (x < toX) {
            int z = chunk.getZ()-Bukkit.getViewDistance();
            while (z < toZ) {
                world.loadChunk(x,z);
                z++;
            }
            x++;
        }
    }

    /**
     * Générer les chunks d'une coordonnée
     *
     * @param world
     * @param location
     */
    public static void generateChunkbyLocation(World world, Location location) {
        int x = (int) location.getX();
        int z = (int) location.getZ();
        world.getChunkAt(x, z).load(true);
    }

    /**
     * Générer les chunks par (x, z)
     *
     * @param world
     * @param x
     * @param z
     */
    public static void generateChunk(World world, int x, int z) {
        world.getChunkAt(x, z).load(true);
    }

    /**
     * Générer les chunks par (x, z)
     *
     * @param world
     * @param x
     * @param z
     */
    public static void generateChunkSimply(World world, int x, int z) {
        world.loadChunk(x, z, true);
        world.loadChunk(x, z);
    }

    /**
     * Gestion des chunk plus poussé via ChunkGenerator;
     *
     * @param worldName
     * @param uid
     * @return
     */
    public static ChunkGenerator getDefaultWorldGenerator(String worldName, String uid) {
        return new ChunkLoader(DenoCore.getInstance());
    }
}
