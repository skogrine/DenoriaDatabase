package fr.denoria.denocore.support.builder;

import java.util.List;

import org.bukkit.Location;
import org.bukkit.Sound;
import org.bukkit.World;
import org.bukkit.entity.Player;

public class WorldborderBuilder {

    private List<Player> players = null;
    private Location loc = null;
    private Double X = null;
    private Double Z = null;
    private World world = null;
    private Integer radius = 1;
    private Sound son = Sound.BAT_HURT;
    private String message = "";

    public WorldborderBuilder(Location loc) {
        this.setLoc(loc);
    }

    public List<Player> getPlayers() {
        return players;
    }

    public void setPlayers(List<Player> players) {
        this.players = players;
    }

    public Location getLoc() {
        return loc;
    }

    public void setLoc(Location loc) {
        this.loc = loc;
    }

    public Double getX() {
        return X;
    }

    public void setX(Double x) {
        X = x;
    }

    public Double getZ() {
        return Z;
    }

    public void setZ(Double z) {
        Z = z;
    }

    public World getWorld() {
        return world;
    }

    public void setWorld(World world) {
        this.world = world;
    }

    public Integer getRadius() {
        return radius;
    }

    public void setRadius(Integer radius) {
        this.radius = radius;
    }

    public Sound getSon() {
        return son;
    }

    public void setSon(Sound son) {
        this.son = son;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

}
