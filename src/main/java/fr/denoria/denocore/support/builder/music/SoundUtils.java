package fr.denoria.denocore.support.builder.music;

import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.Sound;
import org.bukkit.entity.Player;

public class SoundUtils {

    /**
     * Envoyer un son a un joueur
     * @param player, Sound
     * **/
    public static void sendSoundToPlayer(Player player, Sound sound) {
        player.playSound(player.getLocation(), sound, 0f, 1.0f);
    }

    /**
     * Envoyer un son a tous les joueurs
     * @param sound
     * **/
    public static void sendSoundForAll(Sound sound) {
        for(Player player : Bukkit.getOnlinePlayers()) {
            player.playSound(player.getLocation(), sound, 0f, 1.0f);
        }
    }

    /**
     * Envoyer un son a une list de joueurs
     * @param playerList type Player, Sound
     * **/
    public static void sendSoundToPlayersList(List<Player> playerList, Sound sound) {
        for(Player player : playerList) {
            player.playSound(player.getLocation(), sound, 0f, 1.0f);
        }
    }
}
