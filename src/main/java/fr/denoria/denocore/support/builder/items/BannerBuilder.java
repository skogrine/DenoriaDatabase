package fr.denoria.denocore.support.builder.items;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.bukkit.DyeColor;
import org.bukkit.Material;
import org.bukkit.block.banner.Pattern;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.BannerMeta;

public class BannerBuilder {

    private String name = null;
    private int amount = Integer.valueOf(1);
    private DyeColor baseColor = DyeColor.WHITE;
    private List<Pattern> couches = null;
    private List<String> lore = null;
    private Map<Enchantment, Integer> enchantments = null;

    public BannerBuilder(String name) {
        this.name = name;
    }

    public String getName() {
        return this.name;
    }

    public Integer getAmount() {
        return this.amount;
    }

    public DyeColor getBaseColor() {
        return this.baseColor;
    }

    public List<Pattern> getPatterns() {
        return this.couches;
    }

    public List<String> getDescription() {
        return this.lore;
    }

    public Map<Enchantment, Integer> getEnchantments() {
        return this.enchantments;
    }

    public BannerBuilder setName(String name) {
        this.name = name;
        return this;
    }

    public BannerBuilder setAmount(Integer amount) {
        this.amount = amount;
        return this;
    }

    public BannerBuilder setBaseColor(DyeColor baseColor) {
        this.baseColor = baseColor;
        return this;
    }

    public BannerBuilder setCouches(Pattern... patterns) {
        this.couches = Arrays.asList(patterns);
        return this;
    }

    public BannerBuilder addPattern(Pattern pattern) {
        this.couches.add(pattern);
        return this;
    }

    public BannerBuilder addDescription(String lore) {
        this.lore.add(lore);
        return this;
    }

    public BannerBuilder setDescription(String... strings) {
        this.lore = Arrays.asList(strings);
        return this;
    }

    public BannerBuilder enchant(Enchantment enchantment, Integer level) {
        this.enchantments.put(enchantment, level);
        return this;
    }

    public BannerBuilder enchantments(Map<Enchantment, Integer> enchantments) {
        this.enchantments = enchantments;
        return this;
    }

    public ItemStack build() {
        ItemStack banniere = new ItemStack(Material.BANNER);
        BannerMeta meta = (BannerMeta) banniere.getItemMeta();
        meta.setBaseColor(getBaseColor());
        if ((this.name != null) || (this.lore != null)) {
            if (this.name != null) {
                meta.setDisplayName(this.name);
            }
            if (this.lore != null) {
                meta.setLore(this.lore);
            }
        }
        meta.setPatterns(couches);
        if (this.enchantments != null) {
            for (Enchantment ench : this.enchantments.keySet()) {
                int level = this.enchantments.get(ench);
                banniere.addUnsafeEnchantment(ench, level);
            }
        }
        banniere.setItemMeta(meta);
        banniere.setAmount(getAmount());
        return banniere;
    }

}