package fr.denoria.denocore.support.builder.music.noteapi;

import org.bukkit.Sound;

public class Instrument {

    public static Sound getInstrument(Sound sound) {
        return sound;
    }

    public static org.bukkit.Instrument getBukkitInstrument(byte instrument) {
        switch (instrument) {
            case 0:
                return org.bukkit.Instrument.PIANO;
            case 1:
                return org.bukkit.Instrument.BASS_GUITAR;
            case 2:
                return org.bukkit.Instrument.BASS_DRUM;
            case 3:
                return org.bukkit.Instrument.SNARE_DRUM;
            case 4:
                return org.bukkit.Instrument.STICKS;
            /* No equivalent instrument for the sound.
            case 5:
                return org.bukkit.Instrument.PLINGS; */
            default:
                return org.bukkit.Instrument.PIANO;
        }
    }
}
