package fr.denoria.denocore.support.builder.music;

import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.Instrument;
import org.bukkit.Note;
import org.bukkit.entity.Player;

public class NoteUtils {

    /**
     * Envoyer une not a un joueur
     *
     * @param player,
     *            Instrument, Note
     **/
    public static void sendSoundToPlayer(Player player, Instrument instrument, Note note) {
        player.playNote(player.getLocation(), instrument, note);
    }

    /**
     * Envoyer une note a tous les joueurs
     *
     * @param instrument,
     *            Note
     **/
    public static void sendSoundForAll(Instrument instrument, Note note) {
        for (Player player : Bukkit.getOnlinePlayers()) {
            player.playNote(player.getLocation(), instrument, note);
        }
    }

    /**
     * Envoyer une note a une list de joueurs
     *
     * @param playerList
     *            type Player, Instrument, Note
     **/
    public static void sendSoundToPlayersList(List<Player> playerList, Instrument instrument, Note note) {
        for (Player player : playerList) {
            player.playNote(player.getLocation(), instrument, note);
        }
    }

}
