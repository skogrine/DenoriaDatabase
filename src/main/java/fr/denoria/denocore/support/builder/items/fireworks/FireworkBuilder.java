package fr.denoria.denocore.support.builder.items.fireworks;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.bukkit.Color;
import org.bukkit.FireworkEffect;
import org.bukkit.Location;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Firework;
import org.bukkit.inventory.meta.FireworkMeta;

/**
 * Created by Alex on 10/11/2015.
 */
public class FireworkBuilder {

    private List<FireworkEffect.Builder> builders = new ArrayList<>();
    private FireworkEffect.Builder actualBuilder;

    /**
     * Crée un nouveau FireworkBuilder, avec un FireworkEffect prêt à être utilisé
     */
    public FireworkBuilder() {
        addNewEffect();
    }

    private static FireworkBuilder newFwBuilderEmpty() {
        FireworkBuilder builder = new FireworkBuilder();
        builder.actualBuilder = null;
        builder.builders.clear();
        return builder;
    }

    /**
     * Spawn un Firework avec les différents effets ajoutés précédemment
     *
     * @param loc
     *            La location
     * @return
     */
    public Firework spawn(Location loc) {
        Firework firework = (Firework) loc.getWorld().spawnEntity(loc, EntityType.FIREWORK);
        FireworkMeta meta = firework.getFireworkMeta();

        for (FireworkEffect.Builder builder : builders)
            meta.addEffect(builder.build());

        firework.setFireworkMeta(meta);
        return firework;
    }

    /**
     * Ajoute un nouvel effet, et les méthodes qui seront utilisés prochainement
     * affecteront ce nouvel effet
     */
    public FireworkBuilder addNewEffect() {
        actualBuilder = FireworkEffect.builder();
        builders.add(actualBuilder);
        return this;
    }

    /**
     * Retourne un nouveau FireworkBuilder à partir d'un autre. Equivalent d'un
     * clone
     *
     * @param builder
     */
    public static FireworkBuilder fromFireworkBuilder(FireworkBuilder builder) {
        FireworkBuilder fwBuilder = newFwBuilderEmpty();
        fwBuilder.builders = builder.builders;
        fwBuilder.actualBuilder = builder.actualBuilder;
        return fwBuilder;
    }

    // ////// PROPERTIES //////
    // ====== SETTERS ======

    /**
     * Permet de définir si le FireworkEffect oscillera pendant son lancement
     *
     * @param flicker
     *            Défini ou non le vascillement
     */
    public FireworkBuilder setFlicker(boolean flicker) {
        actualBuilder.flicker(flicker);
        return this;
    }

    /**
     * Permet de définir si le FireworkEffect laissera une trace pendant son
     * lancement
     *
     * @param trail
     *            Défini ou non la trace
     */
    public FireworkBuilder setTrail(boolean trail) {
        actualBuilder.trail(trail);
        return this;
    }

    /**
     * Permet de définir le type du FireworkEffect
     *
     * @param type
     *            Défini le type
     */
    public FireworkBuilder setType(FireworkEffect.Type type) {
        actualBuilder.with(type);
        return this;
    }

    // ====== ADDERS ======

    /**
     * Permet d'ajouter des couleurs (à l'explosion)
     *
     * @param colors
     *            Les couleurs à ajouter
     */
    public FireworkBuilder addColors(Color... colors) {
        actualBuilder.withColor(colors);
        return this;
    }

    /**
     * Permet d'ajouter des couleurs de fondu (couleur quand l'explosion part)
     *
     * @param colors
     *            Les couleurs à ajouter
     */
    public FireworkBuilder addFadeColors(Color... colors) {
        actualBuilder.withFade(colors);
        return this;
    }

    // ====== RANDOM ======

    /**
     * Permet de définir des paramètres Random pour le FireworkEffect
     *
     * @param randomType
     *            Défini ou non un random Type
     * @param randomTrail
     *            Défini ou non un random Trail (trace)
     * @param randomFlicker
     *            Défini ou non un random Flicker (oscillement)
     * @param randomColorNumber
     *            Le nombre de couleurs à définir aléatoirement
     * @param randomFadeNumber
     *            Le nombre de couleurs de fondu à définir aléatoirement
     */
    public FireworkBuilder setRandom(boolean randomType, boolean randomTrail, boolean randomFlicker,
                                     int randomColorNumber, int randomFadeNumber) {
        if (randomType)
            setRandomType();
        if (randomTrail)
            setRandomTrail();
        if (randomFlicker)
            setRandomFlicker();
        addRandomColors(randomColorNumber);
        addRandomFadeColors(randomFadeNumber);
        return this;
    }

    /**
     * Permet de définir des couleurs de fondu aléatoirement
     *
     * @param number
     *            Le nombre de couleurs
     */
    public FireworkBuilder addRandomFadeColors(int number) {
        Random rand = new Random();
        for (int i = 0; i < number; i++)
            actualBuilder.withFade(Color.fromBGR(rand.nextInt(256), rand.nextInt(256), rand.nextInt(256)));
        return this;
    }

    /**
     * Permet de définir des couleurs aléatoirement
     *
     * @param number
     *            Le nombre de couleurs
     */
    public FireworkBuilder addRandomColors(int number) {
        Random rand = new Random();
        for (int i = 0; i < number; i++)
            actualBuilder.withColor(Color.fromBGR(rand.nextInt(256), rand.nextInt(256), rand.nextInt(256)));

        return this;
    }

    /**
     * Permet de définir un Type aléatoirement
     */
    public FireworkBuilder setRandomType() {
        Random rand = new Random();
        actualBuilder.with(FireworkEffect.Type.values()[rand.nextInt(FireworkEffect.Type.values().length)]);
        return this;
    }

    /**
     * Permet de définir un Trail (trace) aléatoirement
     */
    public FireworkBuilder setRandomTrail() {
        Random rand = new Random();
        actualBuilder.trail(rand.nextBoolean());
        return this;
    }

    /**
     * Permet de définir un Flicker (oscillement) aléatoirement
     */
    public FireworkBuilder setRandomFlicker() {
        Random rand = new Random();
        actualBuilder.flicker(rand.nextBoolean());
        return this;
    }
}