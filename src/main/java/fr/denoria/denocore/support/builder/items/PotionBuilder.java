package fr.denoria.denocore.support.builder.items;

import java.util.List;
import java.util.Map;

import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.potion.Potion;
import org.bukkit.potion.PotionType;

public class PotionBuilder {

    private PotionType type = null;
    private int amount = Integer.valueOf(1);
    private int level = 1;
    private boolean extended = false;
    private boolean splash = false;
    private String name = null;
    private List<String> lore = null;
    private Map<Enchantment, Integer> enchantments = null;

    public PotionBuilder(PotionType type) {
        this.setType(type);
    }

    public PotionType getType() {
        return type;
    }

    public PotionBuilder setType(PotionType type) {
        this.type = type;
        return this;
    }

    public int getAmount() {
        return amount;
    }

    public PotionBuilder setAmount(int amount) {
        this.amount = amount;
        return this;
    }

    public int getLevel() {
        return level;
    }

    public PotionBuilder setLevel(int level) {
        this.level = level;
        return this;
    }

    public boolean isExtended() {
        return extended;
    }

    public PotionBuilder setExtended(boolean extended) {
        this.extended = extended;
        return this;
    }

    public String getName() {
        return name;
    }

    public PotionBuilder setName(String name) {
        this.name = name;
        return this;
    }

    public List<String> getLore() {
        return lore;
    }

    public PotionBuilder setLore(List<String> lore) {
        this.lore = lore;
        return this;
    }

    public boolean isSplash() {
        return splash;
    }

    public PotionBuilder setSplash(boolean splash) {
        this.splash = splash;
        return this;
    }

    public Map<Enchantment, Integer> getEnchantments() {
        return enchantments;
    }

    public PotionBuilder addEnchantement(Enchantment enchantment, Integer level) {
        this.enchantments.put(enchantment, level);
        return this;
    }

    public PotionBuilder setEnchantments(Map<Enchantment, Integer> enchantments) {
        this.enchantments = enchantments;
        return this;
    }

    @SuppressWarnings("deprecation")
    public ItemStack build() {
        Potion popo = new Potion(getType(), getLevel(), isSplash());
        popo.setHasExtendedDuration(extended);
        ItemStack item = popo.toItemStack(getAmount());
        ItemMeta meta;
        if (this.name != null || this.lore != null) {
            meta = item.getItemMeta();
            if (this.name != null) {
                meta.setDisplayName(this.name);
            }
            if (this.lore != null) {
                meta.setLore(this.lore);
            }
            item.setItemMeta(meta);
        }
        return item;
    }

}
