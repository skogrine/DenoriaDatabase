package fr.denoria.denocore.support.builder.music.noteapi;

import org.bukkit.Sound;
import org.bukkit.entity.Player;

public class RadioSongPlayer extends SongPlayer {

    public RadioSongPlayer(Song song, Sound sound) {
        super(song, sound);
    }

    @Override
    public void playTick(Player p, int tick, Sound sound) {
        byte playerVolume = NoteBlockPlayerMain.getPlayerVolume(p);

        for (Layer l : song.getLayerHashMap().values()) {
            Note note = l.getNote(tick);
            if (note == null) {
                continue;
            }
            p.playSound(p.getEyeLocation(),
                    Instrument.getInstrument(sound),
                    (l.getVolume() * (int) volume * (int) playerVolume) / 1000000f,
                    NotePitch.getPitch(note.getKey() - 33));
        }
    }
}
