package fr.denoria.denocore.support.builder;

import java.lang.reflect.Field;
import java.lang.reflect.Method;

import org.bukkit.Location;
import org.bukkit.entity.Item;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.Plugin;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.util.Vector;

/**
 * Classe de gestion de spawn récuurent d'items Created by Stornitz On
 * 20/10/2014 At 19:32
 */
public class SpawnersAPI {

    Plugin plugin;

    /**
     * Constructeur
     *
     * @param plugin
     */
    public SpawnersAPI(Plugin plugin) {
        this.plugin = plugin;
    }

    /**
     * Crée un spawner d'item à interval régulier
     *
     * @param location
     *            location du spawner
     * @param itemStack
     *            itemStack à faire spawn
     * @param ticksLived
     *            ticks vécu
     * @param interval
     *            interval de spawn
     * @return id de la BukkitTask
     */
    @Deprecated
    public int addItemWithTicksLivedSpawner(final Location location, final ItemStack itemStack, final int ticksLived,
                                            int interval) {
        return addItemWithTicksLivedSpawner(location, itemStack, ticksLived, interval, true);
    }

    /**
     * Crée un spawner d'item à interval régulier
     *
     * @param location
     *            location du spawner
     * @param itemStack
     *            itemStack à faire spawn
     * @param ticksLived
     *            ticks vécu
     * @param interval
     *            interval de spawn
     * @param naturally
     *            drop avec une vélociteé naturel
     * @return id de la BukkitTask
     */
    public int addItemWithTicksLivedSpawner(final Location location, final ItemStack itemStack, final int ticksLived,
                                            int interval, final boolean naturally) {
        return new BukkitRunnable() {
            public void run() {
                Item item;
                if (naturally) {
                    item = location.getWorld().dropItemNaturally(location, itemStack);
                } else {
                    item = location.getWorld().dropItem(location, itemStack);
                    item.setVelocity(new Vector(0, 0, 0));
                }
                if (ticksLived > 0) {
                    try {
                        Method getHandle = item.getClass().getMethod("getHandle");
                        Object craftItemObject = getHandle.invoke(item);
                        @SuppressWarnings("rawtypes")
                        Class CraftItem = craftItemObject.getClass();
                        Field age = CraftItem.getDeclaredField("age");
                        age.setAccessible(true);
                        age.setInt(craftItemObject, ticksLived);
                        age.setAccessible(false);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        }.runTaskTimer(plugin, 1, interval).getTaskId();
    }

    /**
     * Crée un spawner d'item à interval régulier
     *
     * @param location
     *            location du spawner
     * @param itemStack
     *            itemStack à faire spawn
     * @param interval
     *            interval de spawn
     * @return id de la BukkitTask
     */
    public int addItemSpawner(final Location location, final ItemStack itemStack, int interval) {
        return addItemWithTicksLivedSpawner(location, itemStack, 0, interval);
    }

    /**
     * Crée un spawner d'item à interval régulier
     *
     * @param location
     *            location du spawner
     * @param itemStack
     *            itemStack à faire spawn
     * @param timeBeforeDespawn
     *            Temps avant despawn
     * @param interval
     *            interval de spawn
     * @return id de la BukkitTask
     */
    public int addItemDespawningSpawner(final Location location, final ItemStack itemStack, final int timeBeforeDespawn,
                                        int interval) {
        return addItemWithTicksLivedSpawner(location, itemStack, 6000 - timeBeforeDespawn, interval);
    }

    /**
     * Retire un spawner
     *
     * @param spawnerId
     *            id de la bukkitTask liée au spawner
     */
    public void removeItemSpawner(int spawnerId) {
        plugin.getServer().getScheduler().cancelTask(spawnerId);
    }
}
