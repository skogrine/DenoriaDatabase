package fr.denoria.denocore.support.builder.music;

import java.io.File;

import fr.denoria.denocore.DenoCore;
import fr.denoria.denocore.support.builder.music.noteapi.*;
import org.bukkit.Sound;
import org.bukkit.entity.Player;

public class MusicUtils {


    private Player player;
    private String title;
    private SongPlayer songPlayer;
    private Sound sound;

    public MusicUtils(Player player, String title, Sound sound) {
        this.player = player;
        this.title = title;
        this.sound = sound;
    }

    public void play() {
        Song song = NBSDecoder.parse(new File(DenoCore.getInstance().getDataFolder(), "/tracks/" + title + ".nbs"));
        SongPlayer sp = new RadioSongPlayer(song, sound);
        sp.setAutoDestroy(true);
        sp.addPlayer(player);
        sp.setPlaying(true);
        songPlayer = sp;
    }

    public void stop() {
        songPlayer.removePlayer(player);
        songPlayer.setPlaying(false);
        songPlayer.destroy();
    }

    public void pause() {
        songPlayer.setPlaying(false);
    }

    public void unpause() {
        songPlayer.setPlaying(true);
    }

}
