package fr.denoria.denocore.support.builder.items.fireworks;

import java.util.Arrays;
import java.util.List;
import java.util.Random;

import org.bukkit.Color;
import org.bukkit.FireworkEffect;
import org.bukkit.FireworkEffect.Type;
import org.bukkit.Location;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Firework;
import org.bukkit.inventory.meta.FireworkMeta;

public class FireworksBuilder {

    private Location loc = null;
    private Color color = null;
    private Type type = null;
    private boolean kill = false;
    private Integer power = Integer.valueOf(1);
    private List<FireworkEffect> effects = null;

    public FireworksBuilder(Location loc) {
        this.setLoc(loc);
    }

    public Location getLoc() {
        return loc;
    }

    public Color getColor() {
        return color;
    }

    public Type getType() {
        return type;
    }

    public boolean isKill() {
        return kill;
    }

    public Integer getPower() {
        return this.power;
    }

    public List<FireworkEffect> getEffects() {
        return effects;
    }

    public FireworksBuilder setLoc(Location loc) {
        this.loc = loc;
        return this;
    }

    public FireworksBuilder setColor(Color color) {
        this.color = color;
        return this;
    }

    public FireworksBuilder setType(Type type) {
        this.type = type;
        return this;
    }

    public FireworksBuilder setKill(boolean kill) {
        this.kill = kill;
        return this;
    }

    public FireworksBuilder setPower(int power) {
        this.power = power;
        return this;
    }

    public FireworksBuilder addEffect(FireworkEffect effect) {
        this.effects.add(effect);
        return this;
    }

    public FireworksBuilder setEffect(FireworkEffect... effects) {
        this.effects = Arrays.asList(effects);
        return this;
    }

    public Firework build() {
        final Firework fw = (Firework) getLoc().getWorld().spawnEntity(getLoc(), EntityType.FIREWORK);
        FireworkMeta fm = fw.getFireworkMeta();
        Random r = new Random();
        if (effects == null) {
            FireworkEffect effect = FireworkEffect.builder().flicker(r.nextBoolean()).withColor(getColor())
                    .withFade(getColor()).with(getType()).trail(r.nextBoolean()).build();
            addEffect(effect);
            fw.setFireworkMeta(fm);
        }
        if (effects != null || power != null) {
            if (effects != null)
                fm.addEffects(getEffects());
            if (power != null)
                fm.setPower(getPower());
        }
        fw.setFireworkMeta(fm);

        return fw;
    }

}
