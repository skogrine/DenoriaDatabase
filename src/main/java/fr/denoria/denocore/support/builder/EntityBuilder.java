package fr.denoria.denocore.support.builder;

import java.util.HashSet;
import java.util.Set;

import org.apache.commons.lang.Validate;
import org.bukkit.Location;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.LivingEntity;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;

/**
 * @author Lucidiax
 *
 *         What is that ? - This builder allow to create mob easily
 */
public class EntityBuilder {

    private EntityType type;
    private String name;
    private ItemStack[] armor;
    private ItemStack hand;
    private double maxHealth;
    private double health;
    private Set<PotionEffect> potions;

    /**
     * Instance EntityBuilder with EntityType
     *
     * @param type
     *            EntityType
     */
    public EntityBuilder(EntityType type) {
        this.type = type;
        this.name = null;
        this.armor = new ItemStack[] {};
        this.hand = null;
        this.maxHealth = -1;
        this.health = -1;
        this.potions = new HashSet<>();
    }

    /**
     * Set the type of EntityBuilder
     *
     * @param type
     *            EntityType
     * @return EntityBuilder
     */
    public EntityBuilder type(EntityType type) {
        this.type = type;
        return this;
    }

    /**
     * Set the name of EntityBuilder
     *
     * @param name
     *            String
     * @return EntityBuilder
     */
    public EntityBuilder name(String name) {
        this.name = name;
        return this;
    }

    /**
     * Set the helmet of EntityBuilder
     *
     * @param stack
     *            ItemStack
     * @return EntityBuilder
     */
    public EntityBuilder helmet(ItemStack stack) {
        this.armor[3] = stack;
        return this;
    }

    /**
     * Set the chestplate of EntityBuilder
     *
     * @param stack
     *            ItemStack
     * @return EntityBuilder
     */
    public EntityBuilder chestplate(ItemStack stack) {
        this.armor[2] = stack;
        return this;
    }

    /**
     * Set the leggings of EntityBuilder
     *
     * @param stack
     *            ItemStack
     * @return EntityBuilder
     */
    public EntityBuilder leggings(ItemStack stack) {
        this.armor[1] = stack;
        return this;
    }

    /**
     * Set the boots of EntityBuilder
     *
     * @param stack
     *            ItemStack
     * @return EntityBuilder
     */
    public EntityBuilder boots(ItemStack stack) {
        this.armor[0] = stack;
        return this;
    }

    /**
     * Set the item in hand of EntityBuilder
     *
     * @param stack
     *            ItemStack
     * @return EntityBuilder
     */
    public EntityBuilder hand(ItemStack stack) {
        this.hand = stack;
        return this;
    }

    /**
     * Set the max health of EntityBuilder
     *
     * @param maxHealth
     *            Double
     * @return EntityBuilder
     */
    public EntityBuilder maxHealth(double maxHealth) {
        this.maxHealth = maxHealth;
        return this;
    }

    /**
     * Set the current health of EntityBuilder
     *
     * @param health
     *            Double
     * @return EntityBuilder
     */
    public EntityBuilder health(double health) {
        this.health = health;
        return this;
    }

    /**
     * Add a potion on the EntityBuilder
     *
     * @param effect
     *            PotionEffect
     * @return EntityBuilder
     */
    public EntityBuilder potion(PotionEffect effect) {
        this.potions.add(effect);
        return this;
    }

    /**
     * Spawn the EntityBuilder
     *
     * @param loc
     *            Location
     * @return Entity
     */
    public Entity spawn(Location loc) {
        Validate.notNull(loc, "Location cannot be null");
        Validate.notNull(this.type, "EntityType cannot be null");
        Entity ent = loc.getWorld().spawnEntity(loc, this.type);
        if (!(ent instanceof LivingEntity)) {
            System.out.println("The entity is the method spawn isn't a LivingEntity");
            ent.remove();
            return null;
        }
        LivingEntity entity = (LivingEntity) ent;
        if (this.name != null) {
            entity.setCustomName(this.name);
            entity.setCustomNameVisible(true);
        }
        if (this.armor.length > 0)
            entity.getEquipment().setArmorContents(this.armor);
        if (this.hand != null)
            entity.getEquipment().setItemInHand(this.hand);
        if (this.maxHealth > -1)
            entity.setMaxHealth(this.maxHealth);
        if (this.health > -1)
            entity.setHealth(this.health);
        entity.addPotionEffects(this.potions);
        return entity;
    }

    /**
     * Get an EntityBuilder by Entity
     *
     * @param entity
     *            Entity
     * @return EntityBuilder
     */
    public static EntityBuilder fromEntity(Entity entity) {
        Validate.notNull(entity, "Entity cannot be null");
        if (!(entity instanceof LivingEntity)) {
            System.out.println("The entity is the method fromEntity isn't a LivingEntity");
            return new EntityBuilder(entity.getType());
        }
        LivingEntity ent = (LivingEntity) entity;
        return new EntityBuilder(ent.getType()).name(ent.getCustomName()).helmet(ent.getEquipment().getHelmet())
                .chestplate(ent.getEquipment().getChestplate()).leggings(ent.getEquipment().getLeggings())
                .boots(ent.getEquipment().getBoots()).hand(ent.getEquipment().getItemInHand())
                .maxHealth(ent.getMaxHealth()).health(ent.getHealth());
    }

}
