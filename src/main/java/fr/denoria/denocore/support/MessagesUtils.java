package fr.denoria.denocore.support;

import fr.denoria.denocore.denoriaplayer.rank.Rank;

public class MessagesUtils {

    /**
     * Récupérer le prefix de Denoria.
     *
     * @return Préfix par défaut de Denoria
     */
    public static String getDenoriaPrefix() {
        return "§dDenoria §8❘ ";
    }

    public static String getFriendPrefix() {
        return "§aFriend §8❘ ";
    }

    public static String getSeparator() {
        return "§7§m-----------------------------------------";
    }

    public static String getBlackSeparator() {
        return "§8§m-----------------------------------------";
    }

    public static String getSeparatorWithText(String text) {
        return centerText("§7§m--------------" + text + "§7§m--------------");
    }

    /**
     * Récupérer le préfix rouge de Denoria
     *
     * @return Préfix Admin de Denoria
     */

    public static String sendPrefixAdmin(){
        return "§cDenoria §8❘ ";
    }

    public static String getRankSpace(Rank rank) {
        return " ";
    }

    /**
     * Centrer un texte dans le chat.
     *
     * @param text
     * @return
     */
    public static String centerText(String text) {
        int space = (int) Math.round((80.0D - 1.4D * text.length()) / 2.0D);
        return repeat(" ", space) + text;
    }

    /**
     * Utilitaire pour centrer un Texte dans le tchat.
     *
     * @param text text
     * @param times times
     * @return StringBuilder
     */
    private static String repeat(String text, int times) {
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < times; i++) {
            stringBuilder.append(text);
        }
        return stringBuilder.toString();
    }

}
