package fr.denoria.denocore.support.server;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;

import com.google.common.io.ByteArrayDataOutput;
import com.google.common.io.ByteStreams;



public class SwitchServer {

    /**
     * Envouer un Joueur sur une serveur de Jeu.
     *
     * @param player
     * @param needMap
     * @param mapName
     */
    public static void movePlayerOnServer(Player player, boolean needMap, String mapName) {

    }

    /**
     * Envoyer un Joueur sur un des Lobbys.
     *
     * @param player 's move into lobby info
     */
    public static void movePlayerOnServerLobbyInfos(Player player) {

    }

    /**
     * Envoyer un Joueur sur le Lobby.
     *
     * @param player Joueur envoyé sur le lobby
     * @param canKick Boolean (Kick if Lobby not found)
     */
    public static void sendPlayerToLobby(Player player, boolean canKick) {

    }

    /**
     * Changer le serveur d'un joueur.
     *
     * @param player player moved
     * @param serverName server name
     */
    public static void moveServer(Player player, String serverName, Plugin plugin) {
        Bukkit.getMessenger().registerOutgoingPluginChannel(plugin, "BungeeCord");
        ByteArrayDataOutput out = ByteStreams.newDataOutput();
        out.writeUTF("Connect");
        out.writeUTF(serverName);
        player.getPlayer().sendPluginMessage(plugin, "BungeeCord", out.toByteArray());
    }
}
