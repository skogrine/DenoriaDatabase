/*
 * Copyright (c) 2019 - 2021 Denoria Network
 *
 *  ______   _______  _        _______  _______ _________ _______             _______  _        _______  _______  _______ _________ _        _______
 * (  __  \ (  ____ \( (    /|(  ___  )(  ____ )\__   __/(  ___  )       /\  (  ____ \| \    /\(  ___  )(  ____ \(  ____ )\__   __/( (    /|(  ____ \
 * | (  \  )| (    \/|  \  ( || (   ) || (    )|   ) (   | (   ) |      / /  | (    \/|  \  / /| (   ) || (    \/| (    )|   ) (   |  \  ( || (    \/
 * | |   ) || (__    |   \ | || |   | || (____)|   | |   | (___) |     / /   | (_____ |  (_/ / | |   | || |      | (____)|   | |   |   \ | || (__
 * | |   | ||  __)   | (\ \) || |   | ||     __)   | |   |  ___  |    / /    (_____  )|   _ (  | |   | || | ____ |     __)   | |   | (\ \) ||  __)
 * | |   ) || (      | | \   || |   | || (\ (      | |   | (   ) |   / /           ) ||  ( \ \ | |   | || | \_  )| (\ (      | |   | | \   || (
 * | (__/  )| (____/\| )  \  || (___) || ) \ \_____) (___| )   ( |  / /      /\____) ||  /  \ \| (___) || (___) || ) \ \_____) (___| )  \  || (____/\
 * (______/ (_______/|/    )_)(_______)|/   \__/\_______/|/     \|  \/       \_______)|_/    \/(_______)(_______)|/   \__/\_______/|/    )_)(_______/
 *
 */

package fr.denoria.denocore.support.server;

import fr.denoria.denocore.bungeecord.channels.ChannelSMEnum;
import fr.denoria.denocore.redis.RedisManager;
import redis.clients.jedis.Jedis;

public class ServerInstance {

    public boolean isServerExist(String serverType){
        /*try (Jedis jedis = RedisManager.getRedisDatabase().getJedisPool().getResource()) {
            jedis.publish("serverExist", serverType);
        } catch (Exception e) {
            System.out.println("[RedisPubSubSM] Impossible de récupérer la données du serveur.");
        }*/
        return false;
    }

    public static void createServer(InstanceEnum instanceKey){
        try (Jedis jedis = RedisManager.getRedisDatabase().getJedisPool().getResource()) {
            jedis.publish(ChannelSMEnum.START_INSTANCE.getName(), instanceKey.getName());
        } catch (Exception e) {
            System.out.println("[RedisPubSubSM] Impossible de créer un serveur (" + instanceKey.getName() + ").");
        }
    }

    public static void stopServer(InstanceEnum instanceKey, int id) {
        try (Jedis jedis = RedisManager.getRedisDatabase().getJedisPool().getResource()) {
            jedis.publish(ChannelSMEnum.STOP_INSTANCE.getName(), instanceKey.getName() + "." + id);
        } catch (Exception e) {
            System.out.println("[RedisPubSubSM] Impossible de supprimé un serveur (" + instanceKey.getName() + "." + id + ").");
        }
    }


}
