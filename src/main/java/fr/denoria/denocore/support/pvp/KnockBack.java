/*
 * Copyright (c) 2019 - 2021 Denoria Network
 *
 *  ______   _______  _        _______  _______ _________ _______             _______  _        _______  _______  _______ _________ _        _______
 * (  __  \ (  ____ \( (    /|(  ___  )(  ____ )\__   __/(  ___  )       /\  (  ____ \| \    /\(  ___  )(  ____ \(  ____ )\__   __/( (    /|(  ____ \
 * | (  \  )| (    \/|  \  ( || (   ) || (    )|   ) (   | (   ) |      / /  | (    \/|  \  / /| (   ) || (    \/| (    )|   ) (   |  \  ( || (    \/
 * | |   ) || (__    |   \ | || |   | || (____)|   | |   | (___) |     / /   | (_____ |  (_/ / | |   | || |      | (____)|   | |   |   \ | || (__
 * | |   | ||  __)   | (\ \) || |   | ||     __)   | |   |  ___  |    / /    (_____  )|   _ (  | |   | || | ____ |     __)   | |   | (\ \) ||  __)
 * | |   ) || (      | | \   || |   | || (\ (      | |   | (   ) |   / /           ) ||  ( \ \ | |   | || | \_  )| (\ (      | |   | | \   || (
 * | (__/  )| (____/\| )  \  || (___) || ) \ \_____) (___| )   ( |  / /      /\____) ||  /  \ \| (___) || (___) || ) \ \_____) (___| )  \  || (____/\
 * (______/ (_______/|/    )_)(_______)|/   \__/\_______/|/     \|  \/       \_______)|_/    \/(_______)(_______)|/   \__/\_______/|/    )_)(_______/
 *
 */

package fr.denoria.denocore.support.pvp;

import net.minecraft.server.v1_8_R3.Packet;
import net.minecraft.server.v1_8_R3.PacketPlayOutEntityVelocity;
import net.minecraft.server.v1_8_R3.PlayerConnection;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.player.PlayerVelocityEvent;
import org.bukkit.util.Vector;

public class KnockBack implements Listener {

    @EventHandler
    public void onPlayerVelocity(PlayerVelocityEvent e) {
        Player player = e.getPlayer();
        EntityDamageEvent lastDamage = player.getLastDamageCause();
        if (lastDamage == null || !(lastDamage instanceof EntityDamageByEntityEvent))
            return;
        if (((EntityDamageByEntityEvent)lastDamage).getDamager() instanceof Player)
            e.setCancelled(true);
    }

    @EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
    public void onEntityDamageByEntity(EntityDamageByEntityEvent e){
        if (!(e.getEntity() instanceof Player) || !(e.getDamager() instanceof Player))
            return;
        if (e.isCancelled())
            return;
        Player damaged = (Player)e.getEntity();
        Player damager = (Player)e.getDamager();
        if (damaged.getNoDamageTicks() > damaged.getMaximumNoDamageTicks() / 2.0D)
            return;

        double horizontalMultiplier = 0.824D;
        double verticalMultiplier = 0.989D;
        double sprintMultiplier = damager.isSprinting() ? 0.896D : 0.8D;
        double kbMultiplier = (damager.getItemInHand() == null) ? 0.0D : (damager.getItemInHand().getEnchantmentLevel(Enchantment.KNOCKBACK) * 0.2D);
        double airMultiplier = damaged.isOnGround() ? 0.917D : 0.847D;
        Vector knockback = damaged.getLocation().toVector().subtract(damager.getLocation().toVector()).normalize();
        knockback.setX((knockback.getX() * sprintMultiplier + kbMultiplier) * horizontalMultiplier);
        knockback.setY(0.35D * airMultiplier * verticalMultiplier);
        knockback.setZ((knockback.getZ() * sprintMultiplier + kbMultiplier) * horizontalMultiplier);
        PlayerConnection playerConnection = (((CraftPlayer)damaged).getHandle()).playerConnection;
        playerConnection.sendPacket(new PacketPlayOutEntityVelocity(damaged.getEntityId(), knockback.getX(), knockback.getY(), knockback.getZ()));
    }

}
