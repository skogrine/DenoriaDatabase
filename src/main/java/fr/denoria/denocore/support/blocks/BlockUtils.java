package fr.denoria.denocore.support.blocks;

import fr.denoria.denocore.DenoCore;
import org.bukkit.Bukkit;
import org.bukkit.Chunk;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class BlockUtils {

    private static Map<Long, Chunk> loadedChunks = new HashMap<>();
    private static final long CHUNK_LOAD_TIME = 30000L;

    static {
        Bukkit.getScheduler().scheduleSyncRepeatingTask(DenoCore.getInstance(), () -> {
            Iterator<Map.Entry<Long, Chunk>> iterator = loadedChunks.entrySet().iterator();
            while (iterator.hasNext()){
                Map.Entry<Long, Chunk> chunk = iterator.next();
                if(chunk.getKey() >= System.currentTimeMillis()){
                    chunk.getValue().unload();
                    iterator.remove();
                }
            }
        }, 0, 20);
    }


    public static Block getBlockAt(Location location) {
        Chunk chunk = location.getChunk();
        if (!chunk.isLoaded()) {
            chunk.load();
            loadedChunks.put(System.currentTimeMillis() + CHUNK_LOAD_TIME, chunk);
        }
        return location.getBlock();
    }

    public static Block getLastBlockNoAir(Location l) {
        Block previousBlock = null;
        Location customLocation = l.clone();
        for (int y = 0; y < 255; y++) {
            customLocation.setY(y);
            if (previousBlock == null) {
                previousBlock = customLocation.getBlock();
            } else {
                if (customLocation.getBlock().getType() == Material.AIR) {
                    return previousBlock;
                } else {
                    previousBlock = customLocation.getBlock();
                }
            }
        }
        return null;
    }


    public static Block getHighestBlock(Location l) {
        Location customLocation = l.clone();
        for (int y = 255; y > 0; y--) {
            customLocation.setY(y);
            if (customLocation.getBlock().getType() != Material.AIR) {
                return customLocation.getBlock();
            }
        }
        return null;
    }

}
