package fr.denoria.denocore.mysql;

public class DatabaseCredentials {

    private final String host, user, pass, dbName;
    private final int port;


    public DatabaseCredentials(String host, String user, String pass, String dbName, int port) {
        this.host = host;
        this.user = user;
        this.pass = pass;
        this.dbName = dbName;
        this.port = port;
    }
    public String toURI(){
        return "jdbc:mysql://" +
                host +
                ":" +
                port +
                "/" +
                dbName +
                "?useUnicode=yes";
    }

    public int getPort() {
        return port;
    }

    public String getDbName() {
        return dbName;
    }

    public String getHost() {
        return host;
    }

    public String getPass() {
        return pass;
    }

    public String getUser() {
        return user;
    }

}
