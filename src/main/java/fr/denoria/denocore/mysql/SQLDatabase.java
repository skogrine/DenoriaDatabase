package fr.denoria.denocore.mysql;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;

import java.sql.Connection;
import java.sql.SQLException;

public class SQLDatabase {

    private final DatabaseCredentials credentials;
    private HikariDataSource hikariDataSource;

    public SQLDatabase(DatabaseCredentials credentials) {
        this.credentials = credentials;
    }

    public void setupHiaCP() {
        final HikariConfig hikariConfig = new HikariConfig();
        hikariConfig.setMaximumPoolSize(10);
        hikariConfig.setJdbcUrl(credentials.toURI());
        hikariConfig.setUsername(credentials.getUser());
        hikariConfig.setPassword(credentials.getPass());
        hikariConfig.setMaxLifetime(300000);
        hikariConfig.setIdleTimeout(600000L);
        hikariConfig.setLeakDetectionThreshold(60 * 5000);
        hikariConfig.setConnectionTimeout(120 * 1000);

        this.hikariDataSource = new HikariDataSource(hikariConfig);

        System.out.println("[SQLManager] Denoria vient de se connecter à la base de données.");

    }

    public void initPool(){
        setupHiaCP();
    }

    public void closePool(){
        this.hikariDataSource.close();
    }


    public void disconnect() {
        try {
            if (isConnected()) {
                this.hikariDataSource.close();
                System.out.println("[SQLManager] Denoria vient de se deconnecter à la base de données.");
            }
        } catch (Exception e) {
            System.out.println("[DatabaseSQL] Erreur: impossible de se déconnecter à la base de données.");
        }
    }

    public Connection getRessource() throws SQLException {
        if(this.hikariDataSource == null){
            System.out.println("[SQLManager] Impossible de récupérer la base de données.");
            setupHiaCP();
        }
        return this.hikariDataSource.getConnection();
    }

    public boolean isConnected() {
        return hikariDataSource != null && !hikariDataSource.isClosed();
    }


    public HikariDataSource getHikariDataSource() {
        return hikariDataSource;
    }


}
