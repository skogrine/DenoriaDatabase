package fr.denoria.denocore.mysql;

public enum SQLManager {

    DENORIA_SERVER("server", new DatabaseCredentials("141.94.124.78", "server", "I375Uc4wEQL8u6h", "denoriaServer", 3306));

    String name;
    private final SQLDatabase databaseAccess;


    SQLManager(String name, DatabaseCredentials credentials) {
       this.name = name;
       this.databaseAccess = new SQLDatabase(credentials);

    }

    public static void initAllDatabaseConnection() {
        for(SQLManager databaseManager : values()) {
            databaseManager.databaseAccess.initPool();
            System.out.println("La base de donnée " + databaseManager.name.toUpperCase() + " à bien été initié !");
        }

    }

    public static void closeAllDatabaseConnection() {
        for(SQLManager databaseManager : values()) {
            databaseManager.databaseAccess.closePool();
            System.out.println("La base de donnée " + databaseManager.name.toUpperCase() + " à bien été fermé !");
        }
    }

    public String getName() {
        return name;
    }

    public SQLDatabase getDatabaseAccess() {
        return databaseAccess;
    }
}
