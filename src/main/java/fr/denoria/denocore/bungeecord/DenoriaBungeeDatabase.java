package fr.denoria.denocore.bungeecord;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import com.google.common.io.ByteStreams;
import com.imaginarycode.minecraft.redisbungee.RedisBungee;
import com.imaginarycode.minecraft.redisbungee.RedisBungeeAPI;

import fr.denoria.denocore.bungeecord.channels.ChannelEnum;
import fr.denoria.denocore.bungeecord.channels.ChannelListener;
import fr.denoria.denocore.bungeecord.channels.ChannelSMEnum;
import fr.denoria.denocore.bungeecord.commands.*;
import fr.denoria.denocore.bungeecord.commands.party.PartyCommand;
import fr.denoria.denocore.bungeecord.listeners.HandshakeFix;
import fr.denoria.denocore.bungeecord.listeners.PostLoginListener;
import fr.denoria.denocore.bungeecord.listeners.ProxiedPlayerDisconnectListener;
import fr.denoria.denocore.bungeecord.listeners.ProxyListPingListener;
import fr.denoria.denocore.mysql.SQLManager;
import fr.denoria.denocore.network.infos.NetworkAPI;
import fr.denoria.denocore.redis.RedisManager;
import fr.denoria.denocore.support.MessagesUtils;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.plugin.Plugin;
import net.md_5.bungee.api.plugin.PluginManager;
import net.md_5.bungee.config.Configuration;
import net.md_5.bungee.config.ConfigurationProvider;
import net.md_5.bungee.config.YamlConfiguration;


public class DenoriaBungeeDatabase extends Plugin {

    // Base de données.
    public Configuration configuration;
    public boolean useSQL, useRedis;
    public SQLManager sqlManager;
    public RedisManager redisManager;

    // Gestion des MultiBungees.
    private static RedisBungeeAPI redisBungeeAPI;

    // Utilisation des Maps.
    public static Map<String, IBungeePlayer> iBungeePlayerMap = new HashMap<>();

    // Instance du plugin Bungee.
    public static DenoriaBungeeDatabase INSTANCE;

    @Override
    public void onLoad() {

        // Gestion de MySQL.
        SQLManager.initAllDatabaseConnection();
        // Gestion de Redis.
        redisManager = new RedisManager(true);
        redisManager.connect();


        // Gestion de l'instance du plugin.
        INSTANCE = this;

        boolean newConfig = false;
        if (!getDataFolder().exists()) {
            getDataFolder().mkdir();
        }
        File configFile = new File(getDataFolder(), "config.yml");
        if (!configFile.exists()) {
            try {
                configFile.createNewFile();
                try (InputStream is = getResourceAsStream("config.yml");
                     OutputStream os = new FileOutputStream(configFile)) {
                    ByteStreams.copy(is, os);
                }
                newConfig = true;

            } catch (IOException e) {
                throw new RuntimeException("Unable to create configuration file", e);
            }
        }
        try {
            configuration = ConfigurationProvider.getProvider(YamlConfiguration.class).load(configFile);
        } catch (IOException e) {
            e.printStackTrace();
        }

        if (newConfig) {
            // Gestion de la version SQL.
            configuration.set("denoriaSQL.enabled", false);
            configuration.set("denoriaSQL.host", "46.4.213.90");
            configuration.set("denoriaSQL.database", "denoriaServer");
            configuration.set("denoriaSQL.user", "server");
            configuration.set("denoriaSQL.password", "");
            configuration.set("denoriaSQL.port", 3306);

            // Gestion de la version Redis.
            configuration.set("denoriaRedis.enabled", false);
            configuration.set("denoriaRedis.host", "46.4.213.90");
            configuration.set("denoriaRedis.password", "");
            configuration.set("denoriaRedis.port", 6379);
            saveConfig();
        }



        super.onLoad();
    }

    @Override
    public void onEnable() {

        // Gestion des ChannelsRedis.
        redisBungeeAPI = RedisBungee.getApi();
        List<String> channelList = new ArrayList<>();
        List<String> channelListSM = new ArrayList<>();
        for (ChannelEnum channelEnum : ChannelEnum.values()) {
            channelList.add(channelEnum.getName());
            ProxyServer.getInstance().registerChannel(channelEnum.getName());
            System.out.println("[REDIS_PUB_SUB] Register '" + channelEnum.getName() + "' channel.");
        }
        for (ChannelSMEnum channelSMEnum : ChannelSMEnum.values()) {
            channelListSM.add(channelSMEnum.getName());
            ProxyServer.getInstance().registerChannel(channelSMEnum.getName());
            System.out.println("[REDIS_PUB_SUB_SM] Register '" + channelSMEnum.getName() + "' channel.");
        }
        String[] channelArray = channelList.toArray(new String[0]);
        String[] channelSMArray = channelListSM.toArray(new String[0]);
        //redisBungeeAPI.registerPubSubChannels(channelArray);
        //redisBungeeAPI.registerPubSubChannels(channelSMArray);

        // Gestion du Network et ses Informations.
        NetworkAPI.loadNetworkInfo();
        ProxyServer.getInstance().getScheduler().schedule(this, NetworkAPI::loadNetworkInfo, 60, 60, TimeUnit.SECONDS);

        // Gestion des PluginManager.
        PluginManager pluginManager = ProxyServer.getInstance().getPluginManager();

        // Gestion des Listeners.
        pluginManager.registerListener(this, new PostLoginListener());
        pluginManager.registerListener(this, new ProxiedPlayerDisconnectListener());
        pluginManager.registerListener(this, new ProxyListPingListener());
        pluginManager.registerListener(this, new ChannelListener());

        //Pactify/AZ-Laucher
        pluginManager.registerListener(this, new HandshakeFix());

        // Gestion des Commandes.
        pluginManager.registerCommand(this, new StaffChatCommand());
        pluginManager.registerCommand(this, new StaffCommand());
        pluginManager.registerCommand(this, new PartyCommand());
        pluginManager.registerCommand(this, new BroadcastCommand());
        pluginManager.registerCommand(this, new ListCommand());
        pluginManager.registerCommand(this, new MaintenanceCommand());
        pluginManager.registerCommand(this, new ServerCommand());

        pluginManager.registerCommand(this, new LinkCommand());

        pluginManager.registerCommand(this, new LookUpCommand());

        // Annonce.

        ProxyServer.getInstance().getScheduler().schedule(this, () -> {
            ProxyServer.getInstance().broadcast(" ");
            ProxyServer.getInstance().broadcast(MessagesUtils.getDenoriaPrefix() + "§fSignaler un bug sur §abug.denoria.fr");
            ProxyServer.getInstance().broadcast(" ");
        }, 0, 15, TimeUnit.MINUTES);

        super.onEnable();
    }


    private String buildLinkUrl() {
        String url = "";
        if (getConfig().getBoolean("full-friendly-urls-enabled")) {
            url = getConfig().getString("website-url") + "/link-minecraft/?";
        } else {
            url = getConfig().getString("website-url") + "/index.php?link-minecraft/&";
        }
        return url;
    }

    @Override
    public void onDisable() {

        // Gestion des bases de données.
        SQLManager.closeAllDatabaseConnection();


        redisManager.disconnect();


        // Gestion des ChannelsRedis.
        List<String> channelList = new ArrayList<>();
        for (ChannelEnum channelEnum : ChannelEnum.values()) {
            channelList.add(channelEnum.getName());
            ProxyServer.getInstance().unregisterChannel(channelEnum.getName());
        }
        String[] channelArray = channelList.toArray(new String[0]);
        redisBungeeAPI.unregisterPubSubChannels(channelArray);
        redisBungeeAPI = null;
        super.onDisable();
    }

    /**
     * Sauvegarder le fichier de Configuration.
     */
    private void saveConfig() {
        try {
            ConfigurationProvider.getProvider(YamlConfiguration.class).save(configuration,
                    new File(getDataFolder(), "config.yml"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Récupérer l'API de RedisBungee.
     *
     * @return instance
     */
    public static RedisBungeeAPI getRedisBungeeAPI() {
        return redisBungeeAPI;
    }

    public Configuration getConfig() {
        return configuration;
    }
}
