package fr.denoria.denocore.bungeecord.listeners;

import fr.denoria.denocore.bungeecord.DenoriaBungeeDatabase;
import fr.denoria.denocore.bungeecord.IBungeePlayer;
import fr.denoria.denocore.denoriaplayer.DenoriaPlayerInfo;
import fr.denoria.denocore.denoriaplayer.rank.Rank;
import fr.denoria.denocore.network.infos.NetworkAPI;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.event.PostLoginEvent;
import net.md_5.bungee.api.event.ServerConnectedEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;
import net.md_5.bungee.event.EventPriority;


public class PostLoginListener implements Listener {

    @EventHandler(priority = EventPriority.LOWEST)
    public void onPlayerPostLogin(PostLoginEvent event) {
        ProxiedPlayer proxiedPlayer = event.getPlayer();
        IBungeePlayer iBungeePlayer = IBungeePlayer.getIBungeePlayer(proxiedPlayer);
        this.checkInfoAccess(proxiedPlayer);
        ProxyServer.getInstance().getScheduler().runAsync(DenoriaBungeeDatabase.INSTANCE, ()-> {

            // Joueur est banni.
            /*
            if (IBan.isBan(proxiedPlayer.getName())) {
                String reason = IBan.getBanRaison(proxiedPlayer.getName());
                if (IBan.isBanPermanent(proxiedPlayer.getName())) {
                    proxiedPlayer.disconnect(new TextComponent("" + "§c§lDenoria Network"
                            + "\n§fVous avez été banni permanentement." + "\n§fRaison: §e§l" + reason
                            + "\n§b\n§fVous pensez que ceci est une erreur ?" + "\n§fTeamSpeak : §cts.denoria.fr"));
                } else {
                    proxiedPlayer.disconnect(new TextComponent("" + "§c§lDenoria Network"
                            + "\n§fVous avez été banni temporairement..." + "\n§fRaison: §e§l" + reason
                            + "\n§d\n§fExpiration: §b§l" + IBan.getBanDuree(proxiedPlayer.getName())
                            + "\n§b\n§fVous pensez que ceci est une erreur ?" + "\n§fTeamSpeak : §cts.denoria.fr"));
                }
            }*/

            // Détection de Connexions.
            DenoriaPlayerInfo.setPlayerLoginSecurity(proxiedPlayer.getUniqueId());



            // Message de bienvenue.
            iBungeePlayer.sendWelcomeMessage();
        });
    }

    @EventHandler(priority = EventPriority.LOWEST)
    public void onServerSwitch(ServerConnectedEvent event) {

    }

    /**
     * Charger les load access défaut de connexions.
     *
     * @param proxiedPlayer
     */
    public void checkInfoAccess(ProxiedPlayer proxiedPlayer) {

        // Génération du BungeePlayer.
        IBungeePlayer iBungeePlayer = IBungeePlayer.getIBungeePlayer(proxiedPlayer);

        // Maintenance du Network.
        if (NetworkAPI.isMaintenance()) {
            if (iBungeePlayer.getRank().getPower() < NetworkAPI.getRankAllowMaintenance().getPower() && !(iBungeePlayer.getRankspecial().getId() > 0)) {
                proxiedPlayer.disconnect(new TextComponent("" + "§4§l■ §cMaintenance §4§l■" + "\n§e\n"
                        + "§6Le serveur est actuellement en maintenance..."));

            }
        }

        // Accès des grades VIP.
        if ((DenoriaBungeeDatabase.getRedisBungeeAPI().getPlayerCount() >= NetworkAPI.getSlots())) {
            if (iBungeePlayer.getRank().getPower() < Rank.VIP.getPower()) {
                proxiedPlayer.disconnect(new TextComponent("§6§lDenoria Network§f│ §cConnexion impossible\n"
                        + "§eLe serveur est plein ! Vous devez être au minimum §aVIP §epour entrer.\n§b\n"
                        + "§fBoutique: §6https://shop.denoria.fr/"));
            }
        }


    }
}
