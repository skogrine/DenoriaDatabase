package fr.denoria.denocore.bungeecord.listeners;

import fr.denoria.denocore.bungeecord.DenoriaBungeeDatabase;
import fr.denoria.denocore.network.infos.NetworkAPI;
import fr.denoria.denocore.support.MessagesUtils;
import net.md_5.bungee.api.ServerPing;
import net.md_5.bungee.api.event.ProxyPingEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;
import net.md_5.bungee.event.EventPriority;

public class ProxyListPingListener implements Listener {

    private String line1, line2;

    @SuppressWarnings("deprecation")
    @EventHandler(priority = EventPriority.LOWEST)
    public void onProxyPing(final ProxyPingEvent event) {

        final ServerPing sp = event.getResponse();
        if (sp == null) {
            return;
        }

        line1 = MessagesUtils.centerText("§d§lDenoria Network§f│ §bServeur Mini-Jeux §a1.8 §f➡ §a1.12");

        // Définir le MOTD si le serveur est accès VIP.
        if (DenoriaBungeeDatabase.getRedisBungeeAPI().getPlayerCount() >= NetworkAPI.getSlots()) {
            line2 = MessagesUtils.getDenoriaPrefix() + "§cSeul les VIP peuvent se connecter !";
        } else {
            line2 = MessagesUtils
                    .centerText(NetworkAPI.getMotdLine()
                            .replace('&', '§')
                            .replace(">>", "»")
                            .replace("%heart%", "❤")
                            .replace("%valid%", "✔"));
        }

        /* SET INFORMATIONS */
        sp.setDescription(line1 + "\n" + line2);
        ServerPing.Protocol vers = sp.getVersion();
        vers.setName("Denoria Network (1.8-1.17)");
        sp.setPlayers(
                new ServerPing.Players(NetworkAPI.getSlots(), DenoriaBungeeDatabase.getRedisBungeeAPI().getPlayerCount(),
                        event.getResponse().getPlayers().getSample()));
        event.setResponse(sp);

    }
}
