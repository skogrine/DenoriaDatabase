package fr.denoria.denocore.bungeecord.listeners;

import java.util.UUID;

import fr.denoria.denocore.bungeecord.DenoriaBungeeDatabase;
import fr.denoria.denocore.bungeecord.IBungeePlayer;
import fr.denoria.denocore.denoriaplayer.DenoriaPlayerInfo;
import fr.denoria.denocore.denoriaplayer.currency.Currency;
import fr.denoria.denocore.denoriaplayer.currency.ICurrency;
import fr.denoria.denocore.denoriaplayer.infos.DataType;
import fr.denoria.denocore.denoriaplayer.infos.IData;
import fr.denoria.denocore.denoriaplayer.level.ILevelExperience;
import fr.denoria.denocore.denoriaplayer.level.LevelType;
import fr.denoria.denocore.redis.RedisManager;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.event.PlayerDisconnectEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;
import net.md_5.bungee.event.EventPriority;
import redis.clients.jedis.Jedis;

public class ProxiedPlayerDisconnectListener implements Listener {

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onPlayerDisconnect(PlayerDisconnectEvent event) {
        ProxiedPlayer proxiedPlayer = event.getPlayer();
        IBungeePlayer iBungeePlayer = IBungeePlayer.getIBungeePlayer(proxiedPlayer);
        UUID uuid = iBungeePlayer.getUUID();
        DenoriaPlayerInfo.removeKey(uuid);

        /**
         * Gestion des caches des Joueurs.
         */
        /** GESTION DES CURRENCY */
        for (Currency currency : Currency.values()) {
            Integer iCoins = 0;
            try (Jedis jedis = RedisManager.getJedisPool().getResource()) {
                if (jedis.exists("currency:" + uuid.toString() + "_" + currency.getName())) {
                    iCoins = Integer.parseInt(jedis.get("currency:" + uuid.toString() + "_" + currency.getName()));
                    jedis.del("currency:" + uuid.toString() + "_" + currency.getName());
                    ICurrency.setCoins(uuid, currency, iCoins);
                }
            } catch (Exception e) {
                System.out.println("[IPlayer] Impossible de mettre à jours les Coins du Joueur.");
            }
        }

        /** GESTION DES DATAS */
        for (DataType dataType : DataType.values()) {
            Object object = false;
            try (Jedis jedis = RedisManager.getJedisPool().getResource()) {
                if (jedis.exists("dataplayerinfos:" + uuid.toString() + "_" + dataType.getName())) {
                    object = (boolean) Boolean
                            .parseBoolean(jedis.get("dataplayerinfos:" + uuid.toString() + "_" + dataType.getName()));
                    jedis.del("dataplayerinfos:" + uuid.toString() + "_" + dataType.getName());
                    IData.setPlayerData(uuid, dataType, object);
                }
            } catch (Exception e) {
                System.out.println("[IPlayer] Impossible de mettre à jours les informations du Joueur.");
            }
        }

        /** GESTION DES LEVEL */
        for (LevelType levelType : LevelType.values()) {
            Integer iLevel = 0;
            try (Jedis jedis = RedisManager.getJedisPool().getResource()) {
                if (jedis.exists("levelexperience:" + uuid.toString() + "_" + levelType.getName())) {
                    iLevel = Integer
                            .parseInt(jedis.get("levelexperience:" + uuid.toString() + "_" + levelType.getName()));
                    jedis.del("levelexperience:" + uuid.toString() + "_" + levelType.getName());
                    ILevelExperience.setExperienceLevel(uuid, levelType, iLevel);
                }
            } catch (Exception e) {
                System.out.println("[IPlayer] Impossible de mettre à jour les Experiences du Joueur.");
            }
        }

        /** GESTION DU GOLEMAPLAYERINFO */
        if (DenoriaBungeeDatabase.iBungeePlayerMap.containsKey(proxiedPlayer.getName()))
            DenoriaBungeeDatabase.iBungeePlayerMap.remove(proxiedPlayer.getName());
    }
}
