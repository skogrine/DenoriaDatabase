/*
 * Copyright (c) 2019 - 2021 Denoria Network
 *
 *  ______   _______  _        _______  _______ _________ _______             _______  _        _______  _______  _______ _________ _        _______
 * (  __  \ (  ____ \( (    /|(  ___  )(  ____ )\__   __/(  ___  )       /\  (  ____ \| \    /\(  ___  )(  ____ \(  ____ )\__   __/( (    /|(  ____ \
 * | (  \  )| (    \/|  \  ( || (   ) || (    )|   ) (   | (   ) |      / /  | (    \/|  \  / /| (   ) || (    \/| (    )|   ) (   |  \  ( || (    \/
 * | |   ) || (__    |   \ | || |   | || (____)|   | |   | (___) |     / /   | (_____ |  (_/ / | |   | || |      | (____)|   | |   |   \ | || (__
 * | |   | ||  __)   | (\ \) || |   | ||     __)   | |   |  ___  |    / /    (_____  )|   _ (  | |   | || | ____ |     __)   | |   | (\ \) ||  __)
 * | |   ) || (      | | \   || |   | || (\ (      | |   | (   ) |   / /           ) ||  ( \ \ | |   | || | \_  )| (\ (      | |   | | \   || (
 * | (__/  )| (____/\| )  \  || (___) || ) \ \_____) (___| )   ( |  / /      /\____) ||  /  \ \| (___) || (___) || ) \ \_____) (___| )  \  || (____/\
 * (______/ (_______/|/    )_)(_______)|/   \__/\_______/|/     \|  \/       \_______)|_/    \/(_______)(_______)|/   \__/\_______/|/    )_)(_______/
 *
 */

package fr.denoria.denocore.bungeecord.commands;

import fr.denoria.denocore.bungeecord.IBungeePlayer;
import fr.denoria.denocore.denoriaplayer.discord.ILink;
import fr.denoria.denocore.support.MessagesUtils;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class LinkCommand extends Command {
    public LinkCommand() {
        super("link");
    }

    @Override
    public void execute(CommandSender sender, String[] args) {
        if (sender instanceof ProxiedPlayer) {
            ProxiedPlayer player = (ProxiedPlayer) sender;
            IBungeePlayer denoPlayer = new IBungeePlayer(player);
            if (!ILink.isLink(denoPlayer)) {
                if (args.length == 0) {
                    ILink.createAccount(denoPlayer);
                    player.sendMessage(new TextComponent(MessagesUtils.getDenoriaPrefix() + "§7Votre compte a été enregistré !"));
                } else {
                    player.sendMessage(new TextComponent(MessagesUtils.getDenoriaPrefix() + "§cErreur: §7Votre mot de passe doit faire au minimum 5 §7caractères !"));
                }
            } else {
                player.sendMessage(new TextComponent(MessagesUtils.getDenoriaPrefix() + "§cErreur: §7Votre compte a déjà été enregistré !"));
            }
    }
}


    public String byteToHex(byte[] hash) {
        StringBuffer buffer = new StringBuffer();
        for (byte b : hash) {
            String hex = Integer.toHexString(0xFF & b);
            if (hex.length() == 1)
                buffer.append('0');
            buffer.append(hex);
        }
        return buffer.toString();
    }
}
