package fr.denoria.denocore.bungeecord.commands;

import fr.denoria.denocore.bungeecord.IBungeePlayer;
import fr.denoria.denocore.denoriaplayer.DenoriaPlayer;
import fr.denoria.denocore.denoriaplayer.rank.Rank;
import net.md_5.bungee.BungeeCord;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

import java.util.*;

public class StaffCommand extends Command {
    public StaffCommand() {
        super("staff", null);
    }

    @Override
    public void execute(CommandSender sender, String[] args) {
        ProxiedPlayer proxiedPlayer = (ProxiedPlayer) sender;
        IBungeePlayer iBungeePlayer = IBungeePlayer.getIBungeePlayer(proxiedPlayer);

        if (iBungeePlayer.getRank().getPower() < Rank.STAFF.getPower()) {
            iBungeePlayer.sendMessageNoPermission();
        } else {





            proxiedPlayer.sendMessage(new TextComponent("§8§m-----------------------------------"));
            proxiedPlayer.sendMessage(new TextComponent("§c§lDenoria §7(Liste du staff - 0)"));
            proxiedPlayer.sendMessage(new TextComponent(" "));

            HashMap map = new HashMap();
            Comparateur com = new Comparateur(map);
            TreeMap map_trier = new TreeMap(com);
            Map<Integer, IBungeePlayer> dplayers = new HashMap<>();

            for (ProxiedPlayer players : BungeeCord.getInstance().getPlayers()) {
                IBungeePlayer playersList = new IBungeePlayer(players);
                if (playersList.getRank().getPower() > Rank.STAFF.getPower()) {
                    map.put(playersList, playersList.getRank().getPower());
                }

                map_trier.putAll(map);

                String actif = "§8(§a○§8) ";
                String innactif = "§8(§c○§8) ";

                proxiedPlayer.sendMessage(playersList.getRank().getChatColor()
                        + playersList.getRank().getPrefix()
                        + " " + playersList.getProxiedPlayer().getName()
                        + " §8❘ §7" + (playersList.getRank().getPower().equals(Rank.ADMINISTRATOR.getPower())
                        ? "Perdu dans l'univers..."
                        : playersList.getProxiedPlayer().getServer().getInfo().getName())
                );
            }

            proxiedPlayer.sendMessage(new TextComponent(" "));
            proxiedPlayer.sendMessage(new TextComponent("§8§m-----------------------------------"));

        }
    }

    static class Comparateur implements Comparator {

        Map tuple;
        public Comparateur(HashMap map) {
            this.tuple = map;
        }

        @Override
        public int compare(Object o1, Object o2) {
            // TODO Auto-generated method stub
            if ((int) tuple.get(o1) >= (int) tuple.get(o2)) {
                return -1;
            } else {
                return 1;
            }
        }
    }

}
