package fr.denoria.denocore.bungeecord.commands;

import fr.denoria.denocore.bungeecord.DenoriaBungeeDatabase;
import fr.denoria.denocore.bungeecord.IBungeePlayer;
import fr.denoria.denocore.bungeecord.channels.ChannelEnum;
import fr.denoria.denocore.denoriaplayer.rank.Rank;
import fr.denoria.denocore.support.MessagesUtils;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.Objects;

public class BroadcastCommand extends Command {

    public final String PREFIX = MessagesUtils.getDenoriaPrefix();;

    public BroadcastCommand() {
        super("broadcast", null, "bc");
    }

    @SuppressWarnings("deprecation")
    @Override
    public void execute(CommandSender sender, String[] args) {

        ProxiedPlayer proxiedPlayer = (ProxiedPlayer) sender;
        IBungeePlayer iBungeePlayer = IBungeePlayer.getIBungeePlayer(proxiedPlayer);

        // Vérifier si le compte du Joueur existe.
        if (iBungeePlayer == null) {
            proxiedPlayer.sendMessage(MessagesUtils.getDenoriaPrefix() + ChatColor.RED
                    + "Erreur: Votre compte rencontre un problème contactez un Admin.");
            return;
        }

        // Verifier la permission du Joueur.
        if (iBungeePlayer.getRank().getPower() < Rank.ADMINISTRATOR.getPower()) {
            iBungeePlayer.sendMessageNoPermission();
            return;
        }

        // Vérification des arguments.
        if (args.length == 0) {
            proxiedPlayer.sendMessage(PREFIX + ChatColor.RED + "Erreur: §7Sélectionner un mode d'annonce.");
            proxiedPlayer.sendMessage(MessagesUtils.centerText(" §7(Annonce/Libre)"));
            return;
        }



        if (args.length >= 2) {
            String msg = null;

            StringBuilder message = new StringBuilder();
            for (String arg : args) {
                msg = message.append(" ").append(arg).toString();
            }


            if (!args[0].equals("Annonce") || !args[0].equals("Libre")) {
                proxiedPlayer.sendMessage(PREFIX + ChatColor.RED + "Erreur: §7Sélectionner un mode d'annonce.");
                proxiedPlayer.sendMessage(MessagesUtils.centerText(" §7(Annonce/Libre)"));
            } else if (args[0].equalsIgnoreCase("l") || args[0].equalsIgnoreCase("libre")) {
                msg.split("(^a )|(^l )");
                DenoriaBungeeDatabase.getRedisBungeeAPI().sendChannelMessage(ChannelEnum.CHANNEL_BROADCAST.getName(),
                        ChatColor.WHITE + ChatColor.translateAlternateColorCodes('&', msg));
            } else if (args[0].equalsIgnoreCase("a") || args[0].equalsIgnoreCase("annonce")) {
                // Gestion de la commande.
                msg.split("(^a )|(^l )");
                DenoriaBungeeDatabase.getRedisBungeeAPI().sendChannelMessage(ChannelEnum.CHANNEL_BROADCAST.getName(),
                        PREFIX + ChatColor.WHITE + ChatColor.translateAlternateColorCodes('&', msg));
            } else {
                proxiedPlayer.sendMessage(PREFIX + ChatColor.RED + "Erreur: §7Sélectionner un mode d'annonce.");
                proxiedPlayer.sendMessage(MessagesUtils.centerText("§7(Annonce/Libre)"));
                return;
            }
        }

    }
}
