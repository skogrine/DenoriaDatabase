/*
 * Copyright (c) 2019 - 2021 Denoria Network
 *
 *  ______   _______  _        _______  _______ _________ _______             _______  _        _______  _______  _______ _________ _        _______
 * (  __  \ (  ____ \( (    /|(  ___  )(  ____ )\__   __/(  ___  )       /\  (  ____ \| \    /\(  ___  )(  ____ \(  ____ )\__   __/( (    /|(  ____ \
 * | (  \  )| (    \/|  \  ( || (   ) || (    )|   ) (   | (   ) |      / /  | (    \/|  \  / /| (   ) || (    \/| (    )|   ) (   |  \  ( || (    \/
 * | |   ) || (__    |   \ | || |   | || (____)|   | |   | (___) |     / /   | (_____ |  (_/ / | |   | || |      | (____)|   | |   |   \ | || (__
 * | |   | ||  __)   | (\ \) || |   | ||     __)   | |   |  ___  |    / /    (_____  )|   _ (  | |   | || | ____ |     __)   | |   | (\ \) ||  __)
 * | |   ) || (      | | \   || |   | || (\ (      | |   | (   ) |   / /           ) ||  ( \ \ | |   | || | \_  )| (\ (      | |   | | \   || (
 * | (__/  )| (____/\| )  \  || (___) || ) \ \_____) (___| )   ( |  / /      /\____) ||  /  \ \| (___) || (___) || ) \ \_____) (___| )  \  || (____/\
 * (______/ (_______/|/    )_)(_______)|/   \__/\_______/|/     \|  \/       \_______)|_/    \/(_______)(_______)|/   \__/\_______/|/    )_)(_______/
 *
 */

package fr.denoria.denocore.bungeecord.commands;

import fr.denoria.denocore.bungeecord.IBungeePlayer;
import fr.denoria.denocore.commands.nick.NickManager;
import fr.denoria.denocore.denoriaplayer.UUIDFetcher;
import fr.denoria.denocore.denoriaplayer.rank.Rank;
import fr.denoria.denocore.support.MessagesUtils;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

public class LookUpCommand extends Command {


    public LookUpCommand() {
        super("lookup", null);
    }

    @Override
    public void execute(CommandSender sender, String[] args) {

        if(sender instanceof ProxiedPlayer) {
            ProxiedPlayer player = (ProxiedPlayer) sender;
            if(args.length == 1) {
                ProxiedPlayer target = ProxyServer.getInstance().getPlayer(args[0]);
                IBungeePlayer bungeePlayer = IBungeePlayer.getIBungeePlayer(player);

                if(bungeePlayer.getRank().getPower() >= Rank.RESPONSABLE.getPower()) {

                    if(target != null) {
                        IBungeePlayer bungeeTarget = IBungeePlayer.getIBungeePlayer(target);

                        player.sendMessage(MessagesUtils.getSeparatorWithText("§6" + target.getName()));
                        player.sendMessage("§7Unique ID §8» §7" + (target.isConnected() ? target.getUniqueId().toString() : UUIDFetcher.getUUID(args[0])));
                        player.sendMessage("§7NickName §8» " + (NickManager.isNick(target.getUniqueId())
                                ? "§a"+NickManager.getNickName(target.getUniqueId()) : "§eAucun"));
                        player.sendMessage("§7Groupe principal §8» " +
                                bungeeTarget.getRank().getChatColor() + bungeeTarget.getRank().getPrefix());
                        player.sendMessage("§7Serveur §8» " + (target.isConnected() ? "§a"+target.getServer().getInfo().getName() : "§cDéconnecté"));
                        player.sendMessage("§7Proxy §8» §aProxy-1");
                        player.sendMessage("§7Sanction §8» §cDésactivé");
                        player.sendMessage(MessagesUtils.getSeparator());
                    }else {
                        player.sendMessage(MessagesUtils.getDenoriaPrefix() + "§cErreur: §7Ce joueur est hors-ligne !");
                        return;
                    }
                }
                bungeePlayer.sendMessageNoPermission();
                return;
            }
        }

    }
}
