/*
 * Copyright (c) 2019 - 2021 Denoria Network
 *
 *  ______   _______  _        _______  _______ _________ _______             _______  _        _______  _______  _______ _________ _        _______
 * (  __  \ (  ____ \( (    /|(  ___  )(  ____ )\__   __/(  ___  )       /\  (  ____ \| \    /\(  ___  )(  ____ \(  ____ )\__   __/( (    /|(  ____ \
 * | (  \  )| (    \/|  \  ( || (   ) || (    )|   ) (   | (   ) |      / /  | (    \/|  \  / /| (   ) || (    \/| (    )|   ) (   |  \  ( || (    \/
 * | |   ) || (__    |   \ | || |   | || (____)|   | |   | (___) |     / /   | (_____ |  (_/ / | |   | || |      | (____)|   | |   |   \ | || (__
 * | |   | ||  __)   | (\ \) || |   | ||     __)   | |   |  ___  |    / /    (_____  )|   _ (  | |   | || | ____ |     __)   | |   | (\ \) ||  __)
 * | |   ) || (      | | \   || |   | || (\ (      | |   | (   ) |   / /           ) ||  ( \ \ | |   | || | \_  )| (\ (      | |   | | \   || (
 * | (__/  )| (____/\| )  \  || (___) || ) \ \_____) (___| )   ( |  / /      /\____) ||  /  \ \| (___) || (___) || ) \ \_____) (___| )  \  || (____/\
 * (______/ (_______/|/    )_)(_______)|/   \__/\_______/|/     \|  \/       \_______)|_/    \/(_______)(_______)|/   \__/\_______/|/    )_)(_______/
 *
 */

package fr.denoria.denocore.bungeecord.commands.party;

import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.connection.ProxiedPlayer;

public class PartyManager {

    public static void createParty(String p) {
        ProxiedPlayer m = ProxyServer.getInstance().getPlayer(p);
        if (PartyUtils.players.containsKey(p)) {
            m.sendMessage(PartyCommand.getPREFIXGROUPE() + "§cErreur: §7Vous êtes déjà dans un groupe.");
            return;
        }
        if (PartyUtils.lead.containsKey(p)) {
            m.sendMessage(PartyCommand.getPREFIXGROUPE() + "§cErreur: §7Vous êtes déjà le chef d'un groupe.");
            return;
        }
        PartyUtils.i++;
        PartyUtils.lead.put(p, PartyUtils.i);
        m.sendMessage(PartyCommand.getPREFIXGROUPE() + "Vous avez crée une party !");
    }

    public static void inviteParty(String p, String p1) {
        ProxiedPlayer m = ProxyServer.getInstance().getPlayer(p);
        ProxiedPlayer m1 = ProxyServer.getInstance().getPlayer(p1);

        if (!PartyUtils.lead.containsKey(p))
            m.sendMessage(PartyCommand.getPREFIXGROUPE() + "§cErreur: §7Pour inviter un joueur dans votre party, faite /p create");

        if (PartyUtils.lead.containsKey(p1) || PartyUtils.players.containsKey(p1)) {
            m.sendMessage(PartyCommand.getPREFIXGROUPE() + "§cErreur: §7Le joueur " + p1 + " est déjà dans un groupe.");
            return;
        }

        if(PartyUtils.players.containsKey(p)) {
            m.sendMessage(PartyCommand.getPREFIXGROUPE() + "§cErreur: §7Vous êtes le chef du groupe !");
            return;
        }
        PartyUtils.invite.put(p, p1);
        m.sendMessage(PartyCommand.getPREFIXGROUPE() + "Une invitation a été envoyé à " + p1);
        m1.sendMessage(PartyCommand.getPREFIXGROUPE() + p + " souhaite vous inviter dans son groupe ! §e[Cliquez ici pour accepter]");

    }

}
