package fr.denoria.denocore.bungeecord.commands;

import fr.denoria.denocore.bungeecord.DenoriaBungeeDatabase;
import fr.denoria.denocore.bungeecord.IBungeePlayer;
import fr.denoria.denocore.bungeecord.channels.ChannelEnum;
import fr.denoria.denocore.denoriaplayer.rank.Rank;
import fr.denoria.denocore.support.MessagesUtils;
import fr.denoria.denocore.support.utils.PermissionEnum;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

public class StaffChatCommand extends Command {

    public StaffChatCommand() {
        super("staffchat", null, "sc");
    }

    @SuppressWarnings("deprecation")
    @Override
    public void execute(CommandSender sender, String[] args) {

        ProxiedPlayer proxiedPlayer = (ProxiedPlayer) sender;
        IBungeePlayer iBungeePlayer = IBungeePlayer.getIBungeePlayer(proxiedPlayer);

        // Vérifier si le compte du Joueur existe.
        if (iBungeePlayer == null) {
            proxiedPlayer.sendMessage(MessagesUtils.getDenoriaPrefix()
                    + "§cErreur: §7Votre compte rencontre un problème contactez un Admin.");
            return;
        }

        // Verifier la permission du Joueur.
        if ((iBungeePlayer.getRank().getPower() < Rank.COEUR.getPower())
                && (!(iBungeePlayer.hasPermission(PermissionEnum.STAFFCHAT.getName())))) {
            iBungeePlayer.sendMessageNoPermission();
            return;
        }

        // Vérification des arguments.
        if (args.length == 0) {
            sendHelp(proxiedPlayer);
            return;
        }

        // Activation du StaffChat.
        if ((args.length == 1) && (args[0].equalsIgnoreCase("on"))) {
            if (iBungeePlayer.isStaffChat()) {
                proxiedPlayer.sendMessage(
                        MessagesUtils.getDenoriaPrefix() + ChatColor.RED + "Erreur: §7Le StaffChat est déjà activé.");
            } else {
                iBungeePlayer.setStaffChat(true);
                proxiedPlayer.sendMessage(MessagesUtils.getDenoriaPrefix()
                        + "§7Le StaffChat est désormais " + ChatColor.GREEN + "activé§7.");
            }
            return;
        }

        // Désactivation du StaffChat.
        if ((args.length == 1) && (args[0].equalsIgnoreCase("off"))) {
            if (!(iBungeePlayer.isStaffChat())) {
                proxiedPlayer.sendMessage(
                        MessagesUtils.getDenoriaPrefix() + ChatColor.RED + "Erreur: §7Le StaffChat est déjà désactivé.");
            } else {
                iBungeePlayer.setStaffChat(false);
                proxiedPlayer.sendMessage(MessagesUtils.getDenoriaPrefix()
                        + "§7Le StaffChat est désormais " + ChatColor.RED + "désactivé§7.");
            }
            return;
        }

        // Gestion de la commande.
        if (!(iBungeePlayer.isStaffChat())) {
            proxiedPlayer.sendMessage(
                    MessagesUtils.getDenoriaPrefix() + ChatColor.RED + "Erreur: §7Vous devez activer votre StaffChat.");
            return;
        }
        String message = "";
        for (int i = 0; i < args.length; i++) {
            message = message + " " + args[i];
        }
        message.trim();
        Rank rank = IBungeePlayer.getIBungeePlayer(proxiedPlayer).getRank();
        DenoriaBungeeDatabase.getRedisBungeeAPI().sendChannelMessage(ChannelEnum.CHANNEL_STAFFCHAT.getName(),
                "§d(StaffChat) "
                        + rank.getChatColor()
                        + rank.getPrefix()
                        + " "
                        + proxiedPlayer.getName()
                        + " §8» §b"
                        + message);
    }

    public void sendHelp(ProxiedPlayer player) {
        player.sendMessage(new TextComponent(MessagesUtils.getDenoriaPrefix() + "§cErreur: §7Vous n'avez pas mis de message."));
    }
}
