package fr.denoria.denocore.bungeecord.commands;


import fr.denoria.denocore.bungeecord.IBungeePlayer;
import fr.denoria.denocore.denoriaplayer.rank.Rank;
import fr.denoria.denocore.network.infos.NetworkAPI;
import fr.denoria.denocore.support.MessagesUtils;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;


public class MaintenanceCommand extends Command {

    public final static String PREFIX = MessagesUtils.getDenoriaPrefix();

    public MaintenanceCommand() {
        super("maintenance", null, "denomtn");
    }


    public void sendErrorNoPermission(ProxiedPlayer player){
        player.sendMessage(PREFIX + "§cErreur: §7Vous n'avez pas accès à cette commande !");
    }

    @Override
    public void execute(CommandSender sender, String[] strings) {

        if(!(sender instanceof ProxiedPlayer)){
            NetworkAPI.setMaintenance(!NetworkAPI.isMaintenance());
            sender.sendMessage("§7La maintenance à été " + (NetworkAPI.isMaintenance() ? "§cDésactivé" : "§aActivé") + " §7.");
        }
        else {
            ProxiedPlayer player = (ProxiedPlayer) sender;
            IBungeePlayer iBungeePlayer = IBungeePlayer.getIBungeePlayer(player);

            if (iBungeePlayer.getRank().getPower() >= Rank.ADMINISTRATOR.getPower()) {
                NetworkAPI.setMaintenance(!NetworkAPI.isMaintenance());
                player.sendMessage("§7La maintenance à été " + (NetworkAPI.isMaintenance() ? "§cDésactivé" : "§aActivé") + " §7.");
            } else
                sendErrorNoPermission(iBungeePlayer.getProxiedPlayer());
        }
    }
}
