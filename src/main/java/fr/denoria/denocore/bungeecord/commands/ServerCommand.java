/*
 * Copyright (c) 2019 - 2021 Denoria Network
 *
 *  ______   _______  _        _______  _______ _________ _______             _______  _        _______  _______  _______ _________ _        _______
 * (  __  \ (  ____ \( (    /|(  ___  )(  ____ )\__   __/(  ___  )       /\  (  ____ \| \    /\(  ___  )(  ____ \(  ____ )\__   __/( (    /|(  ____ \
 * | (  \  )| (    \/|  \  ( || (   ) || (    )|   ) (   | (   ) |      / /  | (    \/|  \  / /| (   ) || (    \/| (    )|   ) (   |  \  ( || (    \/
 * | |   ) || (__    |   \ | || |   | || (____)|   | |   | (___) |     / /   | (_____ |  (_/ / | |   | || |      | (____)|   | |   |   \ | || (__
 * | |   | ||  __)   | (\ \) || |   | ||     __)   | |   |  ___  |    / /    (_____  )|   _ (  | |   | || | ____ |     __)   | |   | (\ \) ||  __)
 * | |   ) || (      | | \   || |   | || (\ (      | |   | (   ) |   / /           ) ||  ( \ \ | |   | || | \_  )| (\ (      | |   | | \   || (
 * | (__/  )| (____/\| )  \  || (___) || ) \ \_____) (___| )   ( |  / /      /\____) ||  /  \ \| (___) || (___) || ) \ \_____) (___| )  \  || (____/\
 * (______/ (_______/|/    )_)(_______)|/   \__/\_______/|/     \|  \/       \_______)|_/    \/(_______)(_______)|/   \__/\_______/|/    )_)(_______/
 *
 */

package fr.denoria.denocore.bungeecord.commands;

import fr.denoria.denocore.bungeecord.IBungeePlayer;
import fr.denoria.denocore.denoriaplayer.rank.Rank;
import fr.denoria.denocore.support.MessagesUtils;
import fr.denoria.denocore.support.server.InstanceEnum;
import fr.denoria.denocore.support.server.ServerInstance;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

public class ServerCommand extends Command {
    public ServerCommand() {
        super("servermanager");
    }

    @Override
    public void execute(CommandSender commandSender, String[] args) {

        if (commandSender instanceof ProxiedPlayer) {
            ProxiedPlayer player = (ProxiedPlayer) commandSender;
            IBungeePlayer denoPlayer = new IBungeePlayer(player);

            if (denoPlayer.getRank().getPower().equals(Rank.ADMINISTRATOR.getPower())) {
                if (args.length == 2) {
                    if (args[0].equalsIgnoreCase("create") || args[0].equalsIgnoreCase("add")) {
                        ServerInstance.createServer(InstanceEnum.getByName(args[1]));
                    }
                } else if (args.length == 3) {
                    if (args[0].equalsIgnoreCase("remove") || args[0].equalsIgnoreCase("delete")) {
                        int id = Integer.parseInt(args[2]);
                        ServerInstance.stopServer(InstanceEnum.getByName(args[1]), id);
                    }
                } else {
                    denoPlayer.getProxiedPlayer().sendMessage(new TextComponent(MessagesUtils.getBlackSeparator()));
                    denoPlayer.getProxiedPlayer().sendMessage(new TextComponent("§7/servermanager <create/delete> <instanceKey>"));
                    denoPlayer.getProxiedPlayer().sendMessage(new TextComponent(MessagesUtils.getBlackSeparator()));
                }
            } else {
                denoPlayer.sendMessageNoPermission();
            }

        }

    }
}
