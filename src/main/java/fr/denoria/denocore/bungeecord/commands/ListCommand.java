package fr.denoria.denocore.bungeecord.commands;

import fr.denoria.denocore.bungeecord.DenoriaBungeeDatabase;
import fr.denoria.denocore.support.MessagesUtils;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

public class ListCommand extends Command {

    public ListCommand() {
        super("list", null, "liste");
    }

    @SuppressWarnings("deprecation")
    @Override
    public void execute(CommandSender sender, String[] args) {
        ProxiedPlayer proxiedPlayer = (ProxiedPlayer) sender;
        proxiedPlayer.sendMessage(MessagesUtils.getDenoriaPrefix() + ChatColor.YELLOW + "Il y a actuellement "
                + ChatColor.AQUA + DenoriaBungeeDatabase.getRedisBungeeAPI().getPlayerCount() + ChatColor.YELLOW
                + " joueur(s) en ligne.");
    }
}
