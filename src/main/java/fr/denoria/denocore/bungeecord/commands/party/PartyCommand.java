package fr.denoria.denocore.bungeecord.commands.party;

import fr.denoria.denocore.bungeecord.IBungeePlayer;
import fr.denoria.denocore.support.MessagesUtils;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

public class PartyCommand extends Command {

    private static final String PREFIXGROUPE = "§bParty §8❘ §7";
    private final String mtn = PREFIXGROUPE + "§cFonctionnalité en maintenance, disponible rapidement...";

    public PartyCommand() {
        super("party", null, "partys", "p", "groupe", "groupes", "groups", "group", "g");
    }

    @SuppressWarnings("deprecation")
    @Override
    public void execute(CommandSender sender, String[] args) {

        ProxiedPlayer proxiedPlayer = (ProxiedPlayer) sender;
        IBungeePlayer iBungeePlayer = IBungeePlayer.getIBungeePlayer(proxiedPlayer);

        // Vérifier si le compte du Joueur existe.
        if (iBungeePlayer == null) {
            proxiedPlayer.sendMessage(MessagesUtils.getDenoriaPrefix() + ChatColor.RED
                    + "Erreur: Votre compte rencontre un problème contactez un Admin.");
            return;
        }

        if (args.length == 0) {
            helpInfo(proxiedPlayer);
        } else {
            proxiedPlayer.sendMessage(mtn);
        }

    }

    public void helpInfo(ProxiedPlayer player) {
        player.sendMessage(MessagesUtils.getBlackSeparator());
        player.sendMessage("§b§lParty §7(Aide /party) ");
        player.sendMessage("");
        player.sendMessage(" §8() §b/party help §8» §7Message d'aide du party.");
        player.sendMessage(" §8() §b/party add (player) §8» §7Ajouter une personne au groupe.");
        player.sendMessage(" §8() §c/party remove (player) §8» §7Retirer une personne du groupe.");
        player.sendMessage(" §8() §b/party list §8» §7Liste des joueurs dans le groupe");
        player.sendMessage(" §8() §b/party tp/join (pseudo) §8» §7Téléporter votre groupe");
        player.sendMessage(" §8() §b/party [on/off] §8» §7Gérer la réception des demandes");
        player.sendMessage("");
        player.sendMessage(MessagesUtils.getBlackSeparator());
    }

    public static String getPREFIXGROUPE() {
        return PREFIXGROUPE;
    }
}
