/*
 * Copyright (c) 2019 - 2021 Denoria Network
 *
 *  ______   _______  _        _______  _______ _________ _______             _______  _        _______  _______  _______ _________ _        _______
 * (  __  \ (  ____ \( (    /|(  ___  )(  ____ )\__   __/(  ___  )       /\  (  ____ \| \    /\(  ___  )(  ____ \(  ____ )\__   __/( (    /|(  ____ \
 * | (  \  )| (    \/|  \  ( || (   ) || (    )|   ) (   | (   ) |      / /  | (    \/|  \  / /| (   ) || (    \/| (    )|   ) (   |  \  ( || (    \/
 * | |   ) || (__    |   \ | || |   | || (____)|   | |   | (___) |     / /   | (_____ |  (_/ / | |   | || |      | (____)|   | |   |   \ | || (__
 * | |   | ||  __)   | (\ \) || |   | ||     __)   | |   |  ___  |    / /    (_____  )|   _ (  | |   | || | ____ |     __)   | |   | (\ \) ||  __)
 * | |   ) || (      | | \   || |   | || (\ (      | |   | (   ) |   / /           ) ||  ( \ \ | |   | || | \_  )| (\ (      | |   | | \   || (
 * | (__/  )| (____/\| )  \  || (___) || ) \ \_____) (___| )   ( |  / /      /\____) ||  /  \ \| (___) || (___) || ) \ \_____) (___| )  \  || (____/\
 * (______/ (_______/|/    )_)(_______)|/   \__/\_______/|/     \|  \/       \_______)|_/    \/(_______)(_______)|/   \__/\_______/|/    )_)(_______/
 *
 */

package fr.denoria.denocore.bungeecord.channels;

public enum ChannelSMEnum {

    START_INSTANCE("startInstanceRequest"),
    STOP_INSTANCE("stopInstanceRequest"),
    START_ONCE_INSTANCE("startOnceInstanceRequest"),
    STOP_ONCE_INSTANCE("stopOnceInstanceRequest"),
    REPLACE_INSTANCE("replaceInstanceRequest"),
    BUNGEE_SERVERS("bungeeServersRequest"),
    SERVER_ENTRY("serverEntry"),
    SERVER_STOP("serverStop"),
    SERVER_INFO("serverExist");

    public String name;

    /**
     * Récupérer le ChannelEnum.
     *
     * @param name nom du channel
     */
    ChannelSMEnum(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }


}
