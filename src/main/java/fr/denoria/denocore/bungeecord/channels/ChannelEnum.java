package fr.denoria.denocore.bungeecord.channels;

public enum ChannelEnum {

    CHANNEL_STAFFCHAT("CHANNEL_STAFFCHAT"),
    CHANNEL_BROADCAST("CHANNEL_BROADCAST"),
    CHANNEL_MODBROADCAST("CHANNEL_MODBROADCAST"),
    SERVER_SAYING("SERVER_SAYING");

    public String name;

    /**
     * Récupérer le ChannelEnum.
     *
     * @param name name channel
     */
    ChannelEnum(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
