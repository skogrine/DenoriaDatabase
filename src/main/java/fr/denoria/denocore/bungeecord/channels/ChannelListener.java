package fr.denoria.denocore.bungeecord.channels;

import com.imaginarycode.minecraft.redisbungee.events.PubSubMessageEvent;
import fr.denoria.denocore.bungeecord.IBungeePlayer;
import fr.denoria.denocore.denoriaplayer.rank.Rank;
import fr.denoria.denocore.support.utils.PermissionEnum;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;
import net.md_5.bungee.event.EventPriority;

@SuppressWarnings("deprecation")
public class ChannelListener implements Listener {

    @EventHandler(priority = EventPriority.LOWEST)
    public void onRedisMessage(PubSubMessageEvent event) {
        Iterable<ProxiedPlayer> playerOnProxied = ProxyServer.getInstance().getPlayers();
        switch (event.getChannel()) {

            // Gestion du StaffChat.
            case "CHANNEL_STAFFCHAT":
                for (ProxiedPlayer proxiedPlayerOnline : playerOnProxied) {
                    if (event.getMessage() != null) {
                        if (proxiedPlayerOnline != null) {
                            IBungeePlayer iBungeePlayer = IBungeePlayer.getIBungeePlayer(proxiedPlayerOnline);
                            if (((iBungeePlayer.getRank().getPower() >= Rank.COEUR.getPower())
                                    || (iBungeePlayer.hasPermission(PermissionEnum.STAFFCHAT.getName())))
                                    && (iBungeePlayer.isStaffChat())) {
                                proxiedPlayerOnline.sendMessage(event.getMessage());
                            }
                        }
                    }
                }
                break;

            // Gestion du Broadcast.
            case "CHANNEL_BROADCAST":
                for (ProxiedPlayer proxiedPlayerOnline : playerOnProxied) {
                    if (event.getMessage() != null) {
                        if (proxiedPlayerOnline != null) {
                            proxiedPlayerOnline.sendMessage(event.getMessage());
                        }
                    }
                }
                break;

            default:
                break;
        }
    }
}
