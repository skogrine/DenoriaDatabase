package fr.denoria.denocore.bungeecord;

import java.util.*;

import fr.denoria.denocore.denoriaplayer.infos.DataType;
import fr.denoria.denocore.denoriaplayer.misc.IPermissions;
import fr.denoria.denocore.denoriaplayer.rank.IRank;
import fr.denoria.denocore.denoriaplayer.rank.RANKSPECIAL;
import fr.denoria.denocore.denoriaplayer.rank.Rank;
import fr.denoria.denocore.redis.RedisManager;
import fr.denoria.denocore.support.MessagesUtils;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import redis.clients.jedis.Jedis;

@SuppressWarnings("deprecation")
public class IBungeePlayer {

    private UUID uuid;
    private ProxiedPlayer proxiedPlayer;

    private Rank rank;
    private RANKSPECIAL rankspecial;

    private List<String> permissionsList = new ArrayList<>();
    private Map<DataType, Object> mapData = new HashMap<>();

    private boolean staffChat;

    /**
     * Constructeur du IBungeePlayer.
     *
     * @param proxiedPlayer Player
     */
    public IBungeePlayer(ProxiedPlayer proxiedPlayer) {
        this.uuid = proxiedPlayer.getUniqueId();
        if (this.uuid == null) {
            this.uuid = proxiedPlayer.getUniqueId();
        }
        this.proxiedPlayer = proxiedPlayer;
        this.loadAccountData();
        this.rank = IRank.getPlayerRank(uuid);
        this.rankspecial = IRank.getPlayerSpecialRank(uuid);
        this.permissionsList = IPermissions.getPermissions(uuid);
        this.staffChat = true;
    }

    /**
     * Charger les données du Joueur.
     */
    public void loadAccountData() {
        IRank.createAccount(uuid);
    }

    /**
     * Envoyer le message du Joueur qui n'a pas la permission.
     */
    public void sendMessageNoPermission() {
        proxiedPlayer.sendMessage(ChatColor.RED + "Désolé, vous n'avez pas accès à cette commande."
                + " N'hésitez pas à contacter un Administrateur en cas de problèmes.");
    }

    public UUID getUUID() {
        return uuid;
    }

    public ProxiedPlayer getProxiedPlayer() {
        return proxiedPlayer;
    }

    public Rank getRank() {
        return rank;
    }

    public boolean isStaffChat() {
        return staffChat;
    }

    public void setStaffChat(boolean staffChat) {
        this.staffChat = staffChat;
    }

    public RANKSPECIAL getRankspecial() {
        return rankspecial;
    }

    /**
     * Envoyer le message de bienvenue.
     */
    public void sendWelcomeMessage() {
        proxiedPlayer.sendMessage("");
        proxiedPlayer.sendMessage("");
        proxiedPlayer.sendMessage("");
        proxiedPlayer.sendMessage("");
        proxiedPlayer.sendMessage("");
        proxiedPlayer.sendMessage("");
        proxiedPlayer.sendMessage(MessagesUtils.getSeparator());
        proxiedPlayer.sendMessage("§8❘ §7Bienvenue §e" + proxiedPlayer.getName() + "§7 sur §dDenoria §7!");
        proxiedPlayer.sendMessage("");
        proxiedPlayer.sendMessage(" §8() §7Faites §e/help §7pour voir les commandes");
        proxiedPlayer.sendMessage(" §8() §7Faites §e/report §7pour signaler un joueur");
        proxiedPlayer.sendMessage(" §8() §7Vous avez besoin d'aide ? Demandez aux §bAssistants");
        proxiedPlayer.sendMessage("");
        proxiedPlayer.sendMessage("§8❘ §7Le serveur est actuellement en §cbêta§7, signalez vos bugs");
        proxiedPlayer.sendMessage("§8❘ §7Avantages exclusifs et plus sur §astore.denoria.fr");
        proxiedPlayer.sendMessage(MessagesUtils.getSeparator());
        proxiedPlayer.sendMessage("");

    }


    /**
     * Ajouter une permission au Joueur.
     *
     * @param permission
     */
    public void addPermission(String permission) {
        IPermissions.addPermission(uuid, permission);
        permissionsList.add(permission);
    }

    /**
     * Ajouter une permission avec un temps à un Joueur.
     *
     * @param permission
     * @param delayMinutes
     */
    public void addPermissionTime(String permission, int delayMinutes) {
        IPermissions.addPermissionWithTimer(uuid, permission, delayMinutes);
        permissionsList.add(permission);
    }

    /**
     * Supprimer une permission à un Joueur.
     *
     * @param permission
     */
    public void removePermission(String permission) {
        IPermissions.suppPermission(uuid, permission);
        permissionsList.remove(permission);
    }

    /**
     * Vérifier la permission d'un Joueur.
     *
     * @param permission
     * @return
     */
    public boolean hasPermission(String permission) {
        return permissionsList.contains(permission);
    }

    /**
     * Récupérer le IBungeePlayer.
     *
     * @param proxiedPlayer
     * @return
     */
    public static IBungeePlayer getIBungeePlayer(ProxiedPlayer proxiedPlayer) {
        if (DenoriaBungeeDatabase.iBungeePlayerMap.get(proxiedPlayer.getName()) == null) {
            DenoriaBungeeDatabase.iBungeePlayerMap.put(proxiedPlayer.getName(), new IBungeePlayer(proxiedPlayer));
        }
        return DenoriaBungeeDatabase.iBungeePlayerMap.get(proxiedPlayer.getName());
    }

    /**
     * Récupérer une Information d'un Joueur.
     *
     * @param dataType
     * @return
     */
    public boolean getPlayerData(DataType dataType) {
        return (boolean) this.mapData.get(dataType);
    }

    public boolean isNick() {
        try (Jedis jedis = RedisManager.getJedisPool().getResource()) {

            String ressource = jedis.get("dataplayerinfos:" + uuid.toString() + "_" + DataType.AFK.getName());
            return ressource.contains("true");
        }
    }


}
