package fr.denoria.denocore.game;

public enum GameStatus {
    OPEN, CLOSE, WAITING, REBOOT, INGAME;
}
