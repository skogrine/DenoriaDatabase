
/*
 * Copyright (c) 2021.
 */

package fr.denoria.denocore.game;

import java.util.Objects;

public class GameInterceptor {

    private String name;
    private int id;
    private GameType type;
    private GameStatus status;

    public GameInterceptor(String name, int id, GameType type, GameStatus status) {
        this.name = name;
        this.id = id;
        this.type = type;
        this.status = status;
    }

    public void closeServer(int id) {
        if (status == GameStatus.CLOSE) {
            return;
        }

    }

    public void OpenServer(int id) {
        if (status == GameStatus.OPEN) {
            return;
        }

    }

    public void startGame(int id) {
        if (status == GameStatus.INGAME) {
            return;
        }
        if (status == GameStatus.CLOSE) {
            return;
        }
    }

    public void stopGame(int id) {
        if (!(status == GameStatus.INGAME)) {
            return;
        }

    }

    public void startMaintenance(int id) {
        if (status == GameStatus.CLOSE) {
            return;
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        GameInterceptor that = (GameInterceptor) o;
        return id == that.id && Objects.equals(name, that.name) && type == that.type && status == that.status;
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, id, type, status);
    }

    /**
     * get field
     *
     * @return name
     */
    public String getName() {
        return this.name;
    }

    /**
     * get field
     *
     * @return id
     */
    public int getId() {
        return this.id;
    }

    /**
     * get field
     *
     * @return type
     */
    public GameType getType() {
        return this.type;
    }

    /**
     * get field
     *
     * @return status
     */
    public GameStatus getStatus() {
        return this.status;
    }

    /**
     * set field
     *
     * @param name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * set field
     *
     * @param id
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * set field
     *
     * @param type
     */
    public void setType(GameType type) {
        this.type = type;
    }

    /**
     * set field
     *
     * @param status
     */
    public void setStatus(GameStatus status) {
        this.status = status;
    }
}
