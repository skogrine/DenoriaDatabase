package fr.denoria.denocore.game;

public enum GameType {
    BedWars,
    MonsterQuest,
    FreeBuild,
    Hunter,
    CallOfMine;
}
