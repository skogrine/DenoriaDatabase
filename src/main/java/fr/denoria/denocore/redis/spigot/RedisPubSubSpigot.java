package fr.denoria.denocore.redis.spigot;

import fr.denoria.denocore.DenoCore;
import fr.denoria.denocore.denoriaplayer.DenoriaPlayer;
import fr.denoria.denocore.denoriaplayer.infos.DataType;
import fr.denoria.denocore.redis.RedisManager;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPubSub;

import java.io.Closeable;
import java.io.IOException;

public class RedisPubSubSpigot implements Closeable{
    private static final String CHANNEL_BROADCAST = "CHANNELSPIGOT_BROADCAST";
    private static final String CHANNEL_BROADCASTSTAFF = "CHANNELSPIGOT_BROADCASTSTAFF";
    private static final String CHANNEL_BROADCASTMOD = "CHANNELSPIGOT_BROADCASTMOD";
    private static final String CHANNEL_MAKEKICK = "CHANNELSPIGOT_MAKEKICK";
    private static final String CHANNEL_PRIVATEMSG = "CHANNELSPIGOT_MESSAGESPRIVE";
    private static final String CHANNEL_MODBROADCAST = "CHANNEL_MODBROADCAST";
    private BroadcastListener listener;

    public RedisPubSubSpigot() {
        listener = new BroadcastListener();
        new BukkitRunnable() {

            @Override
            public void run() {
                try (Jedis jedis = RedisManager.getRedisDatabase().getJedisPool().getResource()) {

                    jedis.subscribe(listener, CHANNEL_BROADCAST, CHANNEL_BROADCASTSTAFF, CHANNEL_BROADCASTMOD,
                            CHANNEL_MAKEKICK, CHANNEL_PRIVATEMSG, CHANNEL_MODBROADCAST);
                } catch (Exception e) {
                    e.printStackTrace();
                    System.out.println("[RedisPubSubSpigot] Impossible d'enregistrer les Channels..");
                }
            }
        }.runTaskAsynchronously(DenoCore.getInstance());
    }

    @Override
    public void close() throws IOException {
        listener.unsubscribe(CHANNEL_BROADCAST, CHANNEL_BROADCASTSTAFF, CHANNEL_BROADCASTMOD,
                CHANNEL_MAKEKICK, CHANNEL_PRIVATEMSG, CHANNEL_MODBROADCAST);
    }

    /**
     * Faire une broadcast à tout les Joueurs.
     *
     * @param message
     */
    public void broadcastMessage(String message) {
        try (Jedis jedis = RedisManager.getRedisDatabase().getJedisPool().getResource()) {
            jedis.publish(CHANNEL_BROADCAST, message);
        } catch (Exception e) {
            System.out.println("[RedisPubSubSpigot] Impossible d'utiliser le broadcast.");
        }
    }

    /**
     * Faire un broadcast au Staff.
     *
     * @param message
     */
    public void broadcastStaff(String message) {
        try (Jedis jedis = RedisManager.getRedisDatabase().getJedisPool().getResource()) {
            jedis.publish(CHANNEL_BROADCASTSTAFF, message);
        } catch (Exception e) {
            System.out.println("[RedisPubSubSpigot] Impossible d'utiliser le broadcast.");
        }
    }

    /**
     * Send broadcast to mod
     *
     * @param message message du broadcast
     */
    public void broadcastModStaff(String message) {
        try (Jedis jedis = RedisManager.getRedisDatabase().getJedisPool().getResource()) {
            jedis.publish(CHANNEL_MODBROADCAST, message);
        } catch (Exception e) {
            System.out.println("[RredisPubSubSpigot] Impossible d'utiliser le Mod broadcast.");
        }
    }

    /**
     * Faire un broadcast au Staff.
     *
     * @param message
     */
    public void broadcastModeration(String message) {
        try (Jedis jedis = RedisManager.getRedisDatabase().getJedisPool().getResource()) {
            jedis.publish(CHANNEL_BROADCASTMOD, message);
        } catch (Exception e) {
            System.out.println("[RedisPubSubSpigot] Impossible d'utiliser le broadcast.");
        }
    }

    /**
     * Faire un kick a un Joueur.
     * 	 *
     * @param reason
     */
    public void kickPlayer(String playerName, String reason) {
        try (Jedis jedis = RedisManager.getRedisDatabase().getJedisPool().getResource()) {
            jedis.publish(CHANNEL_MAKEKICK, playerName + "#35854#" + reason);
        } catch (Exception e) {
            System.out.println("[RedisPubSubSpigot] Impossible d'utiliser le broadcast.");
        }
    }

    /**
     * Envoyer un message à un Joueur.
     *
     * @param playerName
     * @param message
     */
    public void sendMessage(String playerName, String message) {
        try (Jedis jedis = RedisManager.getRedisDatabase().getJedisPool().getResource()) {
            jedis.publish(CHANNEL_PRIVATEMSG, playerName + "#35854#" + message);
        } catch (Exception e) {
            System.out.println("[RedisPubSubSpigot] Impossible d'utiliser le message privé.");
        }
    }

    protected static class BroadcastListener extends JedisPubSub {
        @Override
        public void onMessage(String channel, String message) {

            /* Broadcast */
            if (CHANNEL_BROADCAST.equalsIgnoreCase(channel)) {
                Bukkit.broadcastMessage(message);
            }

            if (CHANNEL_MODBROADCAST.equalsIgnoreCase(channel)) {
                for (Player player : Bukkit.getOnlinePlayers()) {
                    DenoriaPlayer denoriaPlayer = DenoriaPlayer.getDenoriaPlayer(player);
                    if (denoriaPlayer != null) {
                        if (denoriaPlayer.getRankPower() >= 80) {
                            player.sendMessage("§d(StaffChat §8❘ §cAnnonces§d) §8» §b" + message);
                        }
                    }
                }
            }

            /* Broadcast to Staff */
            if (CHANNEL_BROADCASTSTAFF.equalsIgnoreCase(channel)) {
                for(Player playerOnline : Bukkit.getOnlinePlayers()) {
                    DenoriaPlayer denoriaPlayer = DenoriaPlayer.getDenoriaPlayer(playerOnline);
                    if(denoriaPlayer != null) {
                        if(denoriaPlayer.getRankPower() >= 60) {
                            playerOnline.sendMessage(message);
                        }
                    }
                }
            }

            /* Broadcast to Moderation */
            if (CHANNEL_BROADCASTMOD.equalsIgnoreCase(channel)) {
                for(Player playerOnline : Bukkit.getOnlinePlayers()) {
                    DenoriaPlayer denoriaPlayer = DenoriaPlayer.getDenoriaPlayer(playerOnline);
                    if(denoriaPlayer != null) {
                        if(denoriaPlayer.getRankPower() >= 80) {
                            playerOnline.sendMessage(message);
                        }
                    }
                }
            }

            /* Kick un Joueur */
            if (CHANNEL_MAKEKICK.equalsIgnoreCase(channel)) {
                String playerTarget = message.split("#35854#")[0];
                String reason = message.split("#35854#")[1];
                for(Player playerOnline : Bukkit.getOnlinePlayers()) {
                    DenoriaPlayer denoriaPlayer = DenoriaPlayer.getDenoriaPlayer(playerOnline);
                    if((denoriaPlayer != null)
                            && playerOnline.getName().equalsIgnoreCase(playerTarget)) {
                        Bukkit.getScheduler().runTask(DenoCore.getInstance(), () -> playerOnline.kickPlayer(reason));
                    }
                }
            }

            /* Gestion des MP */
            if (CHANNEL_PRIVATEMSG.equalsIgnoreCase(channel)) {
                String playerTarget = message.split("#35854#")[0];
                String targetMessage = message.split("#35854#")[1];
                for(Player playerOnline : Bukkit.getOnlinePlayers()) {
                    DenoriaPlayer denoriaPlayer = DenoriaPlayer.getDenoriaPlayer(playerOnline);
                    if((denoriaPlayer != null)
                            && playerOnline.getName().equalsIgnoreCase(playerTarget)) {
                        if(denoriaPlayer.getPlayerData(DataType.ALLOW_MESSAGES_PRIVATES)) {
                            playerOnline.sendMessage(targetMessage);
                        }
                    }
                }
            }
        }
    }
}
