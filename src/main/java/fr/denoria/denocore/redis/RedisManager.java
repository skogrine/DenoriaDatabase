package fr.denoria.denocore.redis;

import redis.clients.jedis.JedisPool;

public class RedisManager {

    private final boolean prod;

    private static RedisDatabase redisDatabase;

    /**
     * Initialisation des informations de connection.
     *
     * @param prod if plugin is production
     */
    public RedisManager(boolean prod) {
        this.prod = prod;
    }


    /**
     * Effectuer la connection à Redis.
     */
    public void connect() {
        redisDatabase = new RedisDatabase(prod);
        redisDatabase.connect();
    }

    /**
     * Effectuer la déconnection à Redis.
     */
    public void disconnect() {
        redisDatabase.disconnect();
    }

    /**
     * Récupérer le Jedis Connection.
     *
     * @return
     */
    public static JedisPool getJedisPool() {
        return redisDatabase.getJedisPool();
    }

    /**
     * Récupérer la 'class' RedisDatabase.
     *
     * @return
     */
    public static RedisDatabase getRedisDatabase() {
        return redisDatabase;
    }
}
