package fr.denoria.denocore.redis;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;

public class RedisDatabase {

    private final boolean prod;

    private JedisPool jedisPool;

    /**
     * Initialisation des informations de connection.
     *
     * @param prod if plugin is production
     */
    public RedisDatabase(boolean prod) {
        this.prod = prod;
    }

    /**
     * Effectuer la connection à Redis.
     */
    public void connect() {
        Thread.currentThread().setContextClassLoader(Jedis.class.getClassLoader());
        JedisPoolConfig jedisPoolConfig = new JedisPoolConfig();

        if(prod) {
            jedisPool = new JedisPool(jedisPoolConfig, "141.94.124.78", 6379, 3000, "4$Eq7u*X8");
        } else {
            jedisPool = new JedisPool(jedisPoolConfig, "127.0.0.1", 6379, 3000, "");
        }

        try (Jedis jedisConnect = jedisPool.getResource()) {
            System.out.println("[RedisDatabase] Denoria Network vient de se connecter à Redis.");
        } catch (Exception e) {
            System.out.println("[RedisDatabase] Erreur: impossible de se connecter à Redis.");
        }
    }

    /**
     * Effectuer la deconnection de Redis.
     */
    public void disconnect() {
        try {
            jedisPool.close();
            System.out.println("[RedisDatabase] Denoria Network vient de se déconnecter à Redis.");
        } catch (Exception ignored) {
            System.out.println("[RedisDatabase] Erreur: impossible de se déconnecter à Redis.");
        }
    }

    /**
     * Récupérer le JedisPool.
     *
     * @return jedisPool
     */
    public JedisPool getJedisPool() {
        return jedisPool;
    }
}
