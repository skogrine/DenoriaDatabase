package fr.denoria.denocore.network;

import fr.denoria.denocore.mysql.SQLManager;
import fr.denoria.denocore.support.MessagesUtils;
import fr.denoria.denocore.support.builder.JsonMessageBuilder;
import org.bukkit.entity.Player;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.SimpleDateFormat;

public class IFriend {

    protected static String table = "players_friends";


    /**
     * Ajouter un ami
     *
     * @param player Le joueur qui ajoute un ami
     * @param target Le joueur ajouté
     */
    public static void addPlayer(Player player, Player target){
        JsonMessageBuilder sendConfirm = new JsonMessageBuilder();
        sendConfirm.newJComp(MessagesUtils.getFriendPrefix() + "§7" + player.getName() + " vous a envoyé une demande d'amis ! (Clique pour accepter)").addCommandExecutor("/f confirm " + target.getName());
    }

    public static void confirmFriend(Player player, Player target){
        try {
            final Connection connection = SQLManager.DENORIA_SERVER.getDatabaseAccess().getRessource();
            PreparedStatement ps = connection.prepareStatement("INSERT INTO " + table
                    + " (player_uuid, friend_uuid, date_add) VALUES(?, ?, ?)");
            ps.setString(1, player.getUniqueId().toString());
            ps.setString(2, target.getUniqueId().toString());
            ps.setString(3, new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").format(new java.util.Date()));
            ps.executeUpdate();
            ps.close();
            connection.close();
        } catch (SQLException e){
            e.printStackTrace();
            System.err.println("Impossible d'ajouter un ami a " + player.getName());
        }
    }

    public static void deleteFriend(Player player, Player target){
        try {
            final Connection connection = SQLManager.DENORIA_SERVER.getDatabaseAccess().getRessource();
            PreparedStatement preparedStatement = connection
                    .prepareStatement("DELETE FROM " + table + " WHERE player_uuid = ?");
            preparedStatement.setString(1, player.getUniqueId().toString());
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

}
