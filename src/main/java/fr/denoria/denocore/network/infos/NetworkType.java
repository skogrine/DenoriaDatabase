package fr.denoria.denocore.network.infos;

public enum NetworkType {
    ID(1, "ID", "id"),
    MAINTENANCE(2, "MAINTENANCE", "network_maintenance"),
    MOTD(3, "MOTD", "network_motd"),
    BOOSTERS(4, "BOOSTERS", "network_booster"),
    SLOTS(5, "SLOTS", "network_slots");

    private Integer id;
    private String name;
    private String columName;

    NetworkType(Integer id, String name, String columName) {
        this.id = id;
        this.name = name;
        this.columName = columName;
    }

    public Integer getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getColumName() {
        return columName;
    }

}
