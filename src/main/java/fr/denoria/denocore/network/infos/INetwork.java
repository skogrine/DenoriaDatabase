package fr.denoria.denocore.network.infos;

import fr.denoria.denocore.mysql.SQLManager;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
public class INetwork {

    protected static String table = "network_infos";

    /**
     * Génération du NetworkManager.
     */
    public static void generateNetworkManager() {
        try {
            final Connection connection = SQLManager.DENORIA_SERVER.getDatabaseAccess().getRessource();
            PreparedStatement preparedstatement = connection.prepareStatement("SELECT id FROM " + table);
            ResultSet rs = preparedstatement.executeQuery();
            if (!rs.next()) {
                preparedstatement.close();
                preparedstatement = connection.prepareStatement("INSERT INTO " + table
                        + " (network_maintenance, network_motd, network_booster, network_slots) VALUES (?, ?, ?, ?)");
                preparedstatement.setBoolean(1, true);
                preparedstatement.setString(2, "Motd par défaut.");
                preparedstatement.setInt(3, 1);
                preparedstatement.setInt(4, 100);
                preparedstatement.executeUpdate();
                preparedstatement.close();
            }
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Définir un NetworkInfo sur la base de données.
     *
     * @param networkType Type of network
     * @param value value to set
     */
    public static void setNetworkInfo(NetworkType networkType, Object value) {
        try {
            final Connection connection = SQLManager.DENORIA_SERVER.getDatabaseAccess().getRessource();
            PreparedStatement preparedstatement = connection
                    .prepareStatement("UPDATE " + table + " SET " + networkType.getColumName() + " = ? WHERE id = ?");
            preparedstatement.setObject(1, value);
            preparedstatement.setInt(2, 1);
            preparedstatement.executeUpdate();
            preparedstatement.close();
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Récupérer un NetworkInfo sur la base de données.
     *
     * @param networkType
     * @return
     */
    public static Object getNetworkInfo(NetworkType networkType) {
        Object value = null;
        try {
            final Connection connection = SQLManager.DENORIA_SERVER.getDatabaseAccess().getRessource();
            PreparedStatement preparedstatement = connection
                    .prepareStatement("SELECT " + networkType.getColumName() + " FROM " + table + " WHERE id = ?");
            preparedstatement.setInt(1, 1);
            ResultSet rs = preparedstatement.executeQuery();
            if (!rs.next()) {
                connection.close();
                return null;
            }
            value = rs.getObject(networkType.getColumName());
            preparedstatement.close();
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return value;
    }
}
