package fr.denoria.denocore.network.infos;

import fr.denoria.denocore.denoriaplayer.rank.Rank;

public class NetworkAPI {

    private static boolean maintenance;
    private static String motdLine;
    private static int boosterNetwork;
    private static int slots;
    private static final Rank rankAllowMaintenance = Rank.COEUR;

    /**
     * Charger les données du Network.
     */
    public static void loadNetworkInfo() {
        INetwork.generateNetworkManager();
        maintenance = ((boolean) INetwork.getNetworkInfo(NetworkType.MAINTENANCE));
        motdLine = (String) INetwork.getNetworkInfo(NetworkType.MOTD);
        boosterNetwork = (int) INetwork.getNetworkInfo(NetworkType.BOOSTERS);
        slots = (int) INetwork.getNetworkInfo(NetworkType.SLOTS);
    }

    /**
     * Définir la Maintenance.
     *
     * @param maintenance Maintenance is active or not
     */
    public static void setMaintenance(boolean maintenance) {
        INetwork.setNetworkInfo(NetworkType.MAINTENANCE, maintenance);
        NetworkAPI.maintenance = maintenance;
    }

    /**
     * Définir le Motd.
     *
     * @param motdLine line nb2 of motd
     */
    public static void setMotdLine(String motdLine) {
        INetwork.setNetworkInfo(NetworkType.MOTD, motdLine);
        NetworkAPI.motdLine = motdLine;
    }

    /**
     * Définir le Booster du Network.
     *
     * @param boosterNetwork Value int Booster global
     */
    public static void setBoosterNetwork(int boosterNetwork) {
        INetwork.setNetworkInfo(NetworkType.BOOSTERS, boosterNetwork);
        NetworkAPI.boosterNetwork = boosterNetwork;
    }

    /**
     * Définir le Slot du Network.
     *
     * @param slots slot of Network
     */
    public static void setSlots(int slots) {
        INetwork.setNetworkInfo(NetworkType.SLOTS, slots);
        NetworkAPI.slots = slots;
    }

    /**
     * Vérifier la Maintenance.
     *
     * @return
     */
    public static boolean isMaintenance() {
        return maintenance;
    }

    /**
     * Récupérer le Motd du Serveur.
     *
     * @return
     */
    public static String getMotdLine() {
        return motdLine;
    }

    /**
     * Récupérer le Booster du Network.
     *
     * @return
     */
    public static int getBoosterNetwork() {
        return boosterNetwork;
    }

    /**
     * Récupérer le Slot du Network.
     *
     * @return
     */
    public static int getSlots() {
        return slots;
    }

    /**
     * Grade autorisés sur la maintenance.
     *
     * @return
     */
    public static Rank getRankAllowMaintenance() {
        return rankAllowMaintenance;
    }

}
