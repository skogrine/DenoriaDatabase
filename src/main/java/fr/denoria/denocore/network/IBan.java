package fr.denoria.denocore.network;

import fr.denoria.denocore.mysql.SQLManager;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;

public class IBan {

    protected static String table = "players_banned";

    /**
     * Créer un ban permanent.
     *
     * @param pseudo pseudoname of player
     * @param raison reason of ban
     * @param author moderator name
     */
    public static void createBanLifeTime(String pseudo, String raison, String author) {
        try {
            final Connection connection = SQLManager.DENORIA_SERVER.getDatabaseAccess().getRessource();
            PreparedStatement preparedStatement = connection.prepareStatement("INSERT INTO " + table
                    + " (player_name, ban_raison, ban_date, ban_author, ban_perm) VALUES(?, ?, ?, ?, ?)");
            preparedStatement.setString(1, pseudo);
            preparedStatement.setString(2, raison);
            preparedStatement.setString(3, new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").format(new java.util.Date()));
            preparedStatement.setString(4, author);
            preparedStatement.setInt(5, 1);
            preparedStatement.executeUpdate();
            preparedStatement.close();
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Créer un ban avec un certain temps.
     *
     * @param pseudo
     * @param raison
     * @param author
     * @param hour
     */
    public static void createBanTime(String pseudo, String raison, String author, Integer hour) {
        try {
            final Connection connection = SQLManager.DENORIA_SERVER.getDatabaseAccess().getRessource();
            PreparedStatement preparedStatement = connection.prepareStatement("INSERT INTO " + table
                    + " (player_name, ban_raison, ban_date, ban_author, ban_duree) VALUES(?, ?, ?, ?, DATE_ADD(NOW(), INTERVAL "
                    + hour + " HOUR))");
            preparedStatement.setString(1, pseudo);
            preparedStatement.setString(2, raison);
            preparedStatement.setString(3, new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").format(new java.util.Date()));
            preparedStatement.setString(4, author);
            preparedStatement.executeUpdate();
            preparedStatement.close();
            connection.close();
        } catch (SQLException e) {
            System.err.println("Erreur lors du bannissement.");
        }
    }

    /**
     * Suppression des ban avec le temps.
     */
    public static void suppBanTime() {
        try {
            final Connection connection = SQLManager.DENORIA_SERVER.getDatabaseAccess().getRessource();
            PreparedStatement preparedStatement = connection
                    .prepareStatement("DELETE FROM " + table + " WHERE ban_duree <= NOW()");
            preparedStatement.executeUpdate();
            preparedStatement.close();
            connection.close();
        } catch (SQLException e) {
            System.err.println("Erreur lors du suppression de Ban.");
        }
    }

    /**
     * Vérifier si le Joueur est ban.
     *
     * @param pseudo pseudo du joueur ban
     * @return valeur oui ou non
     */
    public static boolean isBan(String pseudo) {
        suppBanTime();
        try {
            final Connection connection = SQLManager.DENORIA_SERVER.getDatabaseAccess().getRessource();
            PreparedStatement preparedStatement = connection
                    .prepareStatement("SELECT player_name FROM " + table + " WHERE player_name = ?");
            preparedStatement.setString(1, pseudo);
            ResultSet rs = preparedStatement.executeQuery();
            if(rs.next()) {
                if (pseudo.equalsIgnoreCase(rs.getString("player_name"))) {
                    connection.close();
                    return true;
                }
            }
            preparedStatement.close();
            connection.close();
        } catch (SQLException e) {
            System.err.println("Erreur lors du check de bannissement.");
        }
        return false;
    }

    /**
     * Verifier si son ban est permanent.
     *
     * @param pseudo
     * @return
     */
    public static boolean isBanPermanent(String pseudo) {
        try {
            final Connection connection = SQLManager.DENORIA_SERVER.getDatabaseAccess().getRessource();
            PreparedStatement preparedStatement = connection
                    .prepareStatement("SELECT ban_perm FROM " + table + " WHERE player_name = ?");
            preparedStatement.setString(1, pseudo);
            ResultSet rs = preparedStatement.executeQuery();
            while (rs.next()) {
                if (rs.getInt("ban_perm") == 1) {
                    connection.close();
                    return true;
                }
            }
            preparedStatement.close();
            connection.close();
        } catch (SQLException e) {
            System.err.println("Erreur lors du check de bannissement perm.");
        }
        return false;
    }

    /**
     * Deban un Joueur.
     *
     * @param pseudo
     */
    public static void unBanPlayer(String pseudo) {
        try {
            final Connection connection = SQLManager.DENORIA_SERVER.getDatabaseAccess().getRessource();
            PreparedStatement preparedStatement = connection
                    .prepareStatement("DELETE FROM " + table + " WHERE player_name = ?");
            preparedStatement.setString(1, pseudo);
            preparedStatement.executeUpdate();
            preparedStatement.close();
            connection.close();
        } catch (SQLException e) {
            System.err.println("Erreur lors du unbannissement.");
        }
    }

    /**
     * Récupérer la raison d'un ban.
     *
     * @param pseudo
     * @return
     */
    public static String getBanRaison(String pseudo) {
        String raison = null;
        try {
            final Connection connection = SQLManager.DENORIA_SERVER.getDatabaseAccess().getRessource();
            PreparedStatement preparedStatement = connection
                    .prepareStatement("SELECT ban_raison FROM " + table + " WHERE player_name = ?");
            preparedStatement.setString(1, pseudo);
            ResultSet rs = preparedStatement.executeQuery();
            if (!rs.next()) {
                connection.close();
                return null;
            }
            raison = rs.getString("ban_raison");
            preparedStatement.close();
            connection.close();
        } catch (SQLException e) {
            System.err.println("Erreur lors du get raison.");
        }
        return raison;
    }

    /**
     * Récupérer l'auteur d'un ban.
     *
     * @param pseudo
     * @return
     */
    public static String getBanAuthor(String pseudo) {
        String raison = null;
        try {
            final Connection connection = SQLManager.DENORIA_SERVER.getDatabaseAccess().getRessource();
            PreparedStatement preparedStatement = connection
                    .prepareStatement("SELECT ban_author FROM " + table + " WHERE player_name = ?");
            preparedStatement.setString(1, pseudo);
            ResultSet rs = preparedStatement.executeQuery();
            if (!rs.next()) {
                connection.close();
                return null;
            }
            raison = rs.getString("ban_author");
            preparedStatement.close();
            connection.close();
        } catch (SQLException e) {
            System.err.println("Erreur lors du get author.");
        }
        return raison;
    }

    /**
     * Récupérer la date d'un ban.
     *
     * @param pseudo
     * @return
     */
    public static String getBanDate(String pseudo) {
        String raison = null;
        try {
            final Connection connection = SQLManager.DENORIA_SERVER.getDatabaseAccess().getRessource();
            PreparedStatement preparedStatement = connection
                    .prepareStatement("SELECT ban_date FROM " + table + " WHERE player_name = ?");
            preparedStatement.setString(1, pseudo);
            ResultSet rs = preparedStatement.executeQuery();
            if (!rs.next()) {
                connection.close();
                return null;
            }
            raison = rs.getString("ban_date");
            preparedStatement.close();
            connection.close();
        } catch (SQLException e) {
            System.err.println("Erreur lors du get date.");
        }
        return raison;
    }

    @SuppressWarnings("deprecation")
    /**
     * Récupérer la durée du ban.
     *
     * @param pseudo
     * @return
     */
    public static String getBanDuree(String pseudo) {
        String raison = null;
        try {
            final Connection connection = SQLManager.DENORIA_SERVER.getDatabaseAccess().getRessource();
            PreparedStatement preparedStatement = connection
                    .prepareStatement("SELECT ban_duree FROM " + table + " WHERE player_name = ?");
            preparedStatement.setString(1, pseudo);
            ResultSet rs = preparedStatement.executeQuery();
            if (!rs.next()) {
                connection.close();
                return null;
            }
            raison = rs.getDate("ban_duree").toLocaleString();
            preparedStatement.close();
            connection.close();
        } catch (SQLException e) {
            System.err.println("Erreur lors du get duree.");
        }
        return raison;
    }
}
