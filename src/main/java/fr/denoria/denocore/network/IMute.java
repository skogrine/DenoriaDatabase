package fr.denoria.denocore.network;

import fr.denoria.denocore.mysql.SQLManager;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;


public class IMute {

    protected static String table = "players_muted";

    /**
     * Créer un mute permanent.
     *
     * @param pseudo
     * @param raison
     * @param author
     */
    public static void createMuteLifeTime(String pseudo, String raison, String author) {
        try {
            final Connection connection = SQLManager.DENORIA_SERVER.getDatabaseAccess().getRessource();
            PreparedStatement preparedStatement = connection.prepareStatement("INSERT INTO " + table
                    + " (player_name, mute_raison, mute_date, mute_auhtor, mute_perm) VALUES(?, ?, ?, ?, ?)");
            preparedStatement.setString(1, pseudo);
            preparedStatement.setString(2, raison);
            preparedStatement.setString(3, new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").format(new java.util.Date()));
            preparedStatement.setString(4, author);
            preparedStatement.setInt(5, 1);
            preparedStatement.executeUpdate();
            preparedStatement.close();
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Créer un mute avec un certain temps.
     *
     * @param pseudo
     * @param raison
     * @param author
     * @param hour
     */
    public static void createMuteTime(String pseudo, String raison, String author, Integer hour) {
        try {
            final Connection connection = SQLManager.DENORIA_SERVER.getDatabaseAccess().getRessource();
            PreparedStatement preparedStatement = connection.prepareStatement("INSERT INTO " + table
                    + " (player_name, mute_raison, mute_date, mute_auhtor, mute_duree) VALUES(?, ?, ?, ?, DATE_ADD(NOW(), INTERVAL "
                    + hour + " HOUR))");
            preparedStatement.setString(1, pseudo);
            preparedStatement.setString(2, raison);
            preparedStatement.setString(3, new SimpleDateFormat("dd/MM/yyyy hh:mm:ss").format(new java.util.Date()));
            preparedStatement.setString(4, author);
            preparedStatement.executeUpdate();
            preparedStatement.close();
            connection.close();
        } catch (SQLException e) {
            System.err.println("Erreur lors du bannissement.");
        }
    }

    /**
     * Suppression des mute avec le temps.
     */
    public static void suppMuteTime() {
        try {
            final Connection connection = SQLManager.DENORIA_SERVER.getDatabaseAccess().getRessource();
            PreparedStatement preparedStatement = connection
                    .prepareStatement("DELETE FROM " + table + " WHERE mute_duree <= NOW()");
            preparedStatement.executeUpdate();
            preparedStatement.close();
            connection.close();
        } catch (SQLException e) {
            System.err.println("Erreur lors du suppression de Ban.");
        }
    }

    /**
     * Vérifier si le Joueur est mute.
     *
     * @param pseudo
     * @return
     */
    public static boolean isMute(String pseudo) {
        suppMuteTime();
        try {
            final Connection connection = SQLManager.DENORIA_SERVER.getDatabaseAccess().getRessource();
            PreparedStatement preparedStatement = connection
                    .prepareStatement("SELECT player_name FROM " + table + " WHERE player_name = ?");
            preparedStatement.setString(1, pseudo);
            ResultSet rs = preparedStatement.executeQuery();
            while (rs.next()) {
                if (pseudo.equalsIgnoreCase(rs.getString("player_name"))) {
                    connection.close();
                    return true;
                }
            }
            preparedStatement.close();
            connection.close();
        } catch (SQLException e) {
            System.err.println("Erreur lors du check de bannissement.");
        }
        return false;
    }

    /**
     * Verifier si son mute est permanent.
     *
     * @param pseudo
     * @return
     */
    public static boolean isMutePermanent(String pseudo) {
        try {
            final Connection connection = SQLManager.DENORIA_SERVER.getDatabaseAccess().getRessource();
            PreparedStatement preparedStatement = connection
                    .prepareStatement("SELECT mute_perm FROM " + table + " WHERE player_name = ?");
            preparedStatement.setString(1, pseudo);
            ResultSet rs = preparedStatement.executeQuery();
            while (rs.next()) {
                if (rs.getInt("mute_perm") == 1) {
                    connection.close();
                    return true;
                }
            }
            preparedStatement.close();
            connection.close();
        } catch (SQLException e) {
            System.err.println("Erreur lors du check de bannissement perm.");
        }
        return false;
    }

    /**
     * Unmute un Joueur.
     *
     * @param pseudo
     */
    public static void unMutePlayer(String pseudo) {
        try {
            final Connection connection = SQLManager.DENORIA_SERVER.getDatabaseAccess().getRessource();
            PreparedStatement preparedStatement = connection
                    .prepareStatement("DELETE FROM " + table + " WHERE player_name = ?");
            preparedStatement.setString(1, pseudo);
            preparedStatement.executeUpdate();
            preparedStatement.close();
            connection.close();
        } catch (SQLException e) {
            System.err.println("Erreur lors du unbannissement.");
        }
    }

    /**
     * Récupérer la raison d'un mute.
     *
     * @param pseudo
     * @return
     */
    public static String getMuteRaison(String pseudo) {
        String raison = null;
        try {
            final Connection connection = SQLManager.DENORIA_SERVER.getDatabaseAccess().getRessource();
            PreparedStatement preparedStatement = connection
                    .prepareStatement("SELECT mute_raison FROM " + table + " WHERE player_name = ?");
            preparedStatement.setString(1, pseudo);
            ResultSet rs = preparedStatement.executeQuery();
            if (!rs.next()) {
                connection.close();
                return null;
            }
            raison = rs.getString("mute_raison");
            preparedStatement.close();
            connection.close();
        } catch (SQLException e) {
            System.err.println("Erreur lors du get raison.");
        }
        return raison;
    }

    /**
     * Récupérer l'auteur d'un mute.
     *
     * @param pseudo
     * @return
     */
    public static String getMuteAuthor(String pseudo) {
        String raison = null;
        try {
            final Connection connection = SQLManager.DENORIA_SERVER.getDatabaseAccess().getRessource();
            PreparedStatement preparedStatement = connection
                    .prepareStatement("SELECT mute_auhtor FROM " + table + " WHERE player_name = ?");
            preparedStatement.setString(1, pseudo);
            ResultSet rs = preparedStatement.executeQuery();
            if (!rs.next()) {
                connection.close();
                return null;
            }
            raison = rs.getString("mute_auhtor");
            preparedStatement.close();
            connection.close();
        } catch (SQLException e) {
            System.err.println("Erreur lors du get author.");
        }
        return raison;
    }

    /**
     * Récupérer la date d'un mute.
     *
     * @param pseudo
     * @return
     */
    public static String getMuteDate(String pseudo) {
        String raison = null;
        try {
            final Connection connection = SQLManager.DENORIA_SERVER.getDatabaseAccess().getRessource();
            PreparedStatement preparedStatement = connection
                    .prepareStatement("SELECT mute_date FROM " + table + " WHERE player_name = ?");
            preparedStatement.setString(1, pseudo);
            ResultSet rs = preparedStatement.executeQuery();
            if (!rs.next()) {
                connection.close();
                return null;
            }
            raison = rs.getString("mute_date");
            preparedStatement.close();
            connection.close();
        } catch (SQLException e) {
            System.err.println("Erreur lors du get date.");
        }
        return raison;
    }

    @SuppressWarnings("deprecation")
    /**
     * Récupérer la durée du mute.
     *
     * @param pseudo
     * @return
     */
    public static String getMuteDuree(String pseudo) {
        String raison = null;
        try {
            final Connection connection = SQLManager.DENORIA_SERVER.getDatabaseAccess().getRessource();
            PreparedStatement preparedStatement = connection
                    .prepareStatement("SELECT mute_duree FROM " + table + " WHERE player_name = ?");
            preparedStatement.setString(1, pseudo);
            ResultSet rs = preparedStatement.executeQuery();
            if (!rs.next()) {
                connection.close();
                return null;
            }
            raison = rs.getDate("mute_duree").toLocaleString();
            preparedStatement.close();
            connection.close();
        } catch (SQLException e) {
            System.err.println("Erreur lors du get duree.");
        }
        return raison;
    }
}