package fr.denoria.denocore.commands.friends;

import java.util.*;

import fr.denoria.denocore.commands.AbstractCommand;
import fr.denoria.denocore.denoriaplayer.DenoriaPlayer;
import fr.denoria.denocore.denoriaplayer.infos.DataType;
import fr.denoria.denocore.support.MessagesUtils;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;



public class FriendsCommand extends AbstractCommand {

    private final String PREFIX = MessagesUtils.getFriendPrefix();
    private final String PREFIXFRIEND = "§aFriend §8❘ §7";

    private final String mtn = PREFIX + ChatColor.RED + "Fonctionnalité en maintenance, disponible rapidement...";


    public FriendsCommand() {
        this.cmd = Arrays.asList("friends", "friend", "f", "amis", "ami");
        this.register(this);
    }



    @Override
    public void handle(CommandSender sender, List<String> params) {
        if (!(sender instanceof Player)) {
            sender.sendMessage("Vous devez etre un joueur.");
            return;
        }
        Player player = (Player) sender;
        DenoriaPlayer iPlayer = new DenoriaPlayer(player);

        // Gérer les arguements.
        if (params.size() == 0) {
            helpInfo(player);
            return;
        }


        // Gérer les commandes à un seul Argument.
        if (params.size() == 1 && params.get(0).equalsIgnoreCase("help")) {
            helpInfo(player);
            return;
        }
        if (params.size() == 1) {


            // Activer les friends.
            if (params.get(0).equalsIgnoreCase("on")) {
                if (iPlayer.getPlayerData(DataType.ALLOW_FRIEND)) {
                    player.sendMessage(PREFIXFRIEND + "§cErreur: §7Vous acceptez déjà les demande d'amis");
                    return;
                }
                iPlayer.setPlayerData(DataType.ALLOW_FRIEND, true);
                player.sendMessage(PREFIXFRIEND + "Vous acceptez désormais les demandes d'amis");

                // Désactiver les friends.
            } else if (params.get(0).equalsIgnoreCase("off")) {
                if (!(iPlayer.getPlayerData(DataType.ALLOW_FRIEND))) {
                    player.sendMessage(PREFIXFRIEND + "§cErreur: §7Vous refusez déjà les demandes d'amis");
                    return;
                }
                iPlayer.setPlayerData(DataType.ALLOW_FRIEND, false);
                player.sendMessage(PREFIXFRIEND + "Vous refusez désormais les demandes d'amis");

                // Afficher la liste des friends.
            } else if (params.get(0).equalsIgnoreCase("list")) {


                // Faux Arguements
            } else {
                helpInfo(player);
                return;
            }

            // Gérer les commandes à deux Arguments.
        } else if (params.size() == 2) {

            Player target = Bukkit.getPlayer(params.get(1));

            if(target == null) {
                player.sendMessage(mtn);
                return;
            }

            // Ajouter un Ami.
            if ((params.get(0).equalsIgnoreCase("add")) || (params.get(0).equalsIgnoreCase("invite"))) {
                //IFriend.addPlayer(player, target);
                player.sendMessage(mtn);

            // Supprimer un Ami.
            } else if (params.get(0).equalsIgnoreCase("remove")) {
                player.sendMessage(mtn);

                // Faux Arguements
            } else if((params.get(0).equalsIgnoreCase("confirm"))){
                player.sendMessage(mtn);
                //IFriend.confirmFriend(player, target);
            } else {
                helpInfo(player);
            }

            // Faux Arguements.
        } else {
            helpInfo(player);
        }
    }

    @Override
    public void helpInfo(Player player) {
        player.sendMessage(MessagesUtils.getBlackSeparator());
        player.sendMessage("§d§lFriend §7(Aide /friends) ");
        player.sendMessage("");
        player.sendMessage(" §8() §d/friends help §8» §7Message d'aide du friends.");
        player.sendMessage(" §8() §d/friends add (player) §8» §7Ajouter un ami.");
        player.sendMessage(" §8() §c/friends remove (player) §8» §7Retirer un ami.");
        player.sendMessage(" §8() §d/friends list §8» §7Liste des amis");
        player.sendMessage(" §8() §d/friends tp/join (pseudo) §8» §7Rejoindre un ami");
        player.sendMessage(" §8() §d/friends [on/off] §8» §7Gérer la réception des demandes");
        player.sendMessage("");
        player.sendMessage(MessagesUtils.getBlackSeparator());
    }




    public void addFriend(Player player, Player player2){

    }

}
