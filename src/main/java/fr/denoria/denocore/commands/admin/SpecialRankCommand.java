/*
 * Copyright (c) 2019 - 2021 Denoria Network
 *
 *  ______   _______  _        _______  _______ _________ _______             _______  _        _______  _______  _______ _________ _        _______
 * (  __  \ (  ____ \( (    /|(  ___  )(  ____ )\__   __/(  ___  )       /\  (  ____ \| \    /\(  ___  )(  ____ \(  ____ )\__   __/( (    /|(  ____ \
 * | (  \  )| (    \/|  \  ( || (   ) || (    )|   ) (   | (   ) |      / /  | (    \/|  \  / /| (   ) || (    \/| (    )|   ) (   |  \  ( || (    \/
 * | |   ) || (__    |   \ | || |   | || (____)|   | |   | (___) |     / /   | (_____ |  (_/ / | |   | || |      | (____)|   | |   |   \ | || (__
 * | |   | ||  __)   | (\ \) || |   | ||     __)   | |   |  ___  |    / /    (_____  )|   _ (  | |   | || | ____ |     __)   | |   | (\ \) ||  __)
 * | |   ) || (      | | \   || |   | || (\ (      | |   | (   ) |   / /           ) ||  ( \ \ | |   | || | \_  )| (\ (      | |   | | \   || (
 * | (__/  )| (____/\| )  \  || (___) || ) \ \_____) (___| )   ( |  / /      /\____) ||  /  \ \| (___) || (___) || ) \ \_____) (___| )  \  || (____/\
 * (______/ (_______/|/    )_)(_______)|/   \__/\_______/|/     \|  \/       \_______)|_/    \/(_______)(_______)|/   \__/\_______/|/    )_)(_______/
 *
 */

package fr.denoria.denocore.commands.admin;

import fr.denoria.denocore.commands.AbstractCommand;
import fr.denoria.denocore.denoriaplayer.DenoriaPlayer;
import fr.denoria.denocore.denoriaplayer.UUIDFetcher;
import fr.denoria.denocore.denoriaplayer.rank.IRank;
import fr.denoria.denocore.denoriaplayer.rank.RANKSPECIAL;
import fr.denoria.denocore.denoriaplayer.rank.Rank;
import fr.denoria.denocore.support.MessagesUtils;
import fr.denoria.denocore.support.boards.TeamsTagsManager;
import net.md_5.bungee.api.ChatColor;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.Collections;
import java.util.List;

public class SpecialRankCommand extends AbstractCommand {

    public SpecialRankCommand() {
        this.cmd = Collections.singletonList("specialrank");
        this.register(this);
    }

    @Override
    public void handle(CommandSender sender, List<String> params) {
        if (sender instanceof Player) {
            Player player = (Player) sender;
            DenoriaPlayer iPlayer = DenoriaPlayer.getDenoriaPlayer(player);
            if (iPlayer.getRankPower() < Rank.ADMINISTRATOR.getPower()) {
                iPlayer.sendMessageNoPermission("ADMINISTRATOR");
                return;
            }
        }

        if ((params.size() == 1) && (params.get(0).equalsIgnoreCase("list"))) {
            sender.sendMessage("");
            sender.sendMessage(
                    "§8§m-----------------------------------");
            sender.sendMessage("");
            for (RANKSPECIAL rankList : RANKSPECIAL.values()) {
                sender.sendMessage("§8❘ " + rankList.getName() + ChatColor.DARK_GRAY
                        + " - §7/specialrank (joueur) " + rankList.getIdentificatorName());
            }
            sender.sendMessage("§8§m-----------------------------------");
            sender.sendMessage("");
            return;
        }

        // Vérification des arguments.
        if (params.size() != 2) {
            sender.sendMessage(MessagesUtils.getDenoriaPrefix() + ChatColor.RED + "Erreur: §7/specialrank (joueur) (grade)");
            return;
        }

        String targetName = params.get(0);
        RANKSPECIAL rankSpecial = RANKSPECIAL.getRankWithIdentificatorName(params.get(1));
        RANKSPECIAL targetRankSpecial = IRank.getPlayerSpecialRank(UUIDFetcher.getUUID(targetName));
        Rank rankBase = IRank.getPlayerRank(UUIDFetcher.getUUID(targetName));

        if (targetRankSpecial == null) {
            sender.sendMessage(
                    MessagesUtils.getDenoriaPrefix() + ChatColor.RED + "Erreur: §7Le joueur n'est pas trouvable.");
            return;
        }
        if (rankSpecial == null) {
            sender.sendMessage(MessagesUtils.getDenoriaPrefix() + ChatColor.RED + "Erreur: §7Ce grade est introuvable.");
            return;
        }
        if (targetRankSpecial.getIdentificatorName().equals(rankSpecial.getIdentificatorName())) {
            sender.sendMessage(
                    MessagesUtils.getDenoriaPrefix() + ChatColor.RED + "Erreur: §7Le joueur possède déjà ce grade.");
            return;
        }

        IRank.setPlayerRank(UUIDFetcher.getUUID(targetName), rankSpecial);
        sender.sendMessage(
                MessagesUtils.sendPrefixAdmin()
                        + "§7Vous avez rank §e"
                        + targetName
                        + " §7en tant que "
                        + rankSpecial.getName()
                        + " §7!");

        TeamsTagsManager.setNameTag(Bukkit.getPlayer(targetName), rankBase.getName(), rankBase.getChatColor() + rankBase.getPrefix(), rankSpecial.getSuffix());



    }

    @Override
    public void helpInfo(Player player) {

    }
}
