package fr.denoria.denocore.commands.admin;

import fr.denoria.denocore.commands.AbstractCommand;
import fr.denoria.denocore.denoriaplayer.DenoriaPlayer;
import fr.denoria.denocore.denoriaplayer.UUIDFetcher;
import fr.denoria.denocore.denoriaplayer.rank.IRank;
import fr.denoria.denocore.denoriaplayer.rank.RANKSPECIAL;
import fr.denoria.denocore.denoriaplayer.rank.Rank;
import fr.denoria.denocore.support.FunctionRaccourcie;
import fr.denoria.denocore.support.MessagesUtils;
import fr.denoria.denocore.support.boards.TeamsTagsManager;
import net.md_5.bungee.api.ChatColor;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.Collections;
import java.util.List;

public class RankCommand extends AbstractCommand {

    public RankCommand() {
        this.cmd = Collections.singletonList("rank");
        this.register(this);
    }

    @Override
    public void handle(CommandSender sender, List<String> params) {

        // Vérification des permissions.
        if (sender instanceof Player) {
            Player player = (Player) sender;
            DenoriaPlayer iPlayer = DenoriaPlayer.getDenoriaPlayer(player);
            if (!FunctionRaccourcie.isAdmin(iPlayer)) {
                iPlayer.sendMessageNoPermission("ADMINISTRATOR");
                return;
            }
        }

        // Récupérer la liste des Grades.
        if ((params.size() == 1) && (params.get(0).equalsIgnoreCase("list"))) {
            sender.sendMessage("");
            sender.sendMessage(
                    "§8§m-----------------------------------");
            sender.sendMessage("");
            for (Rank rankList : Rank.values()) {
                sender.sendMessage("§8❘ " + rankList.getChatColor() + rankList.getName() + ChatColor.DARK_GRAY
                        + " - §7/rank (joueur) " + rankList.getIdentificatorName());
            }
            sender.sendMessage("§8§m-----------------------------------");
            sender.sendMessage("");
            return;
        }

        // Vérification des arguments.
        if (params.size() != 2) {
            sender.sendMessage(MessagesUtils.getDenoriaPrefix() + ChatColor.RED + "Erreur: §7/rank (joueur) (grade)");
            return;
        }

        // Gestion de la commande.
        String targetName = params.get(0);
        Rank rank = Rank.getRankWithIdentificatorName(params.get(1));
        Rank targetRank = IRank.getPlayerRank(UUIDFetcher.getUUID(targetName));
        RANKSPECIAL targetRankSpecial = IRank.getPlayerSpecialRank(UUIDFetcher.getUUID(targetName));

        if (targetRank == null) {
            sender.sendMessage(
                    MessagesUtils.getDenoriaPrefix() + ChatColor.RED + "Erreur: §7Le joueur n'est pas trouvable.");
            return;
        }
        if (rank == null) {
            sender.sendMessage(MessagesUtils.getDenoriaPrefix() + ChatColor.RED + "Erreur: §7Ce grade est introuvable.");
            return;
        }
        if (targetRank.getIdentificatorName().equals(rank.getIdentificatorName())) {
            sender.sendMessage(
                    MessagesUtils.getDenoriaPrefix() + ChatColor.RED + "Erreur: §7Le joueur possède déjà ce grade.");
            return;
        }
        IRank.setPlayerRank(UUIDFetcher.getUUID(targetName), rank);
        sender.sendMessage(
                MessagesUtils.sendPrefixAdmin()
                        + "§7Vous avez rank §e"
                        + targetName
                        + " §7en tant que "
                        + rank.getChatColor()
                        + rank.getName()
                        + " §7!");

        if(Bukkit.getPlayer(targetName) != null) {
            Bukkit.getPlayer(targetName).sendMessage(
                    MessagesUtils.getDenoriaPrefix()
                            + "§7Vous avez reçu le grade "
                            + rank.getChatColor()
                            + rank.getName()
                            + " §7!");
        }

        TeamsTagsManager.setNameTag(Bukkit.getPlayer(targetName), rank.getOrderRank(), rank.getChatColor() + rank.getPrefix(), targetRankSpecial.getSuffix());


    }




    @Override
    public void helpInfo(Player player) {
    }
}
