package fr.denoria.denocore.commands.admin;

import java.util.Collections;
import java.util.List;
import java.util.UUID;

import fr.denoria.denocore.commands.AbstractCommand;
import fr.denoria.denocore.denoriaplayer.DenoriaPlayer;
import fr.denoria.denocore.denoriaplayer.UUIDFetcher;
import fr.denoria.denocore.denoriaplayer.misc.IPermissions;
import fr.denoria.denocore.denoriaplayer.rank.Rank;
import fr.denoria.denocore.support.MessagesUtils;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import net.md_5.bungee.api.ChatColor;

public class PermissionCommand extends AbstractCommand {

    public PermissionCommand() {
        this.cmd = Collections.singletonList("dperm");
        this.register(this);
    }

    @Override
    public void handle(CommandSender sender, List<String> params) {

        // Vérification des permissions.
        if (sender instanceof Player) {
            Player player = (Player) sender;
            DenoriaPlayer iPlayer = DenoriaPlayer.getDenoriaPlayer(player);
            if (iPlayer.getRankPower() < Rank.ADMINISTRATOR.getPower()) {
                iPlayer.sendMessageNoPermission("ADMINISTRATOR");
                return;
            }
        }

        // Vérification des arguments.
        if (params.size() != 3) {
            sender.sendMessage(MessagesUtils.getDenoriaPrefix() + ChatColor.RED
                    + "Utiliser: /dperm [player] [add|remove] [permission]");
            return;
        }

        // Récupération du Joueur.
        String targetName = params.get(0);
        UUID uuidTarget = UUIDFetcher.getUUID(targetName);
        if (uuidTarget == null) {
            sender.sendMessage(
                    MessagesUtils.getDenoriaPrefix() + ChatColor.RED + "Erreur: Le joueur n'est pas trouvable.");
            return;
        }
        String permission = params.get(2);

        // Ajouter une permission à un Joueur.
        if (params.get(1).equalsIgnoreCase("add")) {
            if (!(IPermissions.hasPermission(uuidTarget, permission))) {
                IPermissions.addPermission(uuidTarget, permission);
            }
            sender.sendMessage(MessagesUtils.getDenoriaPrefix() + "§fAjout de la permission §a" + permission
                    + " §fau joueur §d" + targetName + "§f.");
            return;

            // Supprimer la permission à un Joueur.
        } else if (params.get(1).equalsIgnoreCase("remove")) {
            if (IPermissions.hasPermission(uuidTarget, permission)) {
                IPermissions.suppPermission(uuidTarget, permission);
            }
            sender.sendMessage(MessagesUtils.getDenoriaPrefix() + "§fSuppresion de la permission §c" + permission
                    + " §fau joueur §d" + targetName + "§f.");
            return;

        } else {
            sender.sendMessage(MessagesUtils.getDenoriaPrefix() + ChatColor.RED
                    + "Utiliser: /dperm [player] [add|remove] [permission]");
            return;
        }
    }

    @Override
    public void helpInfo(Player player) {
    }
}
