/*
 * Copyright (c) 2019 - 2021 Denoria Network
 *
 *  ______   _______  _        _______  _______ _________ _______             _______  _        _______  _______  _______ _________ _        _______
 * (  __  \ (  ____ \( (    /|(  ___  )(  ____ )\__   __/(  ___  )       /\  (  ____ \| \    /\(  ___  )(  ____ \(  ____ )\__   __/( (    /|(  ____ \
 * | (  \  )| (    \/|  \  ( || (   ) || (    )|   ) (   | (   ) |      / /  | (    \/|  \  / /| (   ) || (    \/| (    )|   ) (   |  \  ( || (    \/
 * | |   ) || (__    |   \ | || |   | || (____)|   | |   | (___) |     / /   | (_____ |  (_/ / | |   | || |      | (____)|   | |   |   \ | || (__
 * | |   | ||  __)   | (\ \) || |   | ||     __)   | |   |  ___  |    / /    (_____  )|   _ (  | |   | || | ____ |     __)   | |   | (\ \) ||  __)
 * | |   ) || (      | | \   || |   | || (\ (      | |   | (   ) |   / /           ) ||  ( \ \ | |   | || | \_  )| (\ (      | |   | | \   || (
 * | (__/  )| (____/\| )  \  || (___) || ) \ \_____) (___| )   ( |  / /      /\____) ||  /  \ \| (___) || (___) || ) \ \_____) (___| )  \  || (____/\
 * (______/ (_______/|/    )_)(_______)|/   \__/\_______/|/     \|  \/       \_______)|_/    \/(_______)(_______)|/   \__/\_______/|/    )_)(_______/
 *
 */

package fr.denoria.denocore.commands.admin;

import fr.denoria.denocore.commands.AbstractCommand;
import fr.denoria.denocore.denoriaplayer.DenoriaPlayer;
import fr.denoria.denocore.denoriaplayer.currency.Currency;
import fr.denoria.denocore.denoriaplayer.rank.Rank;
import fr.denoria.denocore.support.MessagesUtils;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.Collections;
import java.util.List;

public class CurrencyCommand extends AbstractCommand {

    public CurrencyCommand() {
        this.cmd = Collections.singletonList("dcurrency");
        this.register(this);
    }

    @Override
    public void handle(CommandSender sender, List<String> params) {

        if (sender instanceof Player) {
            DenoriaPlayer denoriaPlayer = new DenoriaPlayer((Player)sender);
            if (denoriaPlayer.getRank().getPower() >= Rank.RESPONSABLE.getPower()) {
                if (params.size() == 0) {
                    helpInfo(denoriaPlayer.getPlayer());
                    return;
                }
                if (params.size() == 3) {
                    Player target = Bukkit.getPlayer(params.get(0));
                    if (target == null) {
                        denoriaPlayer.getPlayer().sendMessage(MessagesUtils.getDenoriaPrefix() + "§cErreur: §7Ce joueur n'est pas en ligne !");
                        return;
                    }

                    DenoriaPlayer denoriaTarget = new DenoriaPlayer(target);



                    if (params.get(1).equalsIgnoreCase("denogemmes")){
                        if (params.get(2).equalsIgnoreCase("reset")) {
                            denoriaTarget.removeCoins(Currency.DENOGEMMES, denoriaTarget.getCoins(Currency.DENOGEMMES));
                        } else {
                            int montant = Integer.parseInt(params.get(2));
                            denoriaTarget.addCoins(Currency.DENOGEMMES, montant);
                            denoriaPlayer.getPlayer().sendMessage(MessagesUtils.getDenoriaPrefix() + "§7Vous avez envoyé §c" + montant + " §7DenoGemmes à " +
                                    denoriaTarget.getPlayer().getName());
                        }
                        return;

                    }
                    if (params.get(1).equalsIgnoreCase("denocoins")){
                        if (params.get(2).equalsIgnoreCase("reset")) {
                            denoriaTarget.removeCoins(Currency.DENOCOINS, denoriaTarget.getCoins(Currency.DENOCOINS));
                            return;
                        } else {
                            int montant = Integer.parseInt(params.get(2));
                            denoriaTarget.addCoins(Currency.DENOCOINS, montant);
                            denoriaPlayer.getPlayer().sendMessage(MessagesUtils.getDenoriaPrefix() + "§7Vous avez envoyé §d" + montant + " §7DenoCoins à " +
                                    denoriaTarget.getPlayer().getName());
                        }
                    }


                    else {
                        denoriaPlayer.getPlayer().sendMessage(MessagesUtils.getDenoriaPrefix() + "§cErreur: §7Cette money n'existe pas !");
                    }
                    return;
                }
                else {
                    helpInfo(denoriaPlayer.getPlayer());
                    return;
                }
            }
        }
    }

    @Override
    public void helpInfo(Player player) {
        player.sendMessage(MessagesUtils.getSeparatorWithText("§6Currency"));
        player.sendMessage("§7/currency (joueur) (DenoGemmes/DenoCoins) (montant)");
        player.sendMessage(MessagesUtils.getSeparator());
    }
}
