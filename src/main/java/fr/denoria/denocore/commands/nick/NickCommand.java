package fr.denoria.denocore.commands.nick;

import java.util.Collections;
import java.util.List;

import fr.denoria.denocore.commands.AbstractCommand;
import fr.denoria.denocore.denoriaplayer.DenoriaPlayer;
import fr.denoria.denocore.denoriaplayer.UUIDFetcher;
import fr.denoria.denocore.denoriaplayer.rank.Rank;
import fr.denoria.denocore.support.MessagesUtils;
import fr.denoria.denocore.support.utils.PermissionEnum;
import net.md_5.bungee.api.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class NickCommand extends AbstractCommand {

    private static final String nickPrefix = "§dAnti-Focus §8❘ §7";

    public NickCommand() {
        this.cmd = Collections.singletonList("nick");
        this.register(this);
    }

    @Override
    public void handle(CommandSender sender, List<String> params) {

        Player player = (Player) sender;
        DenoriaPlayer iPlayer = DenoriaPlayer.getDenoriaPlayer(player);

        // Vérifier si le compte du Joueur existe.
        if (iPlayer == null) {
            player.sendMessage(MessagesUtils.getDenoriaPrefix() + ChatColor.RED
                    + "Erreur: §7Votre compte rencontre un problème contactez un Admin.");
            return;
        }

        // Verifier la permission du Joueur.
        if ((iPlayer.getRankPower() < Rank.FRIEND.getPower())
                && (!(iPlayer.hasPermission(PermissionEnum.NICK.getName())))) {
            iPlayer.sendMessageNoPermission("FRIEND");
            return;
        }

        // Vérification des arguments.
        if ((params.size() == 0) || (params.size() > 2)) {
            helpInfo(player);
            return;
        }

        // Gestion des commandes à 1 ARGUMENT.
        if (params.size() == 1) {
            // Afficher la liste des grades.
            if (params.get(0).equalsIgnoreCase("ranklist")) {
                player.sendMessage(MessagesUtils.getSeparator());
                player.sendMessage(nickPrefix + "Liste des grades disponible pour l'Anti-Focus:");
                player.sendMessage("§8➥ §7Joueur§8, §aVIP§8, §eVIP+§8, §bHype§8, §dLégende");
                player.sendMessage(MessagesUtils.getSeparator());
                return;

                // Le joueur demande de l'aide sur la commande.
            } else if (params.get(0).equalsIgnoreCase("help")) {
                helpInfo(player);
                return;

                // Skin & Rank erroné.
            } else if (params.get(0).equalsIgnoreCase("rank")) {
                player.sendMessage(MessagesUtils.getDenoriaPrefix() +"§cErreur: §7Merci d'entrer un grade valable.");
                return;

                // Afficher les informations.
            } else if (params.get(0).equalsIgnoreCase("skin"))  {
                player.sendMessage(MessagesUtils.getDenoriaPrefix() + "§cErreur: §7Merci d'entrer un pseudonyme.");
                return;

            } else if ((params.get(0).equalsIgnoreCase("infos")) || (params.get(0).equalsIgnoreCase("info"))) {
                if (!(iPlayer.isNick())) {
                    player.sendMessage(nickPrefix + ChatColor.RED + "Erreur: §7Votre anti-focus n'est pas activé.");
                    return;
                } else {

                    player.sendMessage("§dAnti-Focus §8❘ §7Votre nick est: " +
                            NickManager.getRankName(player.getUniqueId()).getChatColor() +
                            NickManager.getRankName(player.getUniqueId()).getPrefix() +
                            " " +
                            NickManager.getNickName(player.getUniqueId()));
                    player.sendMessage("§dAnti-Focus §8❘ §7Votre skin est celui de: §d" +
                            NickManager.getSkinName(player.getUniqueId()));

                }

                // Supprimer l'anti-focus.
            } else if (params.get(0).equalsIgnoreCase("reset")) {
                if (!(iPlayer.isNick())) {
                    player.sendMessage(nickPrefix + ChatColor.RED + "Erreur: §7Votre anti-focus n'est pas activé.");
                    return;
                } else {
                    NickManager.removeNick(iPlayer.getUUID());
                    iPlayer.setNick(false);
                    player.sendMessage(nickPrefix + "Vous avez §cdésactivé §7l'Anti-Focus.");
                    player.sendMessage(
                            "§8➥ §7Ce changement aura lieu au changement de serveur.");
                    return;
                }

                // Créer l'anti-focus.
            } else {
                String nickName = params.get(0);
                if (nickName.length() <= 2) {
                    player.sendMessage(
                            nickPrefix + ChatColor.RED + "Erreur: §7Le pseudo '§b" + nickName + "§7' est trop court.");
                    return;
                } else if (nickName.length() > 16) {
                    player.sendMessage(
                            nickPrefix + ChatColor.RED + "Erreur: §7Le pseudo '§b" + nickName + "§7' est trop long.");
                    return;
                } else {
                    // Vérifier si le pseudo n'est pas déjà utilisé.
                    DenoriaPlayer denoriaPlayer = new DenoriaPlayer(player);
                    if (!(denoriaPlayer.getRank().getPower() >= Rank.DESIGNER.getPower()) ) {
                        if (UUIDFetcher.getUUID(nickName) != null) {
                            player.sendMessage(
                                    nickPrefix + ChatColor.RED + "Erreur: §7Le pseudo '§b" + nickName + "§7' est déjà utilisé.");
                            return;
                        }
                    }

                    // Déjà utilisé par un autre joueur en ligne.
                    if (NickManager.getNickList().contains(nickName)) {
                        player.sendMessage(
                                nickPrefix + ChatColor.RED + "Erreur: §7Le pseudo '§b" + nickName + "§7' est déjà utilisé.");
                        return;
                    }

                    // Vérifier d'un nom correct.
                    String checkName = safeChar(nickName);
                    if (checkName.length() != nickName.length()) {
                        player.sendMessage(
                                nickPrefix + ChatColor.RED + "Erreur: §7Merci d'utiliser les caractères alphanumérique.");
                        return;
                    }

                    // Changer le pseudo du Joueur.
                    if (iPlayer.isNick()) {
                        NickManager.enableNick(iPlayer.getUUID(), NickManager.getRealName(iPlayer.getUUID()), nickName,
                                NickManager.getSkinName(iPlayer.getUUID()));
                    } else {
                        NickManager.enableNick(iPlayer.getUUID(), player.getName(), nickName,
                                NickPseudoEnum.getRandomPseudo());
                    }
                    iPlayer.setNick(true);
                    player.sendMessage(nickPrefix + "Votre nouveau pseudonyme est §a" + NickManager.getNickName(player.getUniqueId()));
                    player.sendMessage(
                            "§8➥ §7Ce changement aura lieu au changement de serveur.");
                    return;
                }
            }

            // Gestion des commandes à 2 ARGUMENTS.
        } else if (params.size() == 2) {
            // Changer de Grade.
            if ((params.get(0).equalsIgnoreCase("rank")) || (params.get(0).equalsIgnoreCase("grade"))) {

                // Vérifier si le joueur est en nick.
                if (!(iPlayer.isNick())) {
                    player.sendMessage(nickPrefix + ChatColor.RED + "Erreur: Votre anti-focus n'est pas activé.");
                    return;
                }

                String rankName = params.get(1);
                Rank rankType;
                if (rankName.equalsIgnoreCase("VIP+")) {
                    rankType = Rank.VIPP;
                } else if (rankName.equalsIgnoreCase("joueur")){
                    rankType = Rank.PLAYER;
                } else if (rankName.equalsIgnoreCase("légende")) {
                    rankType = Rank.LEGEND;
                } else {
                    rankType = Rank.getRankWithIdentificatorName(rankName);
                }

                // Grade trop puissant.
                if ((rankType == null)
                        ||
                        ((rankType.getPower() > Rank.LEGEND.getPower())
                        &&
                        (iPlayer.getRankPower() < Rank.DESIGNER.getPower()))) {
                    player.sendMessage(nickPrefix + "§cErreur: §7Le grade que vous cherchez est introuvable.");
                    return;
                }





                if(iPlayer.getRankPower() < rankType.getPower()){
                    player.sendMessage(nickPrefix + "§cErreur: §7Vous ne pouvez pas mettre un préfix suppérieur de votre grade !");
                }

                // Appplication du nouveau grade.
                NickManager.updateRank(iPlayer.getUUID(), rankType);
                player.sendMessage(nickPrefix + ChatColor.WHITE + "Vous utilisez maintenant le grade: "
                        + rankType.getChatColor() + rankType.getName() + ChatColor.WHITE + ".");
                player.sendMessage(nickPrefix + ChatColor.RED + "Changer de serveur pour appliquer les changements...");
                return;

                // Changer de Skin.
            } else if (params.get(0).equalsIgnoreCase("skin")) {

                // Vérifier si le joueur est en nick.
                if (!(iPlayer.isNick())) {
                    player.sendMessage(MessagesUtils.getDenoriaPrefix()+ "§cErreur: §7Votre anti-focus n'est pas activé.");
                    return;
                }

                // Commande.
                String skinName = params.get(1);

                // Vérifier si le pseudo existe.
                if (UUIDFetcher.getUUID(skinName) == null) {
                    player.sendMessage(
                            nickPrefix + ChatColor.RED + "Erreur: §7Le pseudo '" + skinName + "' n'existe pas.");
                    return;
                }

                NickManager.updateSkin(iPlayer.getUUID(), skinName);
                player.sendMessage(nickPrefix + "§7Vous utilisez maintenant le skin §b" + skinName + "§7.");
                player.sendMessage(nickPrefix + ChatColor.RED + "Changer de serveur pour appliquer les changements...");
                return;

            } else {
                player.sendMessage(nickPrefix + ChatColor.RED + "Erreur: §7La commande /nick '§c" + params.get(0) +"§7' est introuvable.");
                return;
            }
        }
    }

    /**
     * Vérificatio de l'alphanumérique.
     *
     * @param input
     * @return
     */
    public String safeChar(String input) {
        char[] allowed = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ_".toCharArray();
        char[] charArray = input.toCharArray();
        StringBuilder result = new StringBuilder();
        for (char c : charArray) {
            for (char a : allowed) {
                if (c == a)
                    result.append(a);
            }
        }
        return result.toString();
    }

    @Override
    public void helpInfo(Player player) {
        DenoriaPlayer denoriaPlayer = new DenoriaPlayer(player);
        player.sendMessage(MessagesUtils.getSeparator());
        player.sendMessage("§d§lAnti-Focus §7(Aide /nick)");
        player.sendMessage("");
        player.sendMessage(" §8() §d/nick help §8» §7Message d'aide de l'Anti-Focus.");
        player.sendMessage(" §8() §d/nick (pseudo) §8» §7Activer l'Anti-Focus.");
        player.sendMessage(" §8() §d/nick skin (pseudo) §8» §7Changer le skin.");
        player.sendMessage(" §8() §d/nick rank (grade) §8» §7Changer le grade.");
        player.sendMessage(" §8() §d/nick reset §8» §7Désactiver l'anti-focus.");
        player.sendMessage("");
        player.sendMessage(" §8() §d/nick ranklist §8» §7Voir la liste des grades disponible.");
        player.sendMessage(" §8() §d/nick infos §8» §7Voir les informations de son Anti-Focus.");
        if(denoriaPlayer.getRank().getPower() >= Rank.STAFF.getPower()) {
            player.sendMessage(" §8() §c/nicklist §8» §7Voir les joueurs en /nick");
        }
        player.sendMessage("");
        player.sendMessage(MessagesUtils.getSeparator());
    }
}
