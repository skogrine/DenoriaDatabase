package fr.denoria.denocore.commands.nick;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class NickPseudoEnum {

    public NickPseudoEnum() {}

    /**
     * Récupérer un pseudo aléatoire.
     *
     * @return
     */
    public static String getRandomPseudo() {
        List<String> pseudoList = new ArrayList<>();
        pseudoList.add("Syrize");
        pseudoList.add("Frigiel");
        pseudoList.add("PopiGames");
        pseudoList.add("Hetinox");
        pseudoList.add("StigAxe_");
        pseudoList.add("2004David103");
        pseudoList.add("jujubruleur");
        pseudoList.add("Hypixel");
        pseudoList.add("hugo4715");
        pseudoList.add("TuEstSimple");
        pseudoList.add("JeSuisTheolas");
        pseudoList.add("Guibenville");
        pseudoList.add("Siphano");
        pseudoList.add("DjoccoSiffredi");
        pseudoList.add("DiiaBloMax");
        pseudoList.add("IBaFien");
        pseudoList.add("ExanaIG");
        pseudoList.add("Charox53");
        pseudoList.add("WixterOne");
        pseudoList.add("Parmatt");
        pseudoList.add("iRockyMax");
        pseudoList.add("Bloody_Diana");
        pseudoList.add("elolita");
        pseudoList.add("Siphano");
        pseudoList.add("Skogrine");
        pseudoList.add("skatoux");
        return pseudoList.get(new Random().nextInt(pseudoList.size()));
    }
}
