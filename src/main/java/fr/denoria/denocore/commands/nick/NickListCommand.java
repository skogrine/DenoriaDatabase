package fr.denoria.denocore.commands.nick;

import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import fr.denoria.denocore.commands.AbstractCommand;
import fr.denoria.denocore.denoriaplayer.DenoriaPlayer;
import fr.denoria.denocore.denoriaplayer.UUIDFetcher;
import fr.denoria.denocore.denoriaplayer.rank.IRank;
import fr.denoria.denocore.denoriaplayer.rank.Rank;
import fr.denoria.denocore.support.MessagesUtils;
import net.md_5.bungee.api.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class NickListCommand extends AbstractCommand {

    private String nickPrefix = MessagesUtils.getDenoriaPrefix();

    public NickListCommand() {
        this.cmd = Arrays.asList("nicklist");
        this.register(this);
    }

    @Override
    public void handle(CommandSender sender, List<String> params) {

        Player player = (Player) sender;
        DenoriaPlayer iPlayer = DenoriaPlayer.getDenoriaPlayer(player);

        // Vérifier si le compte du Joueur existe.
        if (iPlayer == null) {
            player.sendMessage(MessagesUtils.getDenoriaPrefix() + ChatColor.RED
                    + "Erreur: §7Votre compte rencontre un problème contactez un Admin.");
            return;
        }

        // Verifier la permission du Joueur.
        if (iPlayer.getRankPower() < Rank.MODERATOR.getPower()) {
            iPlayer.sendMessageNoPermission("MODERATOR");
            return;
        }

        // Vérifier si il y a des joueurs en nick.
        if ((NickManager.getNickList().isEmpty()) || (NickManager.getNickList().size() == 0)) {
            player.sendMessage(nickPrefix + "§cErreur: §7Il n'y a pas de joueurs en mode anti-focus.");
            return;
        }

        // Gestion de la commande.
        player.sendMessage(MessagesUtils.getSeparator());
        iPlayer.sendCenteredMessage("§dJoueur actuellement en /nick §7(Hub-1)");
        player.sendMessage("");
        int i = 0;
        for (String nameList : NickManager.getNickList()) {
            i++;
            UUID uuidPlayer = UUIDFetcher.getUUID(nameList);
            Rank realRank = IRank.getPlayerRank(uuidPlayer);
            Rank nickRank = NickManager.getRankName(uuidPlayer);
            String nickName = NickManager.getNickName(uuidPlayer);
            String realName = NickManager.getRealName(uuidPlayer);


            player.sendMessage(" §8() "
                    + nickRank.getChatColor()
                    + nickRank.getPrefix()
                    + " "
                    + nickName
                    + " §7est en réalité "
                    + realRank.getChatColor()
                    + realRank.getPrefix()
                    + " "
                    + realName
                    + "§7.");
        }
        player.sendMessage("§7§oIl y a actuellement §d§o" + i + " §7§opersonnes en /nick sur ce serveur.");
        player.sendMessage("");
        player.sendMessage(MessagesUtils.getSeparator());
    }

    @Override
    public void helpInfo(Player player) {}
}
