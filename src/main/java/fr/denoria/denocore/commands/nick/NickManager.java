package fr.denoria.denocore.commands.nick;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;


import fr.denoria.denocore.denoriaplayer.rank.Rank;
import fr.denoria.denocore.redis.RedisManager;
import redis.clients.jedis.Jedis;

public class NickManager {

    /**
     * Activer un /nick.
     *
     * @param uuid uuid du joueur
     * @param name
     */
    public static void enableNick(UUID uuid, String playerName, String name, String skinName) {
        try (Jedis jedis = RedisManager.getRedisDatabase().getJedisPool().getResource()) {
            boolean isNick = isNick(uuid);
            if (isNick) {
                jedis.lrem("nicklist", 0, getRealName(uuid));
            }
            jedis.set("nickplayers:" + uuid.toString() + "_" + "nick", "true");
            jedis.set("nickplayers:" + uuid.toString() + "_" + "nickname", name);
            if (!isNick) {
                jedis.set("nickplayers:" + uuid.toString() + "_" + "rank", Rank.PLAYER.getIdentificatorName());
            }
            if (!isNick) {
                jedis.set("nickplayers:" + uuid.toString() + "_" + "skin", skinName);
            }
            if (!isNick) {
                jedis.set("nickplayers:" + uuid.toString() + "_" + "realname", playerName);
            }
            jedis.rpush("nicklist", playerName);
            System.out.println("[NickManager] Activation du Nick.");
        } catch (Exception e) {
            System.out.println("[NickManager] Impossible d'activer le Nick.");
        }
    }

    /**
     * Supprimer un /nick.
     *
     * @param uuid uuid du joueur
     */
    public static void removeNick(UUID uuid) {
        try (Jedis jedis = RedisManager.getRedisDatabase().getJedisPool().getResource()) {
            jedis.lrem("nicklist", 0, getRealName(uuid));
            jedis.del("nickplayers:" + uuid.toString() + "_" + "nick");
            jedis.del("nickplayers:" + uuid.toString() + "_" + "nickname");
            jedis.del("nickplayers:" + uuid.toString() + "_" + "rank");
            jedis.del("nickplayers:" + uuid.toString() + "_" + "skin");
            jedis.del("nickplayers:" + uuid.toString() + "_" + "realname");
            System.out.println("[NickManager] Désactivation du Nick.");
        } catch (Exception e) {
            System.out.println("[NickManager] Impossible de désactiver le Nick.");
        }
    }

    /**
     * Update le Grade.
     *
     * @param uuid uuid du joueur uuid du joueur
     * @param name rank
     */
    public static void updatePseudo(UUID uuid, String name) {
        try (Jedis jedis = RedisManager.getRedisDatabase().getJedisPool().getResource()) {
            jedis.set("nickplayers:" + uuid.toString() + "_" + "nickname", name);
        } catch (Exception e) {
            System.out.println("[NickManager] Impossible de changer le grade du Nick.");
        }
    }

    /**
     * Update le Grade.
     *
     * @param uuid uuid du joueur
     * @param rank
     */
    public static void updateRank(UUID uuid, Rank rank) {
        try (Jedis jedis = RedisManager.getRedisDatabase().getJedisPool().getResource()) {
            jedis.set("nickplayers:" + uuid.toString() + "_" + "rank", rank.getIdentificatorName());
        } catch (Exception e) {
            System.out.println("[NickManager] Impossible de changer le grade du Nick.");
        }
    }

    /**
     * Update le Skin.
     *
     * @param uuid uuid du joueur
     * @param skinName
     */
    public static void updateSkin(UUID uuid, String skinName) {
        try (Jedis jedis = RedisManager.getRedisDatabase().getJedisPool().getResource()) {
            jedis.set("nickplayers:" + uuid.toString() + "_" + "skin", skinName);
        } catch (Exception e) {
            System.out.println("[NickManager] Impossible de changer le skin du Nick.");
        }
    }

    /**
     * Récupérer le pseudo du Joueur réel.
     *
     * @param uuid uuid du joueur
     * @return
     */
    public static String getRealName(UUID uuid) {
        try (Jedis jedis = RedisManager.getRedisDatabase().getJedisPool().getResource()) {
            return jedis.get("nickplayers:" + uuid.toString() + "_" + "realname");
        } catch (Exception e) {
            System.out.println("[NickManager] Impossible de récupérer le pseudo réel du Nick.");
        }
        return null;
    }

    /**
     * Récupérer le pseudo du Joueur.
     *
     * @param uuid uuid du joueur
     * @return
     */
    public static String getNickName(UUID uuid) {
        try (Jedis jedis = RedisManager.getRedisDatabase().getJedisPool().getResource()) {
            return jedis.get("nickplayers:" + uuid.toString() + "_" + "nickname");
        } catch (Exception e) {
            System.out.println("[NickManager] Impossible de récupérer le pseudo du Nick.");
        }
        return null;
    }

    /**
     * Récupérer le skin Joueur.
     *
     * @param uuid uuid du joueur
     * @return
     */
    public static String getSkinName(UUID uuid) {
        try (Jedis jedis = RedisManager.getRedisDatabase().getJedisPool().getResource()) {
            return jedis.get("nickplayers:" + uuid.toString() + "_" + "skin");
        } catch (Exception e) {
            System.out.println("[NickManager] Impossible de récupérer le Skin du Nick.");
        }
        return null;
    }

    /**
     * Récupérer le Grade du Joueur.
     *
     * @param uuid uuid du joueur
     * @return
     */
    public static Rank getRankName(UUID uuid) {
        try (Jedis jedis = RedisManager.getRedisDatabase().getJedisPool().getResource()) {
            return Rank.getRankWithIdentificatorName(jedis.get("nickplayers:" + uuid.toString() + "_" + "rank"));
        } catch (Exception e) {
            System.out.println("[NickManager] Impossible de récupérer le grade du Nick.");
        }
        return null;
    }

    /**
     * Vérfier si le Joueur est en /nick.
     *
     * @param uuid uuid du joueur
     * @return return
     */
    public static boolean isNick(UUID uuid) {
        try (Jedis jedis = RedisManager.getRedisDatabase().getJedisPool().getResource()) {
            if ((jedis.get("nickplayers:" + uuid.toString() + "_" + "nick") != null)
                    && (jedis.get("nickplayers:" + uuid.toString() + "_" + "nick").equalsIgnoreCase("true")))
                return true;
        } catch (Exception e) {
            System.out.println("[NickManager] Impossible de récupérer la valeur boolean.");
        }
        return false;
    }

    /**
     * Récupérer la list des /nicks.
     *
     * @return
     */
    public static List<String> getNickList() {
        List<String> nameNickList = new ArrayList<>();
        try (Jedis jedis = RedisManager.getRedisDatabase().getJedisPool().getResource()) {
            nameNickList.addAll(jedis.lrange("nicklist", 0, 50));
        } catch (Exception e) {
            System.out.println("[NickManager] Impossible de récupérer la liste des joueurs nick.");
        }
        return nameNickList;
    }
}
