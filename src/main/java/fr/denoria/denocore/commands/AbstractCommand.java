package fr.denoria.denocore.commands;

import java.util.List;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public abstract class AbstractCommand {

    public List<String> cmd;

    public AbstractCommand() {

    }

    public void register(AbstractCommand command) {

    }

    public abstract void handle(CommandSender sender, List<String> params);

    /**
     * Help player
     *
     * @param player
     **/
    public abstract void helpInfo(Player player);

}
