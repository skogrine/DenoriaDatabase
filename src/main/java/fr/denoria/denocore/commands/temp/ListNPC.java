/*
 * Copyright (c) 2019 - 2021 Denoria Network
 *
 *  ______   _______  _        _______  _______ _________ _______             _______  _        _______  _______  _______ _________ _        _______
 * (  __  \ (  ____ \( (    /|(  ___  )(  ____ )\__   __/(  ___  )       /\  (  ____ \| \    /\(  ___  )(  ____ \(  ____ )\__   __/( (    /|(  ____ \
 * | (  \  )| (    \/|  \  ( || (   ) || (    )|   ) (   | (   ) |      / /  | (    \/|  \  / /| (   ) || (    \/| (    )|   ) (   |  \  ( || (    \/
 * | |   ) || (__    |   \ | || |   | || (____)|   | |   | (___) |     / /   | (_____ |  (_/ / | |   | || |      | (____)|   | |   |   \ | || (__
 * | |   | ||  __)   | (\ \) || |   | ||     __)   | |   |  ___  |    / /    (_____  )|   _ (  | |   | || | ____ |     __)   | |   | (\ \) ||  __)
 * | |   ) || (      | | \   || |   | || (\ (      | |   | (   ) |   / /           ) ||  ( \ \ | |   | || | \_  )| (\ (      | |   | | \   || (
 * | (__/  )| (____/\| )  \  || (___) || ) \ \_____) (___| )   ( |  / /      /\____) ||  /  \ \| (___) || (___) || ) \ \_____) (___| )  \  || (____/\
 * (______/ (_______/|/    )_)(_______)|/   \__/\_______/|/     \|  \/       \_______)|_/    \/(_______)(_______)|/   \__/\_______/|/    )_)(_______/
 *
 */

package fr.denoria.denocore.commands.temp;

import fr.denoria.denocore.commands.AbstractCommand;
import fr.denoria.denocore.support.npc.CustomNPC;
import fr.denoria.denocore.support.npc.NPCManager;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.Collections;
import java.util.List;

public class ListNPC extends AbstractCommand {

    public ListNPC() {
        this.cmd = Collections.singletonList("npc");
        this.register(this);
    }

    @Override
    public void handle(CommandSender sender, List<String> params) {

        NPCManager npcManager = new NPCManager();

        if(params.size() == 0) {
            for (CustomNPC entity : npcManager.getEntities().keySet()) {
                sender.sendMessage(entity.displayName);
            }
        }

        if(params.size() == 1) {
            if(params.get(0).equalsIgnoreCase("remove")) {
                npcManager.removeAllNPC();
            }
        }


    }

    @Override
    public void helpInfo(Player player) {

    }
}
