package fr.denoria.denocore.commands;

import java.util.ArrayList;
import java.util.List;

import fr.denoria.denocore.DenoCore;

import fr.denoria.denocore.denoriaplayer.DenoriaPlayer;
import fr.denoria.denocore.denoriaplayer.rank.Rank;
import fr.denoria.denocore.support.server.SwitchServer;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;

public class PlayerProcessCommandListener implements Listener {

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onPlayerCommandPreprocess(PlayerCommandPreprocessEvent event) {
        Player player = event.getPlayer();
        DenoriaPlayer denoriaPlayer = DenoriaPlayer.getDenoriaPlayer(player);
        String message = event.getMessage();
        String[] args = message.split(" ");

        // Bloquer les commandes chiantes.
        if ((args[0].contains(":"))) {
            player.sendMessage(ChatColor.WHITE + "Commande inconnue.");
            event.setCancelled(true);
            return;
        }

        // Lock des commandes inutiles.
        List<String> fuckedCommand = new ArrayList<>();
        fuckedCommand.add("/minecraft");
        fuckedCommand.add("/bukkit");
        fuckedCommand.add("/me");
        fuckedCommand.add("/pl");
        fuckedCommand.add("/plugins");
        fuckedCommand.add("/?");
        fuckedCommand.add("/ver");
        fuckedCommand.add("/version");
        fuckedCommand.add("/plugin");
        fuckedCommand.add("/ban-ip");
        fuckedCommand.add("/banlist");
        fuckedCommand.add("/scoreboard");
        fuckedCommand.add("/seed");
        fuckedCommand.add("/restart");
        fuckedCommand.add("/viaver");
        fuckedCommand.add("/viaversion");
        fuckedCommand.add("/bungee");
        fuckedCommand.add("/icanhasbukkit");
        fuckedCommand.add("/me");
        fuckedCommand.add("/gc");
        for (String commandLock : fuckedCommand) {
            if(args[0].equalsIgnoreCase(commandLock)) {
                event.setCancelled(true);
                player.sendMessage(ChatColor.WHITE + "Commande inconnue.");
                return;
            }
        }

        // Gestion de quelques commandes.
        switch (args[0]) {
            case "/helpmod":
                event.setCancelled(true);
                break;
            case "/help":
                denoriaPlayer.sendPlayerHelp();
                event.setCancelled(true);
                break;
            case "/stop":
                if(denoriaPlayer.getRankPower() >= Rank.ADMINISTRATOR.getPower()) {
                    for(Player playerOnline : Bukkit.getOnlinePlayers()) {
                        DenoriaPlayer denoriaPlayerOnline = DenoriaPlayer.getDenoriaPlayer(playerOnline);
                        if(denoriaPlayerOnline != null) {
                            playerOnline.sendMessage("");
                            denoriaPlayerOnline.sendCenteredMessage("§4■ §cProblème technique §4■");
                            denoriaPlayerOnline.sendCenteredMessage("§7Un problème avec le serveur est survenue.");
                            playerOnline.sendMessage("");
                        }
                        SwitchServer.sendPlayerToLobby(playerOnline, true);
                    }
                    Bukkit.getScheduler().runTaskLater(DenoCore.getInstance(), () -> {
                        Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "stop");
                        Bukkit.shutdown();
                    }, 200L);
                }
                event.setCancelled(true);
                break;
            case "/tps":
                player.performCommand("lag");
                event.setCancelled(true);
                break;
            default:
                break;
        }
    }
}
