package fr.denoria.denocore.commands.mods.mute;

import java.util.Arrays;
import java.util.List;

import fr.denoria.denocore.DenoCore;
import fr.denoria.denocore.commands.AbstractCommand;
import fr.denoria.denocore.denoriaplayer.DenoriaPlayer;
import fr.denoria.denocore.denoriaplayer.rank.Rank;
import fr.denoria.denocore.network.IMute;
import fr.denoria.denocore.support.MessagesUtils;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class UnmuteCommand extends AbstractCommand {

    private String PREFIX = MessagesUtils.getDenoriaPrefix();

    public UnmuteCommand() {
        this.cmd = Arrays.asList("unmute");
        this.register(this);
    }

    @Override
    public void handle(CommandSender sender, List<String> params) {

        // Console
        if (!(sender instanceof Player)) {
            sender.sendMessage("Vous devez etre un joueur.");
            return;
        }

        Player player = (Player) sender;
        DenoriaPlayer denoriaPlayer = DenoriaPlayer.getDenoriaPlayer(player);

        // Permission
        if (denoriaPlayer.getRankPower() < Rank.MODERATOR.getPower()) {
            denoriaPlayer.sendMessageNoPermission("MODERATOR");
            return;
        }

        // Argument
        if (params.size() != 1) {
            helpInfo(player);
            return;
        }




        // Récupération du target.
        String target = params.get(0);

        if(Bukkit.getPlayer(target) != null) {

            Player targetPlayer = Bukkit.getPlayer(target);

            if (!(IMute.isMute(target))) {
                player.sendMessage(PREFIX + ChatColor.RED + "Erreur: §7Le joueur '" + params.get(0) + "' n'est pas mute.");
                return;
            }

            IMute.unMutePlayer(target);
            player.sendMessage(MessagesUtils.sendPrefixAdmin() + "§7Vous venez d'unmute le joueur §c" + target + "§7 !");
            targetPlayer.sendMessage(MessagesUtils.getDenoriaPrefix() + "§7Vous avez été unmute par l'administration !");

            DenoCore.getInstance().getRedisPubSubSpigot().broadcastModeration(
                    "§d(SC-Sanction) "
                            + denoriaPlayer.getRank().getChatColor()
                            + denoriaPlayer.getRank().getPrefix()
                            + " "
                            + denoriaPlayer.getPlayerRealName()
                            + " §8» §bJ'ai unmute '" + target + "'.");
            return;
        }
    }

    @Override
    public void helpInfo(Player player) {
        player.sendMessage("§8§m-----------------------------------");
        player.sendMessage("§c§lUnmute §7(Aide /unmute)");
        player.sendMessage("");
        player.sendMessage(" §8() §c/mute help §8» §7Message d'aide du mute");
        player.sendMessage(" §8() §c/mute (player) §8» §7Sanctionner un joueur");
        player.sendMessage(" §8() §c/unmute (player) §8» §7Retirer la sanction d'un joueur");
        player.sendMessage("");
        player.sendMessage("§8§m-----------------------------------");
    }
}
