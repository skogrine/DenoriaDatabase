package fr.denoria.denocore.commands.mods;

import java.util.*;

import fr.denoria.denocore.DenoCore;
import fr.denoria.denocore.commands.AbstractCommand;
import fr.denoria.denocore.denoriaplayer.DenoriaPlayer;
import fr.denoria.denocore.denoriaplayer.rank.Rank;
import fr.denoria.denocore.support.MessagesUtils;
import fr.denoria.denocore.support.builder.InventoryBuilder;
import fr.denoria.denocore.support.builder.items.ItemBuilder;
import fr.denoria.denocore.support.builder.items.heads.HeadBuilder;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.inventory.Inventory;

public class KickCommand extends AbstractCommand {

    private final String kickPrefix = MessagesUtils.getDenoriaPrefix();

    public KickCommand() {
        this.cmd = Collections.singletonList("kick");
        this.register(this);
    }

    @Override
    public void handle(CommandSender sender, List<String> params) {




        // Console
        if (!(sender instanceof Player)) {
            sender.sendMessage("Vous devez etre un joueur.");
            return;
        }

        Player player = (Player) sender;
        DenoriaPlayer denoriaPlayer = DenoriaPlayer.getDenoriaPlayer(player);

        // Permission
        if (denoriaPlayer.getRankPower() < Rank.GUIDE.getPower()) {
            denoriaPlayer.sendMessageNoPermission("ASSISTANT");
            return;
        }

        // Argument
        if (params.size() != 1) {
            helpInfo(player);
            return;
        }

        // Récupération du target.
        Player target = Bukkit.getPlayer(params.get(0));
        if (target == null) {
            player.sendMessage(
                    kickPrefix + ChatColor.RED + " Erreur: §7Le joueur '" + params.get(0) + "' n'est pas en ligne.");
            return;
        }

        // Si l'auteur == la victime
        if (player.getName().equalsIgnoreCase(target.getName())) {
            player.sendMessage(kickPrefix + "§cErreur: §7Vous ne pouvez pas sanctionner ce joueur.");
            return;
        }

        // Sanction d'une personne plus puissante que lui.
        if ((DenoriaPlayer.getDenoriaPlayer(target).getRankPower() >= denoriaPlayer.getRankPower())
                && (!(player.getName().equalsIgnoreCase("Skogrine")))) {
            player.sendMessage(kickPrefix + "§cErreur: §7Vous ne pouvez pas sanctionner ce joueur.");
            return;
        }

        new KickMenu(player, target);
        player.sendMessage(kickPrefix + "§7Ouverture du menu de sanction pour §b" + target.getName() + "§7.");
    }

    @Override
    public void helpInfo(Player player) {
        player.sendMessage("§8§m-----------------------------------");
        player.sendMessage("§c§lKick §7(Aide /kick)");
        player.sendMessage("");
        player.sendMessage(" §8() §c/kick help §8» §7Message d'aide du kick");
        player.sendMessage(" §8() §c/kick (player) §8» §7Sanctionner un joueur");
        player.sendMessage("");
        player.sendMessage("§8§m-----------------------------------");
    }

    /**
     * Gestion du menu de Kick.
     *
     * @author Faiden
     *
     */
    public class KickMenu implements Listener {

        public Map<Player, Inventory> menuInventoryMap = new HashMap<>();

        /**
         * Création du Menu d'Exemple.
         *
         * @param player player
         */
        public KickMenu(Player player, Player target) {
            Bukkit.getPluginManager().registerEvents(this, DenoCore.getInstance());
            DenoriaPlayer iplayer = new DenoriaPlayer(target);
            Inventory inventory = new InventoryBuilder("§cÉjecter " + target.getName())
                    .addLine(new String[] { "", "", "", "", "infos", "", "", "", ""})
                    .addLine(null)
                    .addLine(new String[] { "", "spam", "lag", "usebug", "avertissement", "", "", "", "" })
                    .addLine(null)
                    .addLine(new String[] { "", "", "", "", "cancel", "", "", "", "" })
                    .setItem("cancel", new ItemBuilder()
                            .type(Material.ARROW)
                            .name("§cRetour")
                            .build())
                    .setItem("infos",
                            new HeadBuilder()
                                    .setHead(target.getName())
                                    .setName("§cMute " + target.getName())
                                    .setLore("§8❘ §7Grade: " + iplayer.getRank().getChatColor() + iplayer.getRank().getPrefix())
                                    .build())


                    .setItem("usebug", new ItemBuilder()
                            .type(Material.DIRT)
                            .name("§bUsebug")
                            .lore("§7Sanction instantanée / Kick", "", "§7Sanction: §cKick", "", "§7Exemple:",
                                    "§8❘ §7Usebug Block...", "", "§8➟ §eClique droit pour sanctionner")
                            .build())

                    .setItem("lag",
                            new ItemBuilder()
                                    .type(Material.ICE)
                                    .name("§cLag")
                                    .lore("§7Sanction instantanée / Kick", "", "§7Sanction: §cKick", "",
                                            "§7Exemple:", "§8❘ §7Le joueur a §c+250MS§7 donc, il lag", "", "§8➟ §eClique droit pour sanctionner")
                                    .build())
                    .setItem("spam",
                            new ItemBuilder()
                                    .type(Material.PAPER)
                                    .name("§cSpam")
                                    .lore("§7Sanction instantanée / Kick", "", "§7Sanction: §cKick", "", "§7Exemple:", "§8❘ §7Salut",
                                            "§8❘ §7Salut", "§8❘ §7Salut", "§8❘ §7Salut", "", "§8➟ §eClique droit pour sanctionner")
                                    .build())

                    .setItem("avertissement", new ItemBuilder()
                            .type(Material.REDSTONE_TORCH_ON)
                            .name("§cAvertissement")
                            .lore("§7Sanction instantanée / Kick", "", "§cSanction: §cKick", "",
                                    "§7Exemple:", "§8❘ §7Avertissement", "", "§8➟ §eClique droit pour sanctionner")
                            .build())

                    .build(player);
            this.menuInventoryMap.put(player, inventory);
            player.openInventory(menuInventoryMap.get(player));
        }

        @EventHandler
        public void onInventoryClose(InventoryCloseEvent event) {
            Player player = (Player) event.getPlayer();
            if (event.getInventory() == null)
                return;
            if (menuInventoryMap.get(player) == null)
                return;
            if (menuInventoryMap.get(player).equals(event.getInventory()))
                menuInventoryMap.remove(player);
        }

        @EventHandler
        public void onInventoryClick(InventoryClickEvent event) {
            Player player = (Player) event.getWhoClicked();
            DenoriaPlayer denoriaPlayer = DenoriaPlayer.getDenoriaPlayer(player);
            if (event.getInventory() == null)
                return;
            if (event.getCurrentItem() == null)
                return;
            if (event.getCurrentItem().getType().equals(Material.AIR))
                return;
            if (!(event.getInventory().equals(menuInventoryMap.get(player))))
                return;
            event.setCancelled(true);
            Player target = Bukkit.getPlayer(event.getInventory().getName().replace("§cÉjecter ", ""));
            if(target == null) {
                player.sendMessage(kickPrefix + "§cErreur: §7Le Joueur n'est plus en ligne.");
                player.closeInventory();
                return;
            }
            switch (event.getCurrentItem().getType()) {
                case ARROW:
                    player.sendMessage(kickPrefix + "§7La sanction vient d'être §cannulée§f.");
                    player.closeInventory();
                    break;
                case DIRT:
                    target.kickPlayer(
                            "§d§lDenoria Network\n§cVous venez d'être kick du serveur.\n§3\n§fRaison: §eUtilisation d'un bug§f.");
                    player.sendMessage(kickPrefix + "§7La sanction vient d'être appliquée à §b" + target.getName() + "§7.");
                    DenoCore.getInstance().getRedisPubSubSpigot()
                            .broadcastModeration("§d(SC-Sanction) "
                                    + denoriaPlayer.getRank().getChatColor()
                                    + denoriaPlayer.getRank().getPrefix()
                                    + " "
                                    + denoriaPlayer.getPlayerRealName()
                                    + " §8» §b"
                                    + target.getName()
                                    + " a été kick pour \"Utilisation d'un bug\"");
                    player.closeInventory();
                    break;
                case ICE:
                    target.kickPlayer(
                            "§d§lDenoria Network\n§cVous venez d'être kick du serveur.\n§3\n§fRaison: §eLag perturbant§f.");
                    player.sendMessage(kickPrefix + "§7La sanction vient d'être appliquée à §b" + target.getName() + "§7.");
                    DenoCore.getInstance().getRedisPubSubSpigot()
                            .broadcastModeration("§d(SC-Sanction) "
                                    + denoriaPlayer.getRank().getChatColor()
                                    + denoriaPlayer.getRank().getPrefix()
                                    + " "
                                    + denoriaPlayer.getPlayerRealName()
                                    + " §8» §b"
                                    + target.getName()
                                    + " a été kick pour \"Lag perturbant\"");
                    player.closeInventory();
                    break;
                case PAPER:
                    target.kickPlayer(
                            "§d§lDenoria Network\n§cVous venez d'être kick du serveur.\n§3\n§fRaison: §eSpam dans le chat§f.");
                    player.sendMessage(kickPrefix + "§7La sanction vient d'être appliquée à §b" + target.getName() + "§7.");
                    DenoCore.getInstance().getRedisPubSubSpigot()
                            .broadcastModeration("§d(SC-Sanction) "
                                    + denoriaPlayer.getRank().getChatColor()
                                    + denoriaPlayer.getRank().getPrefix()
                                    + " "
                                    + denoriaPlayer.getPlayerRealName()
                                    + " §8» §b"
                                    + target.getName()
                                    + " a été kick pour \"Spam\"");
                    player.closeInventory();
                    break;
                case REDSTONE_TORCH_ON:
                    target.kickPlayer(
                            "§d§lDenoria Network\n§cVous venez d'être kick du serveur.\n§3\n§fRaison: §eAvertissement avant la sanction§f.");
                    player.sendMessage(kickPrefix + "§7La sanction vient d'être appliquée à §b" + target.getName() + "§7.");
                    DenoCore.getInstance().getRedisPubSubSpigot()
                            .broadcastModeration("§d(SC-Sanction) "
                                    + denoriaPlayer.getRank().getChatColor()
                                    + denoriaPlayer.getRank().getPrefix()
                                    + " "
                                    + denoriaPlayer.getPlayerRealName()
                                    + " §8» §b"
                                    + target.getName()
                                    + " a été kick pour \"Avertissement\"");
                    player.closeInventory();
                    break;
                default:
                    break;
            }
        }
    }
}
