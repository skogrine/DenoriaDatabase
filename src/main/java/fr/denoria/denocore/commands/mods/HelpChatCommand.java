package fr.denoria.denocore.commands.mods;

import java.util.Collections;
import java.util.List;

import fr.denoria.denocore.commands.AbstractCommand;
import fr.denoria.denocore.denoriaplayer.DenoriaPlayer;
import fr.denoria.denocore.denoriaplayer.rank.IRank;
import fr.denoria.denocore.denoriaplayer.rank.Rank;
import fr.denoria.denocore.support.MessagesUtils;
import org.bukkit.Bukkit;
import org.bukkit.Sound;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import net.md_5.bungee.api.ChatColor;

public class HelpChatCommand extends AbstractCommand {

    public final static String HELPPREFIX = MessagesUtils.getDenoriaPrefix();

    public HelpChatCommand() {
        this.cmd = Collections.singletonList("a");
        this.register(this);
    }

    @Override
    public void handle(CommandSender sender, List<String> params) {

        // Console
        if (!(sender instanceof Player)) {
            sender.sendMessage("Vous devez etre un joueur.");
            return;
        }

        Player player = (Player) sender;
        DenoriaPlayer denoriaPlayer = DenoriaPlayer.getDenoriaPlayer(player);

        // Permission du Joueur.
        if (denoriaPlayer.getRankPower() < Rank.GUIDE.getPower()) {
            denoriaPlayer.sendMessageNoPermission("ASSISTANT");
            return;
        }

        // Argument
        if (params.size() <= 1) {
            player.sendMessage(HELPPREFIX + ChatColor.RED + "Erreur: §7/a [joueur] [message].");
            return;
        }

        // Gestion de la commande.
        String targetName = params.get(0);
        StringBuilder message = new StringBuilder();
        for (int i = 1; i < params.size(); i++) {
            message.append(" ").append(params.get(i));
        }
        message.toString().trim();
        Player targetPlayer = Bukkit.getPlayer(targetName);
        if (targetPlayer == null) {
            player.sendMessage(HELPPREFIX + ChatColor.RED + "Erreur: §7Le joueur '" + targetName + "' n'est pas en ligne.");
            return;
        }
        if (targetName.equalsIgnoreCase(player.getName())) {
            player.sendMessage(HELPPREFIX + ChatColor.RED + "Erreur: §7Impossible de répondre à soi-même.");
            return;
        }
        String playerName = denoriaPlayer.getPlayerRealName();
        Rank playerRank = denoriaPlayer.getRank();
        if (denoriaPlayer.isNick()) {
            playerRank = IRank.getPlayerRank(denoriaPlayer.getUUID());
        }
        for (Player playerOnline : Bukkit.getOnlinePlayers()) {
            if (playerOnline.getName().equalsIgnoreCase(targetPlayer.getName())) {
                playerOnline.sendMessage(
                        playerRank.getChatColor()
                                + playerRank.getPrefix()
                                + MessagesUtils.getRankSpace(playerRank)
                                + playerName
                                + " §8» §b"
                                + targetPlayer.getName()
                                + " ➟ §f"
                                + message
                );
                playerOnline.playSound(playerOnline.getLocation(), Sound.NOTE_STICKS, 1.0f, 1.0f);
            } else {
                playerOnline.sendMessage(
                        playerRank.getChatColor()
                                + playerRank.getPrefix()
                                + MessagesUtils.getRankSpace(playerRank)
                                + playerName
                                + " §8» §b"
                                + targetPlayer.getName()
                                + " ➟ §f"
                                + message
                );
            }
        }
    }

    @Override
    public void helpInfo(Player player) {}
}
