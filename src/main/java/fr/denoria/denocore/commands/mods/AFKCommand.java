package fr.denoria.denocore.commands.mods;


import java.util.Collections;
import java.util.List;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import fr.denoria.denocore.DenoCore;
import fr.denoria.denocore.commands.AbstractCommand;
import fr.denoria.denocore.denoriaplayer.DenoriaPlayer;
import fr.denoria.denocore.denoriaplayer.infos.DataType;
import fr.denoria.denocore.denoriaplayer.rank.Rank;
import fr.denoria.denocore.support.MessagesUtils;



import net.md_5.bungee.api.ChatColor;

public class AFKCommand extends AbstractCommand {

    public static String AFK_PREFIX = MessagesUtils.getDenoriaPrefix();

    public AFKCommand() {
        this.cmd = Collections.singletonList("afk");
        this.register(this);
    }

    @Override
    public void handle(CommandSender sender, List<String> params) {

        // Console.
        if (!(sender instanceof Player)) {
            sender.sendMessage("Vous devez etre un joueur.");
            return;
        }

        Player player = (Player) sender;
        DenoriaPlayer denoriaPlayer = DenoriaPlayer.getDenoriaPlayer(player);

        // Permission du Joueur.
        if (denoriaPlayer.getRankPower() < Rank.STAFF.getPower()) {
            denoriaPlayer.sendMessageNoPermission("STAFF");
            return;
        }

        // Gestion des arguments.
        if ((params.size() != 0)) {
            player.sendMessage(MessagesUtils.getDenoriaPrefix() + ChatColor.RED + "Erreur: §7/afk");
            return;
        }


        // Gestion de la commande.
        boolean afk = denoriaPlayer.getPlayerData(DataType.AFK);
        if(afk) {
            denoriaPlayer.setPlayerData(DataType.AFK, false);
            player.sendMessage(AFK_PREFIX + "§7Vous êtes désormais §aDisponible§7.");
            DenoCore.getInstance().getRedisPubSubSpigot().broadcastStaff("§d(StaffChat) "
                    + denoriaPlayer.getRank().getChatColor()
                    + denoriaPlayer.getRank().getPrefix()
                    + " "
                    + denoriaPlayer.getPlayerRealName()
                    + " §7est désormais §aDisponible§7.");
        } else {
            denoriaPlayer.setPlayerData(DataType.AFK, true);
            player.sendMessage(AFK_PREFIX + "§7Vous êtes désormais §cIndisponible§7.");
            DenoCore.getInstance().getRedisPubSubSpigot().broadcastStaff("§d(StaffChat) "
                    + denoriaPlayer.getRank().getChatColor()
                    + denoriaPlayer.getRank().getPrefix()
                    + " "
                    + denoriaPlayer.getPlayerRealName()
                    + " §7est désormais §cIndisponible§7.");
        }
    }

    @Override
    public void helpInfo(Player player) {}
}
