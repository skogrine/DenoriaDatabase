package fr.denoria.denocore.commands.mods;

import java.util.Arrays;
import java.util.List;

import fr.denoria.denocore.commands.AbstractCommand;
import fr.denoria.denocore.denoriaplayer.DenoriaPlayer;
import fr.denoria.denocore.denoriaplayer.rank.Rank;
import fr.denoria.denocore.support.MessagesUtils;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import net.md_5.bungee.api.ChatColor;

public class LocalBroadcastCommand extends AbstractCommand {

    public static String PREFIX = MessagesUtils.getDenoriaPrefix();

    public LocalBroadcastCommand() {
        this.cmd = Arrays.asList("localbroadcast", "lbc", "localbc");
        this.register(this);
    }

    @Override
    public void handle(CommandSender sender, List<String> params) {

        // Console
        if (!(sender instanceof Player)) {
            sender.sendMessage("Vous devez etre un joueur.");
            return;
        }

        Player player = (Player) sender;
        DenoriaPlayer denoriaPlayer = DenoriaPlayer.getDenoriaPlayer(player);

        // Permission du Joueur.
        if (denoriaPlayer.getRankPower() < Rank.MODERATOR.getPower()) {
            denoriaPlayer.sendMessageNoPermission("MODERATOR");
            return;
        }

        // Gestion des arguments.
        if (params.size() < 1) {
            player.sendMessage(PREFIX + ChatColor.RED + "Erreur: §7Vous n'avez pas mis de message.");
            return;
        }

        // Gestion de la commande.
        StringBuilder message = new StringBuilder();
        for (String param : params) {
            message.append(" ").append(param);
        }
        String playerName = player.getName();
        if (denoriaPlayer.isNick()) {
            playerName = denoriaPlayer.getPlayerRealName();
        }
        Bukkit.broadcastMessage("§9Modération §8❘ §f" + message);
    }

    @Override
    public void helpInfo(Player player) {}
}
