package fr.denoria.denocore.commands.mods;


import fr.denoria.denocore.DenoCore;
import fr.denoria.denocore.commands.AbstractCommand;
import fr.denoria.denocore.denoriaplayer.DenoriaPlayer;
import fr.denoria.denocore.denoriaplayer.DenoriaPlayerInfo;
import fr.denoria.denocore.denoriaplayer.UUIDFetcher;
import fr.denoria.denocore.denoriaplayer.rank.Rank;
import fr.denoria.denocore.support.MessagesUtils;
import fr.denoria.denocore.support.server.SwitchServer;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.Collections;
import java.util.List;
import java.util.UUID;


public class DenoriaTeleportCommand extends AbstractCommand {

    private final String PREFIX = MessagesUtils.getDenoriaPrefix();

    public DenoriaTeleportCommand() {
        this.cmd = Collections.singletonList("dtp");
        this.register(this);
    }

    @Override
    public void handle(CommandSender sender, List<String> params) {

        // Console
        if (!(sender instanceof Player)) {
            sender.sendMessage("Vous devez etre un joueur.");
            return;
        }

        Player player = (Player) sender;
        DenoriaPlayer golemaPlayer = DenoriaPlayer.getDenoriaPlayer(player);

        // Gestion des permissions.
        if (golemaPlayer.getRankPower() < Rank.GUIDE.getPower()) {
            golemaPlayer.sendMessageNoPermission("ASSISTANT");
            return;
        }

        // Argument de la commande.
        if (params.size() != 1) {
            player.sendMessage(PREFIX + ChatColor.RED + "Erreur: §7/gtp [pseudo].");
            return;
        }

        // Récupération du target.
        String targetName = params.get(0);
        Player targetPlayer = Bukkit.getPlayer(targetName);
        if (targetPlayer != null) {
            player.sendMessage(PREFIX + ChatColor.RED + "Erreur: §7Vous êtes déjà connecté sur le serveur.");
            return;
        }

        // Si l'auteur == la victime.
        if (player.getName().equalsIgnoreCase(targetName)) {
            player.sendMessage(PREFIX + ChatColor.RED + "Erreur: §7Impossible de se téléporter à soi-même.");
            return;
        }

        // Gestion de la commande.
        UUID uuidTarget = UUIDFetcher.getUUID(targetName);
        if ((uuidTarget == null) || (DenoriaPlayerInfo.getServerName(uuidTarget) == null)) {
            player.sendMessage(PREFIX + ChatColor.RED + "Erreur: §7Le joueur n'est pas en ligne.");
            return;
        }
        SwitchServer.moveServer(player, DenoriaPlayerInfo.getServerName(uuidTarget), DenoCore.getInstance());
        player.sendMessage(MessagesUtils.sendPrefixAdmin()
                + "§7Vous avez été téléporté vers le serveur de §b"
                + targetName
                 + "§7.");
    }

    @Override
    public void helpInfo(Player player) {}
}
