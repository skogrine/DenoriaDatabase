package fr.denoria.denocore.commands.mods;

import java.util.Collections;
import java.util.List;

import fr.denoria.denocore.DenoCore;
import fr.denoria.denocore.commands.AbstractCommand;
import fr.denoria.denocore.denoriaplayer.DenoriaPlayer;
import fr.denoria.denocore.denoriaplayer.rank.Rank;
import fr.denoria.denocore.support.MessagesUtils;
import net.md_5.bungee.api.ChatColor;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitTask;

public class SlowChatCommand extends AbstractCommand {

    public static int slow = 0;
    static BukkitTask task;

    public static String SLOW_TAG = MessagesUtils.getDenoriaPrefix();

    public SlowChatCommand() {
        this.cmd = Collections.singletonList("slowchat");
        this.register(this);
    }

    @Override
    public void handle(CommandSender sender, List<String> params) {

        // Console
        if (!(sender instanceof Player)) {
            sender.sendMessage("Vous devez etre un joueur.");
            return;
        }

        Player player = (Player) sender;
        DenoriaPlayer golemaPlayer = DenoriaPlayer.getDenoriaPlayer(player);

        // Permission du Joueur.
        if (golemaPlayer.getRankPower() < Rank.GUIDE.getPower()) {
            golemaPlayer.sendMessageNoPermission("ASSISTANT");
            return;
        }

        // Gestion des arguments.
        if (params.size() != 1) {
            helpInfo(player);
            return;
        }

        // Gestions de la commande.
        if (params.size() == 1) {
            try {

                // Annuler le Slowchat.
                if(params.get(0).equalsIgnoreCase("off")) {
                    if(slow == 0) {
                        player.sendMessage(SLOW_TAG + "§cErreur: §7Le mode ralenti n'est pas activé.");
                        return;
                    }
                    if (task != null)
                        task.cancel();
                    Bukkit.broadcastMessage(SLOW_TAG + "§cErreur: §7Le tchat n'est plus ralenti.");
                    slow = 0;
                    return;
                }

                // Confirmation du Slow.
                int time = Integer.parseInt(params.get(0));
                if (time < 0 || time > 300) {
                    player.sendMessage(SLOW_TAG + "§cErreur: §7La valeur doit être comprise entre 0 et 300 seconde(s).");
                    return;
                }

                slow = time;
                Bukkit.broadcastMessage(SLOW_TAG + ChatColor.GOLD + " Le tchat a été ralenti." + ChatColor.GRAY
                        + " Vous pouvez envoyer un message toutes les " + ChatColor.GOLD + slow + " seconde(s)"
                        + ChatColor.GRAY + ".");
                runSlowChatOffTask();

            } catch (Exception e) {
                player.sendMessage(ChatColor.RED
                        + "Erreur: Merci d'utiliser une valeur numérique. En cas de problèmes veuillez contacter un Administrateur.");
                return;
            }
        }

    }

    @Override
    public void helpInfo(Player player) {
        player.sendMessage("");
        player.sendMessage(" " + ChatColor.GRAY + "» " + ChatColor.YELLOW + "" + ChatColor.BOLD + "Aide"
                + ChatColor.WHITE + "│ " + ChatColor.YELLOW + "" + ChatColor.BOLD + "SlowChat");
        player.sendMessage("");
        player.sendMessage(" " + ChatColor.GRAY + "" + ChatColor.BOLD + "■ " + ChatColor.GOLD + "/slowchat [duree] "
                + ChatColor.WHITE + "» " + ChatColor.AQUA + "Ralentir le (t)chat du serveur.");
        player.sendMessage(" " + ChatColor.GRAY + "" + ChatColor.BOLD + "■ " + ChatColor.GOLD + "/slowchat off "
                + ChatColor.WHITE + "» " + ChatColor.AQUA + "Rétablir le (t)chat.");
        player.sendMessage("");
    }

    //Tâche de SlowChat.

    public void runSlowChatOffTask() {
        if (task != null)
            task.cancel();
        task = Bukkit.getScheduler().runTaskLater(DenoCore.getInstance(), () -> {
            Bukkit.broadcastMessage(SLOW_TAG + ChatColor.GOLD + " Le tchat n'est plus ralenti.");
            slow = 0;
            return;
        }, 20 * 60 * 10);
    }
}
