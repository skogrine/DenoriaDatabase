package fr.denoria.denocore.commands.mods;

import java.util.Arrays;
import java.util.List;

import fr.denoria.denocore.commands.AbstractCommand;
import fr.denoria.denocore.denoriaplayer.DenoriaPlayer;
import fr.denoria.denocore.denoriaplayer.rank.Rank;
import fr.denoria.denocore.support.MessagesUtils;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class TeleportCommand extends AbstractCommand {

    private String PREFIX = MessagesUtils.getDenoriaPrefix();

    public TeleportCommand() {
        this.cmd = Arrays.asList("teleport", "tp");
        this.register(this);
    }

    @Override
    public void handle(CommandSender sender, List<String> params) {

        // Console
        if (!(sender instanceof Player)) {
            sender.sendMessage("Vous devez etre un joueur.");
            return;
        }

        Player player = (Player) sender;
        DenoriaPlayer denoriaPlayer = DenoriaPlayer.getDenoriaPlayer(player);

        // Permission
        if (denoriaPlayer.getRankPower() < Rank.COEUR.getPower()) {
            denoriaPlayer.sendMessageNoPermission("FRIEND2");
            return;
        }

        // Argument
        if (params.size() != 1) {
            player.sendMessage(PREFIX + ChatColor.RED + "Utiliser: /teleport [pseudo].");
            return;
        }

        // Récupération du target.
        String targetName = params.get(0);
        Player targetPlayer = Bukkit.getPlayer(targetName);
        if (targetPlayer == null) {
            player.sendMessage(PREFIX + ChatColor.RED + " Erreur: Le joueur '" + targetName + "' n'est pas en ligne.");
            return;
        }

        // Si l'auteur == la victime.
        if (player.getName().equalsIgnoreCase(targetName)) {
            player.sendMessage(PREFIX + ChatColor.RED + "Impossible de se téléporter à soi-même.");
            return;
        }

        // Gestion de la commande.
        player.teleport(targetPlayer.getLocation());
        player.sendMessage(PREFIX + ChatColor.WHITE + "Téléportation vers: " + ChatColor.RED + targetName
                + ChatColor.WHITE + ".");
        return;
    }

    @Override
    public void helpInfo(Player player) {}
}
