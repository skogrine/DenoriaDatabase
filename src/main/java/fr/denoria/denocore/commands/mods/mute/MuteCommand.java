package fr.denoria.denocore.commands.mods.mute;

import java.util.*;

import fr.denoria.denocore.DenoCore;
import fr.denoria.denocore.commands.AbstractCommand;
import fr.denoria.denocore.denoriaplayer.DenoriaPlayer;
import fr.denoria.denocore.denoriaplayer.rank.IRank;
import fr.denoria.denocore.denoriaplayer.rank.Rank;
import fr.denoria.denocore.network.IMute;
import fr.denoria.denocore.support.MessagesUtils;
import fr.denoria.denocore.support.builder.InventoryBuilder;
import fr.denoria.denocore.support.builder.items.ItemBuilder;
import fr.denoria.denocore.support.builder.items.heads.HeadBuilder;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.inventory.Inventory;

public class MuteCommand extends AbstractCommand {

    private final String PREFIX = MessagesUtils.getDenoriaPrefix();

    public MuteCommand() {
        this.cmd = Collections.singletonList("mute");
        this.register(this);
    }

    @Override
    public void handle(CommandSender sender, List<String> params) {

        // Console
        if (!(sender instanceof Player)) {
            sender.sendMessage("Vous devez etre un joueur.");
            return;
        }

        Player player = (Player) sender;
        DenoriaPlayer denoriaPlayer = DenoriaPlayer.getDenoriaPlayer(player);

        // Permission
        if (denoriaPlayer.getRankPower() < Rank.GUIDE.getPower()) {
            denoriaPlayer.sendMessageNoPermission("ASSISTANT");
            return;
        }

        // Argument
        if (params.size() != 1) {
            helpInfo(player);
            return;
        }

        // Récupération du target.
        String target = params.get(0);
        if (IMute.isMute(target)) {
            player.sendMessage(PREFIX + ChatColor.RED + "Erreur: Le joueur '" + params.get(0) + "' est déjà mute.");
            return;
        }

        // Vérifier la connexion du Joueur.
        Player playerTarget = Bukkit.getPlayer(target);
        if (playerTarget == null) {
            player.sendMessage(
                    PREFIX + ChatColor.RED + "Erreur: §7Le joueur '" + params.get(0) + "' n'est pas en ligne.");
            return;
        }

        // Si l'auteur == la victime
        if (player.getName().equalsIgnoreCase(target)) {
            player.sendMessage(PREFIX + ChatColor.RED + "§cErreur: §7Ce joueur ne peut pas être sanctionné.");
            return;
        }

        // Sanction d'une personne plus puissante que lui.
        if ((IRank.getPlayerRank(playerTarget.getUniqueId()).getPower() > denoriaPlayer.getRankPower())) {
            player.sendMessage(PREFIX + ChatColor.RED + "§cErreur: §7Vous ne pouvez pas sanctionner ce joueur.");
            return;
        }

        new MuteMenu(player, target);
        player.sendMessage(PREFIX + "§7Ouverture du menu de sanction pour §b" + target + "§7.");
    }

    @Override
    public void helpInfo(Player player) {
        player.sendMessage("§8§m-----------------------------------");
        player.sendMessage("§c§lMute §7(Aide /mute)");
        player.sendMessage("");
        player.sendMessage(" §8() §c/mute help §8» §7Message d'aide du mute");
        player.sendMessage(" §8() §c/mute (player) §8» §7Sanctionner un joueur");
        player.sendMessage(" §8() §c/unmute (player) §8» §7Retirer la sanction d'un joueur");
        player.sendMessage("");
        player.sendMessage("§8§m-----------------------------------");
    }

    /**
     * Gestion du menu de Kick.
     *
     * @author Faiden
     *
     */
    public class MuteMenu implements Listener {

        public Map<Player, Inventory> menuInventoryMap = new HashMap<>();

        /**
         * Création du Menu de Mute.
         *
         * @param player
         */
        public MuteMenu(Player player, String target) {
            Bukkit.getPluginManager().registerEvents(this, DenoCore.getInstance());
            DenoriaPlayer iplayer = new DenoriaPlayer(Bukkit.getPlayer(target));
            Inventory inventory = new InventoryBuilder("§cMuter " + target)


                    .addLine(new String[] { "", "", "", "", "infos", "", "", "", ""})
                    .addLine(null)
                    .addLine(new String[] { "", "provocations", "insultes", "insultes_staff", "spam_flood",
                            "incitation_haine", "link", "link_porn", "" })
                    .addLine(null)
                    .addLine(new String[] { "", "", "", "", "cancel", "", "", "", "" })

                    .setItem("cancel", new ItemBuilder().type(Material.ARROW).name("§cRetour").build())
                    .setItem("infos", new HeadBuilder()
                            .setHead(target)
                            .setName("§cMute " + target)
                            .setLore("§8❘ §7Grade: " + iplayer.getRank().getChatColor() + iplayer.getRank().getPrefix()).build())
                    .setItem("provocations",
                            new ItemBuilder().type(Material.BOOK).name("§cProvocations / Mauvais langage")
                                    .lore("§7Sanction longue / Mute", "", "§7Sanction: §c2 heures de mute", "",
                                            "§7Exemple:", "§8❘ §7Haha, c'était facile", "§8❘ §7T'es vraiment nul", "§8❘ §7EZ",
                                            "", "§8➟ §eClique droit pour sanctionner"
                                    )
                                    .build())
                    .setItem("insultes",
                            new ItemBuilder().type(Material.WOOD_HOE).name("§cInsultes")
                                    .lore("§7Sanction longue / Mute", "", "§7Sanction: §c12 heures de mute", "",
                                            "§7Exemple:", "§8❘ §7Sale pute de merde", "§8❘ §7Va te faire enculer", "",
                                            "§8➟ §eClique droit pour sanctionner"
                                    )
                                    .build())
                    .setItem("insultes_staff",
                            new ItemBuilder().type(Material.DIAMOND_HOE).name("§cInsultes Staff")
                                    .lore("§7Sanction longue / Mute", "", "§7Sanction: §c24 heures de mute", "",
                                            "§7Exemple:", "§8❘ §7Les modos sont vraiment des enculer", "§8❘ §7Les Builder mdrrr, vraiment nul xD",
                                            "", "§8➟ §eClique droit pour sanctionner"
                                    )
                                    .build())
                    .setItem("spam_flood",
                            new ItemBuilder().type(Material.PAPER).name("§cSpam §7(4 messages)§c / Flood")
                                    .lore("§7Sanction longue / Mute", "", "§7Sanction: §c4 heures de mute", "",
                                            "§7Exemple:",
                                            "§8❘ §7Haha vraiment pas sympa",
                                            "§8❘ §7Haha vraiment pas sympa",
                                            "§8❘ §7Haha vraiment pas sympa",
                                            "§8❘ §7Haha vraiment pas sympa",
                                            "§8❘ §7dfzerfghsdfgnefrghpb^jrgfn", "",
                                            "§8➟ §eClique droit pour sanctionner"
                                    )
                                    .build())
                    .setItem("incitation_haine",
                            new ItemBuilder().type(Material.FISHING_ROD).name("§cIncitation à la haine")
                                    .lore("§7Sanction longue / Mute", "",
                                            "§7Sanction: §c24 heures de mute", "",
                                            "§7Exemple:",
                                            "§8❘ §7Suicide toi, tu mérites rien",
                                            "§8❘ §7Foncez tous l'insulter",
                                            "", "§8➟ §eClique droit pour sanctionner"
                                    )
                                    .build())
                    .setItem("link",
                            new ItemBuilder().type(Material.SIGN).name("§cLiens / Publicité")
                                    .lore("§7Sanction longue / Mute", "", "§7Sanction: §c2 heures de mute", "",
                                            "§7Exemple:", "", "§8❘ §7Venez sur www.monserver.fr", "§8❘ §7Mon discord discord.gg/S3rv3ur",
                                            "", "§8➟ §eClique droit pour sanctionner"
                                    )
                                    .build())
                    .setItem("link_porn",
                            new ItemBuilder().type(Material.NAME_TAG).name("§cLiens pornographiques")
                                    .lore("§7Sanction longue / Mute",
                                            "",
                                            "§7Sanction: §c24 heures de mute", "",
                                            "§7Exemple:",
                                            "§8❘ §7Nouvelle vidéo www.pornhub.fr/view/maman",
                                            "§8❘ §7On connait xnxx.com héhé",
                                            "",
                                            "§8➟ §eClique droit pour sanctionner"
                                    )
                                    .build())
                    .build(player);
            this.menuInventoryMap.put(player, inventory);
            player.openInventory(menuInventoryMap.get(player));
        }

        @EventHandler
        public void onInventoryClose(InventoryCloseEvent event) {
            Player player = (Player) event.getPlayer();
            if (event.getInventory() == null)
                return;
            if (menuInventoryMap.get(player) == null)
                return;
            if (menuInventoryMap.get(player).equals(event.getInventory()))
                menuInventoryMap.remove(player);
        }

        @EventHandler
        public void onInventoryClick(InventoryClickEvent event) {
            Player player = (Player) event.getWhoClicked();
            DenoriaPlayer denoriaPlayer = DenoriaPlayer.getDenoriaPlayer(player);
            if (event.getInventory() == null)
                return;
            if (event.getCurrentItem() == null)
                return;
            if (event.getCurrentItem().getType().equals(Material.AIR))
                return;
            if (!(event.getInventory().equals(menuInventoryMap.get(player))))
                return;
            event.setCancelled(true);
            Player targetPlayer = Bukkit.getPlayer(event.getInventory().getName().replace("§cMuter ", ""));
            if (targetPlayer == null) {
                player.sendMessage(PREFIX + "§7Le Joueur n'est plus en ligne.");
                player.closeInventory();
                return;
            }
            switch (event.getCurrentItem().getType()) {
                case ARROW:
                    player.sendMessage(PREFIX + "§7La sanction vient d'être §cannulée§7.");
                    player.closeInventory();
                    break;
                /** Provocations / Langage incorrect */
                case BOOK:
                    String reasonProvocation = "Provocations / Langage incorrect";
                    IMute.createMuteTime(targetPlayer.getName(), reasonProvocation, denoriaPlayer.getPlayerRealName(), 2);
                    player.sendMessage(PREFIX + "§7La sanction vient d'être appliquée à §b" + targetPlayer.getName() + "§7.");
                    DenoCore.getInstance().getRedisPubSubSpigot()
                            .broadcastModeration("§d(SC-Sanction) "
                                    + denoriaPlayer.getRank().getChatColor()
                                    + denoriaPlayer.getRank().getPrefix()
                                    + " "
                                    + denoriaPlayer.getPlayerRealName()
                                    + " §8» §7J'ai mute le joueur '§b"
                                    + targetPlayer.getName()
                                    + "§7' pour le motif '§c"
                                    + reasonProvocation
                                    + "§7'. Durée: §c2 Heures§b.");
                    targetPlayer.sendMessage(PREFIX + "§7Vous venez de vous faire mute de ce serveur pour la raison: '§b"
                            + reasonProvocation + "§7', cette sanction est appliquée pour une durce de §c2 heures§7.");
                    DenoriaPlayer.getDenoriaPlayer(targetPlayer).setMute(true);
                    player.closeInventory();
                    break;
                /** Insultes */
                case WOOD_HOE:
                    String reasonInsult = "Insultes";
                    IMute.createMuteTime(targetPlayer.getName(), reasonInsult, denoriaPlayer.getPlayerRealName(), 12);
                    player.sendMessage(PREFIX + "§7La sanction vient d'être appliquée à §b" + targetPlayer.getName() + "§b.");
                    DenoCore.getInstance().getRedisPubSubSpigot()
                            .broadcastModeration("§d(SC-Sanction) "
                                    + denoriaPlayer.getRank().getChatColor()
                                    + denoriaPlayer.getRank().getPrefix()
                                    + " "
                                    + denoriaPlayer.getPlayerRealName()
                                    + " §8» §7J'ai mute le joueur '§b"
                                    + targetPlayer.getName()
                                    + "§7' pour le motif '§c"
                                    + reasonInsult
                                    + "§7'. Durée: §c12 Heures§b.");
                    targetPlayer.sendMessage(PREFIX + "§7Vous venez de vous faire mute de ce serveur pour la raison: '§b"
                            + reasonInsult + "§7', cette sanction est appliquée pour une durée de §c12 heures§7.");
                    DenoriaPlayer.getDenoriaPlayer(targetPlayer).setMute(true);
                    player.closeInventory();
                    break;
                /** Insultes Staff */
                case DIAMOND_HOE:
                    String reasonInsultStaff = "Insultes Staff";
                    IMute.createMuteTime(targetPlayer.getName(), reasonInsultStaff, denoriaPlayer.getPlayerRealName(), 24);
                    player.sendMessage(PREFIX + "§7La sanction vient d'être appliquée à §b" + targetPlayer.getName() + "§b.");
                    DenoCore.getInstance().getRedisPubSubSpigot()
                            .broadcastModeration("§d(SC-Sanction) "
                                    + denoriaPlayer.getRank().getChatColor()
                                    + denoriaPlayer.getRank().getPrefix()
                                    + " "
                                    + denoriaPlayer.getPlayerRealName()
                                    + " §8» §7J'ai mute le joueur '§b"
                                    + targetPlayer.getName()
                                    + "§7' pour le motif '§c"
                                    + reasonInsultStaff
                                    + "§7'. Durée: §c24 Heures§b.");
                    targetPlayer.sendMessage(PREFIX + "§7Vous venez de vous faire mute de ce serveur pour la raison: '§b"
                            + reasonInsultStaff + "§7', cette sanction est appliquée pour une durce de §c24 heures§7.");
                    DenoriaPlayer.getDenoriaPlayer(targetPlayer).setMute(true);

                    player.closeInventory();
                    break;
                /** Spam | Flood */
                case PAPER:
                    String reasonSpam = "Spam / Flood";
                    IMute.createMuteTime(targetPlayer.getName(), reasonSpam, denoriaPlayer.getPlayerRealName(), 4);
                    player.sendMessage(PREFIX + "§7La sanction vient d'être appliquée à §b" + targetPlayer.getName() + "§b.");
                    DenoCore.getInstance().getRedisPubSubSpigot()
                            .broadcastModeration("§d(SC-Sanction) "
                                    + denoriaPlayer.getRank().getChatColor()
                                    + denoriaPlayer.getRank().getPrefix()
                                    + " "
                                    + denoriaPlayer.getPlayerRealName()
                                    + " §8» §7J'ai mute le joueur '§b"
                                    + targetPlayer.getName()
                                    + "§7' pour le motif '§c"
                                    + reasonSpam
                                    + "§7'. Durée: §c4 Heures§b.");
                    targetPlayer.sendMessage(PREFIX + "§7Vous venez de vous faire mute de ce serveur pour la raison: '§b"
                            + reasonSpam + "§7', cette sanction est appliquée pour une durée de §c4 heures§7.");
                    DenoriaPlayer.getDenoriaPlayer(targetPlayer).setMute(true);
                    player.closeInventory();
                    break;
                /** Incitation à la haine */
                case FISHING_ROD:
                    String reasonHaine = "Incitation à la haine";
                    IMute.createMuteTime(targetPlayer.getName(), reasonHaine, denoriaPlayer.getPlayerRealName(), 24);
                    player.sendMessage(PREFIX + "§7La sanction vient d'être appliquée à §b" + targetPlayer.getName() + "§b.");
                    DenoCore.getInstance().getRedisPubSubSpigot()
                            .broadcastModeration("§d(SC-Sanction) "
                                    + denoriaPlayer.getRank().getChatColor()
                                    + denoriaPlayer.getRank().getPrefix()
                                    + " "
                                    + denoriaPlayer.getPlayerRealName()
                                    + " §8» §7J'ai mute le joueur '§b"
                                    + targetPlayer.getName()
                                    + "§7' pour le motif '§c"
                                    + reasonHaine
                                    + "§7'. Durée: §c24 Heures§b.");
                    targetPlayer.sendMessage(PREFIX + "§7Vous venez de vous faire mute de ce serveur pour la raison: '§b"
                            + reasonHaine + "§7', cette sanction est appliquée pour une durée de §c24 heures§7.");
                    DenoriaPlayer.getDenoriaPlayer(targetPlayer).setMute(true);
                    player.closeInventory();
                    break;
                /** Lien | Pub */
                case SIGN:
                    String reasonLink = "Liens / Pub Serveur";
                    IMute.createMuteTime(targetPlayer.getName(), reasonLink, denoriaPlayer.getPlayerRealName(), 2);
                    player.sendMessage(PREFIX + "§7La sanction vient d'être appliquée à §b" + targetPlayer.getName() + "§b.");
                    DenoCore.getInstance().getRedisPubSubSpigot()
                            .broadcastModeration("§d(SC-Sanction) "
                                    + denoriaPlayer.getRank().getChatColor()
                                    + denoriaPlayer.getRank().getPrefix()
                                    + " "
                                    + denoriaPlayer.getRank().getChatColor()
                                    + denoriaPlayer.getRank().getPrefix()
                                    + " "
                                    + denoriaPlayer.getPlayerRealName()
                                    + " §8» §7J'ai mute le joueur '§b"
                                    + targetPlayer.getName()
                                    + "§7' pour le motif '§c"
                                    + reasonLink
                                    + "§7'. Durée: §c2 Heures§b.");
                    targetPlayer.sendMessage(PREFIX + "§7Vous venez de vous faire mute de ce serveur pour la raison: '§b"
                            + reasonLink + "§7', cette sanction est appliquée pour une durée de §c2 heures§7.");
                    DenoriaPlayer.getDenoriaPlayer(targetPlayer).setMute(true);
                    player.closeInventory();
                    break;
                /** Lien pornographique */
                case NAME_TAG:
                    String reasonPorn = "Liens pornographiques";
                    IMute.createMuteTime(targetPlayer.getName(), reasonPorn, denoriaPlayer.getPlayerRealName(), 24);
                    player.sendMessage(PREFIX + "§7La sanction vient d'être appliquée à §b" + targetPlayer.getName() + "§b.");
                    DenoCore.getInstance().getRedisPubSubSpigot()
                            .broadcastModeration("§d(SC-Sanction) "
                                    + denoriaPlayer.getRank().getChatColor()
                                    + denoriaPlayer.getRank().getPrefix()
                                    + " "
                                    + denoriaPlayer.getPlayerRealName()
                                    + " §8» §7J'ai mute le joueur '§b"
                                    + targetPlayer.getName()
                                    + "§7' pour le motif '§c"
                                    + reasonPorn
                                    + "§7'. Durée: §c24 Heures§b.");
                    targetPlayer.sendMessage(PREFIX + "§7Vous venez de vous faire mute de ce serveur pour la raison: '§b"
                            + reasonPorn + "§7', cette sanction est appliquée pour une durée de §c24 heures§7.");
                    DenoriaPlayer.getDenoriaPlayer(targetPlayer).setMute(true);
                    player.closeInventory();
                    break;
                default:
                    break;
            }
        }
    }
}
