package fr.denoria.denocore.commands.misc;

import java.util.*;

import fr.denoria.denocore.DenoCore;
import fr.denoria.denocore.commands.AbstractCommand;
import fr.denoria.denocore.support.MessagesUtils;
import fr.denoria.denocore.support.builder.InventoryBuilder;
import fr.denoria.denocore.support.builder.items.ItemBuilder;
import fr.denoria.denocore.support.builder.items.heads.HeadBuilder;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.inventory.Inventory;

public class ReportCommand extends AbstractCommand {

    private final String PREFIX = MessagesUtils.getDenoriaPrefix();

    public ReportCommand() {
        this.cmd = Collections.singletonList("report");
        this.register(this);
    }

    @Override
    public void handle(CommandSender sender, List<String> params) {

        // Console
        if (!(sender instanceof Player)) {
            sender.sendMessage("Vous devez etre un joueur.");
            return;
        }

        Player player = (Player) sender;

        // Argument
        if (params.size() != 1) {
            helpInfo(player);
            return;
        }

        // Récupération du target.
        Player target = Bukkit.getPlayer(params.get(0));
        if (target == null) {
            player.sendMessage(
                    PREFIX + ChatColor.RED + "Erreur: §7Le joueur '" + params.get(0) + "' n'est pas en ligne.");
            return;
        }

        // Si l'auteur == la victime
        if (player.getName().equalsIgnoreCase(target.getName())) {
            player.sendMessage(PREFIX + ChatColor.RED + "Erreur: §7Vous ne pouvez pas vous report vous-même.");
            return;
        }

        new ReportMenu(player, target);
        player.sendMessage(PREFIX + "Ouverture du menu de report pour §e" + target.getName() + "§f.");
    }

    @Override
    public void helpInfo(Player player) {
        player.sendMessage("");
        player.sendMessage(" " + ChatColor.GRAY + "» " + ChatColor.YELLOW + "" + ChatColor.BOLD + "Aide"
                + ChatColor.WHITE + "│ " + ChatColor.YELLOW + "" + ChatColor.BOLD + "Report");
        player.sendMessage("");
        player.sendMessage(" " + ChatColor.GRAY + "" + ChatColor.BOLD + "■ " + ChatColor.GOLD + "/report [pseudo] "
                + ChatColor.WHITE + "» " + ChatColor.AQUA + "Report un joueur.");
        player.sendMessage("");
    }

    public class ReportMenu implements Listener {

        public Map<Player, Inventory> menuInventoryMap = new HashMap<>();

        /**
         * Création du Menu de report.
         *
         * @param player modérateur
         * @param target joueur en infraction
         */
        public ReportMenu(Player player, Player target) {
            Bukkit.getPluginManager().registerEvents(this, DenoCore.getInstance());
            Inventory inventory = new InventoryBuilder("Report » " + target.getName())
                    .addLine(new String[] { "chat", "cheat", "antijeux", "other", "", "", "", "infos", "cancel" })
                    .setItem("cancel", new ItemBuilder().type(Material.BARRIER).name("§cAnnuler").build())
                    .setItem("infos",
                            new HeadBuilder().setHead(target.getName()).setName("§e" + target.getName()).build())

                    .setItem("chat", new ItemBuilder().type(Material.BOOK_AND_QUILL).name("§eChat")
                            .lore("", "§7Pensez à utiliser le report", "§7avec une certaine intelligence.", "",
                                    "§7■ §bProvocations", "§7■ §bInsultes", "§7■ §bSpams/Flood", "",
                                    "§7Joueur: §c" + target.getName(), "", "§6§l» §eCliquez pour report")
                            .build())

                    .setItem("cheat", new ItemBuilder().type(Material.DIAMOND_SWORD).name("§eCheat/HackedClient")
                            .lore("", "§7Pensez à utiliser le report", "§7avec une certaine intelligence.", "",
                                    "§7■ §bForcefield/KillAura", "§7■ §bFly/NoFall", "§7■ §bReach/...", "",
                                    "§7Joueur: §c" + target.getName(), "", "§6§l» §eCliquez pour report")
                            .build())

                    .setItem("antijeux", new ItemBuilder().type(Material.ANVIL).name("§eAnti-Jeu")
                            .lore("", "§7Pensez à utiliser le report", "§7avec une certaine intelligence.", "",
                                    "§7■ §bCache-Cache", "§7■ §bTower", "§7■ §bAutres...", "",
                                    "§7Joueur: §c" + target.getName(), "", "§6§l» §eCliquez pour report")
                            .build())

                    .setItem("other", new ItemBuilder().type(Material.REDSTONE).name("§eAutres")
                            .lore("", "§7Pensez à utiliser le report", "§7avec une certaine intelligence.", "",
                                    "§7■ §bSkin incorrect", "§7■ §bAutres...", "", "§7Joueur: §c" + target.getName(),
                                    "", "§6§l» §eCliquez pour report")
                            .build())
                    .build(player);
            this.menuInventoryMap.put(player, inventory);
            player.openInventory(menuInventoryMap.get(player));
        }

        @EventHandler
        public void onInventoryClose(InventoryCloseEvent event) {
            Player player = (Player) event.getPlayer();
            if (event.getInventory() == null)
                return;
            if (menuInventoryMap.get(player) == null)
                return;
            if (menuInventoryMap.get(player).equals(event.getInventory()))
                menuInventoryMap.remove(player);
        }

        @EventHandler
        public void onInventoryClick(InventoryClickEvent event) {
            Player player = (Player) event.getWhoClicked();
            if (event.getInventory() == null)
                return;
            if (event.getCurrentItem() == null)
                return;
            if (event.getCurrentItem().getType().equals(Material.AIR))
                return;
            if (!(event.getInventory().equals(menuInventoryMap.get(player))))
                return;
            event.setCancelled(true);
            Player target = Bukkit.getPlayer(event.getInventory().getName().replace("Report » ", ""));
            if (target == null) {
                player.sendMessage(PREFIX + " Le Joueur n'est plus en ligne.");
                player.closeInventory();
                return;
            }
            switch (event.getCurrentItem().getType()) {
                case BARRIER:
                    player.sendMessage(PREFIX + " Le report vient d'être §cannulée§f.");
                    player.closeInventory();
                    break;
                case REDSTONE:
                    DenoCore.getInstance().getRedisPubSubSpigot()
                            .broadcastModeration(PREFIX + ChatColor.WHITE + "Le joueur '§c" + target.getName()
                                    + "§f' est suspecté de: §bAutres (Skins,...) §fpar le joueur '§b" + player.getName() + "§f'.");
                    player.sendMessage(MessagesUtils.getDenoriaPrefix() + ChatColor.GREEN
                            + "Votre report à été envoyé l'équipe de modération...");
                    player.closeInventory();
                    break;
                case ANVIL:
                    DenoCore.getInstance().getRedisPubSubSpigot()
                            .broadcastModeration(PREFIX + ChatColor.WHITE + "Le joueur '§c" + target.getName()
                                    + "§f' est suspecté de: §bAnti-Jeu §fpar le joueur '§b" + player.getName() + "§f'.");
                    player.sendMessage(MessagesUtils.getDenoriaPrefix() + ChatColor.GREEN
                            + "Votre report à été envoyé l'équipe de modération...");
                    player.closeInventory();
                    break;
                case DIAMOND_SWORD:
                    DenoCore.getInstance().getRedisPubSubSpigot()
                            .broadcastModeration(PREFIX + ChatColor.WHITE + "Le joueur '§c" + target.getName()
                                    + "§f' est suspecté de: §bCheat/HackedClient §fpar le joueur '§b" + player.getName() + "§f'.");
                    player.sendMessage(MessagesUtils.getDenoriaPrefix() + ChatColor.GREEN
                            + "Votre report à été envoyé l'équipe de modération...");
                    player.closeInventory();
                    break;
                case BOOK_AND_QUILL:
                    DenoCore.getInstance().getRedisPubSubSpigot()
                            .broadcastModeration(PREFIX + ChatColor.WHITE + "Le joueur '§c" + target.getName()
                                    + "§f' est suspecté de: §bChat (Insultes,...) §fpar le joueur '§b" + player.getName() + "§f'.");
                    player.sendMessage(MessagesUtils.getDenoriaPrefix() + ChatColor.GREEN
                            + "Votre report à été envoyé l'équipe de modération...");
                    player.closeInventory();
                    break;
                default:
                    break;
            }
        }
    }
}
