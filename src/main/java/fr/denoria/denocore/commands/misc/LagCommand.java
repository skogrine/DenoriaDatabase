package fr.denoria.denocore.commands.misc;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.List;

import fr.denoria.denocore.commands.AbstractCommand;
import net.minecraft.server.v1_8_R3.MinecraftServer;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;


import net.md_5.bungee.api.ChatColor;


public class LagCommand extends AbstractCommand {

    public LagCommand() {
        this.cmd = Arrays.asList("lag", "lags");
        this.register(this);
    }

    @Override
    public void handle(CommandSender sender, List<String> params) {
        if (!(sender instanceof Player)) {
            sender.sendMessage("Vous devez etre un joueur.");
            return;
        }
        Player player = (Player) sender;

        // Gestion de la Command
        player.sendMessage("");
        player.sendMessage(" " + ChatColor.GRAY + "» " + ChatColor.YELLOW + "" + ChatColor.BOLD + "Informations"
                + ChatColor.WHITE + "│ " + ChatColor.YELLOW + "" + ChatColor.BOLD + "Serveur");
        player.sendMessage("");
        player.sendMessage(" §7§l■ §6Serveur: §b" + Bukkit.getServerName() + " §8| §6Joueur(s): §b"
                + Bukkit.getOnlinePlayers().size());
        player.sendMessage(
                " §7§l■ §6Date: §b" + new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").format(new java.util.Date()));

        StringBuilder sb = new StringBuilder(org.bukkit.ChatColor.GOLD + " §7§l■ §6Latence: ");
        double[] var5;
        int var6 = (var5 = MinecraftServer.getServer().recentTps).length;

        for(int var7 = 0; var7 < var6; ++var7) {
            double tps = var5[var7];
            sb.append(this.format(tps));
            sb.append(", ");
        }
        player.sendMessage(sb.substring(0, sb.length() - 2));
        player.sendMessage("");
    }

    private String format(double tps) {
        return (tps > 18.0D ? org.bukkit.ChatColor.GREEN : (tps > 16.0D ? org.bukkit.ChatColor.YELLOW : org.bukkit.ChatColor.RED))
                .toString() + (tps > 20.0D ? "+" : "") +
                Math.min((double)Math.round(tps * 100.0D) / 100.0D, 20.0D);
    }



    @Override
    public void helpInfo(Player player) {}
}
