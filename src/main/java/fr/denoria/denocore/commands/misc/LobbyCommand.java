package fr.denoria.denocore.commands.misc;

import java.util.Arrays;
import java.util.List;

import fr.denoria.denocore.commands.AbstractCommand;
import fr.denoria.denocore.support.MessagesUtils;
import fr.denoria.denocore.support.server.SwitchServer;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;


public class LobbyCommand extends AbstractCommand {

    public LobbyCommand() {
        this.cmd = Arrays.asList("lobby", "hub");
        this.register(this);
    }

    @Override
    public void handle(CommandSender sender, List<String> params) {
        if (!(sender instanceof Player)) {
            sender.sendMessage("Vous devez etre un joueur.");
            return;
        }
        Player player = (Player) sender;
        player.sendMessage(MessagesUtils.getDenoriaPrefix() + ChatColor.YELLOW + "Téléportation vers le hub...");
        SwitchServer.sendPlayerToLobby(player, false);
    }

    @Override
    public void helpInfo(Player player) {}
}