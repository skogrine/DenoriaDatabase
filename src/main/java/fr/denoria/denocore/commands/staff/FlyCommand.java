package fr.denoria.denocore.commands.staff;

import fr.denoria.denocore.commands.AbstractCommand;
import fr.denoria.denocore.commands.nick.NickManager;
import fr.denoria.denocore.denoriaplayer.DenoriaPlayer;
import fr.denoria.denocore.denoriaplayer.rank.Rank;
import fr.denoria.denocore.support.MessagesUtils;
import fr.denoria.denocore.support.locations.Cuboid;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.Collections;
import java.util.List;

public class FlyCommand extends AbstractCommand {


    public FlyCommand() {
        this.cmd = Collections.singletonList("fly");
        this.register(this);
    }

    // fly (player)
    //--------------------------------------ZONE PVP-----------------------------------------
    Location pos1pvp = new Location(Bukkit.getWorld("world"), 372, 255, -196);
    Location pos2pvp = new Location(Bukkit.getWorld("world"), 209, 11, -9);
    //---------------------------------------------------------------------------------------

    @Override
    public void handle(CommandSender sender, List<String> params) {

        Player player = (Player) sender;
        DenoriaPlayer iPlayer = DenoriaPlayer.getDenoriaPlayer(player);

        if (params.size() == 0){
            if(iPlayer.getRank().getPower() >= Rank.DESIGNER.getPower()){

                if (Cuboid.isInCube(player, pos1pvp, pos2pvp)) {
                    player.sendMessage(MessagesUtils.getDenoriaPrefix() + "§cErreur: §7Vous ne pouvez pas fly dans la zone PVP");
                    return;
                }




                iPlayer.getPlayer().setAllowFlight(!iPlayer.getPlayer().getAllowFlight());
                player.sendMessage(
                        MessagesUtils.sendPrefixAdmin() + "§7Votre fly a été " + (iPlayer.getPlayer().getAllowFlight() ? "§aActivé" : "§cDésactivé") + "§7.");
            } else {
                if (NickManager.isNick(player.getUniqueId())){
                    DenoriaPlayer nickPlayer = DenoriaPlayer.getDenoriaPlayer(Bukkit.getPlayer(NickManager.getRealName(player.getUniqueId())));
                    if (nickPlayer.getRankPower() >= Rank.DESIGNER.getPower()) {
                        nickPlayer.getPlayer().setAllowFlight(!iPlayer.getPlayer().getAllowFlight());
                        nickPlayer.getPlayer().sendMessage(
                                MessagesUtils.sendPrefixAdmin() + "§7Votre fly a été " + (iPlayer.getPlayer().getAllowFlight() ? "§aActivé" : "§cDésactivé") + "§7.");
                    }
                    return;
                }
                iPlayer.sendMessageNoPermission("DESIGNER");
            }
        }
    }

    @Override
    public void helpInfo(Player player) {

    }
}
