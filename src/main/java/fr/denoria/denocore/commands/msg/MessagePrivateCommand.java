package fr.denoria.denocore.commands.msg;

import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import fr.denoria.denocore.DenoCore;
import fr.denoria.denocore.commands.AbstractCommand;

import fr.denoria.denocore.commands.nick.NickManager;
import fr.denoria.denocore.denoriaplayer.DenoriaPlayer;
import fr.denoria.denocore.denoriaplayer.DenoriaPlayerInfo;
import fr.denoria.denocore.denoriaplayer.UUIDFetcher;
import fr.denoria.denocore.denoriaplayer.infos.DataType;
import fr.denoria.denocore.denoriaplayer.rank.IRank;
import fr.denoria.denocore.denoriaplayer.rank.Rank;
import fr.denoria.denocore.support.MessagesUtils;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitTask;
import net.md_5.bungee.api.ChatColor;

public class MessagePrivateCommand extends AbstractCommand {

    public static int slow = 0;
    static BukkitTask task;

    public static String MESSAGEPRIVATEPREFIX = MessagesUtils.getDenoriaPrefix();

    public MessagePrivateCommand() {
        this.cmd = Arrays.asList("msg", "tell", "w", "mp", "whisper", "whispers", "r");
        this.register(this);
    }

    @Override
    public void handle(CommandSender sender, List<String> params) {

        // Console
        if (!(sender instanceof Player)) {
            sender.sendMessage("Vous devez etre un joueur.");
            return;
        }

        Player player = (Player) sender;
        DenoriaPlayer denoriaPlayer = DenoriaPlayer.getDenoriaPlayer(player);

        // Argument
        if (params.size() == 0) {
            helpInfo(player);
            return;
        }

        // Commands
        if (params.size() == 1) {
            // Activation des MP.
            if (params.get(0).equalsIgnoreCase("on")) {
                player.sendMessage(MESSAGEPRIVATEPREFIX + "§7Réception de messages privés: §aActivée§7.");
                denoriaPlayer.setPlayerData(DataType.ALLOW_MESSAGES_PRIVATES, true);
                return;

                // Désactivation des MP.
            } else if (params.get(0).equalsIgnoreCase("off")) {
                player.sendMessage(MESSAGEPRIVATEPREFIX + "§7Réception de messages privés: §cDésactivé§7.");
                denoriaPlayer.setPlayerData(DataType.ALLOW_MESSAGES_PRIVATES, false);
                return;

                // Commande est inconnue.
            } else {
                helpInfo(player);
                return;
            }

            // Gestion complète du /msg.
        } else {

            // Mute
            if(denoriaPlayer.isMute()) {
                player.sendMessage(MESSAGEPRIVATEPREFIX + "§cErreur: §7Vous êtes mute de ce serveur, merci de patienter.");
                return;
            }

            // Message à soi-même.
            if(params.get(0).equalsIgnoreCase(player.getName())) {
                player.sendMessage(MESSAGEPRIVATEPREFIX + "§cErreur: §7Impossible de s'écrire à soi-même.");
                return;
            }

            // Accepte ses MP.
            if (!((boolean) denoriaPlayer.getPlayerData(DataType.ALLOW_MESSAGES_PRIVATES))) {
                player.sendMessage(MESSAGEPRIVATEPREFIX + "§cErreur: §7Vous devez activer vos messages privés.");
                return;
            }
            // Vérification de la connexion du Joueur.
            String nameTarget = params.get(0);
            UUID targetUUID = UUIDFetcher.getUUID(nameTarget);
            Rank targetRank = null;
            boolean nick = false;
            for (String nickList : NickManager.getNickList()) {
                if (NickManager.getNickName(UUIDFetcher.getUUID(nickList)) != null) {
                    if (NickManager.getNickName(UUIDFetcher.getUUID(nickList)).equalsIgnoreCase(nameTarget)) {
                        targetUUID = UUIDFetcher.getUUID(nickList);
                        targetRank = NickManager.getRankName(targetUUID);
                        nick = true;
                    }
                }
            }
            if ((targetUUID == null)) {
                player.sendMessage(
                        MESSAGEPRIVATEPREFIX + "§cErreur: §7Le joueur '§b" + nameTarget + "§7' n'existe pas.");
                return;
            }
            if ((DenoriaPlayerInfo.getServerName(targetUUID) == null) || ((NickManager.isNick(targetUUID))
                    && (NickManager.getRealName(targetUUID).equalsIgnoreCase(nameTarget)))) {
                player.sendMessage(
                        MESSAGEPRIVATEPREFIX + ChatColor.RED + "§cErreur: §7Le joueur '§b" + nameTarget + "§7' n'est pas en ligne.");
                return;
            }

            // Vérifier si le joueur accepte les MP.
            if (!(DenoriaPlayerInfo.getPlayerData(targetUUID, DataType.ALLOW_MESSAGES_PRIVATES))) {
                player.sendMessage(
                        MESSAGEPRIVATEPREFIX + "§cErreur: §7Le joueur n'accepte pas les messages privés.");
                return;
            }

            // Envoie du Message.
            String message = "";
            for (int i = 1; i < params.size(); i++) {
                message = message + " " + params.get(i);
            }
            message.trim();
            if (!(nick)) {
                targetRank = IRank.getPlayerRank(targetUUID);
            }
            if (targetRank == null) {
                player.sendMessage(MESSAGEPRIVATEPREFIX + "§cErreur: §7Impossible d'envoyer le message privé.");
                return;
            }
            player.sendMessage(ChatColor.AQUA + "§8❘ §7Envoyé à "
                    + targetRank.getChatColor()
                    + targetRank.getPrefix()
                    + MessagesUtils.getRankSpace(targetRank)
                    + nameTarget
                    + " §8» "
                    + ChatColor.WHITE
                    + message);
            // Message pour le récepteur.
            String messageTarget = ChatColor.AQUA + "§8❘ §7Reçu de "
                    + denoriaPlayer.getRank().getChatColor()
                    + denoriaPlayer.getRank().getPrefix()
                    + MessagesUtils.getRankSpace(denoriaPlayer.getRank())
                    + player.getName()
                    + " §8» "
                    + ChatColor.WHITE
                    + message;
            DenoCore.getInstance().getRedisPubSubSpigot().sendMessage(nameTarget, messageTarget);
            return;
        }
    }

    @Override
    public void helpInfo(Player player) {
        player.sendMessage("§8§m-----------------------------------");
        player.sendMessage("§c§lMessage Privés §7(Aide /msg)");
        player.sendMessage("");
        player.sendMessage(" §8() §c/msg help §8» §7Message d'aide des messages privés.");
        player.sendMessage(" §8() §c/msg (player) (message) §8» §7Envoyer un message à un joueur.");
        player.sendMessage(" §8() §c/msg (on/off) §8» §7Activer/désactiver la réception des mp.");
        player.sendMessage("");
        player.sendMessage("§8§m-----------------------------------");
    }
}
