
[![CI to Docker Hub](https://github.com/DenoriaNetwork/DenoriaDatabase/actions/workflows/main.yml/badge.svg)](https://github.com/DenoriaNetwork/DenoriaDatabase/actions/workflows/main.yml)

[![pipeline status](https://gitlab.com/skogrine/DenoriaDatabase/badges/main/pipeline.svg)](https://gitlab.com/skogrine/DenoriaDatabase/-/commits/main)

# DenoriaDatabase
DenoriaDatabase permet la discussion entre le serveur Spigot et le proxy, ainsi qu'une gestion globale des envoies Pub/Sub Redis et MariaDB/MongoDB.

##### Compiling requirements:
>- JDK 1.8
>- Maven
>- Git/Github (Optional)

##### How to compile the project:
>- Clone the project with Git/Github
>- Execute command "mvn clean package"

##### Running requirements:
>- Java 1.8 - 1.9
>- Spigot (1.7.10, 1.8.X, 1.9.X, 1.10.X, 1.11.X, 1.12.X)<br>
>- ProtocolLib
>- DenoTimeSaver       
>                                                                                                                                                                                                                   
## Links and Contacts

- **Dev resources:**
  ```xml
    <repositories>
        <repository>
            <id>codemc-repo</id>
            <url>https://repo.codemc.org/repository/maven-public/</url>
        </repository>
    </repositories>
  
    <dependencies>
        <dependency>
            <groupId>fr.denoria.denocore</groupId>  
	    <artifactId>DenoriaCore</artifactId>  
	    <version>2.1.5</version>
            <scope>provided</scope>
        </dependency>
    </dependencies>
  ```
