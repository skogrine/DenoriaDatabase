FROM openjdk:8-jre-alpine

LABEL maintainer="contact@denoria.fr"

WORKDIR /tmp/server
COPY . .

EXPOSE 25565

CMD ["java", "-jar", "Waterfall.jar"]